package cl.foursoft.eee.comunication.objetos
{
		import cl.foursoft.eee.actionscript.enums.EnumSecciones;
		
		import mx.collections.ArrayCollection;
	
		[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.EnvolventeTO")]
	public class EnvolventeTO
	{
		
		private var _idEdificio : int;
		private var _areaSuperior : SeccionTO;
		private var _areaPiso : SeccionTO;
		private var _ventanas : SeccionTO;
		private var _muros : SeccionTO;
		private var _puertas : SeccionTO;
		private var _secciones : ArrayCollection;
		private var _otrasEnvolventes : ArrayCollection;
		
		
		public function EnvolventeTO(arraySecciones :ArrayCollection = null)
		{
			
			if(arraySecciones){
				crearObjetoEnvolvente(arraySecciones);
			}
			_secciones = new ArrayCollection();
			this._idEdificio = -1;
			
		}

		[Bindable]
		public function get areaSuperior():SeccionTO
		{
			return _areaSuperior;
		}

		public function set areaSuperior(value:SeccionTO):void
		{
			_areaSuperior = value;
		}

		[Bindable]
		public function get areaPiso():SeccionTO
		{
			return _areaPiso;
		}

		public function set areaPiso(value:SeccionTO):void
		{
			_areaPiso = value;
		}

		[Bindable]
		public function get ventanas():SeccionTO
		{
			return _ventanas;
		}

		public function set ventanas(value:SeccionTO):void
		{
			_ventanas = value;
		}

		[Bindable]
		public function get muros():SeccionTO
		{
			return _muros;
		}

		public function set muros(value:SeccionTO):void
		{
			_muros = value;
		}

		[Bindable]
		public function get idEdificio():int
		{
			return _idEdificio;
		}

		public function set idEdificio(value:int):void
		{
			_idEdificio = value;
		}

		[Bindable]
		public function get otrasEnvolventes():ArrayCollection
		{
			return _otrasEnvolventes;
		}

		public function set otrasEnvolventes(value:ArrayCollection):void
		{
			_otrasEnvolventes = value;
		}
		/*OTRAS*/
		private function crearObjetoEnvolvente(_secciones:ArrayCollection):void{
			for each (var _secc : SeccionTO in  _secciones){
				if(_secc != null){
					//	var nombreSeccion:String = _secc.nombreTipo;
					//	var areaTotalSeccion : Number = _secc.area;
					switch(_secc.nombreTipo){
						case EnumSecciones.SECCION_VENTANAS:
							this.ventanas = _secc;
							break;
						case EnumSecciones.SECCION_MUROS : 
							this.muros = _secc;
							break;
						case EnumSecciones.SECCION_PUERTAS : 
							this.puertas = _secc;
							break;
						case EnumSecciones.SECCION_AREA_SUPERIOR : 
							this.areaSuperior = _secc;
							break;
						case EnumSecciones.SECCION_AREA_BASE :
							this.areaPiso = _secc;
							break;
						default:
							break;
					}
				}
			}
		}
		
		public function obtenerListaSecciones() : ArrayCollection{
			
			_secciones.removeAll();
			_secciones.addItem(areaSuperior);
			_secciones.addItem(muros);
			_secciones.addItem(ventanas);
			_secciones.addItem(areaPiso);
			_secciones.addItem(puertas);
			return _secciones;
		}
		public function obtenerEnvolvente(_secciones : ArrayCollection):void{
			crearObjetoEnvolvente(_secciones);
		}

		[Bindable]
		public function get puertas():SeccionTO
		{
			return _puertas;
		}

		public function set puertas(value:SeccionTO):void
		{
			_puertas = value;
		}


	}
}
package cl.foursoft.eee.comunication.objetos
{
	import cl.foursoft.eee.comunication.objetos.MaterialTO;
	
	import mx.collections.ArrayCollection;
	

	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.VentanaTO")]
	public class VentanaTO
	{
		
		private var _cantidadVentanas : int;
		private var _ubicacion:String; //PENDIENTE (norte, sur , oriente, poniente, no/np , so/sp)
		private var _largo:Number;
		private var _ancho:Number;
		private var _area:Number;
		private var _composicion : ArrayCollection;//AC de MaterialTO
		
		
		
		public function VentanaTO(_ubicacion:String = "", _cantidad:int = 0 , _largo : int = 0)
		{
			this._cantidadVentanas = _cantidad;
			this._largo = _largo;
			this._ancho = 0;
			this._area = 0;
			this._composicion = new ArrayCollection();
			this._ubicacion = _ubicacion;
		}
		
		
		 
		
		[Bindable]
		public function get cantidadVentanas():int
		{
			return _cantidadVentanas;
		}

		public function set cantidadVentanas(value:int):void
		{
			_cantidadVentanas = value;
		}
		[Bindable]
		public function get largo():Number
		{
			return _largo;
		}

		public function set largo(value:Number):void
		{
			_largo = value;
		}
		[Bindable]
		public function get ancho():Number
		{
			return _ancho;
		}

		public function set ancho(value:Number):void
		{
			_ancho = value;
		}
		
		[Bindable]
		public function get area():Number
		{
		
			_area = this._ancho * this._largo * this._cantidadVentanas;
			return (this._ancho * this._largo * this._cantidadVentanas);
			//return _area;
		}

		public function set area(value:Number):void
		{
			_area = value;
		}
		[Bindable]
		public function get composicion():ArrayCollection
		{
			return _composicion;
		}

		public function set composicion(value:ArrayCollection):void
		{
			_composicion = value;
		}

		[Bindable]
		public function get ubicacion():String
		{
			return _ubicacion;
		}

		public function set ubicacion(value:String):void
		{
			_ubicacion = value;
		}


	}
}
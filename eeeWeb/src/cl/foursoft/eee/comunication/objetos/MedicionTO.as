package cl.foursoft.eee.comunication.objetos
{
	import cl.foursoft.eee.actionscript.util.UtilCalculos;
	
	import flashx.textLayout.formats.Float;
	
	import mx.collections.ArrayCollection;

	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.MedicionTO")]
	public class MedicionTO
	{
		private var _descripcionMedicion : String;
		private var _fechaMedicion : Date; // Día y Hora
		private var _sensor:SensorTO;
		private var _medicion : Number;
		
		//TEST MULTIPLES MEDICIONES
		private var _mediciones : ArrayCollection;

		private var _idTipoMedicion : String;
		private var _idTipoConsumo : String;
		private var _unidadMedicion : String;
		private var _idSensor : String;
		
		//--Datos de alerta.
		private var _alerta : Boolean;
		private var _promedioMedicion:Number;

		
		
		public function MedicionTO()
		{
			this._fechaMedicion = null;
			this._sensor = null;
			this._medicion = -1;
			_mediciones = new ArrayCollection();
		}
		
		
		[Bindable]
		public function get fechaMedicion():Date
		{
			return _fechaMedicion;
		}

		public function set fechaMedicion(value:Date):void
		{
			_fechaMedicion = value;
		}
		public function get fechaMedicionToString():String{
			
			return (_fechaMedicion.date) +"/"+(_fechaMedicion.month + 1)+"/"+(_fechaMedicion.fullYear) + "   \nHORA :  " + UtilCalculos.formato00(_fechaMedicion.hours)+":"+UtilCalculos.formato00(_fechaMedicion.minutes); 
		}
		[Bindable]
		public function get sensor():SensorTO
		{
			return _sensor;
		}

		public function set sensor(value:SensorTO):void
		{
			_sensor = value;
		}
		[Bindable]
		public function get medicion():Number
		{ 
			return UtilCalculos.round(_medicion);
		}

		public function set medicion(value:Number):void
		{
			_medicion = value;
		}
		[Bindable]
		public function get idTipoMedicion():String
		{
			return _idTipoMedicion;
		}

		public function set idTipoMedicion(value:String):void
		{
			_idTipoMedicion = value;
		}

		[Bindable]
		public function get idTipoConsumo():String
		{
			return _idTipoConsumo;
		}

		public function set idTipoConsumo(value:String):void
		{
			_idTipoConsumo = value;
		}

		[Bindable]
		public function get idSensor():String
		{
			return _idSensor;
		}

		public function set idSensor(value:String):void
		{
			_idSensor = value;
		}
		[Bindable]
		public function get hora():Number{
			return this.fechaMedicion.hours;
		}
		public function get mes():Number{
			return this.fechaMedicion.month;
		}

		[Bindable]
		public function get alerta():Boolean
		{
			return _alerta;
		}

		public function set alerta(value:Boolean):void
		{
			_alerta = value;
		}
		[Bindable]
		public function get promedioMedicion():Number
		{
			return _promedioMedicion;
		}

		public function set promedioMedicion(value:Number):void
		{
			_promedioMedicion = value;
		}

		[Bindable]
		public function get mediciones():ArrayCollection
		{
			return _mediciones;
		}

		public function set mediciones(value:ArrayCollection):void
		{
			_mediciones = value;
		}

		[Bindable]
		public function get descripcionMedicion():String
		{
			return _descripcionMedicion;
		}

		public function set descripcionMedicion(value:String):void
		{
			_descripcionMedicion = value;
		}

		[Bindable]
		public function get unidadMedicion():String
		{
			return _unidadMedicion;
		}

		public function set unidadMedicion(value:String):void
		{
			_unidadMedicion = value;
		}


	}
}
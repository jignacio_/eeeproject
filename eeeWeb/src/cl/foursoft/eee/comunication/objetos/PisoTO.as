package cl.foursoft.eee.comunication.objetos
{
	import cl.foursoft.eee.actionscript.interfaces.ISeleccionVistasParaGestion;
	import cl.foursoft.eee.actionscript.util.Nodo;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;

	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.PisoTO")]
	public class PisoTO extends Nodo implements ISeleccionVistasParaGestion
	{
		
		private var _tipoSeleccioando : Boolean;
		
		private var _prefijo:String;
		private var _numeroPisoActual :int ; 
		private var _idPiso:int;
		private var _nombrePiso:String;
		private var _numeroSensores:int;
		private var _descripcionPiso:String;
		private var _arrCollSensoresPiso : ArrayCollection;
		private var _arrCollZonasPiso : ArrayCollection;
		
		private var _guardado:Boolean;
		
		//Datos del Edificio
		private var _TPisos:int;
		private var _TZonas:int;
		private var _TSensores:int;
		
		public function PisoTO(_numeroPiso : int = 0):void
			
		{
			this._guardado = false; 
			this._prefijo = "Piso";
			this._numeroSensores = 0;
			this._numeroPisoActual = _numeroPiso;
			this._idPiso = 0;
			this._nombrePiso = "";
			this._descripcionPiso = "";
			this._arrCollSensoresPiso = new ArrayCollection();
			this._arrCollZonasPiso = new ArrayCollection();
			
		}

		[Bindable]
		public function get idPiso():int
		{
			return _idPiso;
		}

		public function set idPiso(value:int):void
		{
			_idPiso = value;
		}

		[Bindable]
		public function get nombrePiso():String
		{
			return _nombrePiso;
		}

		public function set nombrePiso(value:String):void
		{
			_nombrePiso = value;
		}
		[Bindable]
		public function get descripcionPiso():String
		{
			return _descripcionPiso;
		}

		public function set descripcionPiso(value:String):void
		{
			_descripcionPiso = value;
		}

		[Bindable]
		public function get arrCollSensoresPiso():ArrayCollection
		{
			return _arrCollSensoresPiso;
		}

		public function set arrCollSensoresPiso(value:ArrayCollection):void
		{
			_arrCollSensoresPiso = value;
		}

		[Bindable]
		public function get arrCollZonasPiso():ArrayCollection
		{
			return _arrCollZonasPiso;
		}

		public function set arrCollZonasPiso(value:ArrayCollection):void
		{
			_arrCollZonasPiso = value;
		}

		[Bindable]
		public function get numeroPisoActual():int
		{
			return _numeroPisoActual;
		}

		public function set numeroPisoActual(value:int):void
		{
			_numeroPisoActual = value;
		}
		 
		
		//Funciones para agregar Zonas y Sensores a los ArrColl 
		public function agregarZona(value : ZonaTO) : int{
			if(value){
				_arrCollZonasPiso.addItem(value);
				return 0;
			}
			return -1;
		
		}
		public function agregarSensor(value : SensorTO) : int {
			
			if(value){
				_arrCollSensoresPiso.addItem(value)
				return 0;
			}
			return -1;

		}


		[Bindable]
		public function get numeroSensores():int
		{
			_numeroSensores = 0;
			_numeroSensores += _arrCollSensoresPiso.length;
			for each(var _z : ZonaTO in _arrCollZonasPiso)
				_numeroSensores += _z.numeroSensores;			
			return _numeroSensores;
		}

		public function set numeroSensores(value:int):void
		{
			_numeroSensores = value;
		}
		override public function get children():ArrayCollection
		{
			// TODO Auto Generated method stub
			return new ArrayCollection(_arrCollZonasPiso.toArray().concat(_arrCollSensoresPiso.toArray()));
			//return _arrCollZonasPiso;
		}
		
		public function toString():String{
			
			if(_numeroPisoActual==0)
				return this._nombrePiso;
			return this._prefijo +" "+this._numeroPisoActual +" ( "+this._nombrePiso+" )";
		}
		
		override public function get name():String{
			return toString();
		}
		
		[Bindable]
		public function get TPisos():int
		{
			return _TPisos;
		}

		public function set TPisos(value:int):void
		{
				_TPisos = value;
		}
		[Bindable]
		public function get TZonas():int
		{
			return _TZonas;
		}

		public function set TZonas(value:int):void
		{
				_TZonas = value;
		}
		[Bindable]
		public function get TSensores():int
		{
			return _TSensores;
		}
	
		public function set TSensores(value:int):void
		{
				_TSensores = value;
		}

		[Bindable]
		public function get guardado():Boolean
		{
			return _guardado;
		}

		public function set guardado(value:Boolean):void
		{
			_guardado = value;
		}
		public function reinicar():void{
			
			this._TZonas = 0;
			this.TSensores = 0;
			this.TPisos = 0;  
			
			this._guardado = false; 
			this._prefijo = "Piso";
			this._numeroSensores = 0;
			this._nombrePiso = "";
			this._descripcionPiso = "";
			this._arrCollSensoresPiso.removeAll();
			this._arrCollZonasPiso.removeAll();
			
		}
		[Bindable]
		public function get nombreElemento():String
		{
			// TODO Auto Generated method stub
			return toString();
		}
		
		public function set tipoSeleccionado(value:Boolean):void
		{

			this._tipoSeleccioando = value;
		}
		[Bindable]
		public function get tipoSeleccionado():Boolean
		{
			// TODO Auto Generated method stub
			return this._tipoSeleccioando;
		}
		
		

	}
}
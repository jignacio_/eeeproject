package cl.foursoft.eee.comunication.objetos
{
	import mx.collections.ArrayCollection;

	/**
	 * @author JGuajardo
	 */
	[Bindable]
	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.UsuarioTO")]
	public class UsuarioTO
	{
		private var _idCliente : int;
		private var _nombreCliente:String;
		
		private var _idUsuario:int;
		private var _idTipoUsuario:int;
		private var _nombreTipoUsuario:String;
		private var _nombreCompleto:String;
		private var _telefono:Number;
		private var _email:String;
		private var _nombreUsuario:String; 
		private var _contrasena:String;
		
		private var _fechaIngreso:String;
		private var _fechaHoraIngreso:Date;
		private var _fechaUltimaSincronizacion:String;
		
		private var _objetoMedicion : Object;
		
		private var _edificios : ArrayCollection;
		private var usuarioActual : UsuarioTO ; 
		
		private var _edificioActual : EdificioTO;
		
		private var _parametrosInicialEdificio : ArrayCollection;
		
		private var _editandoEdificio:Boolean = false;
		private var _creandoEdificio:Boolean = false;
		
		private var _usuarioEditado : Boolean = false;
		
		//Metodo Singleton
		/* SINGLETON               */               
		protected static var _instance:UsuarioTO;      
		
	
		public static function getInstance():UsuarioTO {                       
			if (_instance ==null)            {                                        
				_instance = new UsuarioTO();                         
			}
			return _instance;              
		} 
		
		public static function setUsuario(usuario : UsuarioTO):void{
			
			_instance = getInstance();
			_instance = usuario; 
		}
		public static function eliminarUsuario():void{
			setUsuario(null);
		}
		
		public function UsuarioTO()
		{
			this._idUsuario = -1;
			this._nombreCliente = "";
			this._idCliente = -1;
			this._idTipoUsuario = -1;
			this._nombreTipoUsuario = "Cliente";
			this._nombreCompleto = "";
			this._telefono = 0;
			
			this._fechaIngreso = "";
			
			this._nombreUsuario = "";
			this._contrasena = "";
			
			this.edificioActual = new EdificioTO();
			this.parametrosInicialEdificio = new ArrayCollection();
			this.edificios = new ArrayCollection();
			
		}


		
		
		public function get idUsuario():int
		{
			return _idUsuario;
		}

		public function set idUsuario(value:int):void
		{
			_idUsuario = value;
		}
		[Bindable]
		public function get idTipoUsuario():int
		{
			return _idTipoUsuario;
		}

		public function set idTipoUsuario(value:int):void
		{
			_idTipoUsuario = value;
		}

		[Bindable]
		public function get nombreTipoUsuario():String
		{
			return _nombreTipoUsuario;
		}

		public function set nombreTipoUsuario(value:String):void
		{
			_nombreTipoUsuario = value;
		}

		[Bindable]
		public function get nombreCompleto():String
		{
			return _nombreCompleto;
		}

		public function set nombreCompleto(value:String):void
		{
			_nombreCompleto = value;
		}

		[Bindable]
		public function get telefono():Number
		{
			return _telefono;
		}

		public function set telefono(value:Number):void
		{
			_telefono = value;
		}

		public function get nombreUsuario():String
		{
			return _nombreUsuario;
		}

		public function set nombreUsuario(value:String):void
		{
			_nombreUsuario = value;
		}

		public function get contrasena():String
		{
			return _contrasena;
		}

		public function set contrasena(value:String):void
		{
			_contrasena = value;
		}
		[Bindable]
		public function get idCliente():int
		{
			return _idCliente;
		}

		public function set idCliente(value:int):void
		{
			_idCliente = value;
		}

		public function get nombreCliente():String
		{
			return _nombreCliente;
		}

		public function set nombreCliente(value:String):void
		{
			_nombreCliente = value;
		}

		public function get fechaIngreso():String
		{
			return _fechaIngreso;
		}

		public function set fechaIngreso(value:String):void
		{
			_fechaIngreso = value;
		}

		[Bindable]
		public function get fechaHoraIngreso():Date
		{
			return _fechaHoraIngreso;
		}

		public function set fechaHoraIngreso(value:Date):void
		{
			_fechaHoraIngreso = value;
		}

		[Bindable]
		public function get objetoMedicion():Object
		{
			return _objetoMedicion;
		}

		public function set objetoMedicion(value:Object):void
		{
			_objetoMedicion = value;
		}

		[Bindable]
		public function get edificios():ArrayCollection
		{
			return _edificios;
		}

		public function set edificios(value:ArrayCollection):void
		{
				_edificios = value;
		}

		[Bindable]
		public function get edificioActual():EdificioTO
		{

			return _edificioActual 

		}

		public function set edificioActual(value:EdificioTO):void
		{
			_edificioActual = value;
		}

		[Bindable]
		public function get parametrosInicialEdificio():ArrayCollection
		{
			return _parametrosInicialEdificio;
		}

		public function set parametrosInicialEdificio(value:ArrayCollection):void
		{
			_parametrosInicialEdificio = value;
		}
		
		public function getParametro(parametro : String):ParametrosEdificioTO{
			for each(var obj:ParametrosEdificioTO in this._edificioActual.arrCollParametrosEdificio)
			{
				if(obj.simboloParametro == parametro){
					return obj;
				}
			}
			return null;
		}
		[Bindable] 
 

		[Bindable]
		public function get email():String
		{
			return _email;
		}

		public function set email(value:String):void
		{
			_email = value;
		}

 	[Bindable]
		public function get editandoEdificio():Boolean
		{
			return _editandoEdificio;
		}

		public function set editandoEdificio(value:Boolean):void
		{
			_editandoEdificio = value;
		}

		[Bindable]
		public function get creandoEdificio():Boolean
		{
			return _creandoEdificio;
		}

		public function set creandoEdificio(value:Boolean):void
		{
			_creandoEdificio = value;
		}

		public function get usuarioEditado():Boolean
		{
			return _usuarioEditado;
		}

		public function set usuarioEditado(value:Boolean):void
		{
			_usuarioEditado = value;
		}

		public function get fechaUltimaSincronizacion():String
		{
			return _fechaUltimaSincronizacion;
		}

		public function set fechaUltimaSincronizacion(value:String):void
		{
			_fechaUltimaSincronizacion = value;
		}
 
		
 
		

	}	
}
package cl.foursoft.eee.comunication.objetos
{
	import mx.collections.ArrayCollection;

	public class PeticionDatosMedicionTO
	{
		/*
		 * Tipo de Peticiones :
		 * .PorConsumos
		 * .PorEdificios
		 * .PorPisos
		 * .PorZonas
		 * .PorSensores
		***/
		
		private var _tipoDePeticion : String;
		private var _medicion : String;
		private var _consumos : ArrayCollection;
		private var _edificios : ArrayCollection;
		private var _pisos : ArrayCollection;
		private var _zonas : ArrayCollection;
		private var _sensores : ArrayCollection;
		
		public function PeticionDatosMedicionTO()
		{
			
			_medicion = "";
			_consumos = new ArrayCollection();
			_edificios = new ArrayCollection();
			_pisos = new ArrayCollection();
			_zonas = new ArrayCollection();
			_sensores = new ArrayCollection();
		}

		public function get consumos():ArrayCollection
		{
			return _consumos;
		}

		public function set consumos(value:ArrayCollection):void
		{
			_consumos = value;
		}

		public function get pisos():ArrayCollection
		{
			return _pisos;
		}

		public function set pisos(value:ArrayCollection):void
		{
			_pisos = value;
		}

		public function get zonas():ArrayCollection
		{
			return _zonas;
		}

		public function set zonas(value:ArrayCollection):void
		{
			_zonas = value;
		}

		public function get sensores():ArrayCollection
		{
			return _sensores;
		}

		public function set sensores(value:ArrayCollection):void
		{
			_sensores = value;
		}

		public function get edificios():ArrayCollection
		{
			return _edificios;
		}

		public function set edificios(value:ArrayCollection):void
		{
			_edificios = value;
		}

		public function get medicion():String
		{
			return _medicion;
		}

		public function set medicion(value:String):void
		{
			_medicion = value;
		}


	}
}
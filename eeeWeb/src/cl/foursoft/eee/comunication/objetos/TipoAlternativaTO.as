package cl.foursoft.eee.comunication.objetos
{
	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.TipoAlternativaTO")]
	public class TipoAlternativaTO
	{
		
		private var _nombreAlternativa : String;
		private var _descripcionAlternativa : String;
		private var _idAlternativa : int;
		
		
		public function TipoAlternativaTO()
		{
		}

		[Bindable]
		public function get nombreAlternativa():String
		{
			return _nombreAlternativa;
		}

		public function set nombreAlternativa(value:String):void
		{
			_nombreAlternativa = value;
		}

		[Bindable]
		public function get descripcionAlternativa():String
		{
			return _descripcionAlternativa;
		}

		public function set descripcionAlternativa(value:String):void
		{
			_descripcionAlternativa = value;
		}

		[Bindable]
		public function get idAlternativa():int
		{
			return _idAlternativa;
		}

		public function set idAlternativa(value:int):void
		{
			_idAlternativa = value;
		}
		
		public function toString():String
		{
			// TODO Auto Generated method stub
			if(idAlternativa == 0)
				return this._nombreAlternativa;
			return "Alternativa de " + this._nombreAlternativa;
		}
		
		
		

	}
}
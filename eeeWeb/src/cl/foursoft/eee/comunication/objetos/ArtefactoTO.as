package cl.foursoft.eee.comunication.objetos
{
	import flash.utils.ByteArray;

	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.ArtefactoTO")]
	public class ArtefactoTO
	{
		
		
		private var _idArtefacto : int;
		private var _nombreArtefacto : String;
		private var _tipoMedificionArtefacto : String;
		private var _consumoArtefacto : Number;
		private var _eficiencia_artefacto:Number
		private var _unidadMedidaArtefacto : String;
		private var _cantidadArtefacto : Number;
		private var _costo_unidad_artefacto : Number;
		private var _diferencia : Number;
		
		private var _rutaFotografia : String;
		private var _fotografia : ByteArray;
		
		private var _eliminado : Boolean;
		
		
		public function ArtefactoTO()
		{
			_consumoArtefacto = 0;
			_costo_unidad_artefacto = 0;
			_diferencia = 0;
			_cantidadArtefacto = 1;
			_unidadMedidaArtefacto = "";
			_idArtefacto = -1;
		}
		[Bindable]
		public function get nombreArtefacto():String
		{
			return _nombreArtefacto;
		}

		public function set nombreArtefacto(value:String):void
		{
			_nombreArtefacto = value;
		}
		[Bindable]
		public function get tipoMedificionArtefacto():String
		{
			return _tipoMedificionArtefacto;
		}

		public function set tipoMedificionArtefacto(value:String):void
		{
			_tipoMedificionArtefacto = value;
		}
		[Bindable]
		public function get consumoArtefacto():Number
		{
			return _consumoArtefacto ;
		}

		public function set consumoArtefacto(value:Number):void
		{
			_consumoArtefacto = value;
		}
		[Bindable]
		public function get unidadMedidaArtefacto():String
		{
			return _unidadMedidaArtefacto;
		}

		public function set unidadMedidaArtefacto(value:String):void
		{
			_unidadMedidaArtefacto = value;
		}
		[Bindable]
		public function get cantidadArtefacto():Number
		{
			return _cantidadArtefacto;
		}

		public function set cantidadArtefacto(value:Number):void
		{
			//Guardo el anterior
			//_cantidadAnteriorArtefacto = _cantidadArtefacto;
			//Edito la nueva cantidad
			_cantidadArtefacto = value;
			//Edito la diferencia
			//_cantidadaDiferencia = ((_cantidadArtefacto - _cantidadAnteriorArtefacto)<0) ? _cantidadaDiferencia*-1: (_cantidadaDiferencia + _cantidadArtefacto - _cantidadAnteriorArtefacto) ; // Se suma porque siempre debe ser la diferencia contra la cantidad inicial
			//trace("dif : " + _cantidadArtefacto +"-"+_cantidadAnteriorArtefacto + "+" +_cantidadaDiferencia + "=" );
			//_cantidadaDiferencia = ( _cantidadArtefacto - _cantidadAnteriorArtefacto + _cantidadaDiferencia);//<0) ? _cantidadaDiferencia*-1: (_cantidadaDiferencia + _cantidadArtefacto - _cantidadAnteriorArtefacto) ; // Se suma porque siempre debe ser la diferencia contra la cantidad inicial
			//trace("-->" + _cantidadaDiferencia );

			
			
		}
		[Bindable]
		public function get costo_unidad_artefacto():Number
		{
			return _costo_unidad_artefacto;
		}

		public function set costo_unidad_artefacto(value:Number):void
		{
			_costo_unidad_artefacto = value;
		}
		[Bindable]
		public function get eficiencia_artefacto():Number
		{
			return _eficiencia_artefacto;
		}

		public function set eficiencia_artefacto(value:Number):void
		{
			_eficiencia_artefacto = value;
		}
		
		public function toString():String
		{
			// TODO Auto Generated method stub
			return this.nombreArtefacto;
		}

		[Bindable]
		public function get idArtefacto():int
		{
			return _idArtefacto;
		}

		public function set idArtefacto(value:int):void
		{
			_idArtefacto = value;
		}

 
		[Bindable]
		public function get diferencia():Number
		{
			//_cantidadaDiferencia = _cantidadArtefacto - _cantidadAnteriorArtefacto;	
			return _diferencia
		}

		public function set diferencia(value:Number):void
		{
			_diferencia = value;
		}

		public function get rutaFotografia():String
		{
			return _rutaFotografia;
		}

		public function set rutaFotografia(value:String):void
		{
			_rutaFotografia = value;
		}

		[Bindable]
		public function get fotografia():ByteArray
		{
			return _fotografia;
		}

		public function set fotografia(value:ByteArray):void
		{
			_fotografia = value;
		}

		public function get eliminado():Boolean
		{
			return _eliminado;
		}

		public function set eliminado(value:Boolean):void
		{
			_eliminado = value;
		}
		
		


	}
}
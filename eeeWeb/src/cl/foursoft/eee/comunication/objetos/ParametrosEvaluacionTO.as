package cl.foursoft.eee.comunication.objetos
{
	import cl.foursoft.eee.actionscript.util.UtilCalculos;
 
	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.ParametrosEvaluacionTO")]
	public class ParametrosEvaluacionTO
	{
		
		 private var _nombreEvaluacion : String;
		// Datos Propios
	 	private var _idEvaluacion : int;
		private var _idCliente : int;
	 	
	 
	 	//Variables
		private var _horizonteEvaluacion : int;
		private var _tarifaCombustible : Number;
	 	private var _eficienciaGlobal :Number;
	 	private var _tasaDescuento : Number;
	 	private var _variacionPrecio : Number;
		private var _tarifaFinal : Number;
		
		public function ParametrosEvaluacionTO()
		{
			
			 this._idEvaluacion  = -1;
			 this._nombreEvaluacion = "";
			 this._idCliente = -1;
			//Variables
 			 this._tasaDescuento = 0;
			 this._variacionPrecio = 0;
			 this._horizonteEvaluacion = 0;
			 this._eficienciaGlobal = 0;
			 this._tarifaFinal = 0;
			 this._tarifaCombustible = 0;
			 
		}

		[Bindable]
		public function get idEvaluacion():int
		{
			return _idEvaluacion;
		}

		public function set idEvaluacion(value:int):void
		{
			_idEvaluacion = value;
		}

		[Bindable]
		public function get nombreEvaluacion():String
		{
			return _nombreEvaluacion;
		}

		public function set nombreEvaluacion(value:String):void
		{
			_nombreEvaluacion = value;
		}
 
		[Bindable]
		public function get tasaDescuento():Number
		{
			
			return UtilCalculos.round(_tasaDescuento);
		}

		public function set tasaDescuento(value:Number):void
		{
			_tasaDescuento = value;
		}

		[Bindable]
		public function get variacionPrecio():Number
		{
			return _variacionPrecio;
		}

		public function set variacionPrecio(value:Number):void
		{
			_variacionPrecio = value;
		}

		[Bindable]
		public function get horizonteEvaluacion():int
		{
			return _horizonteEvaluacion;
		}

		public function set horizonteEvaluacion(value:int):void
		{
			_horizonteEvaluacion = value;
		}
		[Bindable]
		public function get tarifaCombustible():Number
		{
			return _tarifaCombustible;
		}

		public function set tarifaCombustible(value:Number):void
		{
			_tarifaCombustible = value;
		}

		[Bindable]
		public function get eficienciaGlobal():Number
		{
			return UtilCalculos.round(_eficienciaGlobal);
		}

		public function set eficienciaGlobal(value:Number):void
		{
			_eficienciaGlobal = value;
		}

		[Bindable]
		public function get tarifaFinal():Number
		{
			return _tarifaFinal;
		}

		public function set tarifaFinal(value:Number):void
		{
			_tarifaFinal = value;
		}

		public function get idCliente():int
		{
			return _idCliente;
		}

		public function set idCliente(value:int):void
		{
			_idCliente = value;
		}
		
		public function toString():String
		{
			return _nombreEvaluacion;
		}
		

	}
}
package cl.foursoft.eee.comunication.objetos
{
	[Bindable]
	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.ArtefactoCombustibleDetalleTO")]
	public class ArtefactoCombustibleDetalleTO
	{
		
		private var _id_artefacto : int;
		private var _tipo_combustible : String; //20 caracteres
		private var _idTipoMedicion : String ;
		private var _potencia : Number;//
		private var _precio_inversion : Number;
		private var _indice_inversion : Number;
		private var _eficiencia : Number;
		private var _energia : Number;
		private var _eficiencia_distribucion_calor:Number;
		private var _valor_unitario_combustible : Number;
		private var _poder_calorifico : Number;
		private var _valor_consumo_final : Number;
		
		
		
		public function ArtefactoCombustibleDetalleTO()
		{
		}

		public function get id_artefacto():int
		{
			return _id_artefacto;
		}

		public function set id_artefacto(value:int):void
		{
			_id_artefacto = value;
		}

		public function get tipo_combustible():String
		{
			return _tipo_combustible;
		}

		public function set tipo_combustible(value:String):void
		{
			_tipo_combustible = value;
		}

		public function get potencia():Number
		{
			return _potencia;
		}

		public function set potencia(value:Number):void
		{
			_potencia = value;
		}

		public function get precio_inversion():Number
		{
			return _precio_inversion;
		}

		public function set precio_inversion(value:Number):void
		{
			_precio_inversion = value;
		}

		public function get indice_inversion():Number
		{
			return _indice_inversion;
		}

		public function set indice_inversion(value:Number):void
		{
			_indice_inversion = value;
		}

		public function get eficiencia():Number
		{
			return _eficiencia;
		}

		public function set eficiencia(value:Number):void
		{
			_eficiencia = value;
		}

		public function get energia():Number
		{
			return _energia;
		}

		public function set energia(value:Number):void
		{
			_energia = value;
		}

		public function get eficiencia_distribucion_calor():Number
		{
			return _eficiencia_distribucion_calor;
		}

		public function set eficiencia_distribucion_calor(value:Number):void
		{
			_eficiencia_distribucion_calor = value;
		}

		public function get valor_unitario_combustible():Number
		{
			return _valor_unitario_combustible;
		}

		public function set valor_unitario_combustible(value:Number):void
		{
			_valor_unitario_combustible = value;
		}

		public function get poder_calorifico():Number
		{
			return _poder_calorifico;
		}

		public function set poder_calorifico(value:Number):void
		{
			_poder_calorifico = value;
		}

		public function get valor_consumo_final():Number
		{
			return _valor_consumo_final;
		}

		public function set valor_consumo_final(value:Number):void
		{
			_valor_consumo_final = value;
		}

		public function get idTipoMedicion():String
		{
			return _idTipoMedicion;
		}

		public function set idTipoMedicion(value:String):void
		{
			_idTipoMedicion = value;
		}


	}
}
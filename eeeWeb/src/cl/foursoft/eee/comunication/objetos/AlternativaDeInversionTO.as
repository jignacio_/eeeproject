package cl.foursoft.eee.comunication.objetos
{
	import flash.events.Event;
	
	import flashx.textLayout.formats.Float;
	
	import mx.collections.ArrayCollection;

	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.AlternativaDeInversionTO")]
	public class AlternativaDeInversionTO
	{
		
		private var _descripcionAlternativa : String;
		private var _idEdificio : int;
		private var _idAlternativa : int ;
		private var _idParametrosEvaluacion : int;
		private var _envolventeNueva : EnvolventeTO;
		private var _sensoresAlternativaArtefacto : ArrayCollection; // _arrayColl de sensores
		private var _idTipoAlternativa : int;
		private var _fechaUltimaEvaluacion : Date;
		private var _fechaSiguienteEvaluacion : Date;
		private var _requerimientoEnergeticoTotal : Number;
		private var _unidadMedicionAlternativa : String;
		
		
		
		/*Cuando es true significa que la evaluacion es mejor que la configuracion actual actual*/
		private var _estadoAlternativa : Boolean;
		private var _envolventeDiferencia : ArrayCollection;
		private var _artefactosDiferencia : ArrayCollection;
		private var _inversionAlternativa : Number;
		
		private var _otroCosto : Number;
		
		public function AlternativaDeInversionTO()
		{
			
			_otroCosto = 0; 
			_unidadMedicionAlternativa = "";
			_idAlternativa = -1;
			_idTipoAlternativa = 0;
			_envolventeDiferencia = new ArrayCollection();
			_sensoresAlternativaArtefacto = new ArrayCollection();
			_artefactosDiferencia = new ArrayCollection(); 
		}

		[Bindable]
		public function get descripcionAlternativa():String
		{
			return _descripcionAlternativa;
		}

		public function set descripcionAlternativa(value:String):void
		{
			_descripcionAlternativa = value;
		}

		[Bindable]
		public function get idParametrosEvaluacion():int
		{
			return _idParametrosEvaluacion;
		}

		public function set idParametrosEvaluacion(value:int):void
		{
				_idParametrosEvaluacion = value;
				
		}

		[Bindable]
		public function get envolventeNueva():EnvolventeTO
		{
			return _envolventeNueva;
		}

		public function set envolventeNueva(value:EnvolventeTO):void
		{
			_envolventeNueva = value;
		}

		[Bindable]
		public function get fechaUltimaEvaluacion():Date
		{
			return _fechaUltimaEvaluacion;
		}

		public function set fechaUltimaEvaluacion(value:Date):void
		{
			_fechaUltimaEvaluacion = value;
		}

		[Bindable]
		public function get fechaSiguienteEvaluacion():Date
		{
			return _fechaSiguienteEvaluacion;
		}

		public function set fechaSiguienteEvaluacion(value:Date):void
		{
			_fechaSiguienteEvaluacion = value;
		}

		[Bindable]
		public function get idEdificio():int
		{
			return _idEdificio;
		}

		public function set idEdificio(value:int):void
		{
			_idEdificio = value;
		}

		[Bindable]
		public function get idTipoAlternativa():int
		{
			return _idTipoAlternativa;
		}

		public function set idTipoAlternativa(value:int):void
		{
			_idTipoAlternativa = value;
		}

		[Bindable]
		public function get requerimientoEnergeticoTotal():Number
		{
			return _requerimientoEnergeticoTotal;
		}

		public function set requerimientoEnergeticoTotal(value:Number):void
		{
			_requerimientoEnergeticoTotal = value;
		}

		[Bindable]
		public function get estadoAlternativa():Boolean
		{
			return _estadoAlternativa;
		}

		public function set estadoAlternativa(value:Boolean):void
		{
			_estadoAlternativa = value;
		}

		[Bindable]
		public function get idAlternativa():int
		{
			return _idAlternativa;
		}

		public function set idAlternativa(value:int):void
		{
			_idAlternativa = value;
		}

		[Bindable]
		public function get envolventeDiferencia():ArrayCollection
		{
			return _envolventeDiferencia;
		}

		public function set envolventeDiferencia(value:ArrayCollection):void
		{
			_envolventeDiferencia = value;
		}

		[Bindable]
		public function get inversionAlternativa():Number
		{
			return _inversionAlternativa;
		}

		public function set inversionAlternativa(value:Number):void
		{
			_inversionAlternativa = value;
		}

		[Bindable]
		public function get otroCosto():Number
		{
			return _otroCosto;
		}
		
		public function set otroCosto(value:Number):void
		{
			_otroCosto = value;
		}

		[Bindable]
		public function get sensoresAlternativaArtefacto():ArrayCollection
		{
			return _sensoresAlternativaArtefacto;
		}

		public function set sensoresAlternativaArtefacto(value:ArrayCollection):void
		{
			_sensoresAlternativaArtefacto = value;
		}

		[Bindable]
		public function get artefactosDiferencia():ArrayCollection
		{
			return _artefactosDiferencia;
		}

		public function set artefactosDiferencia(value:ArrayCollection):void
		{
			_artefactosDiferencia = value;
		}

		[Bindable]
		public function get unidadMedicionAlternativa():String
		{
			return _unidadMedicionAlternativa;
		}

		public function set unidadMedicionAlternativa(value:String):void
		{
			_unidadMedicionAlternativa = value;
		}


	}
}
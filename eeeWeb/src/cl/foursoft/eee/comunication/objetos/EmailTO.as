package cl.foursoft.eee.comunication.objetos
{
	[Bindable]
	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.EmailTO")]
	public class EmailTO
	{
		
		private var _usuario_email : String;
		private var _contrasenia_email : String;
		private var _remitente : String;
		private var _pie : String;
		
		public function EmailTO()
		{
			this._usuario_email = "";
			this._contrasenia_email = "";
			this._remitente = "";
			this._pie = "";
		}
		
		
		public function get usuario_email():String
		{
			return _usuario_email;
		}

		public function set usuario_email(value:String):void
		{
			_usuario_email = value;
		}

		public function get contrasenia_email():String
		{
			return _contrasenia_email;
		}

		public function set contrasenia_email(value:String):void
		{
			_contrasenia_email = value;
		}

		public function get remitente():String
		{
			return _remitente;
		}

		public function set remitente(value:String):void
		{
			_remitente = value;
		}

		public function get pie():String
		{
			return _pie;
		}

		public function set pie(value:String):void
		{
			_pie = value;
		}


	}
}
package cl.foursoft.eee.comunication.objetos
{
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;

	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.ClienteTO")]
	public class ClienteTO
	{
		
		private var _idCliente:int;
		private var _nombreCliente:String;
		private var _direccionPrincipalCliente:String;
		private var _numeroTelefono:int;
		
		private var _rutaFotografia:String;
		private var _fotografiaCliente:ByteArray;
		private var _cantidadUsuatios:int;

		
		
		public function ClienteTO()
		{
		}

		[Bindable]
		public function get idCliente():int
		{
			return _idCliente;
		}

		public function set idCliente(value:int):void
		{
			_idCliente = value;
		}

		[Bindable]
		public function get nombreCliente():String
		{
			return _nombreCliente;
		}

		public function set nombreCliente(value:String):void
		{
			_nombreCliente = value;
		}

		[Bindable]
		public function get direccionPrincipalCliente():String
		{
			return _direccionPrincipalCliente;
		}

		public function set direccionPrincipalCliente(value:String):void
		{
			_direccionPrincipalCliente = value;
		}

		[Bindable]
		public function get numeroTelefono():int
		{
			return _numeroTelefono;
		}

		public function set numeroTelefono(value:int):void
		{
			_numeroTelefono = value;
		}

		[Bindable]
		public function get rutaFotografia():String
		{
			return _rutaFotografia;
		}

		public function set rutaFotografia(value:String):void
		{
			_rutaFotografia = value;
		}

		[Bindable]
		public function get fotografiaCliente():ByteArray
		{
			return _fotografiaCliente;
		}

		public function set fotografiaCliente(value:ByteArray):void
		{
			_fotografiaCliente = value;
		}

		[Bindable]
		public function get cantidadUsuatios():int
		{
			return _cantidadUsuatios;
		}

		public function set cantidadUsuatios(value:int):void
		{
			_cantidadUsuatios = value;
		}


	}
}
package cl.foursoft.eee.comunication.objetos
{
	import cl.foursoft.eee.actionscript.util.UtilCalculos;
	
	import flash.events.EventDispatcher;

	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.EvaluacionEconomicaTO")]
	public class EvaluacionEconomicaTO
	{
		private var _descripcionAlternativa : String;
		
		private var _valorVan:Number;
		private var _valorTir : Number;
		private var _valorPayBack : Number;
		
		private var _idAlternativa : int;
		private var _idParametro : int;
		
		private var _fecha_creacion : String;
		private var _fecha_ultima_evaluacion : String;
		private var _fecha_siguiente_evaluacion : String;
		
		private var _flujos : Array;
		private var _flujosAcumulados : Array;
		private var _valores : Array;
		
		public function EvaluacionEconomicaTO()
		{
		}
		
		[Bindable]
		public function get valorVan():Number
		{
			return  _valorVan;
		}

		public function set valorVan(value:Number):void
		{
				_valorVan = value;
		}

		[Bindable]
		public function get valorTir():Number
		{
			return _valorTir;
		}

		public function set valorTir(value:Number):void
		{
			_valorTir = value;
		}

		[Bindable]
		public function get valorPayBack():Number
		{
			return _valorPayBack;
		}

		public function set valorPayBack(value:Number):void
		{
			_valorPayBack = value;
		}

		[Bindable]
		public function get idAlternativa():int
		{
			return _idAlternativa;
		}

		public function set idAlternativa(value:int):void
		{
			_idAlternativa = value;
		}

		[Bindable]
		public function get idParametro():int
		{
			return _idParametro;
		}

		public function set idParametro(value:int):void
		{
			_idParametro = value;
		}

		[Bindable]
		public function get fecha_creacion():String
		{
			return _fecha_creacion;
		}

		public function set fecha_creacion(value:String):void
		{
			_fecha_creacion = value;
		}

		[Bindable]
		public function get fecha_ultima_evaluacion():String
		{
			return _fecha_ultima_evaluacion;
		}

		public function set fecha_ultima_evaluacion(value:String):void
		{
			_fecha_ultima_evaluacion = value;
		}

		[Bindable]
		public function get fecha_siguiente_evaluacion():String
		{
			return _fecha_siguiente_evaluacion;
		}

		public function set fecha_siguiente_evaluacion(value:String):void
		{
			_fecha_siguiente_evaluacion = value;
		}

		[Bindable]
		public function get flujos():Array
		{
			return _flujos;
		}

		public function set flujos(value:Array):void
		{
			_flujos = value;
		}

		[Bindable]
		public function get flujosAcumulados():Array
		{
			return _flujosAcumulados;
		}

		public function set flujosAcumulados(value:Array):void
		{
			_flujosAcumulados = value;
		}

		[Bindable]
		public function get valores():Array
		{
			return _valores;
		}

		public function set valores(value:Array):void
		{
			_valores = value;
		}

		[Bindable]
		public function get descripcionAlternativa():String
		{
			return _descripcionAlternativa;
		}

		public function set descripcionAlternativa(value:String):void
		{
			_descripcionAlternativa = value;
		}


	}
}
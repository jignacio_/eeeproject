package cl.foursoft.eee.comunication.objetos
{
	import cl.foursoft.eee.actionscript.interfaces.ISeleccionVistasParaGestion;
	import cl.foursoft.eee.actionscript.util.Nodo;
	
	import mx.collections.ArrayCollection;

	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.ZonaTO")]
	public class ZonaTO extends Nodo implements ISeleccionVistasParaGestion
	{
		private var _tipoSeleccionado : Boolean;
		private var _idZona : int;
		private var _prefijo:String;
		private var _nombreZona :String;
		private var _numeroZonaActual:int;
		private var _numeroSensores : int;
		private var _descripcionZona : String;
		private var _arrCollSensoresZona : ArrayCollection;
		private var _guardado : Boolean;
	//	private var _pisoPadreId : int ;
/*		private var _idPiso : int;
*/		
		private var _TSensores:int;
		private var _TZonas : int;
		
		public function ZonaTO(_numeroZona:int=0)
		{
			this._guardado = false;
			this._numeroZonaActual = _numeroZona;
			this._prefijo = "Zona"
			this._numeroSensores = 0;
			this._idZona = 0;
			this._nombreZona = "";
			this._descripcionZona = "";
		//	this._pisoPadreId = -1;
		/*	this._idPiso = -1;*/
			this._arrCollSensoresZona = new ArrayCollection(); 
			
		}

		[Bindable]
		public function get idZona():int
		{
			return _idZona;
		}

		public function set idZona(value:int):void
		{
			_idZona = value;
		}
		[Bindable]
		public function get nombreZona():String
		{
			return _nombreZona;
		}

		public function set nombreZona(value:String):void
		{
			_nombreZona = value;
		}

		[Bindable]
		public function get descripcionZona():String
		{
			return _descripcionZona;
		}

		public function set descripcionZona(value:String):void
		{
			_descripcionZona = value;
		}

		[Bindable]
		public function get arrCollSensoresZona():ArrayCollection
		{
			
			return _arrCollSensoresZona;
		}

		public function set arrCollSensoresZona(value:ArrayCollection):void
		{
			_arrCollSensoresZona = value;
		}
		public function agregarSensor(value : SensorTO ) :int{
			
			if(value){
				this._arrCollSensoresZona.addItem(value);
				return 0;
			}
			return -1;
		}
		public function set numeroSensores(value:int):void{
			this._numeroSensores = value;
		}
		[Bimdable]
		public function get numeroSensores():int{
			return _arrCollSensoresZona.length;
		}

		[Bindable]
		public function get numeroZonaActual():int
		{
			return _numeroZonaActual;
		}

		public function set numeroZonaActual(value:int):void
		{
			_numeroZonaActual = value;
		}
		
		override public function get children():ArrayCollection
		{
			// TODO Auto Generated method stub
			return _arrCollSensoresZona;
		}
		
		public function toString():String{
			if(_numeroZonaActual==0)
				return this._nombreZona;
			return this._prefijo +" "+this._numeroZonaActual+ " ( "+this._nombreZona+" )";
		}

		[Bindable]
		public function get TSensores():int
		{
			return _TSensores;
		}

		public function set TSensores(value:int):void
		{
			_TSensores = value;
		}

		[Bindable]
		public function get TZonas():int
		{
			return _TZonas;
		}

		public function set TZonas(value:int):void
		{
			_TZonas = value;
		}
		
		public function reiniciar():void{
			 _TSensores = 0;
						
			this._guardado = false;
			this._prefijo = "Zona"
			this._nombreZona = "";
			this._descripcionZona = "";
			
			
			this._arrCollSensoresZona = new ArrayCollection();
		}

		[Bindable]
		public function get guardado():Boolean
		{
			return _guardado;
		}

		public function set guardado(value:Boolean):void
		{
			_guardado = value;
		}
		override public function get name():String{
			return toString();
		}
		
		public function get nombreElemento():String
		{
			// TODO Auto Generated method stub
			return toString();
		}
		
		public function set tipoSeleccionado(value:Boolean):void
		{
			this._tipoSeleccionado =  value;
			
		}
		
		public function get tipoSeleccionado():Boolean
		{
			// TODO Auto Generated method stub
			return this._tipoSeleccionado;
		}
		
 		

	}
}
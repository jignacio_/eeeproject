package cl.foursoft.eee.comunication.objetos
{
	import cl.foursoft.eee.actionscript.interfaces.ISeleccionVistasParaGestion;
	import cl.foursoft.eee.actionscript.util.Nodo;
	import cl.foursoft.eee.actionscript.util.UtilCalculos;
	
	import flash.events.Event;
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
 

	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.EdificioTO")]
	public class EdificioTO extends Nodo implements ISeleccionVistasParaGestion
	{
		//vistas para gestion
		private var _tipoSeleccionado:Boolean;
		//DATOS GENÉRICOS DEL EDIFICIO
		private var _idEdificio:int;
		private var _idCliente:Number;
		private var _codigoEdificio : Number;
		private var _nombreEdificio:String;
		private var _direccionCompletaEdificio:String;
		private var _latitud:Number;
		private var _longitud:Number;
		private var _observaciones:String;
		
		private var _direccionEdificio : String;
		private var _ciudadEdificio : String;
		private var _regionEdificio : String;
		private var _paisEdificio : String;
		
		//Dimansiones Generales
		private var _altoPiso:Number;
		private var _largo:Number;
		private var _ancho:Number;
		private var _numeroPisos : Number;
		
		private var _areaTotalEnvolvente : Number;
 
		private var _perimetro:Number;
		private var _superficieBruta:Number;
		private var _superficieUtil : Number;
		private var _volumenDeAire : Number; 
		private var _areaTotal : Number;
		private var _compactacion : Number; 
		
		
		private var _fotografia : ByteArray;
		private var _rutaFotografia : String;
		private var _nombreArchivo : String;
		
		private var _numeroEdificio : int;
		
		//Colecciones
		private var _arrCollSensoresEdificio :ArrayCollection;
		private var _arrCollPisosEdificio : ArrayCollection;
		private var _arrCollSecciones:ArrayCollection;
		private var _arrCollParametrosEdificio : ArrayCollection;

		//Factores
		private var _idFactorRenovacion : int;//<-Enviar a parametros
		private var _valorFactorRenovacion : Number;
		
		private var _aireAcondicionado : Boolean;//<-enviar a Parametros
		private var _fechaCreacion : String; 
		
		/*Parametros unitarios temporales*/
		//private var _RSI : Number;//<-Enviar a Parametros
		//private var _RSE : Number; //<-Enviar a Paramnetros
		
		[Bindable] private var _medicionCombustible:Number;
		[Bindable] private var _medicionElectricidad:Number;
		[Bindable] private var _medicionSensor:Number;
		[Bindable] private var _medicionAgua:Number;
		
/* private var _medicionCombustibleHoy:Number;
 private var _medicionElectricidadHoy:Number;
 private var _medicionAguaHoy:Number;*/
		
		 private var _medicionCombustibleHoy:int;
		private var _medicionElectricidadHoy:int;
		private var _medicionAguaHoy:int;
		
 private var _cantidadAlternativasEnvolvente : int;
 private var _cantidadAlternativasArtefactos : int ;
 
		private var _sensoresEliminados : ArrayCollection;
		
		public function EdificioTO()
		{
			this._idEdificio = -1; 
			this._nombreEdificio = "";
			this._direccionCompletaEdificio  = "";
			this._latitud = -1;
			this._longitud = -1;
			this._observaciones = "";
			
		
			this._altoPiso = 0;
			this._numeroPisos = 0;
			this._largo =0;
			this. _ancho = 0;
			
 			
			this._perimetro = 0;
	 	    this._superficieBruta = 0; 
			this._superficieUtil = 0;
			this._volumenDeAire = 0;

			this._direccionEdificio = "";
			this._ciudadEdificio = "";
			this._regionEdificio = "";
			this._paisEdificio = "";
			this._rutaFotografia = "";
			this._fotografia = null;
			_arrCollSecciones = null;
			this._aireAcondicionado = false;
			
			this._cantidadAlternativasArtefactos = 0;
			this._cantidadAlternativasEnvolvente = 0;
		
			
	/*		this._RSE = 0;
			this._RSI = 0;*/
 
			this._arrCollParametrosEdificio = null;

			_arrCollSensoresEdificio = new ArrayCollection();
			_arrCollPisosEdificio 	 = new ArrayCollection();
			_sensoresEliminados = new ArrayCollection();
			
			
		 	//medicion de los sensores
			this._medicionCombustible = 0;
			this._medicionSensor = 0;
			this._medicionElectricidad = 0;
			this._medicionAgua = 0
		}

		[Bindable]
		public function get medicionSensor():Number
		{
			return _medicionSensor;
		}

		public function set medicionSensor(value:Number):void
		{
			_medicionSensor = value;
		}

/*		[Bindable]
		public function get medicionAgua():Number
		{
			return ( UtilCalculos.round(_medicionAgua) / 100);
		}

		public function set medicionAgua(value:Number):void
		{
			_medicionAgua = value;
		}

		[Bindable]
		public function get medicionElectricidad():Number
		{
			return UtilCalculos.round(_medicionElectricidad);
		}

		public function set medicionElectricidad(value:Number):void
		{
			_medicionElectricidad = value;
		}

		[Bindable]
		public function get medicionCombustible():Number 
		{
		
			return UtilCalculos.round( _medicionCombustible );
		}

		public function set medicionCombustible(value:Number):void
		{ 
			_medicionCombustible = value;
		}*/
		[Bindable]
		public function get medicionAgua():int
		{
			return _medicionAgua;
		}
		
		[Bindable]
		public function get medicionAguaToString():String
		{
			return UtilCalculos.separarMiles(_medicionAgua);
		}
		
		public function set medicionAgua(value:int):void
		{
			_medicionAgua = value;
		}
		
		[Bindable]
		public function get medicionElectricidad():int
		{
			return _medicionElectricidad;
		}
		[Bindable]
		public function get medicionElectricidadToString():String
		{
			return UtilCalculos.separarMiles(_medicionElectricidad);
		}
		
		public function set medicionElectricidad(value:int):void
		{
			_medicionElectricidad = value;
		}
		
		[Bindable]
		public function get medicionCombustible():int 
		{
			
			return  _medicionCombustible ;
		}
		
		[Bindable]
		public function get medicionCombustibleToString():String 
		{
			
			return  UtilCalculos.separarMiles(_medicionCombustible );
		}
		
		public function set medicionCombustible(value:int):void
		{ 
			_medicionCombustible = value;
		}
		[Bindable]
		public function get idEdificio():int
		{
			return _idEdificio;
		}

		public function set idEdificio(value:int):void
		{
			_idEdificio = value;
		}
		[Bindable]
		public function get nombreEdificio():String
		{
			return _nombreEdificio;
		}

		public function set nombreEdificio(value:String):void
		{
			_nombreEdificio = value;
		}
		[Bindable]
		public function get direccionCompletaEdificio():String
		{ 
			return _direccionCompletaEdificio+".";
		}

		public function set direccionCompletaEdificio(value:String):void
		{
			_direccionCompletaEdificio = value;
		}
		[Bindable]
		public function get latitud():Number
		{
			return _latitud;
		}

		public function set latitud(value:Number):void
		{
			_latitud = value;
		}

		[Bindable]
		public function get longitud():Number
		{
			return _longitud;
		}

		public function set longitud(value:Number):void
		{
			_longitud = value;
		}
		 [Bindable]
		public function get observaciones():String
		{
			return _observaciones;
		}

		public function set observaciones(value:String):void
		{
			_observaciones = value;
		}
		public function get altoPiso():int
		{
			return _altoPiso;
		}

		public function set altoPiso(value:int):void
		{
			_altoPiso = value;
		}

		public function get largo():int
		{
			return _largo;
		}

		public function set largo(value:int):void
		{
			_largo = value;
		}

		public function get ancho():int
		{
			return _ancho;
		}

		public function set ancho(value:int):void
		{
			_ancho = value;
		}

		public function get perimetro():Number
		{
			return round(_perimetro);
		}

		public function set perimetro(value:Number):void
		{
			_perimetro = value;
		}
		[Bindable]
		public function get superficieBruta():Number
		{
			return round(_superficieBruta);
		}

		public function set superficieBruta(value:Number):void
		{
			_superficieBruta = value;
		}

	 

		public function get idCliente():Number
		{
			return _idCliente;
		}

		public function set idCliente(value:Number):void
		{
			_idCliente = value;
		}
		[Bindable]
		public function get arrCollSecciones():ArrayCollection
		{
			return _arrCollSecciones;
		}

		public function set arrCollSecciones(value:ArrayCollection):void
		{
			_arrCollSecciones = value;
		}

		[Bindable]
		public function get arrCollSensoresEdificio():ArrayCollection
		{
			return _arrCollSensoresEdificio;
		}

		public function set arrCollSensoresEdificio(value:ArrayCollection):void
		{
			_arrCollSensoresEdificio = value;
		}

		[Bindable]
		public function get arrCollPisosEdificio():ArrayCollection
		{
			return _arrCollPisosEdificio;
		}

		public function set arrCollPisosEdificio(value:ArrayCollection):void
		{
			_arrCollPisosEdificio = value;
		}
 
		
		[Bindable]
		public function get superficieUtil():Number
		{
			return round(_superficieUtil);
		}

		public function set superficieUtil(value:Number):void
		{
			_superficieUtil = value;
		}

		[Bindable]
		public function get volumenDeAire():Number
		{
			return round(_volumenDeAire);
		}

		public function set volumenDeAire(value:Number):void
		{
			_volumenDeAire = value;
		}

		[Bindable]
		public function get ciudadEdificio():String
		{
			return _ciudadEdificio;
		}

		public function set ciudadEdificio(value:String):void
		{
			_ciudadEdificio = value;
		}

		[Bindable]
		public function get regionEdificio():String
		{
			return _regionEdificio;
		}

		public function set regionEdificio(value:String):void
		{
			_regionEdificio = value;
		}

		[Bindable]
		public function get paisEdificio():String
		{
			return _paisEdificio;
		}

		public function set paisEdificio(value:String):void
		{
			_paisEdificio = value;
		}

		[Bindable]
		public function get direccionEdificio():String
		{
			return _direccionEdificio;
		}

		public function set direccionEdificio(value:String):void
		{
			_direccionEdificio = value;
		}

		[Bindable]
		public function get localizacion():Number
		{
			return _compactacion;
		}

		public function set localizacion(value:Number):void
		{
			_compactacion = value;
		}

		[Bindable]
		public function get fotografia():ByteArray
		{
			return _fotografia;

		}
		public function set fotografia(value:ByteArray):void
		{
			this._fotografia = value;
		}

		[Bindable]
		public function get rutaFotografia():String
		{
			return _rutaFotografia;
		}

		public function set rutaFotografia(value:String):void
		{
			_rutaFotografia = value;
		}

		[Bindable]
		public function get nombreArchivo():String
		{
			return _nombreArchivo;
		}

		public function set nombreArchivo(value:String):void
		{
			_nombreArchivo = value;
		}
		
	
		[Bindable]
		public function get numeroEdificio():int
		{
			return _numeroEdificio;
		}

		public function set numeroEdificio(value:int):void
		{
			_numeroEdificio = value;
		}
		
		
		public function toString():String
		{
//			return "Edificio " + this._numeroEdificio +" : "+this._nombreEdificio;
			return this._nombreEdificio;
		}

		[Bindable]
		public function get numeroPisos():Number
		{
			return _numeroPisos;
		}

		public function set numeroPisos(value:Number):void
		{
			_numeroPisos = value;
		}

		[Bindable]
		public function get areaTotal():Number
		{
			return _areaTotal;
		}

		public function set areaTotal(value:Number):void
		{
			_areaTotal = value;
		}
 

		[Bindable]
		public function get areaTotalEnvolvente():Number
		{
			return _areaTotalEnvolvente;
		}

		public function set areaTotalEnvolvente(value:Number):void
		{
			_areaTotalEnvolvente = value;
		}

		[Bindable]
		public function get codigoEdificio():Number
		{
			return _codigoEdificio;
		}

		public function set codigoEdificio(value:Number):void
		{
			_codigoEdificio = value;
		}

		public function get idFactorRenovacion():int
		{
			return _idFactorRenovacion;
		}

		public function set idFactorRenovacion(value:int):void
		{
			_idFactorRenovacion = value;
		}

	 

		[Bindable]
		public function get arrCollParametrosEdificio():ArrayCollection
		{
			return _arrCollParametrosEdificio;
		}

		public function set arrCollParametrosEdificio(value:ArrayCollection):void
		{
			_arrCollParametrosEdificio = value;
		}

		[Bindable]
		public function get aireAcondicionado():Boolean
		{
			return _aireAcondicionado;
		}

		public function set aireAcondicionado(value:Boolean):void
		{
			_aireAcondicionado = value;
		}

		//UTIL
//		[Bindable]
		public function get totalSensoresEdificio():Number{
			var _numeroTotalSensores : Number  = 0;
			
			//Total sensores asignacos al edificio Directamente
			if(_arrCollSensoresEdificio!=null && _arrCollSensoresEdificio.length>0)
				_numeroTotalSensores = _arrCollSensoresEdificio.length;
			
			//Tota, Sensores de los Pisos
			if(_arrCollPisosEdificio!=null && _arrCollPisosEdificio.length>0){
				for each(var _piso : PisoTO in _arrCollPisosEdificio){
					_numeroTotalSensores +=_piso.numeroSensores;//Llamar a una funcion
				}
			}
			
			return _numeroTotalSensores;
			
		}
		
		private  function round(i:Number):Number{
			return (
				(Math.round( (i)*1000 )) /1000
			);
		}

		//Funciones UTIL
		public function costoEdificio() : Number{
			var resp  : Number = 0;
			
			for each (var s : SeccionTO in _arrCollSecciones){
				resp += s.obtenerCostoTotalSeccion; 
			}
			
			return resp;
		}

		[Bindable]
		public function get fechaCreacion():String
		{
			return _fechaCreacion;
		}

		public function set fechaCreacion(value:String):void
		{
			_fechaCreacion = value;
		}
		
		override public function get name():String{
			return toString();
		}
		override public function set children(value : ArrayCollection):void{

		}
		override public function get children():ArrayCollection
		{
			// TODO Auto Generated method stub
			if(_arrCollSensoresEdificio)
				return new ArrayCollection(_arrCollPisosEdificio.toArray().concat(_arrCollSensoresEdificio.toArray()));
			return _arrCollPisosEdificio;
		}

		[Bindable]
		public function get valorFactorRenovacion():Number
		{
			return _valorFactorRenovacion;
		}

		public function set valorFactorRenovacion(value:Number):void
		{
				_valorFactorRenovacion = value;
		}

		[Bindable]
		public function get sensoresEliminados():ArrayCollection
		{
			return _sensoresEliminados;
		}

		public function set sensoresEliminados(value:ArrayCollection):void
		{
			_sensoresEliminados = value;
		}

		[Bindable]
		public function get medicionCombustibleHoy():Number{
	 
			return UtilCalculos.round(_medicionCombustibleHoy);
		}

		public function set medicionCombustibleHoy(value:Number):void
		{
			_medicionCombustibleHoy = value;
		}

		[Bindable]
		public function get medicionElectricidadHoy():Number
		{ 
			return Number(UtilCalculos.separarMiles(_medicionElectricidadHoy,2));;
		}

		public function set medicionElectricidadHoy(value:Number):void
		{
			_medicionElectricidadHoy = value;
		}

		[Bindable]
		public function get medicionAguaHoy():Number
		{
			return Number(UtilCalculos.separarMiles(_medicionAguaHoy,2));
		}

		public function set medicionAguaHoy(value:Number):void
		{
			_medicionAguaHoy = value;
		}
		
		public function get nombreElemento():String
		{
			// TODO Auto Generated method stub
			return this._nombreEdificio;
		}
		
		public function set tipoSeleccionado(value:Boolean):void
		{
			this._tipoSeleccionado = value;
			
		}
		[Bindable]
		public function get tipoSeleccionado():Boolean
		{
			// TODO Auto Generated method stub
			return this._tipoSeleccionado;
		}

 [Bindable]
 public function get cantidadAlternativasEnvolvente():int
 {
	 return _cantidadAlternativasEnvolvente;
 }

 public function set cantidadAlternativasEnvolvente(value:int):void
 {
	 _cantidadAlternativasEnvolvente = value;
 }

 [Bindable]
 public function get cantidadAlternativasArtefactos():int
 {
	 return _cantidadAlternativasArtefactos;
 }

 public function set cantidadAlternativasArtefactos(value:int):void
 {
	 _cantidadAlternativasArtefactos = value;
 }
		
	 
		
		
		
		 
	}
}
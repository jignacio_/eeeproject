package cl.foursoft.eee.comunication.objetos
{
	import flash.utils.ByteArray;
	
	import flashx.textLayout.formats.Float;
	
	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.MaterialTO")]
	public class MaterialTO
	{
		private var _idMaterial : int;
		private var _nombreMaterial:String;
		private var _nombreCorto:String;
		private var _espesor:Number;
		private var _conductividad:Number;
		private var _resistencia:Number;
		private var _transmitancia:Number;
		private var _precioMaterial : Number;
		private var _idSeccionEdificio : int;
		private var _nombreSeccionEdificio : String;
		private var _fotografia : ByteArray;
		private var _rutaFotografia : String;
		
		private var _cantidad_material : Number;
		private var _unidad_material : String;
		
		private var _eliminado : Boolean;
		
		public function MaterialTO()
		{
			_nombreMaterial = "";
			_nombreCorto = "";
			
			_idMaterial = -1;
			_espesor = 0;
			_conductividad = 0;
			_resistencia = 0;
			_transmitancia = 0;
			_precioMaterial = 0;
			_cantidad_material = 1;
			_idSeccionEdificio = -1;
			
			_fotografia = null;
			_rutaFotografia = "";
		
			
		}
		
		
		[Bindable]
		public function get nombreMaterial():String
		{
			return _nombreMaterial;
		}

		public function set nombreMaterial(value:String):void
		{
			_nombreMaterial = value;
		}

		[Bindable]
		public function get conductividad():Number
		{
			return _conductividad;
		}

		public function set conductividad(value:Number):void
		{
			_conductividad = value;
		}

		[Bindable]
		public function get resistencia():Number
		{
			return _resistencia;
		}

		public function set resistencia(value:Number):void
		{
			_resistencia = value;
		}

		[Bindable]
		public function get transmitancia():Number
		{
			return _transmitancia;
		}

		public function set transmitancia(value:Number):void
		{
			_transmitancia = value;
		}

		[Bindable]
		public function get espesor():Number
		{
			return _espesor;
		}

		public function set espesor(value:Number):void
		{
			_espesor = value;
		}

		[Bindable]
		public function get nombreCorto():String
		{
			return _nombreCorto;
		}

		public function set nombreCorto(value:String):void
		{
			_nombreCorto = value;
		}

		[Bindable]
		public function get idMaterial():int
		{
			return _idMaterial;
		}

		public function set idMaterial(value:int):void
		{
			_idMaterial = value;
		}

		[Bindable]
		public function get precioMaterial():Number
		{
			return _precioMaterial;
		}

		public function set precioMaterial(value:Number):void
		{
			_precioMaterial = value;
		}
		
		

		[Bindable]
		public function get idSeccionEdificio():int
		{
			return _idSeccionEdificio;
		}

		public function set idSeccionEdificio(value:int):void
		{
			_idSeccionEdificio = value;
		}
		
		public function toString():String
		{
			// TODO Auto Generated method stub
			return this._nombreMaterial;
		}

		[Bindable]
		public function get fotografia():ByteArray
		{
			return _fotografia;
		}

		public function set fotografia(value:ByteArray):void
		{
			_fotografia = value;
		}

		[Bindable]
		public function get rutaFotografia():String
		{
			return _rutaFotografia;
		}

		public function set rutaFotografia(value:String):void
		{
			_rutaFotografia = value;
		}

		[Bindable]
		public function get nombreSeccionEdificio():String
		{
			return _nombreSeccionEdificio;
		}

		public function set nombreSeccionEdificio(value:String):void
		{
			_nombreSeccionEdificio = value;
		}

		[Bindable]
		public function get cantidad_material():Number
		{
			return _cantidad_material;
		}

		public function set cantidad_material(value:Number):void
		{
			_cantidad_material = value;
		}

		[Bindable]
		public function get unidad_material():String
		{
			return _unidad_material;
		}

		public function set unidad_material(value:String):void
		{
			_unidad_material = value;
		}
		//Funciones UTIL
		public function get costoTotalMaterial():Number{
			return (_precioMaterial * _cantidad_material);
		}

		[Bindable]
		public function get eliminado():Boolean
		{
			return _eliminado;
		}

		public function set eliminado(value:Boolean):void
		{
			_eliminado = value;
		}
		 
		

	}
}
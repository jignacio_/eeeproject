package cl.foursoft.eee.comunication.objetos
{
	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.VistaParaGestionTO")]
	public class VistaParaGestionTO
	{
		
		private var _fechaInicio : Date;
		private var _fechaFin : Date;
		
		// Puede ser 1 o más.- podría ser un array
		
		
		private var _idTipoMedicion : String;
		private var _idConsumo : String; 
		private var _codigoSensor : String ;
		private var _edificio : String;
		private var _pisos : String;
		private var _zonas : String;
		private var _numeroSensor : String;
		
		// Puede ser 1 o más.-
		
		public function VistaParaGestionTO()
		{
		}
		
		
		
		[Bindable]
		public function get fechaInicio():Date
		{
			return _fechaInicio;
		}

		public function set fechaInicio(value:Date):void
		{
			_fechaInicio = value;
		}

		[Bindable]
		public function get fechaFin():Date
		{
			return _fechaFin;
		}

		public function set fechaFin(value:Date):void
		{
			_fechaFin = value;
		}

		[Bindable]
		public function get idTipoMedicion():String
		{
			return _idTipoMedicion;
		}

		public function set idTipoMedicion(value:String):void
		{
			_idTipoMedicion = value;
		}

		[Bindable]
		public function get idConsumo():String
		{
			return _idConsumo;
		}

		public function set idConsumo(value:String):void
		{
			_idConsumo = value;
		}

		[Bindable]
		public function get edificio():String
		{
			return _edificio;
		}

		public function set edificio(value:String):void
		{
			_edificio = value;
		}

		public function get pisos():String
		{
			return _pisos;
		}

		public function set pisos(value:String):void
		{
			_pisos = value;
		}

		[Bindable]
		public function get zonas():String
		{
			return _zonas;
		}

		public function set zonas(value:String):void
		{
			_zonas = value;
		}

		[Bindable]
		public function get numeroSensor():String
		{
			return _numeroSensor;
		}

		public function set numeroSensor(value:String):void
		{
			_numeroSensor = value;
		}

		[Bindable]
		public function get codigoSensor():String
		{
			return _codigoSensor;
		}

		public function set codigoSensor(value:String):void
		{
			_codigoSensor = value;
		}


	}
}
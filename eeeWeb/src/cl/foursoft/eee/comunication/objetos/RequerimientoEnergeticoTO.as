package cl.foursoft.eee.comunication.objetos
{
	import cl.foursoft.eee.actionscript.util.UtilCalculos;
	

	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.RequerimientoEnergeticoTO")]
	public class RequerimientoEnergeticoTO
	{
		
		private var _idEdificio : int;
		private var _idAlternativa : int;
		
		private var _perdidasPorTransmision: Number;
		private var _perdidasPorVentilacion: Number;
		private var _gananciaInterna: Number;
		
		private var _gananciaTermicaSolVerano: Number;
		private var _gananciaTermicaSolInvierno: Number;
		private var _gananciaTermicaSol : Number
		
		private var _requerimientoFrioTotal : Number;
		private var _requerimientoCalorTotal : Number;
		private var _requerimientoEnergeticoTotal : Number;
		
		public function RequerimientoEnergeticoTO()
		{
				
				this._idAlternativa = 0; //valor 0 para la situación actual
				this._idEdificio = -1; //valor 0 para la situación actual
				
		}

		[Bindable]
		public function get idEdificio():int
		{
			return _idEdificio;
		}

		public function set idEdificio(value:int):void
		{
			_idEdificio = value;
		}

		[Bindable]
		public function get idAlternativa():int
		{
			return _idAlternativa;
		}

		public function set idAlternativa(value:int):void
		{
			_idAlternativa = value;
		}

		[Bindable]
		public function get perdidasPorTransmision():Number
		{
			return  UtilCalculos.round(_perdidasPorTransmision);
		}

		public function set perdidasPorTransmision(value:Number):void
		{
			_perdidasPorTransmision = value;
		}

		[Bindable]
		public function get perdidasPorVentilacion():Number
		{
			return UtilCalculos.round(_perdidasPorVentilacion);
		}

		public function set perdidasPorVentilacion(value:Number):void
		{
			_perdidasPorVentilacion = value;
		}

		

		[Bindable]
		public function get gananciaInterna():Number
		{
			return UtilCalculos.round(_gananciaInterna);
		}

		public function set gananciaInterna(value:Number):void
		{
			_gananciaInterna = value;
		}

		[Bindable]
		public function get requerimientoCalorTotal():Number
		{
			return UtilCalculos.round(_requerimientoCalorTotal);
		}

		public function set requerimientoCalorTotal(value:Number):void
		{
			_requerimientoCalorTotal = value;
		}

		[Bindable]
		public function get requerimientoFrioTotal():Number
		{
			return UtilCalculos.round(_requerimientoFrioTotal);
		}

		public function set requerimientoFrioTotal(value:Number):void
		{
			_requerimientoFrioTotal = value;
		}

		[Bindable]
		public function get requerimientoEnergeticoTotal():Number
		{
			return UtilCalculos.round(_requerimientoEnergeticoTotal);
		}

		public function set requerimientoEnergeticoTotal(value:Number):void
		{
			_requerimientoEnergeticoTotal = value;
		}

		[Bindable]
		public function get gananciaTermicaSolVerano():Number
		{
			return UtilCalculos.round(_gananciaTermicaSolVerano);
		}

		public function set gananciaTermicaSolVerano(value:Number):void
		{
			_gananciaTermicaSolVerano = value;
		}

		[Bindable]
		public function get gananciaTermicaSolInvierno():Number
		{
			return UtilCalculos.round(_gananciaTermicaSolInvierno);
		}

		public function set gananciaTermicaSolInvierno(value:Number):void
		{
			_gananciaTermicaSolInvierno = value;
		}

		[Bindable]
		public function get gananciaTermicaSol():Number
		{
			return UtilCalculos.round(_gananciaTermicaSol);
		}

		public function set gananciaTermicaSol(value:Number):void
		{
			_gananciaTermicaSol = value;
		}


	}
}
package cl.foursoft.eee.comunication.objetos
{
	import mx.collections.ArrayCollection;

	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.ResumenInicialMedicionesTO")]
	public class ResumenInicialMedicionesTO
	{
		//RESUMEN INDIVIDUAL
		private var resumenUltimoDia : Number;
		private var resumenUltimoMes : Number
		
		private var resumenHoy : Number;
		
		//RESUMEN POR TIPO DE MEDICION Y TIPO DE CONSUMO
		private var nombreMedicion : String;
		private var id_tipo_medicion : String;
		private var resumenesPorConsumo : ArrayCollection;
		
		public function ResumenInicialMedicionesTO()
		{
		}
	}
}
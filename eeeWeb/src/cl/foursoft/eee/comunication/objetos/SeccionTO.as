package cl.foursoft.eee.comunication.objetos
{
	import cl.foursoft.eee.actionscript.enums.EnumSecciones;
	import cl.foursoft.eee.actionscript.util.UtilCalculos;
	
	import mx.collections.ArrayCollection;
	
	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.SeccionTO")]
	public class SeccionTO {
		
		

		private var _idSeccion : int;
		private var _idTipoSeccion : int;
		private var _idSeccionPadre : int;
		private var _nombreTipo : String;
		private var _nombre_seccion_padre : String;
 		
		private var _area : Number; 
		private var _transmitanciaTermica : Number;
		private var _resistenciaTotalMasRSERSI : Number;
		
		private var _resistenciaSeccion :Number;
	
		private var _composicion : ArrayCollection; //AC de MaterialTO
		private var _seccionesHijo : ArrayCollection;
		
		private  var _perdidaPorTransmision:Number;
		
		
		private var _RSI : Number;
		private var _RSE :Number;
		
		public function SeccionTO(nombreSeccion : String = "", idPadre:int = -1 )
		{
			
			this._idTipoSeccion = idPadre;
			this._idSeccionPadre  = idPadre ;
			this._nombreTipo = nombreSeccion;
			
			this._RSE = 0;
			this._RSI = 0; 
			this._resistenciaSeccion  = 0 ;
 
 			this._area = 0;
			this._resistenciaTotalMasRSERSI = 0;
			this._transmitanciaTermica = 0;
			this._composicion = new ArrayCollection();
			this._seccionesHijo = new ArrayCollection();
		}
	 

	 
	 
		[Bindable]
		public function get area():Number
		{
			
			if(_nombre_seccion_padre == EnumSecciones.SECCION_AREA_BASE){
				if(_seccionesHijo && _seccionesHijo.length > 0){
					_area = 0;
					for each( var _secc : SeccionTO in _seccionesHijo){
						if(_secc.nombreTipo == EnumSecciones.SECCION_AREA_BASE_PISO)
							_area = _secc._area;
					}
				}
			}
			
			
			else{
				if(_seccionesHijo && _seccionesHijo.length > 0){
					_area = 0;
					for each( var _s : SeccionTO in _seccionesHijo)
					_area += _s._area;
				}
			}
			
			
			
			
			return _area;
		}

		public function set area(value:Number):void
		{
			_area = value;
		}

		[Bindable]
		public function get composicion():ArrayCollection
		{
			return _composicion;
		}

		public function set composicion(value:ArrayCollection):void
		{
			_composicion = value;
		}

		[Bindable]
		public function get idTipoSeccion():int
		{
			return _idTipoSeccion;
		}

		public function set idTipoSeccion(value:int):void
		{
			_idTipoSeccion = value;
		}
		[Bindable]
		public function get idSeccionPadre():int
		{
			return _idSeccionPadre;
		}

		public function set idSeccionPadre(value:int):void
		{
			_idSeccionPadre = value;
		}

		[Bindable]
		public function get nombreTipo():String
		{
			return _nombreTipo;
		}

		public function set nombreTipo(value:String):void
		{
			_nombreTipo = value;
		}

		[Bindable]
		public function get nombre_seccion_padre():String
		{
			return _nombre_seccion_padre;
		}
		public function set nombre_seccion_padre(value:String):void
		{
			_nombre_seccion_padre = value;
		}

		[Bindable]
		public function get seccionesHijo():ArrayCollection
		{
			return _seccionesHijo;
		}

		public function set seccionesHijo(value:ArrayCollection):void
		{
			_seccionesHijo = value;
		}

		[Bindable]
		public function get transmitanciaTermica():Number
		{
			
			return UtilCalculos.round(_transmitanciaTermica) ;
		}

		public function set transmitanciaTermica(value:Number):void
		{
			
			_transmitanciaTermica = value;
		}

		[Bindable]
		public function get resistenciaTotalMasRSERSI():Number
		{
			
			
			return UtilCalculos.round(_resistenciaTotalMasRSERSI);
			//+ UsuarioTO.getInstance().edificioActual.RSE + UsuarioTO.getInstance().edificioActual.RSI;
		}

		public function set resistenciaTotalMasRSERSI(value:Number):void
		{
			_resistenciaTotalMasRSERSI = value ;
			transmitanciaTermica = (this.resistenciaTotalMasRSERSI>0 )?( 1 / this.resistenciaTotalMasRSERSI):0;
		}

		[Bindable]
		public function get idSeccion():int
		{
			
			return _idSeccion;
		}

		public function set idSeccion(value:int):void
		{
			_idSeccion = value;
		}

		[Bindable]
		public function get perdidaPorTransmision():Number
		{
			return _perdidaPorTransmision;
		}

		public function set perdidaPorTransmision(value:Number):void
		{
			_perdidaPorTransmision = value;
		}
		//Funciones UTIL
		public function get obtenerCostoTotalSeccion():Number {
			var resp : Number = 0;
			
			for each ( var m:MaterialTO in this._composicion ){
				resp += m.costoTotalMaterial;
			}
			
			return resp;
		}

		[Bindable]
		public function get RSI():Number
		{
			return _RSI;
		}

		public function set RSI(value:Number):void
		{
			_RSI = value;
			resistenciaTotalMasRSERSI = (_resistenciaSeccion>0 ) ?  _resistenciaSeccion + _RSI + _RSE : 0;
		}
		[Bindable]
		public function get RSE():Number
		{
			return _RSE;
		}

		public function set RSE(value:Number):void
		{
			_RSE = value;
			resistenciaTotalMasRSERSI = (_resistenciaSeccion>0 ) ?  _resistenciaSeccion + _RSI + _RSE : 0;

		}

		[Bindable]
		public function get resistenciaSeccion():Number
		{
			return _resistenciaSeccion;
		}

		public function set resistenciaSeccion(value:Number):void
		{
			_resistenciaSeccion = value;
			
			resistenciaTotalMasRSERSI = (_resistenciaSeccion>0 ) ?  _resistenciaSeccion + _RSI + _RSE : 0;

		}
 
		public function toString():String{
			return this._nombreTipo;
		}

	}
}
package cl.foursoft.eee.comunication.objetos
{
	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.TestTO")]
	public class TestTO
	{
		private var _id:int;
		private var _mensaje:String;
		
		public function TestTO()
		{
			this._id = -1;
			this._mensaje = "";
		}

		public function get id():int
		{
			return _id;
		}

		public function set id(value:int):void
		{
			_id = value;
		}
		
		[Bindable]
		public function get mensaje():String
		{
			return _mensaje;
		}

		public function set mensaje(value:String):void
		{
			_mensaje = value;
		}


	}
}
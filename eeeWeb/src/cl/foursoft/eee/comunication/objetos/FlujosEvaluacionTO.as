package cl.foursoft.eee.comunication.objetos
{
	
	
	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.FlujosEvaluacionTO")]
	public class FlujosEvaluacionTO
	{
		
		private var _anio : int;
		private var _flujosNeto : Number;
		private var _flujosAcumulado : Number;
		private var _variacionPrecio : Number;
		
		
		
		
		 
		[Bindable]
		public function get anio():int
		{
			return _anio;
		}

		public function set anio(value:int):void
		{
			_anio = value;
		}

		[Bindable]
		public function get flujosNeto():Number
		{
			return _flujosNeto;
		}

		public function set flujosNeto(value:Number):void
		{
			_flujosNeto = value;
		}

		[Bindable]
		public function get flujosAcumulado():Number
		{
			return _flujosAcumulado;
		}

		public function set flujosAcumulado(value:Number):void
		{
			_flujosAcumulado = value;
		}

		[Bindable]
		public function get variacionPrecio():Number
		{
			return _variacionPrecio;
		}

		public function set variacionPrecio(value:Number):void
		{
			_variacionPrecio = value;
		}


	}
}
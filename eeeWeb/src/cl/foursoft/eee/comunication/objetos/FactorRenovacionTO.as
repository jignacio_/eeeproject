package cl.foursoft.eee.comunication.objetos
{
	[Bindable]
	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.FactorRenovacionTO")]
	public class FactorRenovacionTO
	{
		
		private var _idFactor : int;
		private var _nombreFactor : String;
		private var _cambioAireMinimo : Number;
		private var _cambioAireMaximo : Number;
		private var _discriminador : String; //Unidad{camnbiar}
		
		public function FactorRenovacionTO()
		{
			this._idFactor = -1;
			this._nombreFactor = "";
			this._cambioAireMaximo = 0;
			this._cambioAireMinimo = 0;
			this._discriminador = "";
		}

		public function get idFactor():int
		{
			return _idFactor;
		}

		public function set idFactor(value:int):void
		{
			_idFactor = value;
		}

		public function get nombreFactor():String
		{
			return _nombreFactor;
		}

		public function set nombreFactor(value:String):void
		{
			_nombreFactor = value;
		}

		public function get cambioAireMinimo():Number
		{
			return round( _cambioAireMinimo);
		}

		public function set cambioAireMinimo(value:Number):void
		{
			_cambioAireMinimo = value;
		}
		[Bindable]
		public function get cambioAireMaximo():Number
		{
			return round( _cambioAireMaximo );
		}

		public function set cambioAireMaximo(value:Number):void
		{
			_cambioAireMaximo = value;
		}

		public function get discriminador():String
		{
			return _discriminador;
		}

		public function set discriminador(value:String):void
		{
			_discriminador = value;
		}
		public function toString():String
		{
			// TODO Auto Generated method stub
			return this._nombreFactor;
		}
		
		public function get detalleFactor():String{
			return(
					this._cambioAireMinimo +" - "+this._cambioAireMaximo +" "+ this._discriminador
			);
		}
 		
		private static function round(i:Number):Number{
			var n:Number = (Math.round( (i)*1000 )) /1000;
			if(!isNaN(n)) return n;
			return NaN;
		}

	}
}
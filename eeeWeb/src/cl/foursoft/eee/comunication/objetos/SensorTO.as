package cl.foursoft.eee.comunication.objetos
{
	import cl.foursoft.eee.actionscript.interfaces.ISeleccionVistasParaGestion;
	import cl.foursoft.eee.actionscript.util.Nodo;
	import cl.foursoft.eee.actionscript.util.UtilCalculos;
	
	import mx.collections.ArrayCollection;
	

	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.SensorTO")]
	public class SensorTO extends Nodo implements ISeleccionVistasParaGestion
	{
		public static const ESTADO_OK :String = "ESTADO_OK";
		public static const ESTADO_ERROR :String = "ESTADO_ERROR";
		
		private var _tipoSeleccionado : Boolean;
		
		private var _idSensor : int ; 
		private var _codigoSensor : String ;
		private var _nombreSensor:String;
		private var _nombreTipoMedicion : String;
		private var _codigoTipoMedicion : String;
		private var _nombreTipoConsumo : String;
		private var _codigoTipoConsumo : String;
		private var _numeroSensorActual:int;
		private var _idCliente : int; 
		private var _idEdificio : int;
		private var _idPiso : int;
		private var _idZona : int;
		
		private var _artefactosMedidios : ArrayCollection;
		private var _usoHorasDiarias : Number;
		private var _usoDiasMensuales: Number;
		private var _consumoTotalEstimado : Number;
		private var _descripcionArtefactos:String;
		private var _unidadMedicionSensor : String;
		
		
		private var _guardado : Boolean;
		
		//ELEMENTOS PARA EL ESTADO DEL SENSOR
		private var _estadoSensor : String; 
		
		public function SensorTO(_numeroSensor : int = 0):void
		{
			this._guardado = false;
			this._codigoSensor = "";
			this._nombreSensor = "";
			this._numeroSensorActual = _numeroSensor;
			this._idSensor=-1;
			this._nombreTipoConsumo ="";
			this._codigoTipoConsumo = "";
			this._nombreTipoMedicion = "";
			this._codigoTipoMedicion = "";
			this._idEdificio = -1;
			this._idPiso = -1;
			this._idZona = -1;
			this._artefactosMedidios = new ArrayCollection(); 
			this._estadoSensor = SensorTO.ESTADO_OK;
			
			this._usoDiasMensuales = 20;
			this._usoHorasDiarias = 8; 
			this._consumoTotalEstimado = 0;
			this._descripcionArtefactos = "";
			this._unidadMedicionSensor = "";
		}

		[Bindable]
		public function get nombreTipoConsumo():String
		{
			return _nombreTipoConsumo;
		}

		public function set nombreTipoConsumo(value:String):void
		{
			_nombreTipoConsumo = value;
		}

		[Bindable]
		public function get codigoTipoConsumo():String
		{
			return _codigoTipoConsumo;
		}

		public function set codigoTipoConsumo(value:String):void
		{
			_codigoTipoConsumo = value;
		}

		[Bindable]
		public function get idEdificio():int
		{
			return _idEdificio;
		}

		public function set idEdificio(value:int):void
		{
			_idEdificio = value;
		}

		[Bindable]
		public function get idPiso():int
		{
			return _idPiso;
		}

		public function set idPiso(value:int):void
		{
			_idPiso = value;
		}

		[Bindable]
		public function get idZona():int
		{
			return _idZona;
		}

		public function set idZona(value:int):void
		{
			_idZona = value;
		}

		[Bindable]
		public function get idSensor():int
		{
			return _idSensor;
		}

		public function set idSensor(value:int):void
		{
			_idSensor = value;
		}

		[Bindable]
		public function get numeroSensorActual():int
		{
			return _numeroSensorActual;
		}

		public function set numeroSensorActual(value:int):void
		{
			_numeroSensorActual = value;
		}

		[Bindable]
		public function get nombreTipoMedicion():String
		{
			return _nombreTipoMedicion;
		}

		public function set nombreTipoMedicion(value:String):void
		{
			_nombreTipoMedicion = value;
		}

		[Bindable]
		public function get codigoTipoMedicion():String
		{
			return _codigoTipoMedicion;
		}

		public function set codigoTipoMedicion(value:String):void
		{
			_codigoTipoMedicion = value;
		}

		[Bindable]
		public function get nombreSensor():String
		{
			return _nombreSensor;
		}

		public function set nombreSensor(value:String):void
		{
			_nombreSensor = value;
		}

		[Bindable]
		public function get codigoSensor():String
		{
			return _codigoSensor;
		}

		public function set codigoSensor(value:String):void
		{
			_codigoSensor = value;
		}

		[Bindable]
		public function get idCliente():int
		{
			return _idCliente;
		}

		public function set idCliente(value:int):void
		{
			_idCliente = value;
		}
		public function toString():String{
			return  this._codigoSensor;
		}

		public function get guardado():Boolean
		{
			return _guardado;
		}
	
		public function set guardado(value:Boolean):void
		{
			_guardado = value;
		}
		
		public function get nombreElemento():String
		{
			// TODO Auto Generated method stub
			return toString();
		}
		
		public function set tipoSeleccionado(value:Boolean):void
		{
			this._tipoSeleccionado = value;
			
		}
		
		public function get tipoSeleccionado():Boolean
		{
			// TODO Auto Generated method stub
			return this._tipoSeleccionado;
		}

		[Bindable]
		public function get artefactosMedidios():ArrayCollection
		{
			return _artefactosMedidios;
		}

		public function set artefactosMedidios(value:ArrayCollection):void
		{
			_artefactosMedidios = value;
		}

		[Bindable]
		public function get estadoSensor():String
		{
			return _estadoSensor;
		}

		public function set estadoSensor(value:String):void
		{
			_estadoSensor = value;
		}

		[Bindable]
		public function get usoHorasDiarias():Number
		{
			return _usoHorasDiarias;
		}

		public function set usoHorasDiarias(value:Number):void
		{
			_usoHorasDiarias = value;
		}

		[Bindable]
		public function get usoDiasMensuales():Number
		{
			return _usoDiasMensuales;
		}

		public function set usoDiasMensuales(value:Number):void
		{
			_usoDiasMensuales = value;
		}

		[Bindable]
		public function get consumoTotalEstimado():Number
		{
			 
			
			return _consumoTotalEstimado;
		}

		public function set consumoTotalEstimado(value:Number):void
		{
			_consumoTotalEstimado = value;
		}

		[Bindable]
		public function get descripcionArtefactos():String
		{
			return _descripcionArtefactos;
		}

		public function set descripcionArtefactos(value:String):void
		{
			_descripcionArtefactos = value;
		}

		[Bindable]
		public function get unidadMedicionSensor():String
		{
			return _unidadMedicionSensor;
		}

		public function set unidadMedicionSensor(value:String):void
		{
			_unidadMedicionSensor = value;
		}
		

	}
}
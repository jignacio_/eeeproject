package cl.foursoft.eee.comunication.objetos
{
	

	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.ParametrosEdificioTO")]
	public class ParametrosEdificioTO
	{
		
		private var _idParametro : int;
		private var _nombreParametro : String;
		private var _simboloParametro : String;
		private var _unidadParametro : String;
		private var _valorParametro : Number;
		
		

		
		public function ParametrosEdificioTO( valor : int = 0, nombre_parametro : String = "")
		{
			
			this._idParametro = -1;
			this._nombreParametro = nombre_parametro;
			this._simboloParametro = "";
			this._unidadParametro = "";
			this._valorParametro = valor;
			
		}
 
		  
		[Bindable]
		public function get idParametro():int
		{
			return _idParametro;
		}

		public function set idParametro(value:int):void
		{
			_idParametro = value;
		}

		[Bindable]
		public function get nombreParametro():String
		{
			return _nombreParametro;
		}

		public function set nombreParametro(value:String):void
		{
			_nombreParametro = value;
		}

		[Bindable]
		public function get simboloParametro():String
		{
			return _simboloParametro;
		}

		public function set simboloParametro(value:String):void
		{
			_simboloParametro = value;
		}

		[Bindable]
		public function get unidadParametro():String
		{
			return _unidadParametro;
		}

		public function set unidadParametro(value:String):void
		{
			_unidadParametro = value;
		}

		[Bindable]
		public function get valorParametro():Number
		{
			return round( _valorParametro );
		}
		 
 
		
		public function toString():String
		{
			return this._nombreParametro;
		}
		
		
		public function set valorParametro(value:Number):void
		{
			_valorParametro = value;
		}
		
		private static function round(i:Number):Number{
			return (
				(Math.round( (i)*1000 )) /1000
			);
		}

	}
}
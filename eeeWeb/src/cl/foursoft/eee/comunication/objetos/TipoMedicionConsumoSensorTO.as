package cl.foursoft.eee.comunication.objetos
{
	
	/*
	 * CLASE PARA ADMINSTRAR RECURSIVAMENTE TIPOS DE MEDICIOIN - TIPOS DE CONSUMO
	 */
	import cl.foursoft.eee.actionscript.interfaces.ISeleccionVistasParaGestion;
	
	import mx.collections.ArrayCollection;
[Bindable]
	[RemoteClass(alias="cl.foursoft.eee.dao.transferObject.TipoMedicionConsumoSensorTO")]
	public class TipoMedicionConsumoSensorTO implements ISeleccionVistasParaGestion
	{
	
		private var _eliminado : Boolean;
		private var _idTipoSensor : int;
		private var _nombreTipoMedicion : String;
		private var _codigoTipoMedicion : String;
		private var _descripcionTipoConsumo : String;
		private var _arrColTipoConsumo : ArrayCollection; 
		private var _unidadMedicion : String;
		
		//DATOS PARA VISTAS DE GESTION
		private var _tipoSeleccionado : Boolean = false;
	
		public function TipoMedicionConsumoSensorTO(nombre:String = "" , codigo:String = "")
		{
			this._idTipoSensor = -1;
			this._nombreTipoMedicion = nombre;
			this._codigoTipoMedicion = codigo;
			this._descripcionTipoConsumo = "";
			this._unidadMedicion = "";
			this._arrColTipoConsumo  = new ArrayCollection();
			this._eliminado = false;
		}
		
		
		[Bindable]
		public function get nombreTipoMedicion():String
		{
			return _nombreTipoMedicion;
		}

		public function set nombreTipoMedicion(value:String):void
		{
			_nombreTipoMedicion = value;
		}

		[Bindable]
		public function get codigoTipoMedicion():String
		{
			return _codigoTipoMedicion;
		}

		public function set codigoTipoMedicion(value:String):void
		{
			_codigoTipoMedicion = value;
		}

		[Bindable]
		public function get arrColTipoConsumo():ArrayCollection
		{
			return _arrColTipoConsumo;
		}

		public function set arrColTipoConsumo(value:ArrayCollection):void
		{
			_arrColTipoConsumo = value;
		}

		[Bindable]
		public function get descripcionTipoConsumo():String
		{
			return _descripcionTipoConsumo;
		}

		public function set descripcionTipoConsumo(value:String):void
		{
			_descripcionTipoConsumo = value;
		}
		
		public function toString():String
		{ 
			return this._nombreTipoMedicion;
		}

	/*	[Bindable]
		public function get tipoSeleccionado():Boolean
		{
			return _tipoSeleccionado;
		}

		public function set tipoSeleccionado(value:Boolean):void
		{
			_tipoSeleccionado = value;
		}
*/
		[Bindable]
		public function get idTipoSensor():int
		{
			return _idTipoSensor;
		}

		public function set idTipoSensor(value:int):void
		{
			_idTipoSensor = value;
		}

		[Bindable]
		public function get eliminado():Boolean
		{
			return _eliminado;
		}

		public function set eliminado(value:Boolean):void
		{
			_eliminado = value;
		}

		[Bindable]
		public function get unidadMedicion():String
		{
			return _unidadMedicion;
		}

		public function set unidadMedicion(value:String):void
		{
			_unidadMedicion = value;
		}
		
		public function get nombreElemento():String
		{
			return this._nombreTipoMedicion;
		}
		
		public function set tipoSeleccionado(value:Boolean):void
		{
			this._tipoSeleccionado = value;
			
		}
		
		public function get tipoSeleccionado():Boolean
		{
			// TODO Auto Generated method stub
			return this._tipoSeleccionado;
		}
		
		
		
		//Interfaces
		
		
		/*public function toString():String{
			return (this._codigoTipoMedicion+ " " +this._nombreTipoMedicion)
		}*/


	}
}
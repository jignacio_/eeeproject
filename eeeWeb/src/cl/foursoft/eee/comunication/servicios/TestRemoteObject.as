package cl.foursoft.eee.comunication.servicios
{
	
	import cl.foursoft.eee.comunication.comunes.ChannelManager;
	import cl.foursoft.eee.comunication.objetos.TestTO;
	import cl.foursoft.eee.comunication.objetos.UsuarioTO;
	
	import mx.controls.Alert;
	import mx.messaging.ChannelSet;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.remoting.mxml.RemoteObject;

	public class TestRemoteObject extends RemoteObject
	{
		public function TestRemoteObject(destination:String = null)
		{ 
			super(destination);
			this.destination = "TestService";
			this.showBusyCursor = true;
			this.channelSet = ChannelManager.getChannelSet;
			this.addEventListener(FaultEvent.FAULT, doFault);
		}
		
		protected function doFault(resultFault:FaultEvent):void{
			//Alerta.error("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");			
			if(resultFault.fault.faultDetail != null)
             	Alert.show("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");
        	else
            	Alert.show(resultFault.fault.rootCause["message"]);
        }
        
        public function getTest(id:int):TestTO
        {
        	this.getOperation("getTest").send(id);
			return null;
        }
		public function getUsuario(_usuario:String, _contrasenia:String):UsuarioTO
		{
			this.getOperation("getUsuario").send(_usuario, _contrasenia);
			return null;
		}
		
	}
}
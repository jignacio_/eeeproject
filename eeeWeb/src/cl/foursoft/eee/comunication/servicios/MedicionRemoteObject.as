package cl.foursoft.eee.comunication.servicios
{
	import cl.foursoft.eee.comunication.comunes.ChannelManager;
	import cl.foursoft.eee.comunication.objetos.VistaParaGestionTO;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	public class MedicionRemoteObject extends RemoteObject
	{
		public function MedicionRemoteObject(destination:String=null)
		{
			super(destination);
			this.destination = "MedicionService";
			this.channelSet = ChannelManager.getChannelSet;
			this.addEventListener(FaultEvent.FAULT, doFault);
		}
		
		protected function doFault(resultFault:FaultEvent):void{
			//Alerta.error("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");			
			if(resultFault.fault.faultDetail != null)
				Alert.show("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");
			else
				Alert.show(resultFault.fault.rootCause["message"]);
		}
		
		// VISTAS PARA GESTION
		public function obtenerMedicionEdificioTipoMedicion(idEdificio:int,  idMedicion:String):ArrayCollection{
			this.getOperation("obtenerMedicionEdificioTipoConsumo").send(idEdificio,idMedicion);
			return null;
		}
		
		public function obtenerDatosRango(vpg : VistaParaGestionTO):ArrayCollection{
			this.getOperation("obtenerDatosRango").send(vpg);
			return null;
		}
		public function totalConsumosMes(idMedicion:String, idEdificio : int):ArrayCollection{
			this.getOperation("totalConsumosMes").send(idMedicion , idEdificio);
			return null;
		}
		
		public function obtenerAlertas(idEdificio:int):ArrayCollection{
			this.getOperation("obtenerAlertas").send(idEdificio);
			return null;
		}
		// VISTAS PARA DETALLE
		public function obtenerDatosMedicionHoy(idMedicion : String, idConsumo : String,idEdificio : int, idPiso : int = 0, idZona:int = 0, idSensor : String = "" , minutos:int = 5):ArrayCollection{//Edificio
			this.getOperation("obtenerDatosMedicionHoy").send(idMedicion, idConsumo,  idEdificio, idPiso, idZona,idSensor, minutos	);
			return null;
		}
		public function totalConsumosDia(idMedicion : String , idEdificio : int = 0, idPiso:int = 0 , idZona:int = 0, idSenor : String = ""):ArrayCollection{
			this.getOperation("totalConsumos").send(idMedicion , idEdificio, idPiso, idZona , idSenor, "dia");
			return null;
		}
										  
		
 
	
		
	
	}
}
package cl.foursoft.eee.comunication.servicios
{
	import cl.foursoft.eee.comunication.comunes.ChannelManager;
	import cl.foursoft.eee.comunication.objetos.EdificioTO;
	import cl.foursoft.eee.comunication.objetos.ParametrosEvaluacionTO;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	public class ParametrosEdificioRemoteObject extends RemoteObject
	{
		public function ParametrosEdificioRemoteObject(destination:String=null)
		{
			super(destination);
			this.destination = "ParametrosEdificioService";
			this.channelSet = ChannelManager.getChannelSet;
			this.addEventListener(FaultEvent.FAULT, doFault);
		}
		protected function doFault(resultFault:FaultEvent):void{
			if(resultFault.fault.faultDetail != null)
				Alert.show("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");
			else
				Alert.show(resultFault.fault.rootCause["message"]);		
		}
		
		public function obtenerParametros():ArrayCollection{			
			this.getOperation("obtenerParametros").send();
			return null;
		}
		
		public function editarParametros(arrayParametros:ArrayCollection):void{
			this.getOperation("editarParametros").send(arrayParametros);
		}

	}
}
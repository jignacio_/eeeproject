package cl.foursoft.eee.comunication.servicios
{
	import cl.foursoft.eee.comunication.objetos.RequerimientoEnergeticoTO;
	
	import mx.rpc.remoting.RemoteObject;

	public class RequerimientoEnergeticoServiceRemoteObject extends RemoteObject
	{
		import cl.foursoft.eee.comunication.comunes.ChannelManager;
		import cl.foursoft.eee.comunication.objetos.EdificioTO;
		import cl.foursoft.eee.comunication.objetos.ParametrosEvaluacionTO;
		
		import mx.collections.ArrayCollection;
		import mx.controls.Alert;
		import mx.rpc.events.FaultEvent;
		import mx.rpc.remoting.RemoteObject;
		
		public function RequerimientoEnergeticoServiceRemoteObject(destination : String = null)
		{	super(destination);	
			this.destination = "RequerimientoEnergeticoService";
			this.channelSet = ChannelManager.getChannelSet;
			this.addEventListener(FaultEvent.FAULT, doFault);
			
		}
		
		
		protected function doFault(resultFault:FaultEvent):void{
			//Alerta.error("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");			
			if(resultFault.fault.faultDetail != null)
				Alert.show("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");
			else
				Alert.show(resultFault.fault.rootCause["message"]);
			
			
		}
		
		public function guardarRequerimientoEnergeticoEdificio(idEdificio : Number):RequerimientoEnergeticoTO{
			getOperation("guardarRequerimientoEnergetico").send(idEdificio);
			return null;
		}
		public function guardarRequerimientoEnergeticoAlternativa(idEdificio : Number, idAlternativa : Number):RequerimientoEnergeticoTO{
			getOperation("guardarRequerimientoEnergeticoAlternativa").send(idEdificio, idAlternativa);
			return null;
		}
		
		public function obtenerRequerimientoEnergeticoEdificio(idEdificio : Number):RequerimientoEnergeticoTO{
			getOperation("obtenerRequerimientoEnergeticoEdificio").send(idEdificio);
			return null;
		}
		public function obtenerValorRequerimientoTotal(idEdificio : Number):RequerimientoEnergeticoTO{
			getOperation("obtenerValorRequerimientoTotal").send(idEdificio);
			return null;
		}
	}
}
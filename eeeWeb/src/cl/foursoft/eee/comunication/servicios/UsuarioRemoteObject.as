package cl.foursoft.eee.comunication.servicios
{
	 
	import cl.foursoft.eee.comunication.comunes.ChannelManager;
	import cl.foursoft.eee.comunication.objetos.ClienteTO;
	import cl.foursoft.eee.comunication.objetos.EdificioTO;
	import cl.foursoft.eee.comunication.objetos.UsuarioTO;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.remoting.RemoteObject;

	public class UsuarioRemoteObject extends RemoteObject
	{
		public function UsuarioRemoteObject(destination:String=null) 
		{
			super(destination);
			this.destination = "UsuarioService";
			this.channelSet = ChannelManager.getChannelSet;
			this.addEventListener(FaultEvent.FAULT, doFault);
			
		}
		
		protected function doFault(resultFault:FaultEvent):void{
			//Alerta.error("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");			
			if(resultFault.fault.faultDetail != null)
				Alert.show("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");
			else
				Alert.show(resultFault.fault.rootCause["message"]);
			
		
		}
		public function obtenerCliente():ClienteTO{
			this.getOperation("obtenerCliente").send();
			return null;
		}
		public function obtenerUsuario(_usuario:String, _contrasenia:String):UsuarioTO{
			this.getOperation("obtenerUsuario").send(_usuario, _contrasenia);
			return null;
		}
		public function reenviarDatos(_usuario : UsuarioTO):Boolean{
			this.getOperation("reenviarDatos").send(_usuario);
			return null;
		}
		
		/*FUNCION PARA USUARIO KIPUS*/
		public function obtenerUsuarioKipus(_usuario:String, _contrasenia:String):UsuarioTO{
			
			this.getOperation("obtenerUsuarioKipus").send(_usuario, _contrasenia);
			return null;
		}
		
		public function obtenerUsuarios():ArrayCollection{
			this.getOperation("obtenerUsuarios").send();
			return null;
		}
		
		public function guardarUsuarios(arrayUsuarios:ArrayCollection):void{
			this.getOperation("guardarUsuarios").send(arrayUsuarios);
		}
		
		public function eliminarUsuario(usuario:UsuarioTO):void{
			this.getOperation("eliminarUsuario").send(usuario);
		}
	}
}
package cl.foursoft.eee.comunication.servicios
{
	import cl.foursoft.eee.comunication.comunes.ChannelManager;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	public class ClientesRemoteObject extends RemoteObject
	{
		public function ClientesRemoteObject(destination:String=null)
		{
			super(destination);
			this.destination = "ClientesService";
			this.channelSet = ChannelManager.getChannelSet;
			this.addEventListener(FaultEvent.FAULT, doFault);
			
		}
		
		protected function doFault(resultFault:FaultEvent ):void{
			//Alerta.error("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");			
			if(resultFault.fault.faultDetail != null)
				Alert.show("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");
			else
				Alert.show(resultFault.fault.rootCause["message"]);
			
			
		}
		
		public function obtenerClientes():ArrayCollection{
			getOperation("obtenerTodosLosClientes").send();
			return null;
		}
	}
}
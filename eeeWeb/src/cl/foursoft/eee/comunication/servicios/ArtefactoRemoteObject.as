package cl.foursoft.eee.comunication.servicios
{
	import cl.foursoft.eee.comunication.comunes.ChannelManager;
	import cl.foursoft.eee.comunication.objetos.ArtefactoCombustibleDetalleTO;
	import cl.foursoft.eee.comunication.objetos.ArtefactoTO;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	public class ArtefactoRemoteObject extends RemoteObject
	{
		public function ArtefactoRemoteObject(destination:String=null)
		{
			super(destination);	
			this.destination = "ArtefactoService";
			this.channelSet = ChannelManager.getChannelSet;
			this.addEventListener(FaultEvent.FAULT, doFault);
			
		}
		
		protected function doFault(resultFault:FaultEvent):void{
			//Alerta.error("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");			
			if(resultFault.fault.faultDetail != null)
				Alert.show("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");
			else
				Alert.show(resultFault.fault.rootCause["message"]);
		}
		
		public function obtenerArtefactosPorMedicion(idMedicion : String) : ArrayCollection{
			this.getOperation("obtenerArtefactosPorMedicion").send(idMedicion);
			return null;
		}
		public function obtenerTodosLosArtefactos() : ArrayCollection{
			this.getOperation("obtenerTodosLosArtefactos").send(); // crear medicion
			return null;
		}
		
		public function obtenerArtefactoCombustibleCompleto(idArtefacto:int):ArtefactoCombustibleDetalleTO{
			
			this.getOperation("obtenerArtefactoCombustibleCompleto").send(idArtefacto); // crear medicion
			return null;
			
		}
		public function guardarComposicion(_composionArtefactos : ArrayCollection):Boolean{
			this.getOperation("guardarComposicion").send(_composionArtefactos);
			return false;
		}
		public function actualizarComposicion(_composionArtefactos : ArrayCollection):Boolean{
			this.getOperation("actualizarComposicion").send(_composionArtefactos);
			return false;
		}
		public function guardarArtefactos(_artefactos : ArtefactoTO) : int{
			this.getOperation("guardarArtefacto").send(_artefactos);
			return -1;
		}
		public function eliminarArtefacto(_idArtefacto : int) : Boolean{
			this.getOperation("eliminarArtefacto").send(_idArtefacto);
			return false;
			
		}
		public function guardarDetalleCombustible(_artefactos : ArtefactoCombustibleDetalleTO) : int{
			this.getOperation("guardarArtefactoCombustibleDetalle").send(_artefactos);
			return -1;
		}
	}
}
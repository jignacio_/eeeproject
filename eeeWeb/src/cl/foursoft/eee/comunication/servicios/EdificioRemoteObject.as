package cl.foursoft.eee.comunication.servicios
{
	import cl.foursoft.eee.comunication.comunes.ChannelManager;
	import cl.foursoft.eee.comunication.objetos.EdificioTO;
	import cl.foursoft.eee.comunication.objetos.ParametrosEvaluacionTO;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	public class EdificioRemoteObject extends RemoteObject
	{
		public function EdificioRemoteObject(destination:String=null)
		{
			super(destination);
			this.destination = "EdificioService";
			this.channelSet = ChannelManager.getChannelSet;
			this.addEventListener(FaultEvent.FAULT, doFault);
		}
		protected function doFault(resultFault:FaultEvent):void{
			//Alerta.error("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");			
			if(resultFault.fault.faultDetail != null)
				Alert.show("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");
			else
				Alert.show(resultFault.fault.rootCause["message"]);
			
			
		}
		public function guardarEdificio(_edificio:EdificioTO):int{
			this.getOperation("guardarEdificio").send(_edificio);
			return -1;
			
		}
		public function obtenerTodosLosEdificios(_idCliente:int):ArrayCollection{
			this.getOperation("obtenerTodosLosEdificios").send(_idCliente);
			return null;
		}
		public function obtenerEdificioPorId(idEdificio:int):EdificioTO{
			this.getOperation("obtenerEdificioPorId").send(idEdificio);
			return null;
		}
		public function obtenerEdificioVistaDetalle(idEdificio:int):EdificioTO{
			this.getOperation("obtenerEdificioVistaDetalle").send(idEdificio);
			return null;
		}
		public function eliminarEdificio(_idEdificio : int):int{
			this.getOperation("eliminarEdificio").send(_idEdificio);
			return -1;
		}
		
		//Cambiar
		public function guardarParametros(_parametros : ParametrosEvaluacionTO):int{
			this.getOperation("guardarParametros").send(_parametros);
			return -1;
		}
		
		public function obtenerParametrosCliente(idCliente : int):ArrayCollection{
			
			this.getOperation("obtenerParametrosCliente").send(idCliente);
			return null;
		
		}
		
	/*	public function obtenerTodosLosParametros(idCliente : int):ArrayCollection{
			
			this.getOperation("obtenerTodosLosParametros").send();
			return null;
		}*/
		public function eliminarParametroCliente(idParametro : int, idCliente: int):int{
			
			this.getOperation("eliminarParametroCliente").send(idParametro,idCliente);
			return -1;
		}
		public function guardarInversioEdificio(idEdificio : int) :int{
			this.getOperation("guardarCalculoInversionEdificio").send(idEdificio);
			return -1;
		}
		public function obtenerTodosLosEdificiosCompletos(idCliente:int):ArrayCollection{
			this.getOperation("obtenerTodosLosEdificiosCompletos").send(idCliente);
			return null;
		}

		
		//ACtualizar
		public function actualizarEdificio(_edificio:EdificioTO):int{
			this.getOperation("actualizarEdificio").send(_edificio);
			return -1;
			
		}
		
	}
}
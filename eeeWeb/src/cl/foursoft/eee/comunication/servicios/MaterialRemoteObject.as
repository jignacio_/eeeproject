package cl.foursoft.eee.comunication.servicios
{
	import cl.foursoft.eee.comunication.comunes.ChannelManager;
	import cl.foursoft.eee.comunication.objetos.MaterialTO;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.remoting.RemoteObject;

	public class MaterialRemoteObject extends RemoteObject
	{
		public function MaterialRemoteObject(destination : String = null)
		{
			super(destination);	
			this.destination = "MaterialesService";
			this.channelSet = ChannelManager.getChannelSet;
			this.addEventListener(FaultEvent.FAULT, doFault);
			
		}
		
		
		protected function doFault(resultFault:FaultEvent):void{
			//Alerta.error("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");			
			if(resultFault.fault.faultDetail != null)
				Alert.show("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");
			else
				Alert.show(resultFault.fault.rootCause["message"]);
			
			
		}
		public function obtenerTodosLosMateriales():ArrayList{
			this.getOperation("obtenerTodosLosMateriales").send();
			return null;
		}
		
		public function obtenerMaterialesPorTipoSeccion(_idSeccion : int):ArrayList{
			this.getOperation("obtenerMaterialesPorTipoSeccion").send(_idSeccion);
			return null;
		}
		
		
		public function guardarMaterial(materiales:ArrayCollection):int{
			this.getOperation("guardarMaterial").send(materiales);
			return -1;
		}
		
		public function eliminarMaterial(idMaterial:int):int{
			this.getOperation("eliminarMaterial").send(idMaterial);
			return -1;
		}
	}
}
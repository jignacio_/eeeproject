package cl.foursoft.eee.comunication.servicios
{
	import cl.foursoft.eee.comunication.comunes.ChannelManager;
	import cl.foursoft.eee.comunication.objetos.AlternativaDeInversionTO;
	import cl.foursoft.eee.comunication.objetos.EvaluacionEconomicaTO;
	import cl.foursoft.eee.comunication.objetos.ParametrosEvaluacionTO;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	public class EvaluacionEconomicaRemoteObject extends RemoteObject
	{
		public function EvaluacionEconomicaRemoteObject(destination:String=null)
		{
			super(destination);
			this.destination = "EvaluacionEconomicaService";
			this.channelSet = ChannelManager.getChannelSet;
			this.addEventListener(FaultEvent.FAULT, doFault);
		}
		
		protected function doFault(resultFault:FaultEvent):void{
			//Alerta.error("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");			
			if(resultFault.fault.faultDetail != null)
				Alert.show("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");
			else
				Alert.show(resultFault.fault.rootCause["message"]);
			
		}
		
		public function crearEvaluacionEconomica(idEdificio : int, 
												 parametro : ParametrosEvaluacionTO , 
												 alternativa : AlternativaDeInversionTO):EvaluacionEconomicaTO{
			
			
			this.getOperation("crearEvaluacionEconomica").send(idEdificio, parametro, alternativa);
			return null;
		}
		public function obtenerEvaluacionesEdificioPorParametro(idEdificio : int, idParametro : int):ArrayCollection{
			
			this.getOperation("obtenerEvaluacionesEdificioPorParametro").send(idEdificio, idParametro);
			return null
			
		}
		public function obtenerEvaluacionesPorEdificio(idEdificio:int):ArrayCollection{
			this.getOperation("obtenerEvaluacionesEdificio").send(idEdificio);
			return null;
		}
		public function obtenerFlujos(parametro : ParametrosEvaluacionTO ,  alternativa : AlternativaDeInversionTO, idEdificio : int):ArrayCollection{
			this.getOperation("obtenerFlujos").send(parametro, alternativa, idEdificio);
			return null
		}
	}
}
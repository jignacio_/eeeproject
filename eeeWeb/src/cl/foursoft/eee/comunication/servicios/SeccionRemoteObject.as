package cl.foursoft.eee.comunication.servicios
{
	import cl.foursoft.eee.comunication.comunes.ChannelManager;
	import cl.foursoft.eee.comunication.objetos.EnvolventeTO;
	
	import mx.collections.ArrayList;
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.remoting.RemoteObject;

	public class SeccionRemoteObject extends RemoteObject
	{
		public function SeccionRemoteObject(destination : String = null)
		{
			super(destination);
			this.destination = "SeccionService";
			this.channelSet = ChannelManager.getChannelSet;
			this.addEventListener(FaultEvent.FAULT , doFault);
		}
		
		public function doFault(resultFault : FaultEvent):void{
			//Alerta.error("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");			
			if(resultFault.fault.faultDetail != null)
				Alert.show("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");
			else
				Alert.show(resultFault.fault.rootCause["message"]);
				
		}
		public function obtenerSecciones():ArrayList{
			this.getOperation("obtenerSecciones").send();
			return null;
		}
		
		public function obtenerSeccionesObjetoEnvolvente():EnvolventeTO{
			this.getOperation("obtenerSeccionesObjetoEnvolvente").send();
			return null;
		}
		
		
		public function obtenerSeccionesPadre():ArrayList{
			this.getOperation("obtenerSeccionesPadre").send();
			return null;
		}
	}
}
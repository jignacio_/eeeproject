package cl.foursoft.eee.comunication.servicios
{
	import cl.foursoft.eee.comunication.comunes.ChannelManager;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.messaging.ChannelSet;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.remoting.RemoteObject;

	public class ConstantesRemoteObject extends RemoteObject
	{
		public function ConstantesRemoteObject(destination : String=null)
		{
			super(destination);
			this.destination = "ConstantesService";
			this.channelSet = ChannelManager.getChannelSet;
			this.addEventListener(FaultEvent.FAULT,doFault);
		}
		protected function doFault(resultFault:FaultEvent):void{
			if(resultFault.fault.faultDetail != null)
				Alert.show("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");
			else
				Alert.show(resultFault.fault.rootCause["message"]);
			
		}
		public function obtenerTodasConstantesEdificio():ArrayCollection{
			this.getOperation("obtenerConstantesEdificio").send();
			return null;
		}
		
		public function obtenerTodasConstantesSeccion():ArrayCollection{
			this.getOperation("obtenerConstantesSeccion").send();
			return null;
		}
		
		public function obtenerParametrosEdificio():ArrayCollection{
			this.getOperation("obtenerParametrosEdificio").send();
			return null;
		}
		
		public function obtenerFactorDeRenovacion():ArrayCollection{
			this.getOperation("obtenerFactorDeRenovacion").send();
			return null;
		}
		
		public function guardarFactores(arrayFactores:ArrayCollection):void{
			this.getOperation("guardarFactores").send(arrayFactores);
		}
	}
}
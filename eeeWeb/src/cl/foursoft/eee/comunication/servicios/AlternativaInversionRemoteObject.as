package cl.foursoft.eee.comunication.servicios
{
	import cl.foursoft.eee.comunication.comunes.ChannelManager;
	import cl.foursoft.eee.comunication.objetos.AlternativaDeInversionTO;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.remoting.RemoteObject;

	public class AlternativaInversionRemoteObject extends RemoteObject
	{
		public function AlternativaInversionRemoteObject(destination :String = null)
		{
			super(destination);
			this.destination = "AlternativaInversionService";
			this.channelSet = ChannelManager.getChannelSet;
			this.addEventListener(FaultEvent.FAULT, doFault);
		}
		
		protected function doFault(resultFault:FaultEvent):void{
			//Alerta.error("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");			
			if(resultFault.fault.faultDetail != null)
				Alert.show("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");
			else
				Alert.show(resultFault.fault.rootCause["message"]);
			
			
		}
		public function obtenerTiposAlternativas():ArrayCollection{
			this.getOperation("obtenerTiposAlternativas").send();
			return null ; 
		}
		public function guardarAlternativaPorTipo( alternativa : AlternativaDeInversionTO, idTipo : int ):int{
			this.getOperation("guardarAlternativaEconomicaPorTipo").send(alternativa, idTipo);
			return -1 ; 
		}
		public function obtenerAlternativasPorEdificio(idEdificio : int ) : ArrayCollection {
			this.getOperation("obtenerAlternativasPorEdificio").send(idEdificio);
			return null ; 
		}
		public function obtenerAlternativasPorId(idAlternativa : int ) : AlternativaDeInversionTO  {
			this.getOperation("obgtenerAlternativaPorId").send(idAlternativa);
			return null ; 
		}
		public function eliminarAlternativasPorId(idAlternativa : int ) : AlternativaDeInversionTO  {
			this.getOperation("eliminarAlternativaPorId").send(idAlternativa);
			return null ; 
		}
		
		
	}
}
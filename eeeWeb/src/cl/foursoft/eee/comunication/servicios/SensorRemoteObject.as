package cl.foursoft.eee.comunication.servicios
{
	import cl.foursoft.eee.comunication.comunes.ChannelManager;
	import cl.foursoft.eee.comunication.objetos.TipoMedicionConsumoSensorTO;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	public class SensorRemoteObject extends RemoteObject
	{
		
		
		
		public function SensorRemoteObject(destination:String=null)
		{
			super(destination);
			this.destination = "SensorService";
			this.channelSet = ChannelManager.getChannelSet;
			this.addEventListener(FaultEvent.FAULT, doFault);
		}
		
		protected function doFault(resultFault:FaultEvent):void{
			//Alerta.error("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");			
			if(resultFault.fault.faultDetail != null)
				Alert.show("El sistema no pudo comunicarse con el servidor. Por favor inténtelo en otro momento o comuníquese con el encargado del sistema."+"\n"+resultFault.fault.faultDetail,"Error de comunicación");
			else
				Alert.show(resultFault.fault.rootCause["message"]);
			
		}
		
		public function obtenerTodosLosTiposDeSensores():ArrayCollection{
			getOperation("obtenerTodosLosTiposDeSensores").send();
			return null;
		}
		public function obtenerTodosLosTiposDeSensoresAdmin():ArrayCollection{
			getOperation("obtenerTodosLosTiposDeSensoresAdmin").send();
			return null;
		}
		public function obtenerTipoDeMedicion(_tipoMedicion:String):TipoMedicionConsumoSensorTO{
			getOperation("obtenerTipoMedcion").send(_tipoMedicion);
			return null;
		}
		public function obtenerTipoDeConsumo(_tipoMedicion:String):ArrayCollection{
			getOperation("obtenerTipoDeConsumo").send(_tipoMedicion);
			return null;
		}
		public function guardarSensores(_sensor : TipoMedicionConsumoSensorTO ,   idTipoMedicion :String ):Boolean{
			getOperation("guardarSensor").send(_sensor ,  idTipoMedicion);
			return false;
		}
		public function actualizarSensores(_sensores : ArrayCollection , codigoTipoMedicion : String):Boolean{
			getOperation("actualizarSensores").send(_sensores, codigoTipoMedicion);
			return false;
		}
	
		public function eliminarSensor(_sensor:TipoMedicionConsumoSensorTO):Boolean{
			getOperation("eliminarTipoConsumo").send(_sensor);
			return false;
		}
		
		
	}
}
package cl.foursoft.eee.comunication.comunes
{
	import mx.messaging.ChannelSet;
	import mx.messaging.channels.AMFChannel;
	
	public class ChannelManager
	{

		private static var _channelSet:ChannelSet;	
		private static var _urlServidor:String;

		public static function get getChannelSet():ChannelSet {
				
			if (!_channelSet || _channelSet==null ){				
				_channelSet = new ChannelSet();
			}
			return _channelSet;
		}	
		
		public static function set setChannelSet(channelSet:ChannelSet):void{
			if(channelSet)
				_channelSet = channelSet;
			else
				_channelSet = new ChannelSet();
		}
		
		public function get urlServidor():String{
			return _urlServidor;
		}
		
		public static function loadConfiguration(xml:XML):void {
        	_channelSet = new ChannelSet();
        	_urlServidor = xml.server;
        	
            for each (var channel:XML in xml.channel){
				if (channel.type == "amf"){
					var amfChannel:AMFChannel = new AMFChannel(channel.@id, _urlServidor + channel.endpoint);					
					_channelSet.addChannel(amfChannel);												
				}
			}
        }

	}
}
package cl.foursoft.eee.vistas.componentes
{
	import flash.events.MouseEvent;
	
	import mx.controls.LinkButton;
	
	import spark.components.Panel;
	
	
	public class PanelButton extends Panel
	{
		private var boton:LinkButton;
		private var _clickBoton:Function;
		
		public function PanelButton(){
			super();
			
			this.boton = new LinkButton();
			this.boton.label = "Ver Requerimiento Energético";
			this.boton.width = 300;
			this.boton.height = 50
			this.boton.styleName = "btnVerRequerimientoEnergetico";	
		}
		
		public function set clickBoton(clickListener:Function):void{
			this._clickBoton = clickListener;
			
			this.boton.addEventListener(MouseEvent.CLICK, _clickBoton);
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			
			// Add the title panel
			if (boton)
			{
				//titleBar.addChild(boton);
				this.addElement(boton);
			}
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			// Position the title panel
			if (boton)
			{
				var y:int = - boton.height;
				var x:int = unscaledWidth - boton.width - 25;
				boton.move(x, y);
			}
		}
	}
}
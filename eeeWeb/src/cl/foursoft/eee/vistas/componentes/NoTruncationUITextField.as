package cl.foursoft.eee.vistas.componentes
{
	import mx.core.UITextField;
	
	class NoTruncationUITextField extends UITextField
	{
		public function NoTruncationUITextField()
		{
			super();
		}
		override public function truncateToFit(s:String = null):Boolean
		{
			return false;
		}
		
	}
}
package cl.foursoft.eee.actionscript.eventos
{
	import mx.messaging.messages.IMessage;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.ResultEvent;
	
	public class AlternativaInversionEvent extends ResultEvent
	{
		public static const GURADR_NUEVA_ALTERNATIVA :String= "GURADR_NUEVA_ALTERNATIVA"; 
		public static const ELIMINAR_EVALUACION :String= "ELIMINAR_EVALUACION"; 
		
		public static const EVALUAR : String = "EVALUAR";
		public static const OBTENER_FLUJOS : String= "OBTENER_FLUJOS";
		
		public static const SELECCIONAR_EVALUACION :String = "SELECCIONAR_EVALUACION";
		
		public static const ERROR_GUARDAR_ALTERNATIVA : String = "ERROR_GUARDAR_ALTERNATIVA";
		
		public function AlternativaInversionEvent(type:String, result:Object=null, bubbles:Boolean=false, cancelable:Boolean=true,  token:AsyncToken=null, message:IMessage=null)
		{
			super(type, bubbles, cancelable, result, token, message);
		}
	}
}
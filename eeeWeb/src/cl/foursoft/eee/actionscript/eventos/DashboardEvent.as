package cl.foursoft.eee.actionscript.eventos
{
	import mx.messaging.messages.IMessage;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.ResultEvent;
	
	public class DashboardEvent extends ResultEvent
	{
		public static const EDIFICIOS_CARGADOS : String = "EDIFICIOS_CARGADOS";
		
		
		public function DashboardEvent(type:String, result:Object=null, bubbles:Boolean=false, cancelable:Boolean=true, token:AsyncToken=null, message:IMessage=null)
		{
			super(type, bubbles, cancelable, result, token, message);
		}
	}
}
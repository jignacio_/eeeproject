package cl.foursoft.eee.actionscript.eventos
{
	import mx.messaging.messages.IMessage;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.ResultEvent;
	
	public class VistaParaGestionEvent extends ResultEvent
	{
		
		
		public static const SELECCIONAR_ELEMENTO = "SELECCIONAR_ELEMENTO"; 
		public static const ENVIAR_DATOS = "ENVIAR_DATOS"; 
		
		
		public function VistaParaGestionEvent(type:String, result:Object=null, bubbles:Boolean=false, cancelable:Boolean=true, token:AsyncToken=null, message:IMessage=null)
		{
			super(type, bubbles, cancelable, result, token, message);
		}
	}
}
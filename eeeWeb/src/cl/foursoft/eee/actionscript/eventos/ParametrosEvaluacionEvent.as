package cl.foursoft.eee.actionscript.eventos
{
	import mx.messaging.messages.IMessage;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.ResultEvent;
	
	public class ParametrosEvaluacionEvent extends ResultEvent
	{
		public static const ELIMINAR_PARAMETRO_EVALUACION :String = "ELIMINAR_PARAMETRO_EVALUACION";
		
		public function ParametrosEvaluacionEvent(type:String, result:Object=null, bubbles:Boolean=false, cancelable:Boolean=true,  token:AsyncToken=null, message:IMessage=null)
		{
			super(type, bubbles, cancelable, result, token, message);
		}
	}
}
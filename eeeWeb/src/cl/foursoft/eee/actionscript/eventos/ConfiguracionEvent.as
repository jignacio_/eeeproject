package cl.foursoft.eee.actionscript.eventos
{
	import mx.messaging.messages.IMessage;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.ResultEvent;
	
	public class ConfiguracionEvent extends ResultEvent
	{
		
		public static const EDIFICIO_GUARDADO:String = "EDIFICIO_GUARDADO" 
		public static const EDIFICIO_GUARDADO_CONTINUAR_EDITANDO:String = "EDIFICIO_GUARDADO_CONTINUAR_EDITANDO" 
		public static const COMPONENTE_EDIFICIO_CARGADO : String = "COMPONENTE_EDIFICIO_CARGADO";
		public static const PANTALLA_CARGADA:String = "PANTALLA_CARGADA" ;
		
		
		public static const PANTALLA_CARGADA_EDITAR:String = "PANTALLA_CARGADA_EDITAR" ;
		public static const ALTERNATIVA_GUARDADA:String = "ALTERNATIVA_GUARDADA" ;
		
		//CARGAS DE LOS DATOS EN LOS COMPONENTES QUE CONFORMAN EL FORMULARIO  DEL EDIFICIO.
		public static const COMPSENSORES_CARGADO : String = "COMPSENSORES_CARGADO";
		public static const COMPSECCIONES_CARGADO : String = "COMPSECCIONES_CARGADO";
		public static const COMPPARAMETROS_CARGADO : String = "COMPPARAMETROS_CARGADO";
		public static const ERROR_GUARDADO : String = "ERROR_GUARDADO";
		
		
		//CONFIGURACION DEL SISTEMA
		public static const CERRAR_CONFIGURACION_SISTEMA : String = "CERRAR_CONFIGURACION_SISTEMA";
		
		
		
		//Eventos para componentes
		public static const SUBIR_SCROLL : String = "SUBIR_SCROLL";
		public static const TOGGLE_SCROLL:String = "TOGGLE_SCROLL";

		
		
		public function ConfiguracionEvent(type:String, result:Object=null, bubbles:Boolean=false, cancelable:Boolean=true, token:AsyncToken=null, message:IMessage=null)
		{
			super(type, bubbles, cancelable, result, token, message);
		}
	}
}
package cl.foursoft.eee.actionscript.eventos
{
	import mx.messaging.messages.IMessage;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.ResultEvent;
	
	public class ItemRenderPisosEvent extends ResultEvent
	{
		static public const ELIMINAR_PISO : String   = "ELIMINAR_PISO";
		static public const ELIMINAR_ZONA : String   = "ELIMINAR_ZONA";
		static public const ELIMINAR_SENSOR : String = "ELIMINAR_SENSOR";
		
		static public const MODIFICAR_NUMERO_PISOS : String = "MODIFICAR_NUMERO_PISOS";		
		
		public function ItemRenderPisosEvent(type:String, result:Object=null, bubbles:Boolean=false, cancelable:Boolean=true, token:AsyncToken=null, message:IMessage=null)
		{
			super(type, bubbles, cancelable, result, token, message);
		}
		 
	}
}
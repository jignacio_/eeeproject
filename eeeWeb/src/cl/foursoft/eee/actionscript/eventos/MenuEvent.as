package cl.foursoft.eee.actionscript.eventos
{

	 
	import mx.rpc.AsyncToken;
	import mx.rpc.events.ResultEvent;
	import mx.messaging.messages.IMessage;
	
	public class MenuEvent extends ResultEvent
	{
		public static const  VISTAS_PARA_GESTION_CLICK:String = "VISTAS_PARA_GESTION_CLICK";
		public static const MONITOREO_GENERAL_CLICK:String = "MONITOREO_GENERAL_CLICK";
		public static const ALTERNATIVAS_ECONOMICAS_CLICK:String = "ALTERNATIVAS_ECONOMICAS_CLICK";
		public static const CONFIGURACION_CLICK:String = "CONFIGURACIONCiON_CLICK";
		public static const USUARIO_CLICK:String = "USUARIO_CLICK";
		
		
		/*
		 *CLIENTES Y USUARIOS
		MATERIALES
		PARAMETROS 
		*/
		//menu administracion		
		public static const  CONFIG_CLIENTES_Y_USUARIOS:String = "CLIENTES_Y_USUARIOS";
		public static const CONFIG_MATERIALES:String = "MATERIALES";
		public static const CONFIG_ARTEFACTOS:String = "ARTEFACTOS";
		public static const CONFIG_PARAMETROS:String = "PARAMETROS";
		public static const CONFIG_SENSORES:String = "SENSORES";
		
		public static const MOSTRAR_MENU_INFERIOR:String = "MOSTRAR_MENU_INFERIOR";
		
		public function MenuEvent(type:String, result:Object=null, bubbles:Boolean=false, cancelable:Boolean=true, token:AsyncToken=null, message:IMessage=null)
		{
			super(type, bubbles, cancelable, result, token, message);
		}
	}
}
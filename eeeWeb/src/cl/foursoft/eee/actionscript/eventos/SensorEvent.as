package cl.foursoft.eee.actionscript.eventos
{
	import mx.messaging.messages.IMessage;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.ResultEvent;
	
	public class SensorEvent extends ResultEvent
	{
		
		public static const TIPO_SENSOR_CREADO : String 		= "TIPO_SENSOR_CREADO";
		public static const SELECCIONAR_SENSOR_EDITAR : String 	= "SELECCIONAR_SENSOR_EDITAR";
		public static const GUARDAR_TIPO_CONSUMO : String 		= "GUARDAR_TIPO_CONSUMO"; 
		
		public function SensorEvent(type:String, result:Object=null, bubbles:Boolean=false, cancelable:Boolean=true, token:AsyncToken=null, message:IMessage=null)
		{
			super(type, bubbles, cancelable, result, token, message);
		}
	}
}
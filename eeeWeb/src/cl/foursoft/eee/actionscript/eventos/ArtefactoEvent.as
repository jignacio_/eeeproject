package cl.foursoft.eee.actionscript.eventos
{
	import mx.messaging.messages.IMessage;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.ResultEvent;
	
	public class ArtefactoEvent extends ResultEvent
	{
		
		public static const GUARDAR_COMPOSICION_ARTEFACTO : String = "GUARDAR_COMPOSICION_ARTEFACTO";
		public static const ELIMINAR_ARTEFACTO_COMPOSICION : String=  "ELIMINAR_ARTEFACTO_COMPOSICION";
		public static const ABRIR_VENTANA_ARTEFACTOS : String = "ABRIR_VENTANA_ARTEFACTOS";
		public static const CAMBIAR_CANTIDAD_ARTEFACTOS : String = "CAMBIAR_CANTIDAD_ARTEFACTOS";
		//admin
		public static const EDITAR_ARTEFACTO : String 	= "EDITAR_ARTEFACTO";
		public static const GUARDAR_ARTEFACTO : String 	= "GUARDAR_ARTEFACTO";
		public static const ELIMINAR_ARTEFACTO : String 	= "ELIMINAR_ARTEFACTO";
		public static const ELIMINAR_ARTEFACTO_ADMINISTRADOR : String 	= "ELIMINAR_ARTEFACTO_ADMINISTRADOR";
		
		public function ArtefactoEvent(type:String , result:Object=null , bubbles:Boolean=false, cancelable:Boolean=true,  token:AsyncToken=null, message:IMessage=null){
			super(type, bubbles, cancelable, result, token, message);
		}
	}
}
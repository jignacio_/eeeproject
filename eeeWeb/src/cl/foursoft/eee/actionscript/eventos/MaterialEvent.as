package cl.foursoft.eee.actionscript.eventos
{
	import mx.messaging.messages.IMessage;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.ResultEvent;
	
	public class MaterialEvent extends ResultEvent
	{
		
		public static const GUARDAR_COMPOSICION : String = "GUARDAR_COMPOSICION";
		public static const ELIMINAR_MATERIAL_COMPOSICION : String=  "ELIMINAR_MATERIAL_COMPOSICION";
		
		//admin
		public static const EDITAR_MATERIAL : String 	= "EDITAR_MATERIAL";
		public static const GUARDAR_MATERIAL : String 	= "GUARDAR_MATERIAL";
		public static const ELIMINAR_MATERIAL : String 	= "ELIMINAR_MATERIAL";
		public static const ELIMINAR_MATERIAL_ADMINISTRADOR : String 	= "ELIMINAR_MATERIAL_ADMINISTRADOR";
		
		public function MaterialEvent(type:String , result:Object=null , bubbles:Boolean=false, cancelable:Boolean=true,  token:AsyncToken=null, message:IMessage=null)
		{
			super(type, bubbles, cancelable, result, token, message);
		}
	}
}
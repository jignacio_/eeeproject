package cl.foursoft.eee.actionscript.eventos
{
	import mx.messaging.messages.IMessage;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.ResultEvent;
	
	public class EdificioEvent extends ResultEvent
	{
		
		public static const EDIFICIO_GUARDADO : String = "EDIFICIO_GUARDADO";
		public static const EDIFICIO_ELIMINADO  : String = "EDIFICIO_ELIMINADO";
		public static const EDIFICIO_ELIMINADO_ERROR  : String = "EDIFICIO_ELIMINADO_ERROR";
		public static const EDIFICIO_MODIFICADO : String = "EDIFICIO_MODIFICADO";
		public static const EDIFICIO_AREAS_SECCIONES : String = "EDIFICIO_AREAS_SECCIONES";

		public static const CALCULAR_REQUERIMIENTO_ENERGETICO : String = "CALCULAR_REQUERIMIENTO_ENERGETICO";
		public static const CALCULAR_INVERSION_ECONOMICA : String = "CALCULAR_INVERSION_ECONOMICA";
		
		public static const EDITAR_EDIFICIO : String = "EDITAR_EDIFICIO";
		public static const VER_RESUMEN_EDIFICIO : String = "VER_RESUMEN_EDIFICIO";
		
		
		
		public function EdificioEvent(type:String,result:Object=null, bubbles:Boolean=false, cancelable:Boolean=true,  token:AsyncToken=null, message:IMessage=null)
		{
			super(type, bubbles, cancelable, result, token, message);
		}
	}
}
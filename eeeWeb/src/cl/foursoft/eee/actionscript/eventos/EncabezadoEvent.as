package cl.foursoft.eee.actionscript.eventos
{
	import mx.messaging.messages.IMessage;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.ResultEvent;
	
	public class EncabezadoEvent extends ResultEvent
	{
		public static const CERRAR_SESION : String = "CERRAR_SESION";
		public static const CONFIGURACION_SISTEMA : String = "CONFIGURACION_SISTEMA";
		
		public function EncabezadoEvent(type:String, result:Object=null, bubbles:Boolean=false, cancelable:Boolean=true, token:AsyncToken=null, message:IMessage=null)
		{
			super(type, bubbles, cancelable, result, token, message);
		}
	}
}
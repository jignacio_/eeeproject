package cl.foursoft.eee.actionscript.interfaces
{
	public interface ISeleccionVistasParaGestion
	{
		
		function set tipoSeleccionado(value:Boolean):void;
		function get tipoSeleccionado():Boolean;
		function get nombreElemento():String;
	}
}
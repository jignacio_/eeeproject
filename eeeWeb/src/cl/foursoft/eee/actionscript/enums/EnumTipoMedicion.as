package cl.foursoft.eee.actionscript.enums
{
	public class EnumTipoMedicion
	{
		
		public static const MEDICION_EDIFICIO : String = "MEDICION_EDIFICIO";
		public static const MEDICION_PISO : String = "MEDICION_PISO";
		public static const MEDICION_ZONA : String = "MEDICION_ZONA";
		public static const MEDICION_SENSOR : String = "MEDICION_SENSOR";

		
	}
}
package cl.foursoft.eee.actionscript.enums
{
	public class EnumSecciones
	{
		
		public static const SECCION_AREA_SUPERIOR : String = "Área Superior";
		public static const SECCION_VENTANAS : String = "Ventanas";
		public static const SECCION_MUROS : String = "Muros";
		public static const SECCION_AREA_BASE : String = "Área de la Base";
		public static const SECCION_OTRAS_INSTALACIONES : String = "Otras Instalaciones";
		
		public static  const SECCION_PUERTAS   : String = "Puertas";
		public static  const SECCION_AREA_BASE_ESCALERA  : String  = "Escaleras";
		
		public static const SECCION_AREA_BASE_PISO : String = "Piso"; 
	}
}
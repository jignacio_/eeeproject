package cl.foursoft.eee.actionscript.enums
{
	public class EnumTipoAlternativa {
		
		public static const TIPO_ALTERNATIVA_ENVOLVENTE : int = 1;
		public static const TIPO_ALTERNATIVA_ARTEFACTO :int  = 2;
		
	}
}
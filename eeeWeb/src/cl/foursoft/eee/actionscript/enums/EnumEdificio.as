package cl.foursoft.eee.actionscript.enums
{
	public class EnumEdificio
	{
		
		public static const LARGO : String = "LARGO";
		public static const ANCHO : String = "ANCHO";
		public static const ALTO_PISO : String = "ALTO_PISO";
	    public static const NUMERO_PISOS : String = "NUMERO_PISOS";
		public static const AREA_TOTAL : String = "AREA_TOTAL";
		public static const PERIMETRO : String = "PERIMETRO";
		public static const SUPERFICIE_UTIL : String = "SUPERFICIE_UTIL";
		public static const SUPERFICIE_BRUTA : String = "SUPERFICIE_BRUTA";
		public static const VOLUMEN_AIRE : String = "VOLUMEN_AIRE";
		public static const COMPACTACION : String = "COMPACTACION";
		
		
		public static const AREA_SUPERIOR : String = "AREA_SUPERIOR";
		public static const AREA_BASE : String = "AREA_BASE";
		public static const AREA_MUROS : String = "AREA_MUROS";
		public static const AREA_VENTANAS: String = "AREA_VENTANAS";
		public static const AREA_PUERTAS: String = "AREA_PUERTAS";
		
		
		//Evaluacion Economica Edificio
		public static const DESCRIPCION_EVALUACION :String= "DESCRIPCION_EVALUACION";
		public static const FECHA_ULTIMA_EVALUACION :String = "FECHA_ULTIMA_EVALUACION";
		public static const NUEVA_ENVOLVENTE :String = "NUEVA_ENVOLVENTE";
		
	}
}
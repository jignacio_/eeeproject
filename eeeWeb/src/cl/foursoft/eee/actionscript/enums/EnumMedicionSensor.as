package cl.foursoft.eee.actionscript.enums
{
	public class EnumMedicionSensor
	{
			public static const SENSOR_AGUA :String= "A";
			public static const SENSOR_COMBUSTIBLE : String= "C";
			public static const SENSOR_ELECTRICIDAD : String= "E";

			public static const SENSOR_AGUA_NOMBRE :String= "Agua";
			public static const SENSOR_ELECTRICIDAD_NOMBRE : String= "Electricidad";
			public static const SENSOR_COMBUSTIBLE_NOMBRE: String= "Combustible";
		
		
	}
}
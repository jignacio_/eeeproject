package cl.foursoft.eee.actionscript.enums
{
	public class EnumParametros
	{
		 
		public static const _FSU : String = "FSU";
		public static const _FVA : String = "FVA";
		
		public static const _RSI : String = "RSI";
		public static const _RSE : String = "RSE";
		
		//--Nuevos Valores RSE/RSI por sección
		public static const _RSI_MUROS_EXTERIOR 	 : String = "RSI_MUROS_EXTERIOR";
		public static const _RSE_MUROS_EXTERIOR : String = "RSE_MUROS_EXTERIOR";

		public static const _RSI_PUERTAS_EXTERIOR 	 : String = "RSI_PUERTAS_EXTERIOR";
		public static const _RSE_PUERTAS_EXTERIOR : String = "RSE_PUERTAS_EXTERIOR";

		public static const _RSI_SUPERIOR_EXTERIOR 	 : String = "RSI_SUPERIOR_EXTERIOR";
		public static const _RSE_SUPERIOR_EXTERIOR : String = "RSE_SUPERIOR_EXTERIOR";

		public static const _RSI_BASE_EXTERIOR 	 : String = "RSI_BASE_EXTERIOR";
		public static const _RSE_BASE_EXTERIOR : String = "RSE_BASE_EXTERIOR";
		
		//--Nuevos Valores RSE/RSI por sección
		public static const _RSI_MUROS_SEPARACION 	 : String = "RSI_MUROS_SEPARACION";
		public static const _RSE_MUROS_SEPARACION : String = "RSE_MUROS_SEPARACION";

		public static const _RSI_SUPERIOR_SEPARACION 	 : String = "RSI_SUPERIOR_SEPARACION";
		public static const _RSE_SUPERIOR_SEPARACION : String = "RSE_SUPERIOR_SEPARACION";

		public static const _RSI_BASE_SEPARACION 	 : String = "RSI_BASE_SEPARACION";
		public static const _RSE_BASE_SEPARACION: String = "RSE_BASE_SEPARACION";
		//--
		
		public static const _PVP : String = "PVP";
		public static const _PVT : String = "PVT";
		public static const _FE : String = "FE";
		public static const _FG1 : String = "FG1";
		public static const _FG2 : String = "FG2";
		public static const _FPV : String = "FPV";
		public static const _FGSI1 : String = "FGSI1";
		public static const _FGSI2 : String = "FGSI2";
		public static const _FGSI3 : String = "FGSI3";
		public static const _FGSI4 : String = "FGSI4";
		public static const _FGSI5 : String = "FGSI5";
		public static const _FGSI6 : String = "FGSI6";
		public static const _FGSV1 : String = "FGSV1";
		public static const _FGSV2 : String = "FGSV2";
		public static const _FGSV3 : String = "FGSV3";
		public static const _FGSV4 : String = "FGSV4";
		public static const _FGSV5 : String = "FGSV5";
		public static const _FGSV6 : String = "FGSV6";
		public static const _FS : String = "FS";
		public static const _FM : String = "FM";
		public static const _FC : String = "FC";
		public static const _G : String = "G"; 
		public static const _AVI : String = "AVI";
		public static const _QI : String = "QI"; 
		public static const _FPC : String = "FPC";
		public static const _FGC : String = "FGC";
		public static const _FPF : String = "FPF";
		public static const _FGF : String = "FGF";
		
		public static const _NCA : String = "NCA"; //Numero de Cambios de Aire
		public static const _N : String = "NCA"; //Numero de Cambios de Aire
		
	}
}
package cl.foursoft.eee.actionscript.util
{
	import mx.core.FlexGlobals;

	public class AlturasUtil
	{
		public static const ALTURA_PADDING:int = 10;
		
		public static const ALTURA_ENCABEZADO : int = 100;
		public static const ALTURA_CONTENIDOO:int = FlexGlobals.topLevelApplication.height;
		
		public static const ALTURA_PANEL :int =  ALTURA_CONTENIDOO - ALTURA_ENCABEZADO  ;
		
		public static const ALTURA_ITEMRENDER_RESUMEN : int = 45;
		public static const ALTURA_ITEMRENDER_DETALLE : int = 160;
		
	}
}
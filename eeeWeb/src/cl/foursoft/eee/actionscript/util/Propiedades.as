package cl.foursoft.eee.actionscript.util
{
	
	import cl.foursoft.eee.comunication.comunes.ChannelManager;
	
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;

	/**
	 * ...
	 * @author Cristian Abarca
	 */
	public dynamic class Propiedades
	{	
		private static var callback:Function;
		/**
		 * Método que carga archivo de propiedades a memoria
		 */
		public static function cargarConfiguracion(callback:Function=null):void {
			Propiedades.callback = callback;
			//LibreriaCarga.getData(result_configuracion, "xml/propiedades.xml"); 
			var request:URLRequest = new URLRequest("properties.xml");
			var loader:URLLoader =new URLLoader(request);	            
			loader.addEventListener(Event.COMPLETE, result_configuracion);
			try{
				loader.load(request);
			}
			catch (error:ArgumentError){
				trace("Error al cargar archivo de propiedades: " + error.getStackTrace());
			}
			catch (error:SecurityError){
				trace("Error al cargar archivo de propiedades: " + error.getStackTrace());
			}
        }
		
		private function result_configuracion(event:Event):void {
			var loader:URLLoader = URLLoader(event.target);
			var xmlConfig:XML=new XML(loader.data);            		 
			ChannelManager.loadConfiguration(xmlConfig);
		}
		
		private static function result_configuracion(event:Event):void {
			var loader:URLLoader = URLLoader(event.target);
			var datos:XML=new XML(loader.data);            		 
			
			//Inicializando conexion a servidor
			ChannelManager.loadConfiguration(datos);
			
			//Carga de propiedades
			for each (var propiedad:XML in datos.propiedad){
				try {
					var idPropiedad:String = propiedad.@id;
					Propiedades[idPropiedad] = propiedad.@valor;
				}
				catch(e:Error){}
			}
			
			if(Propiedades.callback != null)
				Propiedades.callback();
		}
        
		/**
		 * Método para obtener el valor de una propiedad
		 */
        public static function obtenerPropiedad(nombrePropiedad:String):*{
        	try{
        		return Propiedades[nombrePropiedad];
        	}
        	catch(e:Error){
        		return "";
        	}
        }
	}
}
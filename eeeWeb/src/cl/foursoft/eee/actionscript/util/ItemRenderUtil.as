package cl.foursoft.eee.actionscript.util
{
	import mx.core.ClassFactory;  
	import mx.core.IFactory;  
	
	public class ItemRendererUtil {  
		
		public static function createWithProperties(renderer:Class):IFactory {  
			var factory:ClassFactory = new ClassFactory(renderer);  
		//	factory.properties = properties;  
			return factory;  
		}  
		
		
	}  
}
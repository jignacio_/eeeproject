package cl.foursoft.eee.actionscript.util
{
	import mx.controls.DateField;
	import mx.formatters.DateFormatter;
	
	public class FechaUtil
	{
		public static const HASTA_AYER:String = "AYER";
		public static const HASTA_HOY:String = "HASTA_HOY";
		public static const DESDE_HOY:String = "DESDE_HOY";
		public static const HOY:String = "HOY";
		public static const DESDE_MANANA:String = "DESDE_MANANA";
		
		
		public static function  dateChooser_init(dt:DateField, selectedDate:Date=null, disabledCustomRange:String=""):void {
			dt.dayNames=['D', 'L', 'M', 'Mi', 'J', 'V', 'S'];
			dt.monthNames=[	'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 
							'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
			dt.firstDayOfWeek = 1;
			dt.formatString = "DD/MM/YYYY";
			dt.yearNavigationEnabled=true;
			
			if(selectedDate)
				dt.selectedDate= selectedDate;		
			
			if(disabledCustomRange != ""){
				switch (disabledCustomRange){
					case FechaUtil.HASTA_AYER:
							dt.disabledRanges = [{rangeEnd: new Date(new Date().getTime() - 1*24*60*60*1000)}];
							break;
					case FechaUtil.HASTA_HOY:
							dt.disabledRanges = [{rangeEnd: new Date()}];
							break;
					case FechaUtil.DESDE_HOY:
							dt.disabledRanges = [{rangeStart: new Date()}];
							break;
					case FechaUtil.DESDE_MANANA:
							dt.disabledRanges = [{rangeStart: new Date(new Date().getTime() + 1*24*60*60*1000)}];
							break;
					case FechaUtil.HOY:
							dt.disabledRanges = [new Date()];
				}
			}
			
			
		}
		public static function obtenerMes(_numeroMes : int):String{
			switch (_numeroMes){
				case 0:
					return "Enero";
					break;
				case 1:
					return "Febrero";
					break;
				case 2:
					return "Marzo";
					break;
				case 3:
					return "Abril";
					break;
				case 4:
					return "Mayo";
					break;
				case 5:
					return "Junio";
					break;
				case 6:
					return "Julio";
					break;
				case 7:
					return "Agosto";
					break;
				case 8:
					return "Septiembre";
					break;
				case 9:
					return "Octubre";
					break;
				case 10:
					return "Noviembre";
					break;
				case 11:
					return "Diciembre";
					break;
				default:
					return "";
					break;
			}
			return "";
		}
	 
	public static function dateToString(d:Date):String
	{
		var  dateFormatter : DateFormatter = new DateFormatter();
		dateFormatter.formatString = 'EEEE DD MMMM YYYY at HH:NN A'
		return dateFormatter.format(d);
	}
		
		
	}
}
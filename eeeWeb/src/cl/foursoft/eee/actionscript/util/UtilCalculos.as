package cl.foursoft.eee.actionscript.util
{
 
	
	import cl.foursoft.eee.actionscript.enums.EnumMedicionSensor;
	import cl.foursoft.eee.comunication.objetos.ArtefactoTO;
	import cl.foursoft.eee.comunication.objetos.MaterialTO;
	
	import flashx.textLayout.formats.Float;
	
	import mx.collections.ArrayCollection;

	public class UtilCalculos  
	{
		
		
		public function UtilCalculos()
		{
		}
			
		public static function perimetro(_largo:Number=1, _ancho:Number=1 ):Number{
			return round(
					(_largo*2) + (_ancho*2)
			);
		}
		
		public static function superficieBruta(_largo:Number=1, _ancho:Number=1):Number{
			return round(
					_largo * _ancho
			);
		}
		
		public static function superficieUtil(_superficieBruta : Number, _FSU:Number, _numeroDePisos : Number, _altoPiso : Number):Number{
			return round(
					_superficieBruta * _FSU * _numeroDePisos * _altoPiso
			);
		}
		public static function volumenDeAire(_largo:Number, _ancho : Number, _alturaDePisos:Number, _numeroDePisos : Number, _FVA : Number):Number{
			return round(
					_largo * _ancho * _alturaDePisos * _numeroDePisos * _FVA
			);
		}
		public static function compactacion(_area : Number, _volumen : Number) : Number{
			return round(
					_area / _volumen
			);
		}
		public static function precioCalefaccionTotal(_precioCombustible : Number, _eficiencia : Number ):Number{
			if(_eficiencia >0){
				return round(
						_precioCombustible / _eficiencia
				);
			}else
				return 0;
		}
		public static function resistenciaComposicion(_composicion : ArrayCollection):Number{
			var _r : Number =0;
			for each(var _m:MaterialTO in _composicion)
				_r +=_m.resistencia;
			return round( _r );
		}
		public static function TransmitanciaComposicion(_rt : Number):Number{
			if(_rt==0)
				return 0;
			return round( 1 / _rt );
		}
		public static function TransmitanciaComposicionVentanas(_composicion : ArrayCollection):Number{
			var _r : Number =0;
			for each(var _m:MaterialTO in _composicion)
				_r +=_m.transmitancia;
			return round( _r );
			
		}
		
		public static function formato00(value:int):String{
			//Restringir a menos a 99
			if(value>9)
				return value.toString();
			else
				return "0"+value.toString();
		}
		public static function formato000(value:int):String{
			
			
			if (value>99)
				return value.toString();
			else if (value>9)
				return "0"+value.toString();	
			else
				return "00"+value.toString();
		}
		
		/*
		 * Para redondear la cantidad de decimales, ya que el tipo de datos NUMBER 
		 *  presenta algunas anormalidades
		 * por ejemplo, 12 x 12.7 = 152.4 entrega como resultado 152.39999999999998; 
		 * con esta forma de redondear entraga a los más 3 decimales
		*/
		public static function round(i:Number):Number{
		/*	var valor : Number  = ((Math.round( (i)*1000 )) /1000);
			var valorSeparado : String = separarMiles(valor,3);
			var valorSeparadoSolo : String = separarMiles(i,3);
			*/
			//trace(valorSeparado +"  (Sin Round : "+ valorSeparado+")"+" ("+ Number(valorSeparadoSolo) +")");
			return 	 ((Math.round( (i)*100 )) /100);
		}
		public static function separarMiles(number:*, maxDecimals:int = 2, forceDecimals:Boolean = false, siStyle:Boolean = true):String
		{
		    var i:int = 0;
			var inc:Number = Math.pow(10, maxDecimals);
			var str:String = String(Math.round(inc * Number(number))/inc);
	    	var hasSep:Boolean = str.indexOf(".") == -1, sep:int = hasSep ? str.length : str.indexOf(".");
	    	var ret:String = (hasSep && !forceDecimals ? "" : (siStyle ? "," : ".")) + str.substr(sep+1);
	    	if (forceDecimals) {
				for (var j:int = 0; j <= maxDecimals - (str.length - (hasSep ? sep-1 : sep)); j++) ret += "0";
			}
	    	while (i + 3 < (str.substr(0, 1) == "-" ? sep-1 : sep)) ret = (siStyle ? "." : ",") + str.substr(sep - (i += 3), 3) + ret;
	    	return (str.substr(0, sep - i) + ret);
		}
		
		public static function obtenerNumeroPiso(codigoPiso : String) : int{
			var num : int = -1; 
			if(codigoPiso.length>0){
				num = int( codigoPiso.substr( codigoPiso.length-2 , codigoPiso.length-1 ) ) ;
					  
			}
			return (isNaN(num)) ? 0 : num;
		}
		public static function obtenerNumeroZona(codigoZona : String) : int{
			var num : int = -1; 
			if(codigoZona.length>0){
				num = int ( codigoZona.substr( codigoZona.length-2 , codigoZona.length-1 ) ) ;
					  
			}
			return (isNaN(num)) ? 0 : num;
		}
		public static function calcularConsumoArtefactos(_artefactos:ArrayCollection, idMedicion : String):Number{
			
			var _reqTotalSensor : Number = 0;
			var _reqAhorro : Number = 0;
			var _cantidad : int = 0;
			var _artefactAguaAnterior :ArtefactoTO;
			
			
			//Electricidad
			if(idMedicion == EnumMedicionSensor.SENSOR_ELECTRICIDAD){
				for each(var _art : ArtefactoTO in _artefactos){
					_reqTotalSensor += _art.consumoArtefacto * _art.cantidadArtefacto;   
				} 
				//Pasar de Watt  kW
				_reqTotalSensor = _reqTotalSensor / 1000 ;
			}
			
			//Agua
			else if(idMedicion== EnumMedicionSensor.SENSOR_AGUA){
				for each(var _art : ArtefactoTO in _artefactos){
					if(_art.unidadMedidaArtefacto=="Litros"){
						_artefactAguaAnterior = _art;
						_reqTotalSensor += _art.consumoArtefacto * _art.cantidadArtefacto;  
					}
					else if(_art.unidadMedidaArtefacto == "%"){
						if(_art.cantidadArtefacto > _artefactAguaAnterior.cantidadArtefacto)
							_cantidad = _artefactAguaAnterior.cantidadArtefacto;
						else
							_cantidad = _art.cantidadArtefacto;
						
							_reqAhorro = _cantidad * _artefactAguaAnterior.consumoArtefacto; 
							_reqAhorro = _reqAhorro * _art.consumoArtefacto;
							_reqTotalSensor -= _reqAhorro; 
						
					}
				}
			}
			else {
				
			}
			
			return _reqTotalSensor; 
			
		}
		
	}
}
package cl.foursoft.eee.actionscript.util
{
	import cl.foursoft.eee.comunication.objetos.ParametrosEdificioTO;
	import cl.foursoft.eee.comunication.objetos.SeccionTO;
	import cl.foursoft.eee.comunication.objetos.UsuarioTO;
	
	import mx.collections.ArrayCollection;
	
	public class UtilParametrosGuardadosEdificio
	{
		private static var valor : Number = 0; 
		/*private static var _FSU : String = "FSU";*/ 
		
		//NUEVOS PARAMETROS
		
		
		private static const _FSU : String = "FSU";
		private static const _FVA : String = "FVA";
		private static const _RSE : String = "RSE";
		private static const _RSI : String = "RSI";
		//--Nuevos Valores RSE/RSI por sección
		public static const _RSI_MUROS_EXTERIOR 	 : String = "RSI_MUROS_EXTERIOR";
		public static const _RSE_MUROS_EXTERIOR : String = "RSE_MUROS_EXTERIOR";
		
		public static const _RSI_SUPERIOR_EXTERIOR 	 : String = "RSI_SUPERIOR_EXTERIOR";
		public static const _RSE_SUPERIOR_EXTERIOR : String = "RSE_SUPERIOR_EXTERIOR";
		
		public static const _RSI_BASE_EXTERIOR 	 : String = "RSI_BASE_EXTERIOR";
		public static const _RSE_BASE_EXTERIOR : String = "RSE_BASE_EXTERIOR";
		
		//--Nuevos Valores RSE/RSI por sección
		public static const _RSI_MUROS_SEPARACION 	 : String = "RSI_MUROS_SEPARACION";
		public static const _RSE_MUROS_SEPARACION : String = "RSE_MUROS_SEPARACION";
		
		public static const _RSI_SUPERIOR_SEPARACION 	 : String = "RSI_SUPERIOR_SEPARACION";
		public static const _RSE_SUPERIOR_SEPARACION : String = "RSE_SUPERIOR_SEPARACION";
		
		public static const _RSI_BASE_SEPARACION 	 : String = "RSI_BASE_SEPARACION";
		public static const _RSE_BASE_SEPARACION: String = "RSE_BASE_SEPARACION";
		//--
		private static const _PVP : String = "PVP";
		private static const _PVT : String = "PVT";
		private static const _FE : String = "FE";
		private static const _FG1 : String = "FG1";
		private static const _FPV : String = "FPV";
		private static const _FGSI1 : String = "FGSI1";
		private static const _FGSI2 : String = "FGSI2";
		private static const _FGSI3 : String = "FGSI3";
		private static const _FGSI4 : String = "FGSI4";
		private static const _FGSI5 : String = "FGSI5";
		private static const _FGSI6 : String = "FGSI6";
		private static const _FGSV1 : String = "FGSV1";
		private static const _FGSV2 : String = "FGSV2";
		private static const _FGSV3 : String = "FGSV3";
		private static const _FGSV4 : String = "FGSV4";
		private static const _FGSV5 : String = "FGSV5";
		private static const _FGSV6 : String = "FGSV6";
		private static const _FS : String = "FS";
		private static const _FM : String = "FM";
		private static const _FC : String = "FC";
		private static const _G : String = "G"; /*COMBOBOX - tipo de vidrio*/
		private static const _AVI : String = "AVI";
		private static const _AC :String = "AC"
		private static const _QI : String = "QI"; /*COMBOBOX dedicacion edificio*/
		
/*		private static const _QI2 : String = "QI";
		private static const _QI3 : String = "QI";*/
		private static const _FPC : String = "FPC";
		private static const _FGC : String = "FGC";
		private static const _FPF : String = "FPF";
		private static const _FGF : String = "FGF";
		private static const _NCA : String = "NCA";
		/*FIN*/
		 
		
		public static function obtenerParametro(_constanteBuscada:String):ParametrosEdificioTO{
			
				for each(var obj:ParametrosEdificioTO in UsuarioTO.getInstance().edificioActual.arrCollParametrosEdificio){
					if(obj.simboloParametro ==_constanteBuscada)
						return obj;
				}
			
			return null;
		}
		
	 
		public static function get RSE ():ParametrosEdificioTO{
			return obtenerParametro(_RSE);
		}
		public static function get RSI ():ParametrosEdificioTO{
			return obtenerParametro(_RSI);
		} 
		//--
		public static function get RSE_SUPERIOR_EXTERIOR ():ParametrosEdificioTO{
			return obtenerParametro(_RSE_SUPERIOR_EXTERIOR);
		}
		public static function get RSI_SUPERIOR_EXTERIOR ():ParametrosEdificioTO{
			return obtenerParametro(_RSI_SUPERIOR_EXTERIOR);
		}
		public static function get RSE_MUROS_EXTERIOR ():ParametrosEdificioTO{
			return obtenerParametro(_RSE_MUROS_EXTERIOR);
		}
		public static function get RSI_MUROS_EXTERIOR ():ParametrosEdificioTO{
			return obtenerParametro(_RSI_MUROS_EXTERIOR);
		}
		public static function get RSE_BASE_EXTERIOR ():ParametrosEdificioTO{
			return obtenerParametro(_RSE_BASE_EXTERIOR);
		}
		public static function get RSI_BASE_EXTERIOR ():ParametrosEdificioTO{
			return obtenerParametro(_RSI_BASE_EXTERIOR);
		}
		//--
		public static function get FVA ():ParametrosEdificioTO{
			return obtenerParametro(_FVA);
		}
		public static function get FSU ():ParametrosEdificioTO{
			return obtenerParametro(_FSU);
		} 
		public static function get FE ():ParametrosEdificioTO{
			return obtenerParametro(_FE);
		} 
		public static function get FG1 ():ParametrosEdificioTO{
			return obtenerParametro(_FG1);
		}
		
		/*
		 * PERDIDAS POR VENTILACIÓN
		 */
		public static function get FPV():ParametrosEdificioTO{
			return obtenerParametro(_FPV);
		}
		
		/*
		 * GANANCIA TERMICA POR SOL DE INVIERNO / VERANO
		 */
		public static function get FGSI1():ParametrosEdificioTO{
			return obtenerParametro(_FGSI1);
		}
		public static function get FGSI2():ParametrosEdificioTO{
			return obtenerParametro(_FGSI2);
		}
		public static function get FGSI3():ParametrosEdificioTO{
			return obtenerParametro(_FGSI3);
		}
		public static function get FGSI4():ParametrosEdificioTO{
			return obtenerParametro(_FGSI4);
		}
		public static function get FGSI5():ParametrosEdificioTO{
			return obtenerParametro(_FGSI5);
		}
		public static function get FGSI6():ParametrosEdificioTO{
			return obtenerParametro(_FGSI6);
		}
			//VERANO 
		public static function get FGSV1():ParametrosEdificioTO{
			return obtenerParametro(_FGSV1);
		}
		public static function get FGSV2():ParametrosEdificioTO{
			return obtenerParametro(_FGSV2);
		}
		public static function get FGSV3():ParametrosEdificioTO{
			return obtenerParametro(_FGSV3);
		}
		public static function get FGSV4():ParametrosEdificioTO{
			return obtenerParametro(_FGSV4);
		}
		public static function get FGSV5():ParametrosEdificioTO{
			return obtenerParametro(_FGSV5);
		}
		public static function get FGSV6():ParametrosEdificioTO{
			return obtenerParametro(_FGSV6);
		}
			//Otros Ganancia Térmica
		public static function get FS():ParametrosEdificioTO{
			return obtenerParametro(_FS);
		}
		public static function get FM():ParametrosEdificioTO{
			return obtenerParametro(_FM);
		}
		public static function get FC():ParametrosEdificioTO{
			return obtenerParametro(_FC);
		}
		public static function get AVI():ParametrosEdificioTO{
			return obtenerParametro(_AVI);
		} 
		
		
		public static function get FPC():ParametrosEdificioTO{
			return obtenerParametro(_FPC);
		}
		public static function get FGC():ParametrosEdificioTO{
			return obtenerParametro(_FGC);
		}
		public static function get FPF():ParametrosEdificioTO{
			return obtenerParametro(_FPF);
		}
		public static function get FGF():ParametrosEdificioTO{
			return obtenerParametro(_FGF);
		} 
 		public static function get AC():ParametrosEdificioTO{
			return obtenerParametro(_AC);
		}
 		public static function get NCA():ParametrosEdificioTO{
			return obtenerParametro(_NCA);
		}
 		public static function get PVT():ParametrosEdificioTO{
			return obtenerParametro(_PVT);
		}
 		public static function get PVP():ParametrosEdificioTO{
			return obtenerParametro(_PVP);
		}
		public static function get QI():ParametrosEdificioTO{
			return obtenerParametro(_QI);
		}
		public static function get G():ParametrosEdificioTO{
			return obtenerParametro(_G);
		}
 		
	}
}
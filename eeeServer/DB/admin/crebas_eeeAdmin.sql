/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     1/8/2015 3:41:53 AM                          */
/*==============================================================*/


/*==============================================================*/
/* Table: ARTEFACTOS                                            */
/*==============================================================*/
create table ARTEFACTOS (
   ID_ARTEFACTO         SERIAL               not null,
   NOMBRE_ARTEFACTO     VARCHAR(500)         null,
   CONSUMO_ARTEFACTO    FLOAT8               null,
   ID_MEDICION          VARCHAR(10)          null,
   UNIDAD_CONSUMO       VARCHAR(10)          null,
   PRECIO_ARTEFACTO     FLOAT4               null,
   ELIMINADO            BOOL                 null,
   constraint PK_ARTEFACTOS primary key (ID_ARTEFACTO)
);

/*==============================================================*/
/* Table: ARTEFACTO_COMBUSTIBLE_COMPLETO                        */
/*==============================================================*/
create table ARTEFACTO_COMBUSTIBLE_COMPLETO (
   ID_ARTEFACTO         INT4                 not null,
   POTENCIA             FLOAT4               null,
   EFICIENCIA           FLOAT4               null,
   CONSUMO              FLOAT4               null,
   EFICIENCIA_DISTRIBUCION_CALOR FLOAT4               null,
   VALOR_UNITARIO       FLOAT4               null,
   VALOR_FINAL          FLOAT4               null,
   constraint PK_ARTEFACTO_COMBUSTIBLE_COMPL primary key (ID_ARTEFACTO)
);

/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE (
   ID_CLIENTE           SERIAL               not null,
   NOMBRE_CLIENTE       VARCHAR(100)         null,
   DIRECCION_CLIENTE    VARCHAR(100)         null,
   NUMERO_TELEFONO      NUMERIC(12)          null,
   RUTA_FOTOGRAFIA      VARCHAR(10)          null,
   constraint PK_CLIENTE primary key (ID_CLIENTE)
);

/*==============================================================*/
/* Table: FACTOR_DE_RENOVACION                                  */
/*==============================================================*/
create table FACTOR_DE_RENOVACION (
   ID_FACTOR            SERIAL               not null,
   NOMBRE_FACTOR        VARCHAR(150)         null,
   CAMBIO_AIRE_MINIMO   FLOAT4               null,
   CAMBIO_AIRE_MAXIMO   FLOAT4               null,
   DISCRIMINADOR        VARCHAR(20)          null,
   constraint PK_FACTOR_DE_RENOVACION primary key (ID_FACTOR)
);

/*==============================================================*/
/* Table: MATERIALES                                            */
/*==============================================================*/
create table MATERIALES (
   ID_MATERIAL          SERIAL               not null,
   ID_TIPO_SECCION_ENVOLVENTE INT4                 null,
   NOMBRE_CORTO         VARCHAR(10)          null,
   NOMBRE_MATERIAL      VARCHAR(200)         null,
   ESPESOR_MATERIAL     FLOAT4               null,
   IMAGEN_MATERIAL      VARCHAR(50)          null,
   CONDUCTIVIDAD_TERMICA FLOAT4               null,
   TRANSMITANCIA_TERMICA FLOAT4               null,
   RESISTENCIA_TERMICA  FLOAT4               null,
   ELIMINADO            BOOL                 null,
   constraint PK_MATERIALES primary key (ID_MATERIAL)
);

/*==============================================================*/
/* Table: MATERIAL_PRECIO                                       */
/*==============================================================*/
create table MATERIAL_PRECIO (
   ID_MATERIAL          INT4                 not null,
   PRECIO_MATERIAL      NUMERIC              not null,
   UNIDAD_MATERIAL      VARCHAR(15)          null,
   constraint PK_MATERIAL_PRECIO primary key (ID_MATERIAL, PRECIO_MATERIAL)
);

/*==============================================================*/
/* Table: PARAMETROS_INICIAL_EDIFICIO                           */
/*==============================================================*/
create table PARAMETROS_INICIAL_EDIFICIO (
   ID_PARAMETRO         SERIAL               not null,
   NOMBRE_PARAMETRO     VARCHAR(200)         null,
   SIMBOLO_PARAMETRO    VARCHAR(30)          null,
   UNIDAD_PARAMETRO     VARCHAR(5)           null,
   VALOR_PARAMETRO      FLOAT4               null,
   constraint PK_PARAMETROS_INICIAL_EDIFICIO primary key (ID_PARAMETRO)
);

/*==============================================================*/
/* Table: TIPO_CONSUMO_SENSOR                                   */
/*==============================================================*/
create table TIPO_CONSUMO_SENSOR (
   ID_SENSOR            SERIAL               not null,
   ID_TIPO_MEDICION     VARCHAR(5)           not null,
   NOMBRE_TIPO_MEDICION VARCHAR(30)          null,
   DESCRIPCION_TIPO_MEDICION VARCHAR(100)         null,
   UNIDAD_MEDICION      VARCHAR(10)          null,
   constraint PK_TIPO_CONSUMO_SENSOR primary key (ID_TIPO_MEDICION)
);

/*==============================================================*/
/* Table: TIPO_SECCION_ENVOLVENTE                               */
/*==============================================================*/
create table TIPO_SECCION_ENVOLVENTE (
   ID_TIPO_SECCION_ENVOLVENTE SERIAL               not null,
   ID_TIPO_SECCION_ENVOLVENTE_PADR INT4                 null,
   NOMBRE_SECCION_ENVOLVENTE VARCHAR(50)          not null,
   constraint PK_TIPO_SECCION_ENVOLVENTE primary key (ID_TIPO_SECCION_ENVOLVENTE)
);

/*==============================================================*/
/* Table: TIPO_USUARIO                                          */
/*==============================================================*/
create table TIPO_USUARIO (
   ID_TIPO_USUARIO      NUMERIC              not null,
   TIPO_USUARIO         VARCHAR(20)          null,
   constraint PK_TIPO_USUARIO primary key (ID_TIPO_USUARIO)
);

/*==============================================================*/
/* Table: USUARIO                                               */
/*==============================================================*/
create table USUARIO (
   ID_USUARIO           SERIAL               not null,
   ID_TIPO_USUARIO      NUMERIC              null,
   NOMBRE_COMPLETO      VARCHAR(100)         null,
   NOMBRE_USUARIO       VARCHAR(10)          not null,
   CONTRASENA           VARCHAR(8)           not null,
   TELEFONO             NUMERIC              null,
   EMAIL                VARCHAR(50)          null,
   constraint PK_USUARIO primary key (ID_USUARIO)
);

alter table ARTEFACTO_COMBUSTIBLE_COMPLETO
   add constraint FK_ARTEFACT_REFERENCE_ARTEFACT foreign key (ID_ARTEFACTO)
      references ARTEFACTOS (ID_ARTEFACTO)
      on delete restrict on update restrict;

alter table MATERIALES
   add constraint FK_MATERIAL_REFERENCE_TIPO_SEC foreign key (ID_TIPO_SECCION_ENVOLVENTE)
      references TIPO_SECCION_ENVOLVENTE (ID_TIPO_SECCION_ENVOLVENTE)
      on delete restrict on update restrict;

alter table MATERIAL_PRECIO
   add constraint FK_MATERIAL_REFERENCE_MATERIAL foreign key (ID_MATERIAL)
      references MATERIALES (ID_MATERIAL)
      on delete restrict on update restrict;

alter table TIPO_SECCION_ENVOLVENTE
   add constraint FK_TIPO_SEC_REFERENCE_TIPO_SEC foreign key (ID_TIPO_SECCION_ENVOLVENTE_PADR)
      references TIPO_SECCION_ENVOLVENTE (ID_TIPO_SECCION_ENVOLVENTE)
      on delete restrict on update restrict;

alter table USUARIO
   add constraint FK_USUARIO_REFERENCE_TIPO_USU foreign key (ID_TIPO_USUARIO)
      references TIPO_USUARIO (ID_TIPO_USUARIO)
      on delete restrict on update restrict;


/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     7/30/2014 5:41:50 AM                         */
/*==============================================================*/


/*==============================================================*/
/* Table: FACTOR_DE_RENOVACION                                  */
/*==============================================================*/
create table FACTOR_DE_RENOVACION (
   ID_FACTOR            SERIAL               not null,
   NOMBRE_FACTOR        VARCHAR(150)         null,
   CAMBIO_AIRE_MINIMO   FLOAT4               null,
   CAMBIO_AIRE_MAXIMO   FLOAT4               null,
   DISCRIMINADOR        VARCHAR(20)          null,
   constraint PK_FACTOR_DE_RENOVACION primary key (ID_FACTOR)
);

/*==============================================================*/
/* Table: MATERIALES                                            */
/*==============================================================*/
create table MATERIALES (
   ID_MATERIAL          SERIAL               not null,
   ID_TIPO_SECCION_ENVOLVENTE INT4                 null,
   NOMBRE_CORTO         VARCHAR(10)          null,
   NOMBRE_MATERIAL      VARCHAR(200)         null,
   ESPESOR_MATERIAL     FLOAT4               null,
   IMAGEN_MATERIAL      VARCHAR(50)          null,
   CONDUCTIVIDAD_TERMICA FLOAT4               null,
   TRANSMITANCIA_TERMICA FLOAT4               null,
   RESISTENCIA_TERMICA  FLOAT4               null,
   ELIMINADO            BOOL                 null,
   constraint PK_MATERIALES primary key (ID_MATERIAL)
);

/*==============================================================*/
/* Table: MATERIAL_PRECIO                                       */
/*==============================================================*/
create table MATERIAL_PRECIO (
   ID_MATERIAL          INT4                 not null,
   PRECIO_MATERIAL      NUMERIC              not null,
   UNIDAD_MATERIAL      VARCHAR(15)          null,
   constraint PK_MATERIAL_PRECIO primary key (ID_MATERIAL, PRECIO_MATERIAL)
);

/*==============================================================*/
/* Table: PARAMETROS_INICIAL_EDIFICIO                           */
/*==============================================================*/
create table PARAMETROS_INICIAL_EDIFICIO (
   ID_PARAMETRO         SERIAL               not null,
   NOMBRE_PARAMETRO     VARCHAR(200)         null,
   SIMBOLO_PARAMETRO    VARCHAR(30)          null,
   UNIDAD_PARAMETRO     VARCHAR(5)           null,
   VALOR_PARAMETRO      FLOAT4               null,
   constraint PK_PARAMETROS_INICIAL_EDIFICIO primary key (ID_PARAMETRO)
);

/*==============================================================*/
/* Table: TIPO_SECCION_ENVOLVENTE                               */
/*==============================================================*/
create table TIPO_SECCION_ENVOLVENTE (
   ID_TIPO_SECCION_ENVOLVENTE SERIAL               not null,
   ID_TIPO_SECCION_ENVOLVENTE_PADR INT4                 null,
   NOMBRE_SECCION_ENVOLVENTE VARCHAR(50)          not null,
   constraint PK_TIPO_SECCION_ENVOLVENTE primary key (ID_TIPO_SECCION_ENVOLVENTE)
);

/*==============================================================*/
/* Table: TIPO_USUARIO                                          */
/*==============================================================*/
create table TIPO_USUARIO (
   ID_TIPO_USUARIO      NUMERIC              not null,
   TIPO_USUARIO         VARCHAR(20)          null,
   constraint PK_TIPO_USUARIO primary key (ID_TIPO_USUARIO)
);

/*==============================================================*/
/* Table: USUARIO                                               */
/*==============================================================*/
create table USUARIO (
   ID_USUARIO           SERIAL               not null,
   ID_TIPO_USUARIO      NUMERIC              null,
   NOMBRE_COMPLETO      VARCHAR(100)         null,
   NOMBRE_USUARIO       VARCHAR(10)          not null,
   CONTRASENA           VARCHAR(8)           not null,
   TELEFONO             NUMERIC              null,
   EMAIL                VARCHAR(50)          null,
   constraint PK_USUARIO primary key (ID_USUARIO)
);

alter table MATERIALES
   add constraint FK_MATERIAL_REFERENCE_TIPO_SEC foreign key (ID_TIPO_SECCION_ENVOLVENTE)
      references TIPO_SECCION_ENVOLVENTE (ID_TIPO_SECCION_ENVOLVENTE)
      on delete restrict on update restrict;

alter table MATERIAL_PRECIO
   add constraint FK_MATERIAL_REFERENCE_MATERIAL foreign key (ID_MATERIAL)
      references MATERIALES (ID_MATERIAL)
      on delete restrict on update restrict;

alter table TIPO_SECCION_ENVOLVENTE
   add constraint FK_TIPO_SEC_REFERENCE_TIPO_SEC foreign key (ID_TIPO_SECCION_ENVOLVENTE_PADR)
      references TIPO_SECCION_ENVOLVENTE (ID_TIPO_SECCION_ENVOLVENTE)
      on delete restrict on update restrict;

alter table USUARIO
   add constraint FK_USUARIO_REFERENCE_TIPO_USU foreign key (ID_TIPO_USUARIO)
      references TIPO_USUARIO (ID_TIPO_USUARIO)
      on delete restrict on update restrict;


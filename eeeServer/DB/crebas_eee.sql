/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     11/23/2014 5:42:23 AM                        */
/*==============================================================*/


/*==============================================================*/
/* Table: ALERTAS                                               */
/*==============================================================*/
create table ALERTAS (
   ID_SENSOR            TIMESTAMP WITH TIME ZONE null,
   MEDICION             FLOAT8               null,
   PROMEDIO_MEDICION    FLOAT8               null
);

/*==============================================================*/
/* Table: ALTERNATIVA                                           */
/*==============================================================*/
create table ALTERNATIVA (
   ID_ALTERNATIVA       SERIAL               not null,
   ID_TIPO_ALTERNATIVA  INT4                 null,
   ID_EDIFICIO          INT4                 null,
   DESCRIPCION_ALTERNATIVA VARCHAR(500)         null,
   constraint PK_ALTERNATIVA primary key (ID_ALTERNATIVA)
);

/*==============================================================*/
/* Table: ALT_CAMBIO_CALEFACCION                                */
/*==============================================================*/
create table ALT_CAMBIO_CALEFACCION (
   ID_DETALLE           INT4                 null
);

/*==============================================================*/
/* Table: ALT_CAMBIO_ENVOLVENTE                                 */
/*==============================================================*/
create table ALT_CAMBIO_ENVOLVENTE (
   ID_DETALLE           INT4                 null
);

/*==============================================================*/
/* Table: ARTEFACTOS                                            */
/*==============================================================*/
create table ARTEFACTOS (
   ID_ARTEFACTO         INT8                 not null,
   NOMBRE_ARTEFACTO     VARCHAR(500)         null,
   CONSUMO_ARTEFACTO    FLOAT8               null,
   UNIDAD_CONSUMO       VARCHAR(10)          null,
   ID_MEDICION          VARCHAR(10)          null,
   PRECIO_ARTEFACTO     FLOAT4               null,
   ELIMINADO            BOOL                 null,
   constraint PK_ARTEFACTOS primary key (ID_ARTEFACTO)
);

/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE (
   ID_CLIENTE           SERIAL               not null,
   NOMBRE_CLIENTE       VARCHAR(100)         null,
   DIRECCION_CLIENTE    VARCHAR(100)         null,
   NUMERO_TELEFONO      NUMERIC(12)          null,
   RUTA_FOTOGRAFIA      VARCHAR(10)          null,
   constraint PK_CLIENTE primary key (ID_CLIENTE)
);

/*==============================================================*/
/* Table: COMBUSTIBLE_METODO_CALEFACCION                        */
/*==============================================================*/
create table COMBUSTIBLE_METODO_CALEFACCION (
   ID_METODO_CALEFACCION INT4                 null,
   ID_COMBUSTIBLE       SERIAL               not null,
   PRE_ID_COMBUSTIBLE   INT4                 null,
   NOMBRE_COMBUSTIBLE   VARCHAR(20)          null,
   constraint PK_COMBUSTIBLE_METODO_CALEFACC primary key (ID_COMBUSTIBLE)
);

/*==============================================================*/
/* Table: COMPOSICION_ALTERNATIVA                               */
/*==============================================================*/
create table COMPOSICION_ALTERNATIVA (
   ID_ALTERNATIVA       INT4                 not null,
   ID_MATERIAL          INT4                 not null,
   CANTIDAD_MATERIAL    INT4                 null,
   ID_TIPO_SECCION      INT4                 null
);

/*==============================================================*/
/* Table: COMPOSICION_ALTERNATIVA_ARTEFAC                       */
/*==============================================================*/
create table COMPOSICION_ALTERNATIVA_ARTEFAC (
   ID_ALTERNATIVA       INT4                 not null,
   ID_SENSOR            VARCHAR(12)          not null,
   CANTIDAD_ARTEFACTOS  INT4                 null,
   ID_ARTEFACTO         INT4                 null
);

/*==============================================================*/
/* Table: COMPOSICION_ARTEFACTOS                                */
/*==============================================================*/
create table COMPOSICION_ARTEFACTOS (
   ID_SENSOR            VARCHAR(12)          null,
   ID_ARTEFACTO         INT4                 null,
   CANTIDAD_ARTEFACTOS  INT8                 null
);

/*==============================================================*/
/* Table: COMPOSICION_ENVOLVENTE                                */
/*==============================================================*/
create table COMPOSICION_ENVOLVENTE (
   ID_MATERIAL          INT4                 not null,
   ID_TIPO_SECCION_ENVOLVENTE INT4                 not null,
   ID_EDIFICIO          INT4                 not null,
   CANTIDAD_MATERIAL    INT4                 null
);

/*==============================================================*/
/* Table: DETALLE_ALTRNATIVA                                    */
/*==============================================================*/
create table DETALLE_ALTRNATIVA (
   ID_DETALLE           SERIAL               not null,
   ID_ALTERNATIVA       INT4                 null,
   ID_TABLA_TIPO        INT2                 null,
   constraint PK_DETALLE_ALTRNATIVA primary key (ID_DETALLE)
);

/*==============================================================*/
/* Table: DETALLE_ARTEFACTOS_COMBUSTIBLE                        */
/*==============================================================*/
create table DETALLE_ARTEFACTOS_COMBUSTIBLE (
   ID_ARTEFACTO         INT4                 null
);

/*==============================================================*/
/* Table: DIFERENCIA_ARTEFACTOS                                 */
/*==============================================================*/
create table DIFERENCIA_ARTEFACTOS (
   ID_ALTERNATIVA       INT4                 null,
   ID_ARTEFACTO         INT4                 null,
   CANTIDAD_ARTEFACTO   FLOAT4               null,
   ID_MEDIICION         VARCHAR(2)           null
);

/*==============================================================*/
/* Table: DIFERENCIA_MATERIALES                                 */
/*==============================================================*/
create table DIFERENCIA_MATERIALES (
   ID_ALTERNATIVA       INT4                 not null,
   ID_MATERIAL          INT4                 not null,
   CANTIDAD_MATERIAL    INT4                 null
);

/*==============================================================*/
/* Table: EDIFICIO                                              */
/*==============================================================*/
create table EDIFICIO (
   ID_EDIFICIO          SERIAL               not null,
   ID_CLIENTE           INT4                 null,
   ID_FACTOR            INT4                 null,
   NOMBRE_EDIFICIO      VARCHAR(50)          null,
   RUTA_FOTOGRAFIA      VARCHAR(200)         null,
   DIRECCION_EDIFICIO   VARCHAR(200)         null,
   LONGITUD_EDIFICIO    FLOAT8               null,
   LATITUD_EDIFICIO     FLOAT8               null,
   ALTURA_PISO          FLOAT4               null,
   LARGO                FLOAT4               null,
   ANCHO                FLOAT4               null,
   OBSERVACIONES_EDIFICIO VARCHAR(100)         null,
   REGION_EDIFICIO      VARCHAR(100)         null,
   CIUDAD_EDIFICIO      VARCHAR(50)          null,
   PAIS_EDIFICIO        VARCHAR(50)          null,
   SUPERFICIE_UTIL      FLOAT8               null,
   SUPERFICIE_BRUTA     FLOAT8               null,
   VOLUMEN_AIRE         FLOAT8               null,
   PERIMETRO            FLOAT8               null,
   DIRECCION_COMPLETA   VARCHAR(300)         null,
   FECHA_CREACION       DATE                 null,
   AIRE_ACONDICIONADO   BOOL                 null,
   VALOR_FACTOR_RENOVACION FLOAT4               null,
   NUMERO_ACTUAL_EDIFICIO INT4                 null,
   constraint PK_EDIFICIO primary key (ID_EDIFICIO)
);

/*==============================================================*/
/* Table: EMAIL_DATOS                                           */
/*==============================================================*/
create table EMAIL_DATOS (
   USUARIO_EMAIL        VARCHAR(50)          null,
   CONTRASENA_EMAIL     VARCHAR(50)          null,
   REMITENTE            VARCHAR(25)          null,
   PIE                  VARCHAR(25)          null
);

/*==============================================================*/
/* Table: EVALUACION_ECONOMICA_DETALLE                          */
/*==============================================================*/
create table EVALUACION_ECONOMICA_DETALLE (
   ID_ALTERNATIVA       INT4                 not null,
   ID_PARAMETRO_EVALUACION INT4                 null,
   VAN_VALOR_ACTUAL_NETO FLOAT8               null,
   PAYBACK_PLAZO_DE_RECUPERACION INT4                 null,
   TIR_TASA_INTERNA_RETORNO FLOAT8               null,
   FECHA_CREACION       TIMESTAMP WITH TIME ZONE null,
   FECHA_SIGUIENTE_EVALUACION TIMESTAMP WITH TIME ZONE null,
   FECHA_ULTIMA_EVALUACION TIME WITH TIME ZONE  null,
   constraint PK_EVALUACION_ECONOMICA_DETALL primary key (ID_ALTERNATIVA)
);

/*==============================================================*/
/* Table: FACTOR_DE_RENOVACION                                  */
/*==============================================================*/
create table FACTOR_DE_RENOVACION (
   ID_FACTOR            INT4                 not null,
   NOMBRE_FACTOR        VARCHAR(150)         null,
   CAMBIO_AIRE_MINIMO   FLOAT4               null,
   CAMBIO_AIRE_MAXIMO   FLOAT4               null,
   DISCRIMINADOR        VARCHAR(20)          null,
   constraint PK_FACTOR_DE_RENOVACION primary key (ID_FACTOR)
);

/*==============================================================*/
/* Table: FECHA_SINCRONIZACION                                  */
/*==============================================================*/
create table FECHA_SINCRONIZACION (
   SINCRONIZACION_DATOS TIMESTAMP            null,
   ID_FECHA_SINCRO      INT2                 null
);

/*==============================================================*/
/* Table: FECHA_ULTIMA_EVALUACION_ALTERNA                       */
/*==============================================================*/
create table FECHA_ULTIMA_EVALUACION_ALTERNA (
   ID_FECHA_ULTIMA_EVALUACION SERIAL               not null,
   ID_ALTERNATIVA       INT4                 not null,
   ULTIMA_EVALUACION_ALTERNATIVA DATE                 null,
   SIGUIENTE_EVALUACION_ALTERNATIV DATE                 null,
   constraint PK_FECHA_ULTIMA_EVALUACION_ALT primary key (ID_FECHA_ULTIMA_EVALUACION, ID_ALTERNATIVA)
);

/*==============================================================*/
/* Table: INVERSION_ALTERNATIVA                                 */
/*==============================================================*/
create table INVERSION_ALTERNATIVA (
   ID_ALTERNATIVA       INT4                 not null,
   COSTO_TOTAL_ALTERNATIVA FLOAT4               null,
   constraint PK_INVERSION_ALTERNATIVA primary key (ID_ALTERNATIVA)
);

/*==============================================================*/
/* Table: INVERSION_ARTEFACTOS_INICIAL                          */
/*==============================================================*/
create table INVERSION_ARTEFACTOS_INICIAL (
   ID_EDIFICIO          INT4                 not null,
   COSTO_TOTAL_ARTEFACTOS FLOAT4               null,
   constraint PK_INVERSION_ARTEFACTOS_INICIA primary key (ID_EDIFICIO)
);

/*==============================================================*/
/* Table: INVERSION_EDIFICIO                                    */
/*==============================================================*/
create table INVERSION_EDIFICIO (
   ID_EDIFICIO          INT4                 not null,
   COSTO_TOTAL_EDIFICIO FLOAT4               null,
   constraint PK_INVERSION_EDIFICIO primary key (ID_EDIFICIO)
);

/*==============================================================*/
/* Table: IP_SINCRONIZACION                                     */
/*==============================================================*/
create table IP_SINCRONIZACION (
   IP_SINCRONIZACION    VARCHAR(50)          null
);

/*==============================================================*/
/* Table: MATERIALES                                            */
/*==============================================================*/
create table MATERIALES (
   ID_MATERIAL          INT8                 not null,
   ID_TIPO_SECCION_ENVOLVENTE INT4                 null,
   NOMBRE_CORTO         VARCHAR(10)          null,
   NOMBRE_MATERIAL      VARCHAR(200)         null,
   ESPESOR_MATERIAL     FLOAT4               null,
   IMAGEN_MATERIAL      VARCHAR(50)          null,
   CONDUCTIVIDAD_TERMICA FLOAT4               null,
   TRANSMITANCIA_TERMICA FLOAT4               null,
   RESISTENCIA_TERMICA  FLOAT4               null,
   ELIMINADO            BOOL                 null,
   constraint PK_MATERIALES primary key (ID_MATERIAL)
);

/*==============================================================*/
/* Table: MATERIAL_PRECIO                                       */
/*==============================================================*/
create table MATERIAL_PRECIO (
   ID_MATERIAL          INT4                 not null,
   PRECIO_MATERIAL      NUMERIC              not null,
   UNIDAD_MATERIAL      VARCHAR(15)          null,
   constraint PK_MATERIAL_PRECIO primary key (ID_MATERIAL, PRECIO_MATERIAL)
);

/*==============================================================*/
/* Table: MEDICION_SENSOR                                       */
/*==============================================================*/
create table MEDICION_SENSOR (
   FECHA_HORA           TIMESTAMP            not null,
   ID_SENSOR            VARCHAR(12)          not null,
   ID_CONSUMO           VARCHAR(5)           not null,
   VALOR_MEDICION       FLOAT8               null,
   ID_EDIFICIO          FLOAT4               null,
   ID_PISO              FLOAT4               null,
   ID_ZONA              FLOAT4               null,
   ESALERTA             BOOL                 null,
   constraint PK_MEDICION_SENSOR primary key (FECHA_HORA, ID_SENSOR, ID_CONSUMO)
);

/*==============================================================*/
/* Table: MEDICION_SENSOR_TEST                                  */
/*==============================================================*/
create table MEDICION_SENSOR_TEST (
   FECHA_HORA           TIMESTAMP            not null,
   ID_SENSOR            VARCHAR(12)          not null,
   ID_CONSUMO           VARCHAR(5)           not null,
   VALOR_MEDICION       FLOAT8               null,
   ID_EDIFICIO          FLOAT4               null,
   ID_PISO              FLOAT4               null,
   ID_ZONA              FLOAT4               null,
   ESALERTA             BOOL                 null,
   constraint PK_MEDICION_SENSOR_TEST primary key (FECHA_HORA, ID_SENSOR, ID_CONSUMO)
);

/*==============================================================*/
/* Table: METODO_CALEFACCION                                    */
/*==============================================================*/
create table METODO_CALEFACCION (
   ID_METODO_CALEFACCION SERIAL               not null,
   NOMBRE_METODO_CALEFACCION VARCHAR(20)          null,
   EFICIENCIA_GOBAL     FLOAT4               null,
   constraint PK_METODO_CALEFACCION primary key (ID_METODO_CALEFACCION)
);

/*==============================================================*/
/* Table: MOMENTO_VALOR_CRITICO                                 */
/*==============================================================*/
create table MOMENTO_VALOR_CRITICO (
   ID_MOMENTO           SERIAL               not null,
   NOMBRE_MOMENTO       VARCHAR(10)          null,
   constraint PK_MOMENTO_VALOR_CRITICO primary key (ID_MOMENTO)
);

/*==============================================================*/
/* Table: PARAMETROS_CALCULADOS_ALTERNATI                       */
/*==============================================================*/
create table PARAMETROS_CALCULADOS_ALTERNATI (
   ID_ALTERNATIVA       INT4                 not null,
   TRANSMITANCIA        FLOAT4               not null,
   RESISTENCIA_TOTAL    FLOAT4               not null,
   ID_TIPO_SECCION      INT4                 null
);

/*==============================================================*/
/* Table: PARAMETROS_CALCULADOS_COMPOSICI                       */
/*==============================================================*/
create table PARAMETROS_CALCULADOS_COMPOSICI (
   ID_TIPO_SECCION_ENVOLVENTE INT4                 not null,
   ID_EDIFICIO          INT4                 not null,
   TRANSMITANCIA        FLOAT4               not null,
   RESISTENCIA_TOTAL    FLOAT4               not null,
   RSE                  FLOAT4               null,
   RSI                  FLOAT4               null
);

/*==============================================================*/
/* Table: PARAMETROS_EDIFICIO                                   */
/*==============================================================*/
create table PARAMETROS_EDIFICIO (
   ID_EDIFICIO          INT4                 null,
   ID_PARAMETRO         INT4                 null,
   VALOR_PARAMETRO      FLOAT4               null
);

/*==============================================================*/
/* Table: PARAMETROS_EVALUACION                                 */
/*==============================================================*/
create table PARAMETROS_EVALUACION (
   ID_PARAMETRO_EVALUACION SERIAL               not null,
   NOMBRE_PARAMETRO     VARCHAR(100)         null,
   HORIZONTE_EVALUACION INT2                 null,
   TASA_DESCUENTO       FLOAT4               null,
   VARIACION_PRECIO     FLOAT4               null,
   TARIFA_COMBUSTIBLE   FLOAT4               null,
   EFICIENCIA_GLOBAL    FLOAT4               null,
   TARIFA_FINAL         FLOAT4               null,
   constraint PK_PARAMETROS_EVALUACION primary key (ID_PARAMETRO_EVALUACION)
);

/*==============================================================*/
/* Table: PARAMETROS_INICIAL_EDIFICIO                           */
/*==============================================================*/
create table PARAMETROS_INICIAL_EDIFICIO (
   ID_PARAMETRO         INT4                 not null,
   NOMBRE_PARAMETRO     VARCHAR(200)         null,
   SIMBOLO_PARAMETRO    VARCHAR(30)          null,
   UNIDAD_PARAMETRO     VARCHAR(5)           null,
   VALOR_PARAMETRO      FLOAT4               null,
   constraint PK_PARAMETROS_INICIAL_EDIFICIO primary key (ID_PARAMETRO)
);

/*==============================================================*/
/* Table: PISO                                                  */
/*==============================================================*/
create table PISO (
   ID_PISO              SERIAL               not null,
   ID_EDIFICIO          INT4                 not null,
   NOMBRE_PISO          VARCHAR(30)          null,
   constraint PK_PISO primary key (ID_PISO)
);

/*==============================================================*/
/* Table: PRECIO_CALEFACCION                                    */
/*==============================================================*/
create table PRECIO_CALEFACCION (
   ID_COMBUSTIBLE       INT4                 not null,
   PRECIO_COMBUSTIBLE   FLOAT4               null,
   constraint PK_PRECIO_CALEFACCION primary key (ID_COMBUSTIBLE)
);

/*==============================================================*/
/* Table: REQUERIMIENTO_ENERGETICO_ALTERN                       */
/*==============================================================*/
create table REQUERIMIENTO_ENERGETICO_ALTERN (
   ID_ALTERNATIVA       INT4                 not null,
   PERDIDA_POR_TRANSMISION FLOAT4               null,
   PERDIDA_POR_VENTILACION FLOAT4               null,
   GANANCIA_TERMICA_VERANO FLOAT4               null,
   GANANCIA_TERMICA_INVIERNO FLOAT4               null,
   GANANCIA_INTERNA     FLOAT4               null,
   REQUERIMIENTO_CALOR_TOTAL FLOAT4               null,
   REQUERIMIENTO_FRIO_TOTAL FLOAT4               null,
   REQUERIMIENTO_ALTERNATIVA_TATAL FLOAT4               null,
   constraint PK_REQUERIMIENTO_ENERGETICO_AL primary key (ID_ALTERNATIVA)
);

/*==============================================================*/
/* Table: REQUERIMIENTO_ENERGETICO_EDIFIC                       */
/*==============================================================*/
create table REQUERIMIENTO_ENERGETICO_EDIFIC (
   ID_EDIFICIO          INT4                 not null,
   PERDIDA_POR_TRANSMISION FLOAT4               null,
   PERDIDA_POR_VENTILACION FLOAT4               null,
   GANANCIA_TERMICA_VERANO FLOAT4               null,
   GANANCIA_TERMICA_INVIERNO FLOAT4               null,
   GANANCIA_INTERNA     FLOAT4               null,
   REQUERIMIENTO_CALOR_TOTAL FLOAT4               null,
   REQUERIMIENTO_FRIO_TOTAL FLOAT4               null,
   REQUERIMIENTO_TOTAL_EDIFICIO FLOAT8               null,
   constraint PK_REQUERIMIENTO_ENERGETICO_ED primary key (ID_EDIFICIO)
);

/*==============================================================*/
/* Table: REQ_ENERGETICO_ARTEFACTOS_ALT                         */
/*==============================================================*/
create table REQ_ENERGETICO_ARTEFACTOS_ALT (
   ID_ALTERNATIVA       INT4                 not null,
   REQUERIMIENTO_TOTAL  FLOAT8               null,
   TIPO_MEDICION        VARCHAR(2)           null,
   UNIDAD_MEDICION_ALTERNATIVA VARCHAR(6)           null,
   constraint PK_REQ_ENERGETICO_ARTEFACTOS_A primary key (ID_ALTERNATIVA)
);

/*==============================================================*/
/* Table: REQ_ENERGETICO_ARTEFAFACTOS                           */
/*==============================================================*/
create table REQ_ENERGETICO_ARTEFAFACTOS (
   ID_SENSOR            VARCHAR(12)          not null,
   REQUERIMIENTO_TOTAL  FLOAT8               null,
   constraint PK_REQ_ENERGETICO_ARTEFAFACTOS primary key (ID_SENSOR)
);

/*==============================================================*/
/* Table: RESUEMEN_REQUERIMIENTO_ALTERNAT                       */
/*==============================================================*/
create table RESUEMEN_REQUERIMIENTO_ALTERNAT (
   ID_ALTERNATIVA       INT4                 null,
   CONSUMO_TOTAL        FLOAT4               null,
   UNIDAD_CONSUMO       VARCHAR(10)          null,
   ID_TIPO_ALTERNATIVA  INT2                 null
);

/*==============================================================*/
/* Table: SECCIONES_ENVOLVENTE_EDIFICIO                         */
/*==============================================================*/
create table SECCIONES_ENVOLVENTE_EDIFICIO (
   ID_SECCION           INT4                 not null,
   ID_EDIFICIO          INT4                 not null,
   ID_TIPO_SECCION_ENVOLVENTE INT4                 null,
   ID_TIPO_SECCION_ENVOLVENTE_PADR INT4                 null,
   AREA_SECCION         FLOAT4               null,
   constraint PK_SECCIONES_ENVOLVENTE_EDIFIC primary key (ID_SECCION)
);

/*==============================================================*/
/* Table: SENSOR                                                */
/*==============================================================*/
create table SENSOR (
   ID_SENSOR            VARCHAR(12)          not null,
   ID_CONSUMO           VARCHAR(5)           not null,
   ID_EDIFICIO          INT4                 null,
   ELIMINADO            BOOL                 null,
   DESCRIPCION_ARTEFACTOS VARCHAR(500)         null,
   HORAS_POR_DIA        INT2                 null,
   DIAS_POR_MES         INT2                 null,
   CONSUMO_ANUAL_ESTIMADO FLOAT8               null,
   constraint PK_SENSOR primary key (ID_SENSOR)
);

/*==============================================================*/
/* Table: SENSOR_EDIFICIO                                       */
/*==============================================================*/
create table SENSOR_EDIFICIO (
   ID_EDIFICIO          INT4                 not null,
   ID_SENSOR            VARCHAR(12)          not null,
   constraint PK_SENSOR_EDIFICIO primary key (ID_SENSOR)
);

/*==============================================================*/
/* Table: SENSOR_PISO                                           */
/*==============================================================*/
create table SENSOR_PISO (
   ID_PISO              INT4                 not null,
   ID_SENSOR            VARCHAR(12)          not null,
   constraint PK_SENSOR_PISO primary key (ID_SENSOR)
);

/*==============================================================*/
/* Table: SENSOR_ZONA                                           */
/*==============================================================*/
create table SENSOR_ZONA (
   ID_ZONA              INT4                 not null,
   ID_SENSOR            VARCHAR(12)          not null,
   constraint PK_SENSOR_ZONA primary key (ID_SENSOR)
);

/*==============================================================*/
/* Table: TIPO_ALTERNATIVA                                      */
/*==============================================================*/
create table TIPO_ALTERNATIVA (
   ID_TIPO_ALTERNATIVA  SERIAL               not null,
   NOMBRE_TIPO_ALTERNATIVA VARCHAR(20)          null,
   OBSERVACION_TIPO_ALTERNATIVA VARCHAR(500)         null,
   constraint PK_TIPO_ALTERNATIVA primary key (ID_TIPO_ALTERNATIVA)
);

/*==============================================================*/
/* Table: TIPO_CONSUMO_SENSOR                                   */
/*==============================================================*/
create table TIPO_CONSUMO_SENSOR (
   ID_SENSOR            SERIAL               not null,
   ID_TIPO_CONSUMO      VARCHAR(5)           not null,
   ID_TIPO_MEDICION     VARCHAR(5)           null,
   NOMBRE_TIPO_CONSUMO  VARCHAR(30)          null,
   DESCRIPCION_TIPO_CONSUMO VARCHAR(100)         null,
   ELIMINADO            BOOL                 null,
   UNIDAD_MEDICION      VARCHAR(10)          null,
   constraint PK_TIPO_CONSUMO_SENSOR primary key (ID_TIPO_CONSUMO)
);

/*==============================================================*/
/* Table: TIPO_SECCION_ENVOLVENTE                               */
/*==============================================================*/
create table TIPO_SECCION_ENVOLVENTE (
   ID_TIPO_SECCION_ENVOLVENTE SERIAL               not null,
   ID_TIPO_SECCION_ENVOLVENTE_PADR INT4                 null,
   NOMBRE_SECCION_ENVOLVENTE VARCHAR(50)          not null,
   constraint PK_TIPO_SECCION_ENVOLVENTE primary key (ID_TIPO_SECCION_ENVOLVENTE)
);

/*==============================================================*/
/* Table: TIPO_USUARIO                                          */
/*==============================================================*/
create table TIPO_USUARIO (
   ID_TIPO_USUARIO      NUMERIC              not null,
   TIPO_USUARIO         VARCHAR(20)          null,
   constraint PK_TIPO_USUARIO primary key (ID_TIPO_USUARIO)
);

/*==============================================================*/
/* Table: USUARIO                                               */
/*==============================================================*/
create table USUARIO (
   ID_USUARIO           SERIAL               not null,
   ID_TIPO_USUARIO      NUMERIC              null,
   NOMBRE_COMPLETO      VARCHAR(100)         null,
   NOMBRE_USUARIO       VARCHAR(10)          not null,
   CONTRASENA           VARCHAR(8)           not null,
   TELEFONO             NUMERIC              null,
   EMAIL                VARCHAR(50)          null,
   constraint PK_USUARIO primary key (ID_USUARIO)
);

/*==============================================================*/
/* Table: VALOR_CRITICO                                         */
/*==============================================================*/
create table VALOR_CRITICO (
   ID_SENSOR            VARCHAR(12)          null,
   ID_MOMENTO           INT4                 null
);

/*==============================================================*/
/* Table: ZONA                                                  */
/*==============================================================*/
create table ZONA (
   ID_ZONA              SERIAL               not null,
   ID_PISO              INT4                 null,
   ID_EDIFICIO          INT4                 null,
   NOMBRE_ZONA          VARCHAR(30)          null,
   constraint PK_ZONA primary key (ID_ZONA)
);

alter table ALTERNATIVA
   add constraint FK_ALTERNAT_REFERENCE_EDIFICIO foreign key (ID_EDIFICIO)
      references EDIFICIO (ID_EDIFICIO)
      on delete cascade on update cascade;

alter table ALTERNATIVA
   add constraint FK_ALTERNAT_REFERENCE_TIPO_ALT foreign key (ID_TIPO_ALTERNATIVA)
      references TIPO_ALTERNATIVA (ID_TIPO_ALTERNATIVA)
      on delete restrict on update restrict;

alter table ALT_CAMBIO_CALEFACCION
   add constraint FK_ALT_CAMB_REFERENCE_DETALLE_ foreign key (ID_DETALLE)
      references DETALLE_ALTRNATIVA (ID_DETALLE)
      on delete restrict on update restrict;

alter table ALT_CAMBIO_ENVOLVENTE
   add constraint FK_ALT_CAMB_REFERENCE_DETALLE_ foreign key (ID_DETALLE)
      references DETALLE_ALTRNATIVA (ID_DETALLE)
      on delete restrict on update restrict;

alter table COMBUSTIBLE_METODO_CALEFACCION
   add constraint FK_COMBUSTI_REFERENCE_METODO_C foreign key (ID_METODO_CALEFACCION)
      references METODO_CALEFACCION (ID_METODO_CALEFACCION)
      on delete restrict on update restrict;

alter table COMBUSTIBLE_METODO_CALEFACCION
   add constraint FK_COMBUSTI_REFERENCE_PRECIO_C foreign key (PRE_ID_COMBUSTIBLE)
      references PRECIO_CALEFACCION (ID_COMBUSTIBLE)
      on delete restrict on update restrict;

alter table COMPOSICION_ALTERNATIVA
   add constraint FK_COMPOSIC_REFERENCE_ALTERNAT foreign key (ID_ALTERNATIVA)
      references ALTERNATIVA (ID_ALTERNATIVA)
      on delete cascade on update cascade;

alter table COMPOSICION_ALTERNATIVA_ARTEFAC
   add constraint FK_COMPOSIC_REFERENCE_ALTERNAT foreign key (ID_ALTERNATIVA)
      references ALTERNATIVA (ID_ALTERNATIVA)
      on delete cascade on update cascade;

alter table COMPOSICION_ALTERNATIVA_ARTEFAC
   add constraint FK_COMPOSIC_REFERENCE_SENSOR foreign key (ID_SENSOR)
      references SENSOR (ID_SENSOR)
      on delete cascade on update cascade;

alter table COMPOSICION_ARTEFACTOS
   add constraint FK_COMPOSIC_REFERENCE_SENSOR foreign key (ID_SENSOR)
      references SENSOR (ID_SENSOR)
      on delete cascade on update cascade;

alter table COMPOSICION_ARTEFACTOS
   add constraint FK_COMPOSIC_REFERENCE_ARTEFACT foreign key (ID_ARTEFACTO)
      references ARTEFACTOS (ID_ARTEFACTO);

alter table COMPOSICION_ENVOLVENTE
   add constraint FK_COMPOSIC_REFERENCE_TIPO_SEC foreign key (ID_TIPO_SECCION_ENVOLVENTE)
      references TIPO_SECCION_ENVOLVENTE (ID_TIPO_SECCION_ENVOLVENTE)
      on delete restrict on update restrict;

alter table COMPOSICION_ENVOLVENTE
   add constraint FK_COMPOSIC_REFERENCE_MATERIAL foreign key (ID_MATERIAL)
      references MATERIALES (ID_MATERIAL)
      on delete restrict on update restrict;

alter table COMPOSICION_ENVOLVENTE
   add constraint FK_COMPOSIC_REFERENCE_EDIFICIO foreign key (ID_EDIFICIO)
      references EDIFICIO (ID_EDIFICIO)
      on delete cascade on update cascade;

alter table DETALLE_ALTRNATIVA
   add constraint FK_DETALLE__REFERENCE_ALTERNAT foreign key (ID_ALTERNATIVA)
      references ALTERNATIVA (ID_ALTERNATIVA)
      on delete restrict on update restrict;

alter table DETALLE_ARTEFACTOS_COMBUSTIBLE
   add constraint FK_DETALLE__REFERENCE_ARTEFACT foreign key (ID_ARTEFACTO)
      references ARTEFACTOS (ID_ARTEFACTO)
      on delete restrict on update restrict;

alter table DIFERENCIA_ARTEFACTOS
   add constraint FK_DIFERENC_REFERENCE_ALTERNAT foreign key (ID_ALTERNATIVA)
      references ALTERNATIVA (ID_ALTERNATIVA)
      on delete restrict on update restrict;

alter table DIFERENCIA_MATERIALES
   add constraint FK_DIFERENC_REFERENCE_ALTERNAT foreign key (ID_ALTERNATIVA)
      references ALTERNATIVA (ID_ALTERNATIVA)
      on delete restrict on update restrict;

alter table EDIFICIO
   add constraint FK_EDIFICIO_REFERENCE_CLIENTE foreign key (ID_CLIENTE)
      references CLIENTE (ID_CLIENTE)
      on delete restrict on update restrict;

alter table EDIFICIO
   add constraint FK_EDIFICIO_REFERENCE_FACTOR_D foreign key (ID_FACTOR)
      references FACTOR_DE_RENOVACION (ID_FACTOR)
      on delete restrict on update restrict;

alter table EVALUACION_ECONOMICA_DETALLE
   add constraint FK_EVALUACI_REFERENCE_ALTERNAT foreign key (ID_ALTERNATIVA)
      references ALTERNATIVA (ID_ALTERNATIVA)
      on delete cascade on update cascade;

alter table EVALUACION_ECONOMICA_DETALLE
   add constraint FK_EVALUACI_REFERENCE_PARAMETR foreign key (ID_PARAMETRO_EVALUACION)
      references PARAMETROS_EVALUACION (ID_PARAMETRO_EVALUACION)
      on delete cascade on update cascade;

alter table FECHA_ULTIMA_EVALUACION_ALTERNA
   add constraint FK_FECHA_UL_REFERENCE_ALTERNAT foreign key (ID_ALTERNATIVA)
      references ALTERNATIVA (ID_ALTERNATIVA)
      on delete restrict on update restrict;

alter table INVERSION_ALTERNATIVA
   add constraint FK_INVERSIO_REFERENCE_ALTERNAT foreign key (ID_ALTERNATIVA)
      references ALTERNATIVA (ID_ALTERNATIVA)
      on delete cascade on update cascade;

alter table INVERSION_ARTEFACTOS_INICIAL
   add constraint FK_INVERSIO_REFERENCE_EDIFICIO foreign key (ID_EDIFICIO)
      references EDIFICIO (ID_EDIFICIO)
      on delete restrict on update restrict;

alter table INVERSION_EDIFICIO
   add constraint FK_INVERSIO_REFERENCE_EDIFICIO foreign key (ID_EDIFICIO)
      references EDIFICIO (ID_EDIFICIO)
      on delete cascade on update cascade;

alter table MATERIALES
   add constraint FK_MATERIAL_REFERENCE_TIPO_SEC foreign key (ID_TIPO_SECCION_ENVOLVENTE)
      references TIPO_SECCION_ENVOLVENTE (ID_TIPO_SECCION_ENVOLVENTE)
      on delete restrict on update restrict;

alter table MATERIAL_PRECIO
   add constraint FK_MATERIAL_REFERENCE_MATERIAL foreign key (ID_MATERIAL)
      references MATERIALES (ID_MATERIAL)
      on delete cascade on update cascade;

alter table PARAMETROS_CALCULADOS_ALTERNATI
   add constraint FK_PARAMETR_REFERENCE_ALTERNAT foreign key (ID_ALTERNATIVA)
      references ALTERNATIVA (ID_ALTERNATIVA)
      on delete cascade on update cascade;

alter table PARAMETROS_CALCULADOS_COMPOSICI
   add constraint FK_PARAMETR_REFERENCE_TIPO_SEC foreign key (ID_TIPO_SECCION_ENVOLVENTE)
      references TIPO_SECCION_ENVOLVENTE (ID_TIPO_SECCION_ENVOLVENTE)
      on delete restrict on update restrict;

alter table PARAMETROS_CALCULADOS_COMPOSICI
   add constraint FK_PARAMETR_REFERENCE_EDIFICIO foreign key (ID_EDIFICIO)
      references EDIFICIO (ID_EDIFICIO)
      on delete cascade on update cascade;

alter table PARAMETROS_EDIFICIO
   add constraint FK_PARAMETR_REFERENCE_PARAMETR foreign key (ID_PARAMETRO)
      references PARAMETROS_INICIAL_EDIFICIO (ID_PARAMETRO)
      on delete restrict on update restrict;

alter table PARAMETROS_EDIFICIO
   add constraint FK_PARAMETR_REFERENCE_EDIFICIO foreign key (ID_EDIFICIO)
      references EDIFICIO (ID_EDIFICIO)
      on delete cascade on update cascade;

alter table PISO
   add constraint FK_PISO_REFERENCE_EDIFICIO foreign key (ID_EDIFICIO)
      references EDIFICIO (ID_EDIFICIO)
      on delete cascade on update cascade;

alter table REQUERIMIENTO_ENERGETICO_ALTERN
   add constraint FK_REQUERIM_REFERENCE_ALTERNAT foreign key (ID_ALTERNATIVA)
      references ALTERNATIVA (ID_ALTERNATIVA)
      on delete cascade on update cascade;

alter table REQUERIMIENTO_ENERGETICO_EDIFIC
   add constraint FK_REQUERIM_REFERENCE_EDIFICIO foreign key (ID_EDIFICIO)
      references EDIFICIO (ID_EDIFICIO)
      on delete cascade on update cascade;

alter table REQ_ENERGETICO_ARTEFACTOS_ALT
   add constraint FK_REQ_ENER_REFERENCE_ALTERNAT foreign key (ID_ALTERNATIVA)
      references ALTERNATIVA (ID_ALTERNATIVA)
      on delete cascade on update cascade;

alter table REQ_ENERGETICO_ARTEFAFACTOS
   add constraint FK_REQ_ENER_REFERENCE_SENSOR foreign key (ID_SENSOR)
      references SENSOR (ID_SENSOR)
      on delete cascade on update cascade;

alter table RESUEMEN_REQUERIMIENTO_ALTERNAT
   add constraint FK_RESUEMEN_REFERENCE_ALTERNAT foreign key (ID_ALTERNATIVA)
      references ALTERNATIVA (ID_ALTERNATIVA)
      on delete restrict on update restrict;

alter table SECCIONES_ENVOLVENTE_EDIFICIO
   add constraint FK_SECCIONE_REFERENCE_EDIFICIO foreign key (ID_EDIFICIO)
      references EDIFICIO (ID_EDIFICIO)
      on delete cascade on update cascade;

alter table SECCIONES_ENVOLVENTE_EDIFICIO
   add constraint FK_SECCIONE_REFERENCE_TIPO_SEC foreign key (ID_TIPO_SECCION_ENVOLVENTE)
      references TIPO_SECCION_ENVOLVENTE (ID_TIPO_SECCION_ENVOLVENTE)
      on delete restrict on update restrict;

alter table SENSOR
   add constraint FK_SENSOR_REFERENCE_TIPO_CON foreign key (ID_CONSUMO)
      references TIPO_CONSUMO_SENSOR (ID_TIPO_CONSUMO)
      on delete restrict on update restrict;

alter table SENSOR
   add constraint FK_SENSOR_REFERENCE_EDIFICIO foreign key (ID_EDIFICIO)
      references EDIFICIO (ID_EDIFICIO)
      on delete cascade on update cascade;

alter table SENSOR_EDIFICIO
   add constraint FK_SENSOR_E_REFERENCE_SENSOR foreign key (ID_SENSOR)
      references SENSOR (ID_SENSOR)
      on delete cascade on update cascade;

alter table SENSOR_EDIFICIO
   add constraint FK_SENSOR_E_REFERENCE_EDIFICIO foreign key (ID_EDIFICIO)
      references EDIFICIO (ID_EDIFICIO)
      on delete cascade on update cascade;

alter table SENSOR_PISO
   add constraint FK_SENSOR_P_REFERENCE_SENSOR foreign key (ID_SENSOR)
      references SENSOR (ID_SENSOR)
      on delete cascade on update cascade;

alter table SENSOR_PISO
   add constraint FK_SENSOR_P_REFERENCE_PISO foreign key (ID_PISO)
      references PISO (ID_PISO)
      on delete cascade on update cascade;

alter table SENSOR_ZONA
   add constraint FK_SENSOR_Z_REFERENCE_ZONA foreign key (ID_ZONA)
      references ZONA (ID_ZONA)
      on delete cascade on update cascade;

alter table SENSOR_ZONA
   add constraint FK_SENSOR_Z_REFERENCE_SENSOR foreign key (ID_SENSOR)
      references SENSOR (ID_SENSOR)
      on delete cascade on update cascade;

alter table TIPO_CONSUMO_SENSOR
   add constraint FK_TIPO_CON_REFERENCE_TIPO_CON foreign key (ID_TIPO_MEDICION)
      references TIPO_CONSUMO_SENSOR (ID_TIPO_CONSUMO)
      on delete restrict on update restrict;

alter table TIPO_SECCION_ENVOLVENTE
   add constraint FK_TIPO_SEC_REFERENCE_TIPO_SEC foreign key (ID_TIPO_SECCION_ENVOLVENTE_PADR)
      references TIPO_SECCION_ENVOLVENTE (ID_TIPO_SECCION_ENVOLVENTE)
      on delete restrict on update restrict;

alter table USUARIO
   add constraint FK_USUARIO_REFERENCE_TIPO_USU foreign key (ID_TIPO_USUARIO)
      references TIPO_USUARIO (ID_TIPO_USUARIO)
      on delete restrict on update restrict;

alter table VALOR_CRITICO
   add constraint FK_VALOR_CR_REFERENCE_MOMENTO_ foreign key (ID_MOMENTO)
      references MOMENTO_VALOR_CRITICO (ID_MOMENTO)
      on delete restrict on update restrict;

alter table VALOR_CRITICO
   add constraint FK_VALOR_CR_REFERENCE_SENSOR_E foreign key (ID_SENSOR)
      references SENSOR_EDIFICIO (ID_SENSOR)
      on delete restrict on update restrict;

alter table ZONA
   add constraint FK_ZONA_REFERENCE_PISO foreign key (ID_PISO)
      references PISO (ID_PISO)
      on delete cascade on update cascade;

alter table ZONA
   add constraint FK_ZONA_REFERENCE_EDIFICIO foreign key (ID_EDIFICIO)
      references EDIFICIO (ID_EDIFICIO)
      on delete cascade on update cascade;


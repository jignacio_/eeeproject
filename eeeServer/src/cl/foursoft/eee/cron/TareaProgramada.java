package cl.foursoft.eee.cron;

import cl.foursoft.eee.util.enums.UnidadDeTiempo;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

public class TareaProgramada {

	private Timer timer = new Timer();
	
	public void programarTareaPorPeriodo(Cron t, String horaInicioHHMM ,int periodo, UnidadDeTiempo u){
		Date inicio = calcularHoraInicio(horaInicioHHMM);
		long p = calcularPeriodo(periodo, u);
		timer.scheduleAtFixedRate(t, inicio, p);		
	}
	
	private static Date calcularHoraInicio(String horaHHMM){
		Date hoy = new Date();
		
		
		Calendar c = Calendar.getInstance();
		c.setTime(hoy);
		
		int horaActual=c.get(Calendar.HOUR_OF_DAY);
		int minutoActual=c.get(Calendar.MINUTE);
		
		int horaDeseada=0;
		int minutoDeseado=0;
		
		try{
			horaDeseada = Integer.parseInt(horaHHMM.substring(0,2));
			minutoDeseado = Integer.parseInt(horaHHMM.substring(2,4));
		}catch(NumberFormatException e){
			e.printStackTrace();
		}
		
		long hActual= 	calcularPeriodo(horaActual, UnidadDeTiempo.hora) +
						calcularPeriodo(minutoActual, UnidadDeTiempo.minuto);
		
		long hDeseada= 	calcularPeriodo(horaDeseada, UnidadDeTiempo.hora) +
						calcularPeriodo(minutoDeseado, UnidadDeTiempo.minuto);
		
		
		/*if(hDeseada<hActual){
			c.add(Calendar.HOUR_OF_DAY, 24);			
		}
		
		c.set(Calendar.HOUR_OF_DAY, horaDeseada);
		c.set(Calendar.MINUTE, minutoDeseado);*/
		
		
		if(hDeseada<hActual){
			c.set(Calendar.HOUR_OF_DAY, horaActual);
			c.set(Calendar.MINUTE, minutoActual);
		}
		else{
			c.set(Calendar.HOUR_OF_DAY, horaDeseada);
			c.set(Calendar.MINUTE, minutoDeseado);
		}
		
		return c.getTime();
	}
	
	public static long calcularPeriodo(int periodo, UnidadDeTiempo u){
		long ret = (long)periodo;		
		switch(u){
			case mes:
				ret*=4;
			case semana:
				ret*=7;
			case dia:
				ret*=24;	
			case hora:
				ret*=60;
			case minuto:
				ret*=60;				
			case segundo:
				ret*=1000;
			case milisegundo:				
				break;	
		}
		return ret;		
	}
}

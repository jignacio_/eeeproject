package cl.foursoft.eee.cron;

import java.util.TimerTask;

public abstract class Cron extends TimerTask {

	@Override
	public abstract void run();
	
}

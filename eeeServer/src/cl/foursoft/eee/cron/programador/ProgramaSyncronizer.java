package cl.foursoft.eee.cron.programador;

import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.client.ClientRequest;


import com.google.gson.Gson;

import cl.foursoft.eee.cron.Cron;
import cl.foursoft.eee.dao.facade.FacadeSistema;
import cl.foursoft.eee.properties.Propiedades;
import cl.foursoft.eee.service.EvaluacionEconomicaService;
import cl.foursoft.eee.service.SistemaService;
import cl.foursoft.eee.util.ProcesoSincronizacion;
import cl.foursoft.eee.util.Sincronizacion;


public class ProgramaSyncronizer extends Cron {

	@Override
	public void run() {	
		
		if(Propiedades.getProperty("ES_MAESTRO").equalsIgnoreCase("NO")){
			

			SistemaService sistemaService = new SistemaService();
			String ipSincro = sistemaService.obtenerIPSincronizacion();

				System.out.println("============== 1/2 Inicia Tarea Programada Sincronizacion =================");
				//String url = Propiedades.getProperty("URL_SERVIDOR_MAESTRO") + "/eeeServer/syncronize/parameter"; // Maquina Virtual 192.168.0.10
				String url = ipSincro + "/eeeAdminServer/syncronize/parameter";							      // Servidor		162.243
			
			
				ClientRequest cr = new ClientRequest(url);
				cr.accept(MediaType.APPLICATION_JSON+ ";charset=utf-8");//Cambiar por MediaType
				String json = "";
				try {
					json = cr.get(String.class).getEntity();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				Gson gson = new Gson();
				Sincronizacion sync = gson.fromJson(json, Sincronizacion.class);
				
				ProcesoSincronizacion procSincronizacion = new ProcesoSincronizacion();
				if(procSincronizacion.cargarDatosSincronizacion(sync)){
			
					sistemaService.guardarFechaUltimaSincro();
				
					System.out.println("==============  Finaliza Tarea Programada Sincronización =================");
					System.out.println("============== 2/2 Actualizar Evaluaciones Económicas =================");
			
					EvaluacionEconomicaService evaluacionSyn = new EvaluacionEconomicaService();
					evaluacionSyn.actualizarEvaluacionesSyn();
			
					System.out.println("============== Fin Actualización Evaluaciones Económicas =================");
				}
				else{
					System.out.println("==============  Error de Sincronizacion =================");
				}
			
			
		}
	}

}

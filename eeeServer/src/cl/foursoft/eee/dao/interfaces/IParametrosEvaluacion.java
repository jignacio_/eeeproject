package cl.foursoft.eee.dao.interfaces;

import java.util.List;

import cl.foursoft.eee.dao.transferObject.ParametrosEvaluacionTO;


public interface IParametrosEvaluacion {
	
	public List<ParametrosEvaluacionTO> obtenerTodosParametrosEvaluacion();
	public List<ParametrosEvaluacionTO> obtenerParametrosCliente(int idCliente);
	public ParametrosEvaluacionTO obtenerParametroEvaluacionPorId();
	public int guradrParametroEvaluacion(ParametrosEvaluacionTO parametros);
	public int obtenerSiguienteId();
	public int eliminarParametroCliente(int idParametro, int idCliente);
	

}

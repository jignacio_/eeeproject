package cl.foursoft.eee.dao.interfaces;

import java.util.List;

import cl.foursoft.eee.dao.transferObject.ClienteTO;

public interface ICliente {

	public List<ClienteTO> obtenerTodosLosClientes();
	
	public int guardarCliente(ClienteTO cliente);
	public int eliminarCliente(int idCliente);
	public int actualizarCliente(ClienteTO cliente);
	

}

package cl.foursoft.eee.dao.interfaces;

import cl.foursoft.eee.dao.transferObject.TestTO;

public interface ITest {
	
	public TestTO getTest(int id);
}

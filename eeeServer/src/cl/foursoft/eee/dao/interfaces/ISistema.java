package cl.foursoft.eee.dao.interfaces;

import java.sql.Connection;
import java.sql.Timestamp;

import cl.foursoft.eee.dao.transferObject.EmailTO;

public interface ISistema {
	
	public boolean gurdarIpSincronizacion(Connection c, String ip);
	public boolean guardarEmail(Connection c, EmailTO email);
	public boolean actualizarIPSincronizacion(String ip);
	public boolean guardarIPSincronizacion(String ip);
	public EmailTO obtenerDatosEmail();
	public boolean exiteEmail();
	public void actualizarDatosEmail(EmailTO email);
	public void guardarDatosEmail(EmailTO email);
	public boolean guardarFechaUltimaSincronizacion(Timestamp fechaActualizacion);
	public Timestamp obtenerFechaUltimaSincronizacion(Connection c);
	public String obtenerIpSincronizacion();
	public boolean existeIp();		
}

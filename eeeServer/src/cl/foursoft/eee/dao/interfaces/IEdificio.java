package cl.foursoft.eee.dao.interfaces;

import java.util.List;

import cl.foursoft.eee.dao.transferObject.EdificioTO;
import cl.foursoft.eee.dao.transferObject.ParametrosEdificioTO;

public interface IEdificio {

	public EdificioTO obtenerEdificio(int idEdififio);
	public EdificioTO obtenerEdificioParaAlternativa(int idEdififio);
	public List<EdificioTO> obtenerTodosLosEdificiosPorCliente(int idCliente);
	public List<EdificioTO> obtenerTodosLosEdificiosCompletosPorCliente(int idCliente);
	
	public int guardarEdificio(EdificioTO edificioNuevo);
	public int modificarEdificio(EdificioTO edificio);
	public int guardarFotografia (byte[] fotografia, String rutaFotografia);
//	public EdificioTO obtenerEdificioPorID(int IdEdificio);
	public int eliminarEdificio(int IdEdificio);
	public byte[] obtenerFotografia(String rutaFotografia);
	public List<ParametrosEdificioTO> obtenerParametrosPorIdEdificio(int idEdificio);
	
	public EdificioTO obtenerEdificioOpciones(int idEdificio, boolean envolvente, boolean parametros, boolean sensores, boolean fotografia);
	public int guardarInversionTotalEdificio(int idEdificio, float costoEdificio);
	public int actualizarEdificio(EdificioTO edificioActualizar);
	 
}
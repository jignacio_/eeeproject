package cl.foursoft.eee.dao.interfaces;

import cl.foursoft.eee.dao.transferObject.RequerimientoEnergeticoTO;

public interface IRequerimientoEnergetico {

	
	public int guardarRequerimientoEnergeticoEdificio(RequerimientoEnergeticoTO requerimiento);
	public int guardarRequerimientoEnergeticoAlternativa(RequerimientoEnergeticoTO requerimiento);
	public RequerimientoEnergeticoTO obtenerRequerimientoEnergeticoEdificio(int idEdifcio);
	public RequerimientoEnergeticoTO obtenerRequerimientoEnergeticoAlternativa(int idEdificio, int idAlternativa);

	public float obtenerValorRequerimientoTotal(int idEdificio, int idAlternativa);
}

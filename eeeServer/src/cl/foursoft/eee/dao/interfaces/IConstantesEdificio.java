package cl.foursoft.eee.dao.interfaces;

import cl.foursoft.eee.dao.transferObject.ParametrosEdificioTO;
import cl.foursoft.eee.dao.transferObject.FactorRenovacionTO;
 

import java.util.List;

public interface IConstantesEdificio {
	
	public List<ParametrosEdificioTO> obtenerConstantes() ;
	public List<FactorRenovacionTO> obtenerFactorDeRenovacion();
	public boolean editarFactor(FactorRenovacionTO factor);
	public boolean guardarFactor(FactorRenovacionTO factor);
	public boolean guardarFactorSync(FactorRenovacionTO factor);
	public boolean existeFactor(int idFactor);
}

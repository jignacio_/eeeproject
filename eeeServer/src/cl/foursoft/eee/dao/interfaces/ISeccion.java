package cl.foursoft.eee.dao.interfaces;

import java.util.List;

import cl.foursoft.eee.dao.transferObject.SeccionTO;

public interface ISeccion {

	
	public List<SeccionTO> obtenerSecciones();
	public List<SeccionTO> obtenerSeccionesPadre();
	public List<SeccionTO> obtenerSeccionesHijosPorId(int idSeccionPadre, String seccionPadre);
	public int guardarSeccionesEdificio(List<SeccionTO> seccionesEnvolvente, int idEdificio);
	public List<SeccionTO> obtenerSeccionesEdificio(int idEdificio);
	public List<SeccionTO> obtenerSeccionesHijoEdificio(int idSeccionPadre, int idEdificio, String nombreSeccionPadre);
}

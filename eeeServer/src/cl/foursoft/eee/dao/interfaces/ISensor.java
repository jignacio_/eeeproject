package cl.foursoft.eee.dao.interfaces;

import java.util.List;

import cl.foursoft.eee.dao.transferObject.TipoMedicionConsumoSensorTO;

public interface ISensor {

	public List<TipoMedicionConsumoSensorTO> obtenerTodosLosTiposDeSensores();
	public List<TipoMedicionConsumoSensorTO> obtenerTodosLosTiposDeSensoresAdmin();
	public TipoMedicionConsumoSensorTO obtenerUnTipoDeMedicion(String idMedicion);
	public List<TipoMedicionConsumoSensorTO> obtenerTodosLosTiposDeSensoresHijo(String tipoMedicion);
	public boolean guardarTipoSensor(TipoMedicionConsumoSensorTO _sensor,  String idTipoMedicion);
	public boolean actulizarTipoSensor(TipoMedicionConsumoSensorTO _sensor);
	public List<TipoMedicionConsumoSensorTO> recuperarTipoDeSensores();

	public boolean agregarSensorNuevamente(TipoMedicionConsumoSensorTO _sensor);
	public boolean eliminarSensorFinal(TipoMedicionConsumoSensorTO sensor);
	public boolean eliminarTipoConsumo(TipoMedicionConsumoSensorTO _sensor);
	
}

package cl.foursoft.eee.dao.interfaces;

import java.sql.Timestamp;
import java.util.List;

import cl.foursoft.eee.dao.transferObject.EvaluacionEconomicaTO;

public interface IEvaluacionEconomica {

	public int guardarEvaluacion(EvaluacionEconomicaTO evaluacion);
	public int actualizarEvaluacion(EvaluacionEconomicaTO evaluacion);
	public boolean existeEvaluacion(int idAlternativa, int idParametro);
	public int eliminarEvaluacion(int idEvaluacion);
	public int eliminarEvaluacionPorIdAlternativaIdParametro(int idAlternativa, int idParametro);
	public EvaluacionEconomicaTO obtenerEvaluacion(int idAlternativa, int idParametro);
	public List<EvaluacionEconomicaTO> obtenerTodasLasEvaluaciones(int idParametro, int idEdificio);
	
	public int actualizarRequerimiento( EvaluacionEconomicaTO evaluacion);
	public Timestamp obtenerFechaUltimaEvaluacion(int idAlternativa, int idParametro);
	public List<EvaluacionEconomicaTO> obtenerTodasLasEvaluacionesEdificio(	int idEdificio);

}

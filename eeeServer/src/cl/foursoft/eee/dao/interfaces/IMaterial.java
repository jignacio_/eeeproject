package cl.foursoft.eee.dao.interfaces;


import java.util.List;

import cl.foursoft.eee.dao.transferObject.MaterialTO;
import cl.foursoft.eee.dao.transferObject.SeccionTO;

public interface IMaterial {
	
	public List<MaterialTO> obtenerTodosLosMateriales();
	public List<MaterialTO> obtenerTodosLosMaterialesSync();
	public List<MaterialTO> obtenerTodosLosMaterialesCompleto();
	public List<MaterialTO> obtenerMaterialesPorTipoSeccion(int idSeccion);
	
	public int guardarComposicionEdificio(List<MaterialTO> materiales, int idSeccion, int idEdificio);
	public int guardarParametroComposicion(SeccionTO seccion, int idEdificio);
	public SeccionTO obtenerParametroComposicion(SeccionTO secc, int idEdificio);
	public List<MaterialTO> obtenerComposicionSeccionEdificio(int idTipoSeccion, int idEdifico);

	public MaterialTO obtenerMaterialPorID(int idMaterial);
	public int guardarMaterial(MaterialTO materiales);
	public int actualizarMaterial(MaterialTO material);
	public int eliminarMaterial(int idMaterial);
	
	public boolean guardarMaterialSync(MaterialTO materiales);
	public boolean existeMaterial(int idMaterial);
}

package cl.foursoft.eee.dao.interfaces;

import java.util.List;

import cl.foursoft.eee.dao.transferObject.AlternativaDeInversionTO;
import cl.foursoft.eee.dao.transferObject.ArtefactoTO;
import cl.foursoft.eee.dao.transferObject.EnvolventeTO;
import cl.foursoft.eee.dao.transferObject.MaterialTO;
import cl.foursoft.eee.dao.transferObject.SensorTO;
import cl.foursoft.eee.dao.transferObject.TipoAlternativaTO;

public interface IAlternativaInversion {

	public int guardarAlternativaTipoEnergetica(AlternativaDeInversionTO alternativa);
	public List<AlternativaDeInversionTO> obtenerAlternativasPorEdificio(int idEdificio);
	public int guardarComposicionAlternativa(EnvolventeTO nuevaEnvolvente, int idAlternativa);
	public EnvolventeTO obtenerEnvolventeAlternativaSinSeccionesHijo(int idAlternativa, int idEdificio);
	public EnvolventeTO obtenerEnvolventeAlternativaConSeccionesHijo(int idAlternativa, int idEdificio);
	public AlternativaDeInversionTO obtenerAlternativaPorId(int idAlternativa);
	public int guardarParametroComposicion(EnvolventeTO envolventeAlternativa, int alternativa);
	public int guardarInversionEdificio(List<MaterialTO> envolventeDiferencia, int idAlternativa, float otroCosto);
	public int eliminarAlternativaPorID(int idAlternativa);
	public int guardarInversionAlternativa(List<SensorTO> sensoresAlternativas, int idAlterativa, float otroCosto);
	public List<TipoAlternativaTO> obtenerTiposAlternativas();
	public int guardarDiferenciaAlternativaEnvolvente(List<MaterialTO> materialesDiferencia, int idAlternativa);

	public int guardarRequerimientoAlternativa(List<SensorTO> sensores, int idAlternativa );
	public int guardarArtefactosAltertnativa(List<SensorTO> sensoresAlternativas, int idAlternativa);
	public int guardarInversionAlternativaArtefacto(List<ArtefactoTO> artefactosDiferencia, int idAlternativa, float otroCosto);
	public int guardarDiferenciaAlternativaArtefacto(List<ArtefactoTO> artefactosDiferencia, int idAlternativa);
	
	public int guardarResumenRequerimientoAlternativa( int idAlternativa , double consumo, String unidad, int idTipoAlternativa);

}

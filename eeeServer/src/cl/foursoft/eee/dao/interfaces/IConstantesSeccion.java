package cl.foursoft.eee.dao.interfaces;

import cl.foursoft.eee.dao.transferObject.ParametrosEdificioTO;

import java.util.List;

public interface IConstantesSeccion {

	
	  public List<ParametrosEdificioTO> obtenerConstantes();
}

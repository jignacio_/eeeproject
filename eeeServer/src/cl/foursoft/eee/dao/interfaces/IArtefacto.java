package cl.foursoft.eee.dao.interfaces;

import java.util.ArrayList;
import java.util.List;

import cl.foursoft.eee.dao.transferObject.ArtefactoCombustibleDetalleTO;
import cl.foursoft.eee.dao.transferObject.ArtefactoTO;

public interface IArtefacto {
	
	public ArrayList<ArtefactoTO> obtenerArtefactosPorMedicion(String idMedicion);
	public boolean guardarComposicionArtefactos(List<ArtefactoTO> artefactos, String idSensor);
	public boolean actualizarComposicion(ArrayList<ArtefactoTO> artefactos);
	public List<ArtefactoTO> cargarArtefactosSensor(String idSensor);
	
	public List<ArtefactoTO> obtenerArtefactosSensor(String idSensor);
	public List<ArtefactoTO> obtenerTodosLosArtefactos();
	public List<ArtefactoTO> obtenerTodosLosArtefactosSync();
	public boolean eliminarArtefacto(int idArtefacto);
	public ArtefactoCombustibleDetalleTO obtenerArtefactoCombustibleCompleto(int idArtefacto);
	public int guardarArtefactoCombustibleDetalle(ArtefactoCombustibleDetalleTO artefacto);
	public int actualizarArtefactoCombustibleDetalle(ArtefactoCombustibleDetalleTO artefacto);
	public boolean existeDetalleArtefactoCombustible(int idArtefacto);
	public int guardarArtefacto(ArtefactoTO a);
	public int ActualizarArtefacto(ArtefactoTO a);
	public boolean existeArtefacto(int idArtefacto) ;
	public boolean guardarArtefactosSync(ArtefactoTO a);

}

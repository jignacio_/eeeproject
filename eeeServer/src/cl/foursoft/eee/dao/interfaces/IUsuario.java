package cl.foursoft.eee.dao.interfaces;

import java.util.List;

import cl.foursoft.eee.dao.transferObject.ClienteTO;
import cl.foursoft.eee.dao.transferObject.UsuarioTO;

public interface IUsuario {

		public UsuarioTO obtenerUsuario(String usuario, String contrasenia);
		public UsuarioTO obtenerUsuarioPorId(UsuarioTO user);
		public UsuarioTO obtenerCliente(UsuarioTO usuario);
		public UsuarioTO obtenerUsuarioKipus(String usuario, String contrasenia);
		public List<UsuarioTO> obtenerUsuarios();
		public boolean guardarUsuario(UsuarioTO usuario);
		public boolean editarUsuario(UsuarioTO usuario);
		public boolean eliminarUsuario(UsuarioTO usuario);
		public ClienteTO obtenerCliente();
}

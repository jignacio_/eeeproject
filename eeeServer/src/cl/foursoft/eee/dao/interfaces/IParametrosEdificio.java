package cl.foursoft.eee.dao.interfaces;

import java.util.List;

import cl.foursoft.eee.dao.transferObject.ParametrosEdificioTO;

public interface IParametrosEdificio {
	
	public List<ParametrosEdificioTO> obtenerParametros();
	public boolean editarParametro(ParametrosEdificioTO parametro);
	public boolean editarParametroSync(ParametrosEdificioTO parametro);
	 
}
package cl.foursoft.eee.dao.interfaces;

import java.util.List;

import cl.foursoft.eee.dao.transferObject.MedicionTO;
import cl.foursoft.eee.dao.transferObject.VistaParaGestionTO;

public interface IMedicion {
	
	
	public List<MedicionTO> obtenerMedicionEdificioTipoConsumo(int idEdificio,String idMedicion);
	public List<MedicionTO> obtenerMedidasSensor(int idEdificio,String idConsumo, String idSensor);
	public List<MedicionTO> obtenerMedidasPiso(int piso, String idConsumo);
	public List<MedicionTO> obtenerMedidasEdificio(int idSensor, String idConsumo);
	public List<MedicionTO> obtenerMedidasZona(int zona, String idConsumo);
		
	
	public double totalMedicionMes(String idMedicion, int idEdificio);
	public double totalMedicionHoy(String idMedicion, int idEdificio);
	public List<MedicionTO> obtenerDatosRango(VistaParaGestionTO peticion);
	
	//VISTA DETALLE
	public List<MedicionTO> obtenerDatosMedicionHoyPath(String idMedicion,int idEdificio ,  int idPiso , int idZona , int minutos);
	public List<MedicionTO> obtenerDatosMedicionHoyPathConsumo(String idMedicion, String Consumo,int idEdificio ,  int idPiso , int idZona , int minutos);
	public List<MedicionTO> obtenerDatosMedicionHoySensor(String idMedicion,int idEdificio ,   String idSensor , int minutos);
	public List<MedicionTO> obtenerDatosMedicionHoySensorConsumo(String idMedicion, String Consumo,int idEdificio , String idSensor , int minutos);
	
 	public List<MedicionTO> totalConsumos(String idMedicion, int idEdificio,  int idPiso, int idZona , String idSensor, String periodo); // crear otro para rango
	public List<MedicionTO> obtenerAlertas(int idEdificio);
		

}

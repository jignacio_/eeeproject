package cl.foursoft.eee.dao.facade;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.implementation.postgresql.MedicionDB;
import cl.foursoft.eee.dao.interfaces.IMedicion;
import cl.foursoft.eee.dao.transferObject.MedicionTO;
import cl.foursoft.eee.dao.transferObject.VistaParaGestionTO;

public class FacadeMedicion {

	
	//int idEdificio, String idConsumo, String idSensor
	public List<MedicionTO> obtenerMedicionEdificioTipoConsumo(int idEdificio, String idMedicion, Connection c){
		List<MedicionTO> resp = null;
		
		IMedicion medicionDB = new MedicionDB(c);
		resp = medicionDB.obtenerMedicionEdificioTipoConsumo(idEdificio, idMedicion);
		return resp;
	}
	public List<MedicionTO> obtenerMedidasPiso(int piso , String idConsumo){
		List<MedicionTO> resp = null;
		
		
		return resp;
	}
	public List<MedicionTO> obtenerMedidasEdificio(int idSensor){
		List<MedicionTO> resp = null;
		
		
		return resp;
	}
	
	public List<MedicionTO> obtenerMedidasZona(int zona){
		List<MedicionTO> resp = null;
		
		
		return resp;
	}
	
	public double totalMedicionMes(String idMEdicion, int idEdificio, Connection c){
		
		double resp = -1;
		IMedicion datosMedicion = new MedicionDB(c);
		resp = datosMedicion.totalMedicionMes(idMEdicion, idEdificio);
		
		return resp;
	}
	public double totalMedicionHoy(String idMEdicion, int idEdificio, Connection c){
		
		double resp = -1;
		IMedicion datosMedicion = new MedicionDB(c);
		resp = datosMedicion.totalMedicionHoy(idMEdicion, idEdificio);
		
		return resp;
	}
	public List<MedicionTO> obtenerDatosRango(VistaParaGestionTO peticion, Connection c) {
		List<MedicionTO> resp = null;
		IMedicion medicionDB = new MedicionDB(c);
		resp = medicionDB.obtenerDatosRango(peticion);
		return resp;
	}
//	public List<MedicionTO> totalConsumosMes(String idMedicion,int idEdificio, Connection c) {
//		// TODO Auto-generated method stub
//		List<MedicionTO> resp;
//		
//		IMedicion mdb = new MedicionDB(c);
//		resp = mdb.totalConsumosMes(idMedicion, idEdificio);
//		
//		return resp;
//	}
	/////VISTAS PARA DETALLE - piso .zona y sensor envia el numero correspondiente, para sensor envia el ID Completo
	public List<MedicionTO> obtenerDatosMedicionHoy(String idMedicion, String idConsumo,int idEdificio,  int idPiso , int idZona , String idSensor , int minutos,  Connection c) {
		// TODO Auto-generated method stub
		List<MedicionTO> resp;
		IMedicion mdb = new MedicionDB(c);
		
		if(idSensor.equalsIgnoreCase("") || idSensor==null){
			if(idMedicion.equalsIgnoreCase(idConsumo) || idConsumo.equalsIgnoreCase("")){
				resp = mdb.obtenerDatosMedicionHoyPath(idMedicion, idEdificio, idPiso, idZona , minutos);
			}else{
				resp = mdb.obtenerDatosMedicionHoyPathConsumo(idMedicion, idConsumo , idEdificio, idPiso, idZona , minutos);
			}
		}
		else{ // El sensor  est� asociado a un consumo, solo se utiliza una funci�n.
				resp = mdb.obtenerDatosMedicionHoySensor(idMedicion, idEdificio, idSensor , minutos);
		}

		return resp;
	}
	public List<MedicionTO> totalConsumos(String idMedicion, int idEdificio, int idPiso, int idZona , String idSensor, String periodo, Connection c) {
		// TODO Auto-generated method stub
		List<MedicionTO> resp;
		
		IMedicion mdb = new MedicionDB(c);
		resp  = mdb.totalConsumos(idMedicion, idEdificio, idPiso, idZona, idSensor, periodo);
		
		return resp;
	}
	public List<MedicionTO> obtenerAlertas(int idEdificio, Connection c) {
		List<MedicionTO> resp;
		
		IMedicion mdb = new MedicionDB(c);
		resp = mdb.obtenerAlertas(idEdificio);
		
		return resp;
	}
	
}

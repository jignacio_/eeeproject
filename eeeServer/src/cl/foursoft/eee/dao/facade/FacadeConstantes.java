package cl.foursoft.eee.dao.facade;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.implementation.postgresql.ConstantesEdificioDB;
import cl.foursoft.eee.dao.implementation.postgresql.ConstantesSeccionesDB;
import cl.foursoft.eee.dao.interfaces.IConstantesEdificio;
import cl.foursoft.eee.dao.interfaces.IConstantesSeccion;
import cl.foursoft.eee.dao.transferObject.FactorRenovacionTO;
import cl.foursoft.eee.dao.transferObject.ParametrosEdificioTO;

public class FacadeConstantes {
		
	public List<ParametrosEdificioTO> obtenerConstantesEdificio(Connection c) {
		List<ParametrosEdificioTO> resp = null;
		
		IConstantesEdificio constantesEdificioDB = new ConstantesEdificioDB(c);
		resp = constantesEdificioDB.obtenerConstantes();

		return resp;		
	}
	
	public List<ParametrosEdificioTO> obtenerConstantesSeccion(Connection c){
		List<ParametrosEdificioTO> resp = null;
		
		IConstantesSeccion constanteSeccion = new ConstantesSeccionesDB(c);
		resp = constanteSeccion.obtenerConstantes();
		
		return resp;		
	}
	
	
	public List<FactorRenovacionTO> obtenerFactorDeRenovacion(Connection c){
		List<FactorRenovacionTO> resp = null;
		
		IConstantesEdificio constantesEdificio = new ConstantesEdificioDB(c);
		resp = constantesEdificio.obtenerFactorDeRenovacion();
		
		return resp;
	}
	
	public boolean guardarFactores(List<FactorRenovacionTO> factores, Connection c){
		IConstantesEdificio constantesDB = new ConstantesEdificioDB(c);
		boolean resp = false;
		
		for(FactorRenovacionTO factor : factores){
			if(factor.getIdFactor() == -1){
				resp = constantesDB.guardarFactor(factor);	
			}
			else{
				resp = constantesDB.editarFactor(factor);
			}
			if(!resp) return false;
		}

	 	return true;
	}
	
	public boolean guardarFactoresSync(List<FactorRenovacionTO> factores, Connection c){
		IConstantesEdificio constantesDB = new ConstantesEdificioDB(c);
		boolean resp = false;
		
		for(FactorRenovacionTO factor : factores){
			boolean existe = constantesDB.existeFactor(factor.getIdFactor());
			if(existe){
				resp = constantesDB.editarFactor(factor);
			}
			else{
				resp = constantesDB.guardarFactorSync(factor);
			}
			if(!resp) return false;
		}

	 	return true;
	}
}

package cl.foursoft.eee.dao.facade;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.implementation.postgresql.MaterialDB;
import cl.foursoft.eee.dao.interfaces.IMaterial;
import cl.foursoft.eee.dao.transferObject.MaterialTO;

public class FacadeMateriales {

	
	public List<MaterialTO> obtenerTodosLosMateriales(Connection c){
		List<MaterialTO> resp = null;
		
		IMaterial materialDB = new MaterialDB(c);
		resp = materialDB.obtenerTodosLosMateriales();
		
		
		return resp;
		
	}
	public List<MaterialTO> obtenerTodosLosMaterialesSync(Connection c){
		List<MaterialTO> resp = null;
		
		IMaterial materialDB = new MaterialDB(c);
		resp = materialDB.obtenerTodosLosMaterialesSync();
		
		
		return resp;
		
	}
	
	public List<MaterialTO> obtenerMaterialesPorTipoSeccion(int idSeccion, Connection c){
		List<MaterialTO> resp = null;
		
		IMaterial materialDB = new MaterialDB(c);
		resp = materialDB.obtenerMaterialesPorTipoSeccion(idSeccion);
		
		
		return resp;
		
	}
	public int guardarMaterial(List<MaterialTO> materiales, Connection c){	
		int resp = -1;
		
		IMaterial materialDB = new MaterialDB(c);
		
		for(MaterialTO m:materiales){
			if(m.getIdMaterial()<0) 	
				resp = materialDB.guardarMaterial(m);
			else
				resp = materialDB.actualizarMaterial(m);
		}
		return resp;
	}

	public int actualizarMaterial(MaterialTO material, Connection c) {
		int resp = -1;
		
		IMaterial materialDB = new MaterialDB(c);
		resp = materialDB.actualizarMaterial(material);
		
		return resp;
	}

	public int eliminarMaterial(int idMaterial, Connection c) {
		int resp = -1;
		
		IMaterial materialDB = new MaterialDB(c);
		resp = materialDB.eliminarMaterial(idMaterial);
		
		return resp;
	}
	
	public boolean editarMaterialesSync(List<MaterialTO> materiales, Connection c){
		boolean exito = true;
		IMaterial materialDB = new MaterialDB(c);
		
		for(MaterialTO material : materiales){
			boolean existe = materialDB.existeMaterial(material.getIdMaterial());
			if(existe)	
				exito = materialDB.actualizarMaterial(material) >= 0;
			else
				exito = materialDB.guardarMaterialSync(material);
			
			if(!exito) return false;
		}

		return exito;
	}
	
}

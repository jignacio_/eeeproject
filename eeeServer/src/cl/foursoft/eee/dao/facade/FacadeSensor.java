package cl.foursoft.eee.dao.facade;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.implementation.postgresql.SensorDB;
import cl.foursoft.eee.dao.interfaces.ISensor;
import cl.foursoft.eee.dao.transferObject.TipoMedicionConsumoSensorTO;

public class FacadeSensor {

	public List<TipoMedicionConsumoSensorTO> obtenerTodosLosTiposDeSensores(Connection c) {
		
		
		List<TipoMedicionConsumoSensorTO> resp = null;
		
		
		ISensor sensorDB = new SensorDB(c);
		resp = sensorDB.obtenerTodosLosTiposDeSensores();
		
		return resp;
	}
	public List<TipoMedicionConsumoSensorTO> obtenerTodosLosTiposDeSensoresAdmin(Connection c) {
		
		
		List<TipoMedicionConsumoSensorTO> resp = null;
		
		
		ISensor sensorDB = new SensorDB(c);
		resp = sensorDB.obtenerTodosLosTiposDeSensoresAdmin();
		
		return resp;
	}
	
	public List<TipoMedicionConsumoSensorTO> obtenerTipoDeConsumo(String tipoMedicion, Connection c) {
		
		
		List<TipoMedicionConsumoSensorTO> resp = null;
		
		
		ISensor sensorDB = new SensorDB(c);
		resp = sensorDB.obtenerTodosLosTiposDeSensoresHijo(tipoMedicion);
		
		return resp;
	}
	
	/* Obtiene un tipo de medicion con sus consumos hijos.
	 * 
	 * 
	 * */ 
	public TipoMedicionConsumoSensorTO obtenerTipoMedicion(String tipoMedicion, Connection c){
		TipoMedicionConsumoSensorTO resp = null;
		
		
		
		ISensor sensorDB = new SensorDB(c);
		resp = sensorDB.obtenerUnTipoDeMedicion(tipoMedicion); //O
		
		
		
		return resp;
	}
	public boolean guardarSensor(TipoMedicionConsumoSensorTO sensor,  String idTipoMedicion , Connection c) {
		// TODO Auto-generated method stub
		boolean resp = false;
		ISensor sensorDB = new SensorDB(c);

		resp = sensorDB.guardarTipoSensor(sensor,   idTipoMedicion);
		if(!resp) return false;

		return resp;
	}
	public boolean actualizarSensores(List<TipoMedicionConsumoSensorTO> sensores, String idTipoMedicion, Connection c) {
		// TODO Auto-generated method stub
		boolean resp = false;
		ISensor sensorDB = new SensorDB(c);
		
		for (TipoMedicionConsumoSensorTO _sensor: sensores){
			
				resp = sensorDB.actulizarTipoSensor(_sensor);
				if(!resp) return false;
		}
		
		return resp;
	}

	public boolean eliminarSensor(TipoMedicionConsumoSensorTO _sensor, Connection c) {
		// TODO Auto-generated method stub
		boolean resp = false;
		ISensor sensorDB = new SensorDB(c);
		
		resp = sensorDB.eliminarSensorFinal(_sensor);
		if(!resp) return false;
		
		
		return resp;
	}

}

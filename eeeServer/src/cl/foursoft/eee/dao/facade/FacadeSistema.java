package cl.foursoft.eee.dao.facade;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Date;

import cl.foursoft.eee.dao.implementation.postgresql.SistemaDB;
import cl.foursoft.eee.dao.interfaces.ISistema;
import cl.foursoft.eee.dao.transferObject.EmailTO;

public class FacadeSistema {

	
	public String obtenerIPSincronizacion(Connection c) {
		String resp = "";
		ISistema sistemaDB = new SistemaDB(c);
		
		resp = sistemaDB.obtenerIpSincronizacion(); 
		

		return resp;
	}
	public boolean gurdarIpSincronizacion(Connection c, String ip) {
		boolean resp = false;
		ISistema sistemaDB = new SistemaDB(c);
		
		if(sistemaDB.existeIp())
			resp = sistemaDB.actualizarIPSincronizacion(ip);
		else
			resp = sistemaDB.guardarIPSincronizacion(ip);
		
		return resp;
	}

	public EmailTO obtenerDatosEmail(Connection c) {
		EmailTO resp = null;
		ISistema sistemaDB = new SistemaDB(c);

		resp = sistemaDB.obtenerDatosEmail();
		
		return resp;
	}
	
	public boolean guardarEmail(Connection c, EmailTO email) {
		boolean resp = false;
		ISistema sistemaDB = new SistemaDB(c);

		if(sistemaDB.exiteEmail())
			sistemaDB.actualizarDatosEmail(email);
		else
			sistemaDB.guardarDatosEmail(email);
		
		return resp;
	}

	
	public boolean guardarFechaUltimaSincronizacion( Connection c){
		boolean resp = false;
		ISistema sistema = new SistemaDB(c);
		
		long time = 0;
		Date fecha = new Date();
		time = fecha.getTime();
		Timestamp ultimaFecha = new Timestamp(time);
		
		resp = sistema.guardarFechaUltimaSincronizacion(ultimaFecha);
		
		return resp;
	}
	
	public Timestamp obtenerFechaUltimaSincronizacion(Connection c){
		Timestamp resp = null;
		
		ISistema sistema = new SistemaDB(c);
		
		resp = sistema.obtenerFechaUltimaSincronizacion(c);
		
		
		return resp;
	}

 

}

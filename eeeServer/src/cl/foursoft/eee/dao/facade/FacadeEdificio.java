package cl.foursoft.eee.dao.facade;



import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import cl.foursoft.eee.dao.implementation.postgresql.EdificioDB;
import cl.foursoft.eee.dao.implementation.postgresql.MedicionDB;
import cl.foursoft.eee.dao.implementation.postgresql.ParametrosEvaluacionDB;
import cl.foursoft.eee.dao.implementation.postgresql.SeccionDB;
import cl.foursoft.eee.dao.interfaces.IEdificio;
import cl.foursoft.eee.dao.interfaces.IMedicion;
import cl.foursoft.eee.dao.interfaces.IParametrosEvaluacion;
import cl.foursoft.eee.dao.interfaces.ISeccion;
import cl.foursoft.eee.dao.transferObject.EdificioTO;
import cl.foursoft.eee.dao.transferObject.ParametrosEdificioTO;
import cl.foursoft.eee.dao.transferObject.ParametrosEvaluacionTO;
import cl.foursoft.eee.dao.transferObject.SeccionTO;
import cl.foursoft.eee.util.CalculosUtil;


public class FacadeEdificio {

	public int guardarEdificio(EdificioTO edificioNuevo, Connection c){
		int idEdificio = 0;
		IEdificio edificioDB = new EdificioDB(c);
		ISeccion seccionDB = new SeccionDB(c);
		int resp = -1;

		
		
		resp = edificioDB.guardarEdificio(edificioNuevo);
		idEdificio = resp;

		//Si se guardaron el edificio/Pisos-Zona-Sensor / Parametros 
		//Entonces, guardo secciones de la envonvente
		if(resp>0){
			resp = seccionDB.guardarSeccionesEdificio(edificioNuevo.getArrCollSecciones(), edificioNuevo.getCodigoEdificio());
		}
		return resp;

	}
	public EdificioTO obtenerEdificio(int idEdificio, Connection c){

		EdificioTO resp = null;
		List<SeccionTO> seccionesEdificio = new ArrayList<SeccionTO>();

		ISeccion seccionDB = new SeccionDB(c);
		IEdificio edificioDB = new EdificioDB(c);

		resp = edificioDB.obtenerEdificio(idEdificio);

		if(resp!=null){//Carg� edificio Correctamente
			seccionesEdificio = seccionDB.obtenerSeccionesEdificio(idEdificio);
			if(seccionesEdificio!=null){
				seccionesEdificio = CalculosUtil.calcularSuperficieTotalSecciones(seccionesEdificio);
				resp.setArrCollSecciones(seccionesEdificio);
			}
		}

		return resp;
	}
	public EdificioTO obtenerEdificioParaAlternativa(int idEdificio, Connection c){

		EdificioTO resp = null;
		List<SeccionTO> seccionesEdificio = new ArrayList<SeccionTO>();

		ISeccion seccionDB = new SeccionDB(c);
		IEdificio edificioDB = new EdificioDB(c);

		resp = edificioDB.obtenerEdificioParaAlternativa(idEdificio);//Cambiar

		if(resp!=null){//Carg� edificio Correctamente
			seccionesEdificio = seccionDB.obtenerSeccionesEdificio(idEdificio);
			if(seccionesEdificio!=null){
				seccionesEdificio = CalculosUtil.calcularSuperficieTotalSecciones(seccionesEdificio);
				resp.setArrCollSecciones(seccionesEdificio);
			}
		}


		return resp;

	}
	public List<EdificioTO> obtenerTodosLosEdificios(int idCliente, Connection c){

		List<EdificioTO> resp = null;
		IEdificio edificioDB = new EdificioDB(c);
		resp = edificioDB.obtenerTodosLosEdificiosPorCliente(idCliente);

		IMedicion medicionEdificio = new MedicionDB(c);
		for (EdificioTO e : resp){
			e.setMedicionAgua(medicionEdificio.totalMedicionMes("A", e.getIdEdificio()));
			e.setMedicionCombustible(medicionEdificio.totalMedicionMes("C", e.getIdEdificio()));
			e.setMedicionElectricidad(medicionEdificio.totalMedicionMes("E", e.getIdEdificio()));
			
			e.setMedicionAguaHoy(medicionEdificio.totalMedicionHoy("A", e.getIdEdificio()));
			e.setMedicionCombustibleHoy(medicionEdificio.totalMedicionHoy("C", e.getIdEdificio()));
			e.setMedicionElectricidadHoy(medicionEdificio.totalMedicionHoy("E", e.getIdEdificio()));
			
			
			
		}
		return resp;
	}


	public int modificarEdificio(EdificioTO edificioNuevo, Connection c){

		int resp = -1;//edificioNuevo.getIdEdificio();// -1;
		IEdificio edificioDb = new EdificioDB(c);
		resp = edificioDb.modificarEdificio(edificioNuevo);
		return resp;
	}

	public int eliminarEdificio(int idEdificio, Connection c){

		IEdificio edificioDB = new EdificioDB(c);
		int resp = -1;
		resp = edificioDB.eliminarEdificio(idEdificio);

		return resp;
	}

	//Cambiar
	public int guardarParametros(ParametrosEvaluacionTO parametros, Connection c){
		int resp = -1;


		IParametrosEvaluacion parametrosDB = new ParametrosEvaluacionDB(c);
		resp = parametrosDB.guradrParametroEvaluacion(parametros);


		return resp;
	}
	public List<ParametrosEvaluacionTO> obtenerParametrosCliente(int idCliente, Connection c){
		List<ParametrosEvaluacionTO> resp = null;

		IParametrosEvaluacion parametrosDB = new ParametrosEvaluacionDB(c);
		resp = parametrosDB.obtenerParametrosCliente(idCliente);

		return resp;
	}
	public int eliminarParametroCliente(int idParametro, int idCliente, Connection c) {
		int resp = -1;

		IParametrosEvaluacion parametrosDB = new ParametrosEvaluacionDB(c); 
		resp = parametrosDB.eliminarParametroCliente(idParametro, idCliente);



		return resp;
	}
	public List<ParametrosEdificioTO> obtenerParametrosEdificioPorId(int idEdificio, Connection c) {

		List<ParametrosEdificioTO> resp =null;

		IEdificio edificioDB = new EdificioDB(c);
		resp = edificioDB.obtenerParametrosPorIdEdificio(idEdificio);
		return resp;
	}
	public int guardarCalculoInversionEdificio(int idEdificio, Connection c) {
		int resp = -1;
		EdificioTO edificio = new EdificioTO();
		IEdificio edificioDB = new EdificioDB(c);
		float costoEdificio = 0;

		//Obtener el edificio
		edificio = edificioDB.obtenerEdificioOpciones(idEdificio, true, false, false, false);
		//calcular el costo de la inversion
		for(SeccionTO s : edificio.getArrCollSecciones()){
			costoEdificio += s.obtenerCostoTotalSeccion();
		}
		//guardar
		resp = edificioDB.guardarInversionTotalEdificio(idEdificio, costoEdificio);
		
		return resp;
	}
	public List<EdificioTO> obtenerTodosLosEdificiosCompletos(int idCliente, Connection c) {
		
		List<EdificioTO> resp = null;
		IEdificio edificioDB = new EdificioDB(c);
		resp = edificioDB.obtenerTodosLosEdificiosCompletosPorCliente(idCliente);

		return resp;
	}
	public EdificioTO obtenerEdificioVistasDetalle(int idEdificio, Connection c){
		EdificioTO resp = null;
		
		IEdificio edificioDB = new EdificioDB(c);
		resp = edificioDB.obtenerEdificioOpciones(idEdificio, false, false, true, true);
 
		IMedicion medicionEdificio = new MedicionDB(c);
		if (resp!=null){
			resp.setMedicionAgua(medicionEdificio.totalMedicionMes("A",resp.getIdEdificio()));
			resp.setMedicionCombustible(medicionEdificio.totalMedicionMes("C", resp.getIdEdificio()));
			resp.setMedicionElectricidad(medicionEdificio.totalMedicionMes("E", resp.getIdEdificio()));
			
			resp.setMedicionAguaHoy(medicionEdificio.totalMedicionHoy("A", resp.getIdEdificio()));
			resp.setMedicionCombustibleHoy(medicionEdificio.totalMedicionHoy("C", resp.getIdEdificio()));
			resp.setMedicionElectricidadHoy(medicionEdificio.totalMedicionHoy("E", resp.getIdEdificio()));
			
			
			
		}
		
		return resp;
	}
	public int actualizarEdificio(EdificioTO edificioActualizar, Connection c) {
		int resp = -1;
		
		IEdificio edificioDB = new EdificioDB(c);
		resp = edificioDB.actualizarEdificio(edificioActualizar);
		return resp;
	}
}

package cl.foursoft.eee.dao.facade;

import java.sql.Connection;

import cl.foursoft.eee.dao.implementation.postgresql.TestDB;
import cl.foursoft.eee.dao.interfaces.ITest;
import cl.foursoft.eee.dao.transferObject.TestTO;

public class FacadeTest {

	public TestTO getTest(int id, Connection c){
		ITest testDB = new TestDB(c);
		TestTO resp = null;
		
		resp = testDB.getTest(id);
		
		return resp;
	}
}

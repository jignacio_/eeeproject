package cl.foursoft.eee.dao.facade;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.implementation.postgresql.SeccionDB;
import cl.foursoft.eee.dao.interfaces.ISeccion;
import cl.foursoft.eee.dao.transferObject.SeccionTO;

public class FacadeSeccion {
	
	public List<SeccionTO> obtenerSecciones ( Connection c){
		List<SeccionTO> resp = null;
		
		ISeccion seccionDB = new SeccionDB(c);
		resp = seccionDB.obtenerSecciones();
		
		return resp;
	}
	public List<SeccionTO> obtenerSeccionesPadre( Connection c){
		List<SeccionTO> resp = null;
		
		ISeccion seccionDB = new SeccionDB(c);
		resp = seccionDB.obtenerSecciones();
		
		return resp;
	}
	
	public List<SeccionTO> obtenerSeccionesHijoEdificio(Connection c, int idSeccionPadre, int idEdificio , String nombreSeccion){

		List<SeccionTO> resp = null;
		
		ISeccion seccionDB = new SeccionDB(c);
		resp = seccionDB.obtenerSeccionesHijoEdificio(idSeccionPadre, idEdificio, nombreSeccion);
		
		return resp;
		
	}
 

}

package cl.foursoft.eee.dao.facade;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.implementation.postgresql.ParametrosEdificioDB;
import cl.foursoft.eee.dao.interfaces.IParametrosEdificio;
import cl.foursoft.eee.dao.transferObject.ParametrosEdificioTO;

public class FacadeParametrosEdificio {
	
	public List<ParametrosEdificioTO> obtenerParametros(Connection c){
		IParametrosEdificio parametrosDB = new ParametrosEdificioDB(c);
		List<ParametrosEdificioTO> resp = null;

	 	resp = parametrosDB.obtenerParametros();
	
	 	return resp;
	}
	
	public boolean editarParametros(List<ParametrosEdificioTO> parametros, Connection c){
		IParametrosEdificio parametrosDB = new ParametrosEdificioDB(c);
		boolean resp = false;
		
		for(ParametrosEdificioTO parametro : parametros){
			resp = parametrosDB.editarParametro(parametro);
			if(!resp) return false;
		}

	 	return true;
	}
	
	public boolean editarParametrosSync(List<ParametrosEdificioTO> parametros, Connection c){
		IParametrosEdificio parametrosDB = new ParametrosEdificioDB(c);
		boolean resp = false;
		
		for(ParametrosEdificioTO parametro : parametros){
			resp = parametrosDB.editarParametroSync(parametro);
			if(!resp) return false;
		}

	 	return true;
	}
}

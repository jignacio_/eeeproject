package cl.foursoft.eee.dao.facade;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.implementation.postgresql.ClienteDB;
import cl.foursoft.eee.dao.interfaces.ICliente;
import cl.foursoft.eee.dao.transferObject.ClienteTO;

public class FacadeCliente {

	public List<ClienteTO> obtenerTodosLosClientes(Connection c) {
		List<ClienteTO> resp = null;
		
		ICliente clienteDB = new ClienteDB(c);
		resp = clienteDB.obtenerTodosLosClientes();
		
		return resp;
	}

}

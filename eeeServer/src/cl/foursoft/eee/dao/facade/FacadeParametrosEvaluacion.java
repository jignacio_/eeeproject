package cl.foursoft.eee.dao.facade;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.implementation.postgresql.ParametrosEvaluacionDB;
import cl.foursoft.eee.dao.interfaces.IParametrosEvaluacion;
import cl.foursoft.eee.dao.transferObject.ParametrosEvaluacionTO;

public class FacadeParametrosEvaluacion {
	
	
	
	
	public List<ParametrosEvaluacionTO> obtenerTodosParametrosEvaluacion(Connection c){
		List<ParametrosEvaluacionTO> resp = null;
 
		IParametrosEvaluacion parametrosDB = new ParametrosEvaluacionDB(c);
		resp = parametrosDB.obtenerTodosParametrosEvaluacion();
		
		return resp;
	}
	
	public ParametrosEvaluacionTO obtenerParametroEvaluacionPorId( Connection c){
		ParametrosEvaluacionTO resp = null;
 
		IParametrosEvaluacion parametrosDB = new ParametrosEvaluacionDB(c);
		resp = parametrosDB.obtenerParametroEvaluacionPorId();
		
		return resp;
	}
	
	public int guradrParametroEvaluacion(Connection c, ParametrosEvaluacionTO parametros){
		
		int resp = -1;
		IParametrosEvaluacion parametrosDB = new ParametrosEvaluacionDB(c);
		resp = parametrosDB.guradrParametroEvaluacion(parametros);
		
		return resp;
	}
	

}

package cl.foursoft.eee.dao.facade;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import cl.foursoft.eee.dao.implementation.postgresql.ArtefactoDB;
import cl.foursoft.eee.dao.implementation.postgresql.MaterialDB;
import cl.foursoft.eee.dao.interfaces.IArtefacto;
import cl.foursoft.eee.dao.interfaces.IMaterial;
import cl.foursoft.eee.dao.transferObject.ArtefactoCombustibleDetalleTO;
import cl.foursoft.eee.dao.transferObject.ArtefactoTO;
import cl.foursoft.eee.dao.transferObject.MaterialTO;

public class FacadeArtefactos {
	
	public ArrayList<ArtefactoTO> obtenerArtefactosPorMedicion(String idMedicion, Connection c){
		ArrayList<ArtefactoTO> resp = null;
		
		IArtefacto artefactoDB = new ArtefactoDB(c);
		resp = artefactoDB.obtenerArtefactosPorMedicion(idMedicion);
		
		return resp;
	}
	public boolean guardarComposicionArtefactos(List<ArtefactoTO> artefactos , String idSensor , Connection c){
		boolean resp = false;

		IArtefacto iartefacto = new ArtefactoDB(c);
		iartefacto.guardarComposicionArtefactos(artefactos, idSensor);
		return resp;
	}
	public boolean actualizarComposicion(ArrayList<ArtefactoTO> artefactos, String idSensor , Connection c){
		boolean resp = false;
		
		
		
		return resp;	
	}
	public List<ArtefactoTO> obtenerArtefactosSensor(String idSensor, Connection c){
		List<ArtefactoTO> resp = null;
		
		IArtefacto adb = new ArtefactoDB(c);
		resp = adb.obtenerArtefactosSensor(idSensor);
		return resp;
	}
	public List<ArtefactoTO> obtenerTodosLosArtefactos(Connection c) {
		List<ArtefactoTO> resp = null;
			
		IArtefacto artefactoDB = new ArtefactoDB(c);
		resp = artefactoDB.obtenerTodosLosArtefactos();
		return resp;
	}
	public List<ArtefactoTO> obtenerTodosLosArtefactosSync(Connection c) {
		List<ArtefactoTO> resp = null;
		
		IArtefacto artefactoDB = new ArtefactoDB(c);
		resp = artefactoDB.obtenerTodosLosArtefactosSync();
		return resp;
	}
	public boolean eliminarArtefacto(int idArtefacto, Connection c) {
		boolean resp = false;
		
		IArtefacto artefactoDB = new ArtefactoDB(c);
		resp = artefactoDB.eliminarArtefacto(idArtefacto);
		return resp;
	}
	public ArtefactoCombustibleDetalleTO obtenerArtefactoCombustibleCompleto( Connection c, int idArtefacto) {
 
		ArtefactoCombustibleDetalleTO resp = null;

		IArtefacto artefactoDB = new ArtefactoDB(c);
		resp = artefactoDB.obtenerArtefactoCombustibleCompleto(idArtefacto);
		return resp;
	}
	public int guardarArtefactoCombustibleDetalle(Connection c, ArtefactoCombustibleDetalleTO artefacto) {
		int resp = -1;
		
		IArtefacto artefacoDB = new ArtefactoDB(c);
		
		if(artefacoDB.existeDetalleArtefactoCombustible(artefacto.getId_artefacto()))
			resp = artefacoDB.actualizarArtefactoCombustibleDetalle(artefacto); //Actualizar
		else
			resp = artefacoDB.guardarArtefactoCombustibleDetalle(artefacto); ///guaradar
		
		return resp;
	}
	public int guardarArtefacto(Connection c, ArtefactoTO a) {
		int resp = -1;
		
		IArtefacto artefactoDB = new ArtefactoDB(c);
		
		if(a.getIdArtefacto() > -1 )
			resp =  artefactoDB.ActualizarArtefacto(a);
		else
			resp = artefactoDB.guardarArtefacto(a);
		
		return resp;
	}
	
	public boolean editarArtefactosSync(List<ArtefactoTO> artefactos, Connection c){
		boolean exito = true;
		IArtefacto artefactoDB = new ArtefactoDB(c);
		
		for(ArtefactoTO artefacto : artefactos){
			
			boolean existe = artefactoDB.existeArtefacto(artefacto.getIdArtefacto());
			if(existe)	
				exito = artefactoDB.ActualizarArtefacto(artefacto) >= 0;
			else
				exito = artefactoDB.guardarArtefactosSync(artefacto);
			
			if(!exito) return false;
		}

		return exito;
	}

}

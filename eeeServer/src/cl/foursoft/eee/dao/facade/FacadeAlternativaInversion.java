package cl.foursoft.eee.dao.facade;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.implementation.postgresql.AlternativaInversionDB;
import cl.foursoft.eee.dao.interfaces.IAlternativaInversion;
import cl.foursoft.eee.dao.transferObject.AlternativaDeInversionTO;
import cl.foursoft.eee.dao.transferObject.EnvolventeTO;
import cl.foursoft.eee.dao.transferObject.TipoAlternativaTO;

public class FacadeAlternativaInversion {

	
	
	public int guardarAlternativaEconomicaEnergetica(Connection c, AlternativaDeInversionTO alternativa) {
		int resp = -1;
		int idAlternativa = -1;
		
		IAlternativaInversion alternativaDB = new AlternativaInversionDB(c);
		resp = alternativaDB.guardarAlternativaTipoEnergetica(alternativa);
		idAlternativa = resp;
		
		//Guardar Envolvente nueva 
		if(resp>0)
			resp =  alternativaDB.guardarComposicionAlternativa(alternativa.getEnvolventeNueva(),idAlternativa);

		if(resp > 0)
			resp = alternativaDB.guardarDiferenciaAlternativaEnvolvente(alternativa.getEnvolventeDiferencia(), idAlternativa);
			
		if(resp>0)
			resp = alternativaDB.guardarParametroComposicion( alternativa.getEnvolventeNueva() , idAlternativa ); /* R, U */
		//--
		if(resp>0)
			resp = alternativaDB.guardarInversionEdificio(alternativa.getEnvolventeDiferencia() , idAlternativa , alternativa.getOtroCosto()); //Inversion  de la alternativa
		//--
		if(resp>0)
			resp = idAlternativa;
		
		
		return resp;
	}

	public int guardarAlternativaDeArtefactos(Connection c,	AlternativaDeInversionTO alternativa) {
		int resp = -1;
		int idAlternativa = -1;
		//guardar parte b�sica de la alternartiva
		IAlternativaInversion alternativaDB = new AlternativaInversionDB(c);
		resp = alternativaDB.guardarAlternativaTipoEnergetica(alternativa);
		idAlternativa = resp;
		
		//guardar opciones de la alternativa
		if(resp>0)
			resp = alternativaDB.guardarArtefactosAltertnativa(alternativa.getSensoresAlternativaArtefacto() , idAlternativa);
		//guardar la diferencia
		//Guardar inversion alternativa
		if(resp>0)
			resp = alternativaDB.guardarInversionAlternativaArtefacto(alternativa.getArtefactosDiferencia(), idAlternativa, alternativa.getOtroCosto());
		if(resp>0)
			resp = alternativaDB.guardarRequerimientoAlternativa(alternativa.getSensoresAlternativaArtefacto() , idAlternativa);
		
		idAlternativa = (resp>0) ? idAlternativa : -1;
		
		return idAlternativa;
	}


	public List<AlternativaDeInversionTO> obtenerAlternativasEdificio(int idEdificio, Connection c) {
		List<AlternativaDeInversionTO> resp = null;
		
		IAlternativaInversion alternativaDB = new AlternativaInversionDB(c);
		resp = alternativaDB.obtenerAlternativasPorEdificio(idEdificio);
		
		//Obtener los requerimientos segun tipo de alternativa
		return resp;
	}

	public AlternativaDeInversionTO obtenerAlternativaPorId(int idAlternativa,int idEdificio,  Connection c) {
		AlternativaDeInversionTO resp = null;
		EnvolventeTO envolvente = null;
		
		IAlternativaInversion alternativaDB = new AlternativaInversionDB(c);
		resp = alternativaDB.obtenerAlternativaPorId(idAlternativa);
		
		if(resp!=null)
			envolvente = alternativaDB.obtenerEnvolventeAlternativaConSeccionesHijo(idAlternativa, idEdificio);
		
		if(envolvente!=null)
			resp.setEnvolventeNueva(envolvente);

		
		
		return resp;
	
	}

	public int eliminarAlternativa(int idAlternativa, Connection c) {
		int resp = -1;
		
		IAlternativaInversion alternativaDB = new AlternativaInversionDB(c);
		resp = alternativaDB.eliminarAlternativaPorID(idAlternativa);

		return resp;
	}

	public List<TipoAlternativaTO> obtenerTiposAlternativas(Connection c) {
		List<TipoAlternativaTO> resp = null;
		//EnvolventeTO envolvente = null;
		
		IAlternativaInversion alternativaDB = new AlternativaInversionDB(c);
		resp = alternativaDB.obtenerTiposAlternativas();
		
		return resp;
	}

}

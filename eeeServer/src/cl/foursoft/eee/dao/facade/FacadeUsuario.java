package cl.foursoft.eee.dao.facade;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.implementation.postgresql.UsuarioDB;
import cl.foursoft.eee.dao.interfaces.IUsuario;
import cl.foursoft.eee.dao.transferObject.ClienteTO;
import cl.foursoft.eee.dao.transferObject.UsuarioTO;
import cl.foursoft.eee.util.MailUtil;

public class FacadeUsuario {

		public UsuarioTO obtenerUsuario(String usuario, String contrasenia, Connection c){
			IUsuario usuarioDB = new UsuarioDB(c);
			UsuarioTO resp = null;

			resp = usuarioDB.obtenerUsuario(usuario, contrasenia);
			if(resp!=null)
				resp = usuarioDB.obtenerCliente(resp);
			
			
			
			return resp;
			
		}

		public UsuarioTO obtenerUsuarioKipus(String usuario, String contrasenia, Connection c) {
			 	IUsuario usuarioDB = new UsuarioDB(c);
			 	UsuarioTO resp = null;

			 	resp = usuarioDB.obtenerUsuarioKipus(usuario, contrasenia);
			
			 	return resp;
		}

		public boolean eliminarUsuario(UsuarioTO usuario, Connection c) {
			IUsuario usuarioDB = new UsuarioDB(c);
			boolean resp = false;

 			resp = usuarioDB.eliminarUsuario(usuario);
		
		 	return resp;
		}
		
		public List<UsuarioTO> obtenerUsuarios(Connection c){
			IUsuario usuarioDB = new UsuarioDB(c);
			List<UsuarioTO> resp = null;

		 	resp = usuarioDB.obtenerUsuarios();
		
		 	return resp;
		}
		
	 
		public boolean guardarUsuarios(List<UsuarioTO> usuarios, Connection c){
			IUsuario usuarioDB = new UsuarioDB(c);
			boolean resp = false;
			
			for(UsuarioTO usuario : usuarios){
				if(usuario.getIdUsuario() == -1){
					resp = usuarioDB.guardarUsuario(usuario);
					if(!resp) return false;
				}
				else{
					resp = usuarioDB.editarUsuario(usuario);
					if(!resp) return false;
				}
				
				if(usuario.getUsuarioEditado()==true)
					enviarEmailAviso(usuario);
			}
			
			return true;
		}
		private void enviarEmailAviso(UsuarioTO usuario){
			MailUtil email = new MailUtil();
			
			if(usuario.getIdUsuario() == -1)
				email.usuarioAgregado(usuario.getEmail(), usuario.getNombreUsuario(), usuario.getContrasena());
	 		else
	 			email.passwordCambiado(usuario.getEmail(), usuario.getNombreUsuario(), usuario.getContrasena());
		}
		private void enviarEmailUsuarioCreado(UsuarioTO usuario){
			MailUtil email = new MailUtil();
			email.usuarioAgregado(usuario.getEmail(), usuario.getNombreUsuario(), usuario.getContrasena());
		}
		public boolean obtenerUsuarioPorId(UsuarioTO user, Connection c){
			boolean resp = false;
			MailUtil email = new MailUtil();
			UsuarioTO usuario = null;
			
			IUsuario usuarioDB = new UsuarioDB(c);
			usuario = usuarioDB.obtenerUsuarioPorId(user);
			
			
			resp = email.reenviarDatos(usuario);
			return resp;

		}

		public ClienteTO obtenerCliente(Connection c) {
			IUsuario usuarioDB = new UsuarioDB(c);
			ClienteTO resp = null;

		
				resp = usuarioDB.obtenerCliente();
			
			return resp;
		}
}

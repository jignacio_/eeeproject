package cl.foursoft.eee.dao.facade;

import java.sql.Connection;

import cl.foursoft.eee.dao.implementation.postgresql.RequerimientoEnergeticoDB;
import cl.foursoft.eee.dao.interfaces.IRequerimientoEnergetico;
import cl.foursoft.eee.dao.transferObject.RequerimientoEnergeticoTO;

public class FacadeRequerimientoEnergetico {

	public int guardarRequerimientoEnergeticoEdificio(RequerimientoEnergeticoTO requerimiento, Connection c) {
		int resp = -1;
		// TODO Auto-generated method stub
		IRequerimientoEnergetico ReqDB = new RequerimientoEnergeticoDB(c);
		resp = ReqDB.guardarRequerimientoEnergeticoEdificio(requerimiento); 
		return resp;
	}

	public RequerimientoEnergeticoTO obtenerRequerimientoEnergeticoEdificio(int idEdificio, Connection c) {
		RequerimientoEnergeticoTO resp = null;
		
		IRequerimientoEnergetico reqDB = new RequerimientoEnergeticoDB(c);
		resp = reqDB.obtenerRequerimientoEnergeticoEdificio(idEdificio);
		return resp;
	}

	public int guardarRequerimientoEnergeticoAlternativa(RequerimientoEnergeticoTO requerimiento, Connection c) {
		int resp = -1;
		// TODO Auto-generated method stub
		IRequerimientoEnergetico ReqDB = new RequerimientoEnergeticoDB(c);
		resp = ReqDB.guardarRequerimientoEnergeticoAlternativa(requerimiento); 
		return resp;
	}
	public RequerimientoEnergeticoTO obtenerRequerimientoEnergeticoAlternativa(	int idEdificio, int idAlternativa, Connection c) {

		RequerimientoEnergeticoTO resp = null;
		
		IRequerimientoEnergetico reqDB = new RequerimientoEnergeticoDB(c);
		resp = reqDB.obtenerRequerimientoEnergeticoAlternativa(idEdificio, idAlternativa);
		
		return resp;
	}

	public float obtenerValorRequerimientoTotal(int idEdificio, int idAlternativa, Connection c) {
		float resp = -1f;
		
		IRequerimientoEnergetico reqDB = new RequerimientoEnergeticoDB(c);
		resp  = reqDB.obtenerValorRequerimientoTotal(idEdificio, idAlternativa);
		
		return resp;
	}




}

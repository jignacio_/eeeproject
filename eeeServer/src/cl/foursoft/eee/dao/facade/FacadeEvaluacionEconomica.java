package cl.foursoft.eee.dao.facade;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.implementation.postgresql.EvaluacionEconomicaDB;
import cl.foursoft.eee.dao.interfaces.IEvaluacionEconomica;
import cl.foursoft.eee.dao.transferObject.AlternativaDeInversionTO;
import cl.foursoft.eee.dao.transferObject.EvaluacionEconomicaTO;
import cl.foursoft.eee.dao.transferObject.ParametrosEvaluacionTO;
import cl.foursoft.eee.dao.transferObject.RequerimientoEnergeticoTO;
import cl.foursoft.eee.util.CalculosMatematicos.EvaluacionEconomicaUtil;

public class FacadeEvaluacionEconomica {

	public int crearEvaluacionEconomica(Connection c, int idEdificio,  ParametrosEvaluacionTO parametro, AlternativaDeInversionTO alternativa) {
		int resp =-1;
		
		IEvaluacionEconomica evaluacionDB = new EvaluacionEconomicaDB(c);
		EvaluacionEconomicaTO evaluacionActual = new EvaluacionEconomicaTO();
		
		//obtener requerimiento Edificio.
		FacadeRequerimientoEnergetico facadeReq = new FacadeRequerimientoEnergetico(); 
		RequerimientoEnergeticoTO req = facadeReq.obtenerRequerimientoEnergeticoEdificio(idEdificio, c);
		
		/*REVISAR SI LA ALTERNATIVA TIENE EVALUACIONES ECONīMICAS PARA EL PARAMETRO INDICADO*/
		boolean existe = evaluacionDB.existeEvaluacion(alternativa.getIdAlternativa() ,  parametro.getIdEvaluacion());
		
		EvaluacionEconomicaUtil e = new EvaluacionEconomicaUtil();
		evaluacionActual = e.obtenerEvaluacion(req, alternativa, parametro); // Obtiene el objeto 'EvaluacionEconomicaTO' con el valor de van, tir y payback.
		
		if(existe==false){
			resp = evaluacionDB.guardarEvaluacion(evaluacionActual);
		}
		else{ //Existe, actualizar requerimiento y modificar fecha
			resp = evaluacionDB.actualizarRequerimiento(evaluacionActual);
		}
		
		/*SI EXISTE -> LEER */ 
		
		return resp;
		

		
	}

	public List<EvaluacionEconomicaTO> obtenerEvaluacionesEdificioPorParametro( Connection c, int idEdificio, int idParametro) {
		List<EvaluacionEconomicaTO> resp = null;
		
		IEvaluacionEconomica evaluacionDB = new EvaluacionEconomicaDB(c);
		resp = evaluacionDB.obtenerTodasLasEvaluaciones(idParametro, idEdificio);
		
		return resp;
	}
	
	public EvaluacionEconomicaTO obtenerEvaluacionEconomica(int idAlternativa, int idParametro, Connection c){
		EvaluacionEconomicaTO resp = null;
		
		IEvaluacionEconomica evaluacionDB = new EvaluacionEconomicaDB(c);
		resp = evaluacionDB.obtenerEvaluacion(idAlternativa, idParametro);
		
		
		return resp;
	}

	public List<EvaluacionEconomicaTO> obtenerEvaluacionesEdificio(Connection c, int idEdificio) {
	List<EvaluacionEconomicaTO> resp = null;
		
		IEvaluacionEconomica evaluacionDB = new EvaluacionEconomicaDB(c);
		resp = evaluacionDB.obtenerTodasLasEvaluacionesEdificio(idEdificio);
		
		return resp;
	}
 

}

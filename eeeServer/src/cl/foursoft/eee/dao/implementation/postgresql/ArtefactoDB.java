package cl.foursoft.eee.dao.implementation.postgresql;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cl.foursoft.eee.dao.implementation.DBBase;
import cl.foursoft.eee.dao.interfaces.IArtefacto;
import cl.foursoft.eee.dao.transferObject.ArtefactoCombustibleDetalleTO;
import cl.foursoft.eee.dao.transferObject.ArtefactoTO;
import cl.foursoft.eee.dao.transferObject.MaterialTO;
import cl.foursoft.eee.properties.Propiedades;
import cl.foursoft.eee.util.DirectoriosImagen;
import cl.foursoft.eee.util.UtilLog;

public class ArtefactoDB extends DBBase implements IArtefacto {
	
	

	public ArtefactoDB(Connection c) {
		 this.conn = c;
	}

	@Override
	public ArrayList<ArtefactoTO> obtenerArtefactosPorMedicion(String idMedicion) {
		ArrayList<ArtefactoTO> resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;
		
		String query = "SELECT nombre_artefacto , consumo_artefacto, unidad_consumo, precio_artefacto , id_artefacto , id_medicion " +
					   "FROM artefactos " +
					   "WHERE id_medicion = ? and eliminado = ? " +
					   "ORDER BY nombre_artefacto ASC;";
		
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setString(1, idMedicion);
			pstmt.setBoolean(2, false);
			rs = pstmt.executeQuery();
			
			resp = new ArrayList<ArtefactoTO>();
			while(rs.next()){
				ArtefactoTO artefacto = new ArtefactoTO();
				
				artefacto.setNombreArtefacto(rs.getString(1));
				artefacto.setConsumoArtefacto(rs.getDouble(2));
				artefacto.setUnidadMedidaArtefacto(rs.getString(3));
				artefacto.setCosto_unidad_artefacto(rs.getDouble(4));
				artefacto.setIdArtefacto(rs.getInt(5));
				artefacto.setTipoMedificionArtefacto(rs.getString(6));
				
				if(artefacto.getRutaFotografia().isEmpty()!=false){
					artefacto.setFotografia(obtenerFotografia(artefacto.getRutaFotografia()));
				}
				
				resp.add(artefacto);
			}
			
			
			
			pstmt.close();
			rs.close();
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
			return null;
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			return null;
		}
		
		return resp;
	}
	 

	@Override
	public boolean guardarComposicionArtefactos(List<ArtefactoTO> artefactos , String idSensor) {
		boolean resp = false;
		int _r = -1;
		PreparedStatement pstmt = null;
		
		String query = "INSERT INTO composicion_artefactos(id_sensor, id_artefacto, cantidad_artefactos) "+
					   "VALUES (?, ?, ?);";
		
		
		try {
			pstmt = this.conn.prepareStatement(query);

			
			for (ArtefactoTO artefactoTO : artefactos) {
				pstmt.setString(1, idSensor);
				pstmt.setInt(2, artefactoTO.getIdArtefacto());
				pstmt.setInt(3, artefactoTO.getCantidadArtefacto());
				_r = pstmt.executeUpdate();
				
			}
			resp = (_r>0) ? true : false;
			pstmt.close();
		}catch (SQLException sqle){
			UtilLog.registrar(sqle);
		}catch (Exception e) {
				UtilLog.registrar(e);
			// TODO: handle exception
		}
		
		// TODO Auto-generated method stub
		return resp;
	}

	@Override
	public boolean actualizarComposicion(ArrayList<ArtefactoTO> artefactos) {
		// TODO Auto-generated method stub
		return false;
	}

	 //La composicion de artefactos de un senosor
	@Override
	public List<ArtefactoTO> obtenerArtefactosSensor(String idSensor) {
		List<ArtefactoTO> resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		
		String query = "SELECT a.nombre_artefacto , a.consumo_artefacto, a.unidad_consumo, a.precio_artefacto , c.cantidad_artefactos ,  a.id_medicion , a.id_artefacto " +
					   "FROM composicion_artefactos c " +
					   "INNER JOIN artefactos a ON a.id_artefacto = c.id_artefacto " +
					   "WHERE c.id_sensor = ?;";
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setString(1,idSensor);
			rs = pstmt.executeQuery();
			
			resp = new ArrayList<ArtefactoTO>();
			while(rs.next()){
				ArtefactoTO artefacto = new ArtefactoTO();
				
				artefacto.setNombreArtefacto(rs.getString(1));
				artefacto.setConsumoArtefacto(rs.getDouble(2));
				artefacto.setUnidadMedidaArtefacto(rs.getString(3));
				artefacto.setCosto_unidad_artefacto(rs.getDouble(4));
				artefacto.setCantidadArtefacto(rs.getInt(5));
				artefacto.setTipoMedificionArtefacto(rs.getString(6));
				artefacto.setIdArtefacto(rs.getInt(7));
				
				if(artefacto.getRutaFotografia().isEmpty()!=false){

					artefacto.setFotografia(obtenerFotografia(artefacto.getRutaFotografia()));

				}
				
				resp.add(artefacto);
			}
			
			
			pstmt.close();
			rs.close();
			
		} 
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}
		catch (Exception e) {
			UtilLog.registrar(e);
		}
		
		return resp;
	}

	@Override
	public List<ArtefactoTO> cargarArtefactosSensor(String idSensor) {
		List<ArtefactoTO> resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;
		String query = "SELECT c.id_artefacto, c.cantidad_artefactos , a.nombre_artefacto, a.consumo_artefacto, a.unidad_consumo "+
					  "FROM composicion_artefactos  c "+
					  "INNER JOIN artefactos a ON a.id_artefacto = c.id_artefacto "+
					  "where c.id_sensor = ?;";		
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setString(1, idSensor);
			
			resp = new ArrayList<ArtefactoTO>();
			rs = pstmt.executeQuery();
			while(rs.next()){
				ArtefactoTO artefacto = new ArtefactoTO();
				
				artefacto.setIdArtefacto(rs.getInt(1));
				artefacto.setCantidadArtefacto(rs.getInt(2));
				artefacto.setNombreArtefacto(rs.getString(3));
				artefacto.setConsumoArtefacto(rs.getDouble(4));
				artefacto.setUnidadMedidaArtefacto(rs.getString(5));
				
				resp.add(artefacto);
				
			}
			
			
			rs.close();
			pstmt.close();
		} catch (SQLException sqle) {
			UtilLog.registrar(sqle);
			// TODO: handle exception
		} catch (Exception e) {
			UtilLog.registrar(e);
			// TODO: handle exception
		}
		
		
		return resp;
	}

	@Override
	public List<ArtefactoTO> obtenerTodosLosArtefactos() {
		ArrayList<ArtefactoTO> resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;
		
		String query = "SELECT  nombre_artefacto , consumo_artefacto, unidad_consumo, precio_artefacto , id_artefacto , id_medicion " +
					   "FROM artefactos " +
					   "WHERE eliminado = ? "+
					   "ORDER BY nombre_artefacto ASC;";
 
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setBoolean(1, false);
			rs = pstmt.executeQuery();
			
			resp = new ArrayList<ArtefactoTO>();
			while(rs.next()){
				ArtefactoTO artefacto = new ArtefactoTO();
				
				artefacto.setNombreArtefacto(rs.getString(1));
				artefacto.setConsumoArtefacto(rs.getDouble(2));
				artefacto.setUnidadMedidaArtefacto(rs.getString(3));
				artefacto.setCosto_unidad_artefacto(rs.getDouble(4));
				artefacto.setIdArtefacto(rs.getInt(5));
				artefacto.setTipoMedificionArtefacto(rs.getString(6));
				
				if(artefacto.getRutaFotografia().isEmpty()!=false){

					artefacto.setFotografia(obtenerFotografia(artefacto.getRutaFotografia()));

				}
				
				resp.add(artefacto);
				
			}
			
			
			
			pstmt.close();
			rs.close();
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
			return null;
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			return null;
		}
		
		return resp;
	}
	public List<ArtefactoTO> obtenerTodosLosArtefactosSync() {
		ArrayList<ArtefactoTO> resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;
		
		String query = "SELECT  nombre_artefacto , " +
								" consumo_artefacto, " +
								" unidad_consumo, " +
								" precio_artefacto , " +
								" id_artefacto ,  " +
								" id_medicion , " +
								" eliminado    " +
		              " FROM artefactos " +
		              " ORDER BY nombre_artefacto ASC;";
		
		try {
			pstmt = this.conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			
			resp = new ArrayList<ArtefactoTO>();
			while(rs.next()){
				ArtefactoTO artefacto = new ArtefactoTO();
				
				artefacto.setNombreArtefacto(rs.getString(1));
				artefacto.setConsumoArtefacto(rs.getDouble(2));
				artefacto.setUnidadMedidaArtefacto(rs.getString(3));
				artefacto.setCosto_unidad_artefacto(rs.getDouble(4));
				artefacto.setIdArtefacto(rs.getInt(5));
				artefacto.setTipoMedificionArtefacto(rs.getString(6));
				artefacto.setEliminado(rs.getBoolean(7));
				
				if(artefacto.getRutaFotografia().isEmpty()!=false){
					artefacto.setFotografia(obtenerFotografia(artefacto.getRutaFotografia()));
				}
				
				resp.add(artefacto);
				
			}
			
			
			
			pstmt.close();
			rs.close();
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
			return null;
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			return null;
		}
		
		return resp;
	}

	public boolean eliminarArtefacto(int idArtefacto) {
		boolean resp = false;
		int resp_numero = -1;
		PreparedStatement pstmt = null;
		
//		String query = "DELETE FROM materiales WHERE id_material = ?;";
		String query = "UPDATE artefactos SET eliminado=true WHERE id_artefacto = ?;";
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1, idArtefacto);
			
			
			resp_numero = pstmt.executeUpdate();
			
			//Eliminar el detalle de artefacto combustible
			
			resp = (resp_numero>0) ? true : false;
			
			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp;
	}
	private byte[] obtenerFotografia(String rutaFotografia) {
		
		File file = new File(DirectoriosImagen.DIRECTORIO_DEFINITIVO_ARTEFACTOS+rutaFotografia);
		byte [] buffer = null;// 
		byte [] resp = null;
        ByteArrayOutputStream ous = new ByteArrayOutputStream();
	    InputStream ios;
	   
		try {
			if(file.exists()){
				buffer = new byte[4096];
				ios = new FileInputStream(file);
				int read = 0;
				while ( (read = ios.read(buffer)) != -1 ) {
					ous.write(buffer, 0, read);
				}
				resp = ous.toByteArray();
			}
		}catch (FileNotFoundException e) {
			UtilLog.registrar(e);
			
		} catch (IOException e) {
			UtilLog.registrar(e);		
		}
        
			return resp;
	}

	@Override
	public ArtefactoCombustibleDetalleTO obtenerArtefactoCombustibleCompleto(int idArtefacto) {
		ArtefactoCombustibleDetalleTO resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		
		String query = "SELECT tipo_combustible, potencia, precio, indice_inversion,  eficiencia, energia, eficiencia_distribucion_calor, valor_unitario, "+ 
					" poder_calorifico, valor_consumo_final "+
					" FROM artefacto_combustible_completo " +
					" WHERE id_artefacto = ? ";
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1, idArtefacto);
			rs = pstmt.executeQuery();
			
			if(rs!=null){
				resp = new ArtefactoCombustibleDetalleTO();
				if(rs.next()){
					resp.setIdTipoMedicion(rs.getString(1));
					resp.setPotencia(rs.getDouble(2));
					resp.setPrecio_inversion(rs.getDouble(3));
					resp.setIndice_inversion(rs.getDouble(4));
					resp.setEficiencia(rs.getDouble(5));
					resp.setEnergia(rs.getDouble(6));
					resp.setEficiencia_distribucion_calo(rs.getDouble(7));
					resp.setValor_unitario_combustible(rs.getDouble(8));
					resp.setPoder_calorifico(rs.getDouble(9));
					resp.setValor_consumo_final(rs.getDouble(10));
				}
			}
			
			
			
			pstmt.close();
		}catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}  
		catch (Exception e) {
			UtilLog.registrar(e);
		}

		
		return resp;
	}

	@Override
	public int guardarArtefactoCombustibleDetalle( ArtefactoCombustibleDetalleTO artefacto) {
		int resp = -1;
		PreparedStatement pstmt = null;
		
		String query = "INSERT INTO artefacto_combustible_completo(  id_artefacto, tipo_combustible, potencia, precio, indice_inversion,   " +
																 " eficiencia, energia, eficiencia_distribucion_calor," +
																 " valor_unitario, poder_calorifico, valor_consumo_final) "+
						" VALUES (?, ?, ?, ?, ?,   ?, ?, ?, ?,   ?, ?); ";
		try {
			pstmt = this.conn.prepareStatement(query);

			pstmt.setInt(1, artefacto.getId_artefacto());
			pstmt.setString(2, artefacto.getTipo_combustible());
			pstmt.setDouble(3, artefacto.getPotencia());
			pstmt.setDouble(4, artefacto.getPrecio_inversion());
			pstmt.setDouble(5, artefacto.getIndice_inversion());
			pstmt.setDouble(6, artefacto.getEficiencia());
			pstmt.setDouble(7, artefacto.getEnergia());
			pstmt.setDouble(8, artefacto.getEficiencia_distribucion_calo());
			pstmt.setDouble(9, artefacto.getValor_unitario_combustible());
			pstmt.setDouble(10, artefacto.getPoder_calorifico());
			pstmt.setDouble(11, artefacto.getValor_consumo_final());
			
			resp = pstmt.executeUpdate();
			
			pstmt.close();
		} catch (SQLException sqle) {
			UtilLog.registrar(sqle);
			// TODO: handle exception
		}catch (Exception e) {
			UtilLog.registrar(e);
			// TODO: handle exception
		}
		return resp;
	}

	@Override
	public int actualizarArtefactoCombustibleDetalle( ArtefactoCombustibleDetalleTO artefacto) {
		int resp = -1;
		PreparedStatement pstmt = null;
		
		String query =  " UPDATE artefacto_combustible_completo " +
						" SET id_artefacto=?, tipo_combustible=?, potencia=?, precio=?, indice_inversion=?, eficiencia=?, " +
						"      energia=?, eficiencia_distribucion_calor=?, valor_unitario=?, poder_calorifico=?, valor_consumo_final=? " +
						" WHERE id_artefacto = ? ;";
		try {
			pstmt = this.conn.prepareStatement(query);
			
			pstmt.setInt(1, artefacto.getId_artefacto());
			pstmt.setString(2, artefacto.getTipo_combustible());
			pstmt.setDouble(3, artefacto.getPotencia());
			pstmt.setDouble(4, artefacto.getPrecio_inversion());
			pstmt.setDouble(5, artefacto.getIndice_inversion());
			pstmt.setDouble(6, artefacto.getEficiencia());
			pstmt.setDouble(7, artefacto.getEnergia());
			pstmt.setDouble(8, artefacto.getEficiencia_distribucion_calo());
			pstmt.setDouble(9, artefacto.getValor_unitario_combustible());
			pstmt.setDouble(10, artefacto.getPoder_calorifico());
			pstmt.setDouble(11, artefacto.getValor_consumo_final());
			
			pstmt.setInt(12, artefacto.getId_artefacto());
			resp = pstmt.executeUpdate();
			
			pstmt.close();
		} catch (SQLException sqle) {
			UtilLog.registrar(sqle);
			// TODO: handle exception
		}catch (Exception e) {
			UtilLog.registrar(e);
			// TODO: handle exception
		}
		return resp;
	}
	public boolean existeDetalleArtefactoCombustible(int idArtefacto){
		boolean resp = false;
		int obj = -1;
		PreparedStatement pstmt = null;
		ResultSet rs =  null;
		
		String query = "SELECT COUNT(ID_ARTEFACTO) FROM artefacto_combustible_completo WHERE id_artefacto = ?;";
		
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1, idArtefacto);

			
			rs = pstmt.executeQuery();
			if(rs.next())
				obj = rs.getInt(1);
			
			resp = (obj>0) ? true : false;
			
			pstmt.close();
		}catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}
		catch (Exception e) {
			UtilLog.registrar(e);
		}
		
		
		return resp;
	}

	@Override
	public int guardarArtefacto(ArtefactoTO a) {
		int resp = -1;
		int idArtefactoNuevo = -1;
		PreparedStatement pstmt = null;
		
		String query = "INSERT INTO artefactos(  id_artefacto, nombre_artefacto, consumo_artefacto, id_medicion,  unidad_consumo, precio_artefacto, eliminado) "+
					   " VALUES (?, ?, ?, ?, ?, ?, ?);";
		try {
			idArtefactoNuevo = this.obtenerSiguienteID();
			pstmt = this.conn.prepareStatement(query);
			
			pstmt.setInt(1, idArtefactoNuevo);
			pstmt.setString(2,a.getNombreArtefacto());
			pstmt.setDouble(3, a.getConsumoArtefacto());
			pstmt.setString(4, a.getTipoMedificionArtefacto());
			pstmt.setString(5, a.getUnidadMedidaArtefacto());
			pstmt.setDouble(6, a.getCosto_unidad_artefacto());
			pstmt.setBoolean(7, false);

			resp = pstmt.executeUpdate();

			
			if(resp>-1 && a.getFotografia()!=null)
				guardarFotografia(a.getFotografia(), idArtefactoNuevo+".jpg");
				
			resp = (resp>-1) ? idArtefactoNuevo : -1; 
			
			pstmt.close();
		} catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}catch (Exception e) {
			UtilLog.registrar(e);
		}
		return resp;
	}

	@Override
	public int ActualizarArtefacto(ArtefactoTO a) {
		int resp = -1;
		PreparedStatement pstmt = null;
		
		String query =  "UPDATE artefactos SET  nombre_artefacto=?, consumo_artefacto=?, id_medicion=?,   unidad_consumo=?, precio_artefacto=?, eliminado=? "+
						" WHERE id_artefacto = ?;";
		try {
			pstmt = this.conn.prepareStatement(query);
			
			pstmt.setString(1,a.getNombreArtefacto());
			pstmt.setDouble(2, a.getConsumoArtefacto());
			pstmt.setString(3, a.getTipoMedificionArtefacto());
			pstmt.setString(4, a.getUnidadMedidaArtefacto());
			pstmt.setDouble(5, a.getCosto_unidad_artefacto());
			pstmt.setBoolean(6, false);
			
			pstmt.setInt(7, a.getIdArtefacto());

			if(resp>-1 && a.getFotografia()!=null)
				this.guardarFotografia(a.getFotografia(), a.getIdArtefacto()+".jpg");

			resp = pstmt.executeUpdate();
			resp = (resp>-1) ? a.getIdArtefacto() : -1; 
			
			pstmt.close();
		} catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}catch (Exception e) {
			UtilLog.registrar(e);
		}
		return resp;
	}
	
	private int obtenerSiguienteID()
	{
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        int resp = -1;
        String query = "SELECT nextval('artefactos_id_artefacto_seq') as NEXTVAL";
        
        try
        {                        
            pstmt = conn.prepareStatement(query);            
            rs = pstmt.executeQuery();
            
            if(rs.next())
                resp = rs.getInt("NEXTVAL");
            
            rs.close();
            pstmt.close();
            
        }
        catch (SQLException sqle) {
        	UtilLog.registrar(sqle);
        }
        finally{
            if (pstmt != null) {
                  try {
                       pstmt.close();
                       rs.close();
                  } catch (SQLException e) {
                       e.printStackTrace();
                  }
            }
        }        
        return resp;
    }
	
	 private int guardarFotografia(byte[] fotografia, String nombreFotografia) {
			int resp = -1;
			File directorio; 
			
			OutputStream out;
			try {
				//comprobar si esta el directorio, sino lo crea
				directorio = new File(DirectoriosImagen.DIRECTORIO_DEFINITIVO_ARTEFACTOS);
			  	directorio.mkdirs();
			  	
			  	
				out = new FileOutputStream(DirectoriosImagen.DIRECTORIO_DEFINITIVO_ARTEFACTOS+nombreFotografia);
				out.write(fotografia);
				out.close();
				resp = 0;
			} 
			catch (FileNotFoundException e) {
				UtilLog.registrar(e);
			}
			catch (IOException e) {
				UtilLog.registrar(e);
			}
			
			return resp;
		}
	 
		@Override
		public boolean guardarArtefactosSync(ArtefactoTO a) {
			int resp_int = -1;
			boolean resp = false;
			int idArtefactoNuevo = -1;
			PreparedStatement pstmt = null;
			
			String query = "INSERT INTO artefactos(  id_artefacto, nombre_artefacto, consumo_artefacto, id_medicion,  unidad_consumo, precio_artefacto, eliminado) "+
						   " VALUES (?, ?, ?, ?, ?, ?, ?);";
			try {
			//	idArtefactoNuevo = this.obtenerSiguienteID();
				pstmt = this.conn.prepareStatement(query);
				
				pstmt.setInt(1, a.getIdArtefacto());
				pstmt.setString(2,a.getNombreArtefacto());
				pstmt.setDouble(3, a.getConsumoArtefacto());
				pstmt.setString(4, a.getTipoMedificionArtefacto());
				pstmt.setString(5, a.getUnidadMedidaArtefacto());
				pstmt.setDouble(6, a.getCosto_unidad_artefacto());
				pstmt.setBoolean(7, a.getEliminado());

				resp_int = pstmt.executeUpdate();

				
				if(resp_int>-1 && a.getFotografia()!=null)
					guardarFotografia(a.getFotografia(), idArtefactoNuevo+".jpg");
					
				resp = (resp_int>-1) ? true : false; 
				
				pstmt.close();
			} catch (SQLException sqle) {
				UtilLog.registrar(sqle);
			}catch (Exception e) {
				UtilLog.registrar(e);
			}
			return resp;
		}

		@Override
		public boolean existeArtefacto(int idArtefacto) {
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			boolean existe = false;
			String query = " SELECT COUNT(id_artefacto) FROM artefactos WHERE id_artefacto = ?;";
			
			try{
				pstmt = conn.prepareStatement(query);
				
				pstmt.setInt(1, idArtefacto);			
				rs = pstmt.executeQuery();
			
				//rs = pstmt.getResultSet();
				if(rs.next()){
					existe = (rs.getInt(1) > 0) ? true : false;
				}
				
				pstmt.close();
			}
			catch (SQLException sqle) {
				UtilLog.registrar(sqle);
				return true;
			}
			catch (Exception e) { 
				UtilLog.registrar(e);
				return true;
			}
			
			return existe;
		}
	 
	 
}

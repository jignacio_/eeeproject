package cl.foursoft.eee.dao.implementation.postgresql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cl.foursoft.eee.dao.facade.FacadeRequerimientoEnergetico;
import cl.foursoft.eee.dao.facade.FacadeSeccion;
import cl.foursoft.eee.dao.implementation.DBBase;
import cl.foursoft.eee.dao.interfaces.IAlternativaInversion;
import cl.foursoft.eee.dao.transferObject.AlternativaDeInversionTO;
import cl.foursoft.eee.dao.transferObject.ArtefactoTO;
import cl.foursoft.eee.dao.transferObject.EnvolventeTO;
import cl.foursoft.eee.dao.transferObject.MaterialTO;
import cl.foursoft.eee.dao.transferObject.SeccionTO;
import cl.foursoft.eee.dao.transferObject.SensorTO;
import cl.foursoft.eee.dao.transferObject.TipoAlternativaTO;
import cl.foursoft.eee.service.RequerimientoEnergeticoService;
import cl.foursoft.eee.util.CalculosUtil;
import cl.foursoft.eee.util.UtilLog;
import cl.foursoft.eee.util.UtilSeccion;
import cl.foursoft.eee.util.tipoAlternativaUtil;

public class AlternativaInversionDB extends DBBase implements IAlternativaInversion {
	
	public AlternativaInversionDB(Connection c){
		this.conn = c;
	}

	@Override
	public int guardarAlternativaTipoEnergetica(AlternativaDeInversionTO alternativa) {
		int resp = -1;
		int idAlternativa = -1;
		PreparedStatement pstmt = null;
		
		String query = "INSERT INTO ALTERNATIVA(  id_alternativa, " +
												" id_tipo_alternativa ," +
												" id_edificio , " +
												" descripcion_alternativa	) " +
						"VALUES( ? , ? , ? , ? )	";
		
		try {
			
			if(alternativa.getIdAlternativa()<0){
				pstmt = this.conn.prepareStatement(query);
				idAlternativa = obtenerSiguienteID();

				pstmt.setInt(1, idAlternativa);
				pstmt.setInt(2, alternativa.getIdTipoAlternativa());
				pstmt.setInt(3, alternativa.getIdEdificio());
				pstmt.setString(4, alternativa.getDescripcionAlternativa());

				resp = pstmt.executeUpdate();

				//Guardar Envolvente nueva 
				//resp = this.guardarComposicionAlternativa(alternativa.getEnvolventeNueva(),idAlternativa);

				resp = (resp>0) ? idAlternativa : resp ;
			}
			else
				resp = 0;
			
			
			pstmt.close();
			
			
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp;
	}
 
	public int guardarComposicionAlternativa(EnvolventeTO nuevaEnvolvente, int idAlternativa){
		int resp = -1;
		PreparedStatement pstmt = null;
		List<SeccionTO> seccionesAlternativa = new ArrayList<SeccionTO>();
		String query = "INSERT INTO composicion_alternativa(id_alternativa, " +
															" id_tipo_seccion , " +
															" id_material , cantidad_material) " +
						"VALUES (?,?, ? , ?)";
//		MODIFICAR CUANDO LOS HIJOS TENGAN SU PROPiA COMPOSICION (VENTANAS Y TECHO-CUPULA Y MUROS-PUERTAS)
		try {
			pstmt = this.conn.prepareStatement(query);
			seccionesAlternativa = nuevaEnvolvente.obtenerListaSecciones();
			
			for (SeccionTO s : seccionesAlternativa){
				for(MaterialTO m : s.getComposicion()){
					//MaterialTO m = seccionActual.getComposicion().get(i);
					pstmt.setInt(1, idAlternativa);
					pstmt.setInt(2, s.getIdSeccion());
					pstmt.setInt(3, m.getIdMaterial());
					pstmt.setFloat(4, m.getCantidad_material());
					resp = pstmt.executeUpdate();
				}
			}
			resp = idAlternativa;
			pstmt.close();
		} catch (Exception e) {
			resp = -1;
				UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		
		return resp;
	}

	@Override
	public List<AlternativaDeInversionTO> obtenerAlternativasPorEdificio(int idEdificio) {
		List<AlternativaDeInversionTO> resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int idAlternativa;
		RequerimientoEnergeticoService reqService = new RequerimientoEnergeticoService();
		FacadeRequerimientoEnergetico facadeRequerimiento = new FacadeRequerimientoEnergetico();
		float requerimientoTotal = -1f;

		String query = "SELECT  a.id_alternativa, " +
						"a.descripcion_alternativa , " +
						"alt.costo_total_alternativa , " +
						"r.requerimiento_alternativa_tatal ," +
						"a.id_tipo_alternativa " +
						"FROM  ALTERNATIVA a " +
						"LEFT OUTER JOIN INVERSION_ALTERNATIVA alt ON  a.id_alternativa  = alt.id_alternativa "+
						"LEFT OUTER JOIN REQUERIMIENTO_ENERGETICO_ALTERN r ON  a.id_alternativa  = r.id_alternativa "+
						"WHERE id_edificio = ? ";
		
		
		String query2 = "SELECT  a.id_alternativa, " +
						"a.descripcion_alternativa , " +
						"alt.costo_total_alternativa , " +
						"r.consumo_total ," +
						"a.id_tipo_alternativa ," +
						"r.unidad_consumo " +
						"FROM  ALTERNATIVA a " +
						"LEFT OUTER JOIN INVERSION_ALTERNATIVA alt ON  a.id_alternativa  = alt.id_alternativa "+
						"LEFT OUTER JOIN RESUEMEN_REQUERIMIENTO_ALTERNAT r ON  a.id_alternativa  = r.id_alternativa "+
						"WHERE id_edificio = ? ";

 	
								
		
		try {
			
			pstmt = this.conn.prepareStatement(query2);
			pstmt.setInt(1, idEdificio);
			
			rs = pstmt.executeQuery();
			resp = new ArrayList<AlternativaDeInversionTO>();
			while(rs.next()){
				AlternativaDeInversionTO alternativaActual = new AlternativaDeInversionTO();
				EnvolventeTO envolventeActual = new EnvolventeTO();
				idAlternativa = rs.getInt(1); 
				
				alternativaActual.setIdAlternativa(idAlternativa);
				alternativaActual.setDescripcionAlternativa(rs.getString(2));
				alternativaActual.setIdTipoAlternativa(tipoAlternativaUtil.tipoAlternativaEnergetica);
				alternativaActual.setIdEdificio(idEdificio);
				alternativaActual.setInversionAlternativa(rs.getFloat(3));
				alternativaActual.setRequerimientoEnergeticoTotal(rs.getFloat(4));
				alternativaActual.setIdTipoAlternativa(rs.getInt(5));
				alternativaActual.setUnidadMedicionAlternativa(rs.getString(6));
				
				if(alternativaActual.getIdTipoAlternativa() == tipoAlternativaUtil.tipoAlternativaEnergetica){
					envolventeActual = obtenerEnvolventeAlternativaSinSeccionesHijo(idAlternativa, idEdificio);
					if(envolventeActual!=null)
						alternativaActual.setEnvolventeNueva(envolventeActual);

					requerimientoTotal = alternativaActual.getRequerimientoEnergeticoTotal();
					if(requerimientoTotal<=0 && idAlternativa>0 ){
						reqService.guardarRequerimientoEnergeticoAlternativa(idEdificio, idAlternativa);
						requerimientoTotal = facadeRequerimiento.obtenerValorRequerimientoTotal(idEdificio, idAlternativa, this.conn);//Obtener el requerimiento total de la alternativa
						alternativaActual.setRequerimientoEnergeticoTotal(requerimientoTotal);


					}
					
				}
				else if(alternativaActual.getIdTipoAlternativa() == tipoAlternativaUtil.tipoAlternativaArtefacto){
						//recalcularAlternativa
				}
				resp.add(alternativaActual);
			}
			
			
			rs.close();
			pstmt.close();
		}
		catch (Exception e) {
			UtilLog.registrar(e);
		}
		finally{
			if(pstmt!=null){
				try {
					if(rs!=null)
						rs.close();
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		
		return resp;
	}

		public int guardarParametroComposicion(EnvolventeTO envolventeAlternativa, int idAlternativa){
		int resp = 0;
		List<SeccionTO> seccionesAlternativa = new ArrayList<SeccionTO>();
		PreparedStatement pstmt = null;
		String query ="INSERT INTO PARAMETROS_CALCULADOS_ALTERNATI(ID_TIPO_SECCION , " +
																   "TRANSMITANCIA, " +
																   "RESISTENCIA_TOTAL ," +
																   "ID_ALTERNATIVA) " +
							  "VALUES(?,?,?,?)";
		
		try {
			
			pstmt = this.conn.prepareStatement(query);
			
			seccionesAlternativa = envolventeAlternativa.obtenerListaSecciones();
			
			for (SeccionTO s : seccionesAlternativa){
				pstmt.setInt(1, s.getIdSeccionPadre());
				pstmt.setFloat(2, s.getTransmitanciaTermica());
				pstmt.setFloat(3, s.getResistenciaTotalMasRSERSI());
				pstmt.setInt(4,idAlternativa); 

				resp = pstmt.executeUpdate();
			}
			
			 
			
			pstmt.close();
			
			
		} catch (Exception e) {
			UtilLog.registrar(e);
		}
		finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sql) {
				 UtilLog.registrar(sql);
				}
			}
		}
		
		
		return resp;
	}
	 

	public EnvolventeTO obtenerEnvolventeAlternativaSinSeccionesHijo(int idAlternativa, int idEdificio){
		EnvolventeTO resp = null;
		int idTipoSeccion;
		String nombreTipoSeccion;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = "SELECT id_tipo_seccion_envolvente , nombre_seccion_envolvente " +
					   "FROM tipo_seccion_envolvente " +
					   "WHERE id_tipo_seccion_envolvente = id_tipo_seccion_envolvente_padr;";
		
		try {
			pstmt = this.conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			resp = new  EnvolventeTO();
			while(rs.next()){
				SeccionTO seccActual = new SeccionTO();
				idTipoSeccion = rs.getInt(1);
				nombreTipoSeccion = rs.getString(2);
				
				seccActual.setIdTipoSeccion(idTipoSeccion);
				seccActual.setIdTipoSeccion(idTipoSeccion);
				seccActual.setNombreTipo(nombreTipoSeccion);
				seccActual.setNombre_seccion_padre(nombreTipoSeccion);
				seccActual.setComposicion(this.obtenerComposicionAlternativa(idAlternativa, idTipoSeccion));
				
				
				if(nombreTipoSeccion.equals(UtilSeccion.SECCION_AREA_SUPERIOR)){
					resp.setAreaSuperior(seccActual);
				}
				else if(nombreTipoSeccion.equals( UtilSeccion.SECCION_MUROS) ){
					resp.setMuros(seccActual);
				}
				else if(nombreTipoSeccion.equals(UtilSeccion.SECCION_VENTANAS) ){
					resp.setVentanas(seccActual);
				}
				else if(nombreTipoSeccion.equals(UtilSeccion.SECCION_AREA_BASE)){
					resp.setAreaPiso(seccActual);
				}
				else if(nombreTipoSeccion.equals(UtilSeccion.SECCION_PUERTAS)){
					resp.setPuertas(seccActual);
				}
				else if(nombreTipoSeccion.equals(UtilSeccion.SECCION_OTRAS_INSTALACIONES)){
					resp.getOtrasSecciones().add(seccActual);
				}
			}
			
			
			rs.close();
			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					if(rs!=null)
						rs.close();
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		
		
		return resp;
		
	}
	
	public EnvolventeTO obtenerEnvolventeAlternativaConSeccionesHijo(int idAlternativa, int idEdificio){
		EnvolventeTO resp = null;
		int idTipoSeccion;
		String nombreTipoSeccion;
		PreparedStatement pstmt = null;
		FacadeSeccion fs = new FacadeSeccion();
		List<SeccionTO> _hijos = null;
		ResultSet rs = null;
		
		String query = "SELECT ts.id_tipo_seccion_envolvente , " +
							 " ts.nombre_seccion_envolvente , " +
							 "p.transmitancia, " +
							 "p.resistencia_total " +
					   "FROM TIPO_SECCION_ENVOLVENTE ts " +
					   "INNER JOIN PARAMETROS_CALCULADOS_ALTERNATI p  ON ts.id_tipo_seccion_envolvente_padr = p.id_tipo_seccion " +
					   "WHERE ts.id_tipo_seccion_envolvente = ts.id_tipo_seccion_envolvente_padr and p.id_alternativa = ? ;";
		
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1, idAlternativa);
			rs = pstmt.executeQuery();
			resp = new  EnvolventeTO();
			while(rs.next()){
				SeccionTO seccActual = new SeccionTO();
				idTipoSeccion = rs.getInt(1);
				nombreTipoSeccion = rs.getString(2);
				
				seccActual.setIdTipoSeccion(idTipoSeccion);
				seccActual.setNombreTipo(nombreTipoSeccion);
				seccActual.setNombre_seccion_padre(nombreTipoSeccion);
				seccActual.setComposicion(this.obtenerComposicionAlternativa(idAlternativa, idTipoSeccion));
				seccActual.setTransmitanciaTermica(rs.getFloat(3));
				seccActual.setResistenciaTotalMasRSERSI(rs.getFloat(4));	
				
				_hijos = fs.obtenerSeccionesHijoEdificio(this.conn, idTipoSeccion, idEdificio, nombreTipoSeccion);
				seccActual.setSeccionesHijo(_hijos);
				seccActual.setArea( CalculosUtil.calcularSuperficieSeccion(seccActual.getSeccionesHijo()) );
				
				
				
				if(nombreTipoSeccion.equals(UtilSeccion.SECCION_AREA_SUPERIOR)){
					resp.setAreaSuperior(seccActual);
				}
				else if(nombreTipoSeccion.equals( UtilSeccion.SECCION_MUROS) ){
					resp.setMuros(seccActual);
				}
				else if(nombreTipoSeccion.equals(UtilSeccion.SECCION_VENTANAS) ){
					resp.setVentanas(seccActual);
				}
				else if(nombreTipoSeccion.equals(UtilSeccion.SECCION_AREA_BASE)){
					resp.setAreaPiso(seccActual);
				}
				else if(nombreTipoSeccion.equals(UtilSeccion.SECCION_PUERTAS)){
					resp.setPuertas(seccActual);
				}
				else if(nombreTipoSeccion.equals(UtilSeccion.SECCION_OTRAS_INSTALACIONES)){
					resp.getOtrasSecciones().add(seccActual);
				}
			}
			
			
			rs.close();
			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					if(rs!=null)
						rs.close();
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		
		
		return resp;
		
	}
	
	private List<MaterialTO> obtenerComposicionAlternativa(int idAlternativa, int idTipoSeccion){
		List<MaterialTO> resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String query 			= "SELECT c.id_material , " +
										 "m.nombre_material, " +
										 "m.imagen_material , " +
										 "m.espesor_material, " +
										 "m.conductividad_termica , " +
										 "m.transmitancia_termica , " +
										 "m.resistencia_termica , " +
										 "mp.unidad_material , " +
										 "c.cantidad_material " +
								  "FROM composicion_alternativa c " +
								  "INNER JOIN materiales m on c.id_material = m.id_material " +
								  "INNER JOIN material_precio mp on c.id_material = mp.id_material " +
								  "WHERE c.id_alternativa = ? and c.id_tipo_seccion = ?;";
		
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1,idAlternativa);
			pstmt.setInt(2,idTipoSeccion);
			rs = pstmt.executeQuery();
			resp = new ArrayList<MaterialTO>();
			
			while(rs.next()){
				MaterialTO m = new MaterialTO();
				m.setIdSeccionEdificio(idTipoSeccion);
				m.setIdMaterial(rs.getInt(1));
				m.setNombreMaterial(rs.getString(2));
				m.setRutaFotografia(rs.getString(3));
				m.setEspesor(rs.getFloat(4));
				m.setConductividad(rs.getFloat(5));
				m.setTransmitancia(rs.getFloat(6));
				m.setResistencia(rs.getFloat(7));
				
				m.setUnidad_material(rs.getString(8));
				m.setCantidad_material(rs.getInt(9));
				
				//Obtener Fotograf�a
				resp.add(m);
			}
			
			pstmt.close();
			rs.close();
			
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
					if(rs!=null)
						rs.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		
		return resp;		
	}
	private int obtenerSiguienteID(){
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        int resp = -1;
        String query = "SELECT nextval('alternativa_id_alternativa_seq') as NEXTVAL";
        
        try
        {                        
            pstmt = conn.prepareStatement(query);            
            rs = pstmt.executeQuery();
            
            if(rs.next())
                resp = rs.getInt("NEXTVAL");
            
            rs.close();
            pstmt.close();
            
        }
        catch (SQLException sqle) {
        	UtilLog.registrar(sqle);
        }
        finally{
            if (pstmt != null) {
                  try {
                       pstmt.close();
                       rs.close();
                  } catch (SQLException e) {
                       e.printStackTrace();
                  }
            }
        }        
        return resp;
	}

	@Override
	
	
	public AlternativaDeInversionTO obtenerAlternativaPorId(int idAlternativa) {
		AlternativaDeInversionTO resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = "SELECT  a.id_edificio, " +
								"a.descripcion_alternativa , " +
								"alt.costo_total_alternativa , " +
								"r.requerimiento_alternativa_tatal " +
						"FROM  alternativa a "+
						"LEFT OUTER JOIN INVERSION_ALTERNATIVA alt ON  a.id_alternativa  = alt.id_alternativa "+
						"LEFT OUTER JOIN REQUERIMIENTO_ENERGETICO_ALTERN r ON a.id_alternativa = r.id_alternativa "+
						"WHERE a.id_alternativa = ? ";
 							
		
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1, idAlternativa);
			
			rs = pstmt.executeQuery();
			resp = new AlternativaDeInversionTO();
			if(rs.next()){
				//AlternativaDeInversionTO alternativaActual = new AlternativaDeInversionTO();
				//EnvolventeTO envolventeActual = new EnvolventeTO();
				
				resp.setIdTipoAlternativa(tipoAlternativaUtil.tipoAlternativaEnergetica);
				resp.setIdEdificio(rs.getInt(1));
				resp.setIdAlternativa(idAlternativa);
				resp.setDescripcionAlternativa(rs.getString(2));
				resp.setInversionAlternativa(rs.getFloat(3));
				resp.setRequerimientoEnergeticoTotal(rs.getFloat(4));
				
			}
			
			
			rs.close();
			pstmt.close();
		}
		catch (Exception e) {
			UtilLog.registrar(e);
		}
		finally{
			if(pstmt!=null){
				try {
					if(rs!=null)
						rs.close();
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		
		return resp;
	}

	@Override
	public int guardarInversionEdificio(List<MaterialTO> envolventeDiferencia, int idAlternativa, float otroCosto) {
		int resp = -1;
		PreparedStatement pstmt = null;
			
		String query = "INSERT INTO INVERSION_ALTERNATIVA (id_alternativa, costo_total_alternativa) VALUES (?,?)";
		try {
			pstmt = conn.prepareStatement(query);
			float costo = 0f;
		
			for(MaterialTO m : envolventeDiferencia){
				costo += m.getCostoTotalMaterial(); 
			}
			costo +=otroCosto;
			pstmt.setInt(1,idAlternativa);
			pstmt.setFloat(2,costo);
			
			resp = pstmt.executeUpdate();
			
			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);		
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		
		
		return resp;
	}

	@Override
	public int eliminarAlternativaPorID(int idAlternativa) {
		int resp = -1;
		PreparedStatement pstmt = null;
		
		String query  = "DELETE FROM ALTERNATIVA WHERE id_alternativa = ? ; ";
		try {
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, idAlternativa);
			resp =	pstmt.executeUpdate();
			
			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
						UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp;
	}

	@Override
	public List<TipoAlternativaTO> obtenerTiposAlternativas() {
		List<TipoAlternativaTO> resp = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs =null;
			
			String query = "SELECT id_tipo_alternativa, nombre_tipo_alternativa, observacion_tipo_alternativa "+
						  " FROM tipo_alternativa;";
		
		
		try {
			pstmt = this.conn.prepareStatement(query);
			
			rs = pstmt.executeQuery();
			resp = new ArrayList<TipoAlternativaTO>();

			while(rs.next()){
				TipoAlternativaTO _alter = new TipoAlternativaTO();
				
				_alter.setIdAlternativa(rs.getInt(1));
				_alter.setNombreAlternativa(rs.getString(2));
				_alter.setDescripcionAlternativa(rs.getString(3));
				
				resp.add(_alter);
			}
			
			rs.close();
			pstmt.close();
		}catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}catch (Exception e) {
			UtilLog.registrar(e);
		} 
		return resp; 
	}

	@Override
	public int guardarArtefactosAltertnativa(List<SensorTO> sensoresAlternativas, int idAlterativa) {
		int resp = -1;
		PreparedStatement pstmt = null;

		String query = "INSERT INTO composicion_alternativa_artefac(id_alternativa, id_sensor, id_artefacto , cantidad_artefactos) " +
					   "  VALUES (?, ?, ?, ?);";
		try {
			pstmt = this.conn.prepareStatement(query);
			
			
			for(SensorTO s : sensoresAlternativas){
				for(ArtefactoTO a :s.getArtefactosMedidios()){
					pstmt.setInt(1, idAlterativa);
					pstmt.setString(2,s.getCodigoSensor());
					pstmt.setInt(3, a.getIdArtefacto());
					pstmt.setInt(4, a.getCantidadArtefacto());
					
					resp = pstmt.executeUpdate();
				}
			}
			
			
			pstmt.close();	
		} catch (SQLException ex) {
			UtilLog.registrar(ex);
		} catch (Exception e) {
			UtilLog.registrar(e);
		} 
		return resp;
	}
	@Override
	public int  guardarInversionAlternativa(List<SensorTO> sensoresAlternativas, int idAlterativa , float otroCosto){
		int resp = -1;
		double totalInversionAlternativa = 0;
		PreparedStatement pstmt = null;

		
		totalInversionAlternativa = calcularInversionAlternativa(sensoresAlternativas, otroCosto );
		String query = "INSERT INTO inversion_alternativa(id_alternativa, costo_total_alternativa) "+
					   " VALUES (?, ?);";
		
		try {
			pstmt = this.conn.prepareStatement(query);
			
			pstmt.setInt(1, idAlterativa);
			pstmt.setDouble(2, totalInversionAlternativa); 
			
			resp = pstmt.executeUpdate();
			
			pstmt.close();	
		} catch (SQLException ex) {
			UtilLog.registrar(ex);
		} catch (Exception e) {
			UtilLog.registrar(e);
		} 
		
		
		return resp;
	}
	private double calcularInversionAlternativa(List<SensorTO> sensoresAlternativas, float otroCosto){
		double total = 0;
		
		for(SensorTO s : sensoresAlternativas){
			for(ArtefactoTO a : s.getArtefactosMedidios()){
					total += ( a.getCosto_unidad_artefacto() * a.getCantidadArtefacto()); 
			}
		}
		
		total +=otroCosto;
		
		return total;
	}

	@Override
	public int guardarDiferenciaAlternativaEnvolvente( List<MaterialTO> materialesDiferencia, int idAlternativa) {
		// TODO Auto-generated method stub
		int resp = -1;
		PreparedStatement pstmt = null;
		String query = "INSERT INTO diferencia_materiales( id_alternativa, id_material, cantidad_material) "+
					   "  VALUES (?, ?, ?);";
		try {
			pstmt = this.conn.prepareStatement(query);
			
				for(MaterialTO m : materialesDiferencia){
					//MaterialTO m = seccionActual.getComposicion().get(i);
					pstmt.setInt(1, idAlternativa);
					pstmt.setInt(2, m.getIdMaterial());
					pstmt.setFloat(3, m.getCantidad_material());
					resp = pstmt.executeUpdate();
				}
			resp = idAlternativa;
			pstmt.close();
		} catch (Exception e) {
			resp = -1;
				UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		
		return resp;
	}

	@Override
	public int guardarRequerimientoAlternativa(List<SensorTO> sensores, int idAlternativa) {
		int resp = -1;
		PreparedStatement pstmt = null ;
		double requerimientoTotal = 0;
		String idMedicion = "";
		String unidadMedicion ="";
		
		String query = " INSERT INTO req_energetico_artefactos_alt( id_alternativa, requerimiento_total, tipo_medicion, unidad_medicion_alternativa) "+
					   " VALUES (?, ?, ?, ?);";
		try {
			pstmt = this.conn.prepareStatement(query);
			
			//Calcular Requerimiento de los artefactos
			for(SensorTO s : sensores){
					requerimientoTotal += s.getConsumoTotalEstimado();
			}
			
			if(sensores.get(0)!=null){
				idMedicion = sensores.get(0).getCodigoTipoMedicion();
				unidadMedicion = sensores.get(0).getUnidadMedicionSensor();
			}
			//
			pstmt.setInt(1, idAlternativa);
			pstmt.setDouble(2, requerimientoTotal);
			pstmt.setString(3, idMedicion);
			pstmt.setString(4, unidadMedicion);
			//
			
			resp = pstmt.executeUpdate();
			
			if(resp>0)
				guardarResumenRequerimientoAlternativa(idAlternativa, requerimientoTotal, unidadMedicion, tipoAlternativaUtil.tipoAlternativaArtefacto);
			
			pstmt.close();
		}catch (SQLException sqle) {
				UtilLog.registrar(sqle);
		}catch (Exception e) {
			UtilLog.registrar(e);
		}
		
		return resp;
	}
	public int guardarResumenRequerimientoAlternativa( int idAlternativa , double consumo, String unidad, int idTipoAlternativa){
		int resp = -1;
		PreparedStatement pstmt = null;
		String query = "INSERT INTO resuemen_requerimiento_alternat( id_alternativa, consumo_total, unidad_consumo, id_tipo_alternativa) "+
					   "  VALUES (?, ?, ?, ?);";
		try{
			pstmt = this.conn.prepareStatement(query);
			
			pstmt.setInt(1, idAlternativa);
			pstmt.setDouble(2, consumo);
			pstmt.setString(3, unidad);
			pstmt.setInt(4, idTipoAlternativa);
			
			
			resp = pstmt.executeUpdate();
			pstmt.close();
		}catch (SQLException sqle  ) {
				UtilLog.registrar(sqle);
		}catch (Exception e) {
			UtilLog.registrar(e);
			// TODO: handle exception
		}
		
		
		return resp;
	}
	@Override
	public int guardarInversionAlternativaArtefacto(List<ArtefactoTO> artefactosDiferencia, int idAlternativa,float otroCosto) {

		int resp = -1;
		PreparedStatement pstmt = null;
			
		String query = "INSERT INTO INVERSION_ALTERNATIVA (id_alternativa, costo_total_alternativa) VALUES (?,?)";
		try {
			pstmt = conn.prepareStatement(query);
			float costo = 0f;
		
			for(ArtefactoTO a : artefactosDiferencia){
				costo += a.getCostoTotalArtefactos(); 
			}
			costo +=otroCosto;
			pstmt.setInt(1,idAlternativa);
			pstmt.setFloat(2,costo);
			
			resp = pstmt.executeUpdate();
			
			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);		
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		
		
		return resp;
	}

	@Override
	public int guardarDiferenciaAlternativaArtefacto(
			List<ArtefactoTO> artefactosDiferencia, int idAlternativa) {
		// TODO Auto-generated method stub
		return 0;
	}
}

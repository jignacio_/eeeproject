package cl.foursoft.eee.dao.implementation.postgresql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cl.foursoft.eee.dao.implementation.DBBase;
import cl.foursoft.eee.dao.interfaces.ICliente;
import cl.foursoft.eee.dao.transferObject.ClienteTO;
import cl.foursoft.eee.util.UtilLog;

public class ClienteDB extends DBBase implements ICliente {

	public ClienteDB( Connection c){
		this.conn = c;
	}
	
	@Override
	public List<ClienteTO> obtenerTodosLosClientes() {
		List<ClienteTO> resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs =  null;
		
 
		
		String query = "SELECT id_cliente, nombre_cliente, direccion_cliente, numero_telefono, ruta_fotografia " +
					  " FROM cliente ";
		
		try {
			pstmt = this.conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			
			
			
			resp = new ArrayList<ClienteTO>();
			while(rs.next()){
				ClienteTO _nuevoCliente = new ClienteTO();
				
				_nuevoCliente.setIdCliente(rs.getInt(1));
				_nuevoCliente.setNombreCliente(rs.getString(2));
				_nuevoCliente.setDireccionPrincipalCliente(rs.getString(3));
				_nuevoCliente.setNumeroTelefono(rs.getInt(4));
				_nuevoCliente.setRutaFotografia(rs.getString(5));
				
				resp.add(_nuevoCliente);
			}
			
			
			rs.close();
			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					if(rs!=null)
						rs.close();
					pstmt.close();
				} catch (SQLException sqle) {
						UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp;
	}
	
	public int obtenerSiguienteID()
	{
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        int resp = -1;
        String query = "SELECT nextval('cliente_id_cliente_seq') as NEXTVAL";
        
        try
        {                        
            pstmt = conn.prepareStatement(query);            
            rs = pstmt.executeQuery();
            
            if(rs.next())
                resp = rs.getInt("NEXTVAL");
            
            rs.close();
            pstmt.close();
            
        }
        catch (SQLException sqle) {
        	UtilLog.registrar(sqle);
        }
        finally{
            if (pstmt != null) {
                  try {
                       pstmt.close();
                       rs.close();
                  } catch (SQLException e) {
                       e.printStackTrace();
                  }
            }
        }        
        return resp;
    }

	@Override
	public int guardarCliente(ClienteTO cliente) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int eliminarCliente(int idCliente) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int actualizarCliente(ClienteTO cliente) {
		// TODO Auto-generated method stub
		return 0;
	}
}

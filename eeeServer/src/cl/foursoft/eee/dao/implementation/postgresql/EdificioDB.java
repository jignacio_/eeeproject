package cl.foursoft.eee.dao.implementation.postgresql;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import cl.foursoft.eee.dao.facade.FacadeArtefactos;
import cl.foursoft.eee.dao.implementation.DBBase;
import cl.foursoft.eee.dao.interfaces.IEdificio;
import cl.foursoft.eee.dao.interfaces.ISeccion;
import cl.foursoft.eee.dao.transferObject.EdificioTO;
import cl.foursoft.eee.dao.transferObject.ParametrosEdificioTO;
import cl.foursoft.eee.dao.transferObject.PisoTO;
import cl.foursoft.eee.dao.transferObject.SensorTO;
import cl.foursoft.eee.dao.transferObject.ZonaTO;
import cl.foursoft.eee.util.DirectoriosImagen;
import cl.foursoft.eee.util.UtilFechas;
import cl.foursoft.eee.util.UtilLog;

public class EdificioDB extends DBBase implements IEdificio{

	
	public EdificioDB(Connection c) {
		 this.conn = c;
	}
	public EdificioTO obtenerEdificio(int idEdificio){
		EdificioTO resp = null; 
		PreparedStatement pstmt  = null;
		ResultSet rs = null;
		String query = "SELECT   id_cliente, " +
						"nombre_edificio, " +
						"direccion_edificio, " +
						"ciudad_edificio, " + 
						"longitud_edificio, " +
						"latitud_edificio, " +
						"observaciones_edificio,  " +
						"ruta_fotografia, " +
						"superficie_util, " +
						"superficie_bruta, " +
						"volumen_aire, " +
						"perimetro," +
						"direccion_completa, " +
						"largo," +
						"ancho," +
						"altura_piso," +
						"id_factor ," +
						"aire_acondicionado , " +
						"fecha_creacion ," +
						"region_edificio ," +
						"pais_edificio ," +
						"valor_factor_renovacion , " +
						"numero_actual_edificio " +
						"FROM edificio " + 
						"WHERE id_edificio = ? ";
		
		
		 
		List<ParametrosEdificioTO> parametrosEdificio = null;
		List<PisoTO> pisosEdificio  = null;
		List<SensorTO> sensoresEdificio = null;
		//List<SeccionTO> seccionesEdificio = null;
		
		
		try{
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1,idEdificio);
			
			rs = pstmt.executeQuery();
			if(rs.next()){
				resp = new EdificioTO();
				resp.setIdEdificio(idEdificio);
				resp.setCodigoEdificio(idEdificio);
				
				
				resp.setIdCliente(rs.getInt(1));
				resp.setNombreEdificio(rs.getString(2));
				resp.setDireccionEdificio(rs.getString(3));
				resp.setCiudadEdificio(rs.getString(4));
				resp.setLongitud(rs.getDouble(5));
				resp.setLatitud(rs.getDouble(6));
				resp.setObservaciones(rs.getString(7));
				resp.setRutaFotografia(rs.getString(8));

				resp.setSuperficieUtil (rs.getFloat(9));
				resp.setSuperficieBruta(rs.getFloat(10));
				resp.setVolumenDeAire(rs.getFloat(11));
				resp.setPerimetro(rs.getFloat(12));
				resp.setDireccionCompletaEdificio(rs.getString(13));	
				
				resp.setLargo(rs.getFloat(14));
				resp.setAncho(rs.getFloat(15));
				resp.setAltoPiso(rs.getFloat(16));
				
				resp.setIdFactorRenovacion(rs.getInt(17));
				resp.setAireAcondicionado(rs.getBoolean(18));
				
				resp.setFotografia(obtenerFotografia(resp.getRutaFotografia()));
				
				
				//--Fecha
				Timestamp fechaCreacion = rs.getTimestamp(19);
				String fecha = UtilFechas.fechaComoString(fechaCreacion);
				resp.setFechaCreacion(fecha);
				//--
				
				resp.setRegionEdificio(rs.getString(20));
				resp.setPaisEdificio(rs.getString(21));
				resp.setValorFactorRenovacion(rs.getFloat(22));
				resp.setNumeroEdificio(rs.getInt(23));
				//Cargar Parametros
				parametrosEdificio = obtenerParametrosPorIdEdificio(idEdificio);
				if(parametrosEdificio!= null) 
					resp.setArrCollParametrosEdificio(parametrosEdificio);
				
				
				// Cargar Pisos - Zonas ( Sensores-Piso, Sensores-Zonas )
				 pisosEdificio 	 = obtenerPisosEdificio(idEdificio); //Incluyen Zonas y Sensores
				 if(pisosEdificio!=null)
					resp.setArrCollPisosEdificio(pisosEdificio);
				
				 sensoresEdificio = obtenerSensoresPorUbicacion(idEdificio,0,0); //Sensores del Edificio
				 if(sensoresEdificio!=null)
					resp.setArrCollSensoresEdificio(sensoresEdificio); 
			}
			
			
			//Cargar Seccions <- Materiales.
			
			rs.close();
			pstmt.close();
			
			
		}catch(Exception e){
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					if(rs!=null)
						rs.close();
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);	
				}
			}
		}
		
		
		
		return resp;
	}
	//Edificio para Alternativa
	
	public EdificioTO obtenerEdificioParaAlternativa(int idEdificio){
		EdificioTO resp = null; 
		PreparedStatement pstmt  = null;
		ResultSet rs = null;
		String query = "SELECT   id_cliente, " +
						"nombre_edificio, " +
						"direccion_edificio, " +
						"ciudad_edificio, " + 
						"longitud_edificio, " +
						"latitud_edificio, " +
						"observaciones_edificio,  " +
						"ruta_fotografia, " +
						"superficie_util, " +
						"superficie_bruta, " +
						"volumen_aire, " +
						"perimetro," +
						"direccion_completa, " +
						"largo," +
						"ancho," +
						"altura_piso," +
						"id_factor ," +
						"aire_acondicionado ," +
						"region_edificio , " +
						"pais_edificio ," +
						"valor_factor_renovacion , " +
						"numero_actual_edificio " +
						"FROM edificio " + 
						"WHERE id_edificio = ? ";
		
		List<ParametrosEdificioTO> parametrosEdificio = null;
		
		try{
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1,idEdificio);
			
			rs = pstmt.executeQuery();
			if(rs.next()){
				resp = new EdificioTO();
				resp.setIdEdificio(idEdificio);
				resp.setCodigoEdificio(idEdificio);
				
				
				resp.setIdCliente(rs.getInt(1));
				resp.setNombreEdificio(rs.getString(2));
				resp.setDireccionEdificio(rs.getString(3));
				resp.setCiudadEdificio(rs.getString(4));
				resp.setLongitud(rs.getDouble(5));
				resp.setLatitud(rs.getDouble(6));
				resp.setObservaciones(rs.getString(7));
				resp.setRutaFotografia(rs.getString(8));

				resp.setSuperficieUtil (rs.getFloat(9));
				resp.setSuperficieBruta(rs.getFloat(10));
				resp.setVolumenDeAire(rs.getFloat(11));
				resp.setPerimetro(rs.getFloat(12));
				resp.setDireccionCompletaEdificio(rs.getString(13));	
				
				resp.setLargo(rs.getFloat(14));
				resp.setAncho(rs.getFloat(15));
				resp.setAltoPiso(rs.getFloat(16));
				
				resp.setIdFactorRenovacion(rs.getInt(17));
				resp.setAireAcondicionado(rs.getBoolean(18));
				
				
				resp.setRegionEdificio(rs.getString(19));
				resp.setPaisEdificio(rs.getString(20));
				resp.setValorFactorRenovacion(rs.getFloat(21));
				resp.setNumeroEdificio(rs.getInt(22));
				resp.setFotografia(obtenerFotografia(resp.getRutaFotografia()));
				
				//Cargar Parametros
				parametrosEdificio = obtenerParametrosPorIdEdificio(idEdificio);
				if(parametrosEdificio!= null) 
					resp.setArrCollParametrosEdificio(parametrosEdificio);
				
			}
			
			rs.close();
			pstmt.close();
			
			
		}catch(Exception e){
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					if(rs!=null)
						rs.close();
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);	
				}
			}
		}
		
		
		
		return resp;
	}
	private List<SensorTO> obtenerSensoresPorUbicacion(int idEdificio, int idPiso, int idZona){
		List<SensorTO> resp = null;
		String codigoMedicion;
		PreparedStatement pstmt = null;
		ResultSet rs 			= null;
		
		FacadeArtefactos facadeArtefactos = new FacadeArtefactos();
		String query = "";
		try {
			resp = new ArrayList<SensorTO>();
			if(idPiso == 0 ){//SENSOR EDIFICIO
				query = "SELECT s.id_sensor, s.id_consumo , ts.id_tipo_medicion,   ts.nombre_tipo_consumo , tsm.nombre_tipo_consumo , ss.descripcion_artefactos , ss.horas_por_dia , ss.dias_por_mes , ss.consumo_anual_estimado ,  tsm.unidad_medicion"+
						" FROM sensor_edificio s " + 
						" INNER JOIN tipo_consumo_sensor ts on ts.id_tipo_consumo = s.id_consumo "+
						" INNER JOIN  tipo_consumo_sensor tsm on ts.id_tipo_medicion = tsm.id_tipo_consumo " +
						" INNER JOIN sensor ss on ss.id_sensor = s.id_sensor "+
						" WHERE s.id_edificio = ? ;";
				
				pstmt = this.conn.prepareStatement(query);
				pstmt.setInt(1, idEdificio);
				rs = pstmt.executeQuery();
				while(rs.next()){
					SensorTO s = new SensorTO();
					
					s.setCodigoSensor(rs.getString(1));
					s.setCodigoTipoConsumo(rs.getString(2));
					s.setCodigoTipoMedicion(rs.getString(3));
					s.setNombreTipoConsumo(rs.getString(4));
					s.setNombreTipoMedicion(rs.getString(5));
					
					s.setDescripcionArtefactos(rs.getString(6));
					s.setUsoHorasDiarias(rs.getInt(7));
					s.setUsoDiasMensuales(rs.getInt(8));
					s.setConsumoTotalEstimado(rs.getDouble(9));
					s.setUnidadMedicionSensor(rs.getString(10));
					 
					int numero_actual = Integer.parseInt( s.getCodigoSensor().substring(10) ) ;//String.valueOf(s.getCodigoTipoConsumo().charAt());
					s.setNumeroSensorActual(numero_actual);
					s.setGuardado(true);
					
					s.setIdEdificio(idEdificio);
					s.setIdPiso(0);
					s.setIdZona(0);
					
					s.setArtefactosMedidios( facadeArtefactos.obtenerArtefactosSensor(s.getCodigoSensor(), this.conn));
					
					resp.add(s);
				}
				
			}
			else if(idPiso>0 && idZona==0){//SENSOR PISO
				
				query = "SELECT s.id_sensor, s.id_consumo , ts.id_tipo_medicion,   ts.nombre_tipo_consumo , tsm.nombre_tipo_consumo  , ss.descripcion_artefactos , ss.horas_por_dia , ss.dias_por_mes , ss.consumo_anual_estimado ,  tsm.unidad_medicion"+
						" FROM sensor_piso s " + 
						" INNER JOIN tipo_consumo_sensor ts on ts.id_tipo_consumo = s.id_consumo "+
						" INNER JOIN  tipo_consumo_sensor tsm on ts.id_tipo_medicion = tsm.id_tipo_consumo " +
						" INNER JOIN sensor ss on ss.id_sensor = s.id_sensor "+
						" WHERE s.id_piso = ?;";
				
				pstmt = this.conn.prepareStatement(query);
				pstmt.setInt(1, idPiso);
				rs = pstmt.executeQuery();
				while(rs.next()){
					SensorTO s = new SensorTO();
					
					s.setCodigoSensor(rs.getString(1));
					s.setCodigoTipoConsumo(rs.getString(2));
					s.setCodigoTipoMedicion(rs.getString(3));
					s.setNombreTipoConsumo(rs.getString(4));
					s.setNombreTipoMedicion(rs.getString(5));
					
					s.setDescripcionArtefactos(rs.getString(6));
					s.setUsoHorasDiarias(rs.getInt(7));
					s.setUsoDiasMensuales(rs.getInt(8));
					s.setConsumoTotalEstimado(rs.getDouble(9));
					s.setUnidadMedicionSensor(rs.getString(10));
					
					int numero_actual = Integer.parseInt( s.getCodigoSensor().substring(10) ) ;//String.valueOf(s.getCodigoTipoConsumo().charAt());
					s.setNumeroSensorActual(numero_actual);
					s.setGuardado(true);
					
					
					s.setIdEdificio(idEdificio);
					s.setIdPiso(idPiso);
					s.setIdZona(0);
					
					s.setArtefactosMedidios( facadeArtefactos.obtenerArtefactosSensor(s.getCodigoSensor(), this.conn));

					resp.add(s);
				}
			}
			else if(idPiso>0 && idZona>0){//SENSOR ZONA
				
				query = "SELECT s.id_sensor, s.id_consumo , ts.id_tipo_medicion,   ts.nombre_tipo_consumo , tsm.nombre_tipo_consumo , ss.descripcion_artefactos , ss.horas_por_dia , ss.dias_por_mes , ss.consumo_anual_estimado ,  tsm.unidad_medicion"+
						" FROM sensor_zona s " + 
						" INNER JOIN tipo_consumo_sensor ts on ts.id_tipo_consumo = s.id_consumo "+
						" INNER JOIN  tipo_consumo_sensor tsm on ts.id_tipo_medicion = tsm.id_tipo_consumo "+
						" INNER JOIN sensor ss on ss.id_sensor = s.id_sensor "+
						" WHERE s.id_zona = ?;";
				
				pstmt = this.conn.prepareStatement(query);
				pstmt.setInt(1, idZona);
				rs = pstmt.executeQuery();
				while(rs.next()){
					SensorTO s = new SensorTO();
					
					s.setCodigoSensor(rs.getString(1));
					s.setCodigoTipoConsumo(rs.getString(2));
					s.setCodigoTipoMedicion(rs.getString(3));
					s.setNombreTipoConsumo(rs.getString(4));
					s.setNombreTipoMedicion(rs.getString(5));
					
					int numero_actual = Integer.parseInt( s.getCodigoSensor().substring(10) ) ;//String.valueOf(s.getCodigoTipoConsumo().charAt());
					s.setNumeroSensorActual(numero_actual);
					s.setGuardado(true);
					
					s.setIdEdificio(idEdificio);
					s.setIdPiso(idPiso);
					s.setIdZona(idZona);
					
					s.setDescripcionArtefactos(rs.getString(6));
					s.setUsoHorasDiarias(rs.getInt(7));
					s.setUsoDiasMensuales(rs.getInt(8));
					s.setConsumoTotalEstimado(rs.getDouble(9));
					s.setUnidadMedicionSensor(rs.getString(10));
					s.setArtefactosMedidios( facadeArtefactos.obtenerArtefactosSensor(s.getCodigoSensor(), this.conn));

					
					resp.add(s);
				}
			}
			
			
			
			
			
			rs.close();
			pstmt.close();
			
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
					if(rs!=null)
						rs.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		
		return resp;
	}
	private List<PisoTO> obtenerPisosEdificio(int IdEdificio){
		List<PisoTO> resp = null;
		List<ZonaTO> zonas = null;
		List<SensorTO> sensores = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String query 			= "SELECT id_piso, nombre_piso " +
								  "FROM piso " +
								  "WHERE id_edificio = ?";
		
		try {
			
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, IdEdificio);
			
			rs = pstmt.executeQuery();
			
			resp = new ArrayList<PisoTO>();
			
			//Agrewgar un contador o obtener valor desde el ID, pero enumerar los pisos
			String nombrePiso = ""; 
			while (rs.next()){
				PisoTO p = new PisoTO();
				p.setIdPiso(rs.getInt(1));
				p.setNombrePiso(rs.getString(2));
				
				int l = String.valueOf(p.getIdPiso()).length();
				int numeroActual = Integer.parseInt( String.valueOf(p.getIdPiso()).substring(l-2) );
				p.setNumeroPisoActual(numeroActual);
				p.setGuardado(true);
				
				//ObtenerZonasPiso
				zonas = obtenerZonasPiso(p.getIdPiso(), IdEdificio);
				if(zonas!=null)
					p.setArrCollZonasPiso(zonas);
				//ObtenerSensoresPiso
				sensores = obtenerSensoresPorUbicacion(IdEdificio, p.getIdPiso(),0);
				if(zonas!=null)
					p.setArrCollSensoresPiso(sensores);
				
				resp.add(p);
			}
			

			pstmt.close();
			rs.close();
			
			
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
					if(rs!=null)
						rs.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		
		return resp;
	}
	
	
	private List<ZonaTO> obtenerZonasPiso(int idPiso, int idEdificio){
		List<ZonaTO> resp = null;
		List<SensorTO> sensores = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String query 			= "SELECT id_zona, nombre_zona " +
								  "FROM zona " +
								  "WHERE id_piso = ? ; ";
		
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1,idPiso);
			
			rs = pstmt.executeQuery();
			
			resp = new ArrayList<ZonaTO>();
			sensores = new ArrayList<SensorTO>();
			while (rs.next()){
				ZonaTO z = new ZonaTO();
				z.setIdZona(rs.getInt(1));
				z.setNombreZona(rs.getString(2));
				z.setIdPiso(idPiso);
				
				int l = String.valueOf(z.getIdZona()).length();
				int numeroActual = Integer.parseInt( String.valueOf(z.getIdZona()).substring(l-2) );
				z.setNumeroZonaActual(numeroActual);
				z.setGuardado(true);
				resp.add(z);
				
				//obtener Sensores Zona
				sensores = this.obtenerSensoresPorUbicacion(idEdificio, idPiso, z.getIdZona());
				if(sensores!=null)
					z.setArrCollSensoresZona(sensores);
			}
			
			rs.close();
			pstmt.close();
			
			
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
					if(rs!=null)
						rs.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		
		return resp;
	}

	public List<EdificioTO> obtenerTodosLosEdificiosPorCliente(int idCliente){
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<EdificioTO> resp = null;
		
		
		String query = "SELECT   id_edificio, " +
								"nombre_edificio, " +
								"direccion_edificio, " +
								"ciudad_edificio, " + 
								"longitud_edificio, " +
								"latitud_edificio, " +
								"observaciones_edificio , " +
								"ruta_fotografia," +
								"direccion_completa , " +
								"fecha_creacion , " +
								"numero_actual_edificio " +
								"FROM edificio " + 
								"WHERE id_cliente = ? " +
								"ORDER BY id_edificio ASC;	";
		try{
			
			
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, idCliente);
			rs = pstmt.executeQuery();
			
			resp = new ArrayList<EdificioTO>();
			
			while(rs.next()){
				EdificioTO edificio = new EdificioTO();
				
				edificio.setIdEdificio(rs.getInt(1));
				edificio.setCodigoEdificio(rs.getInt(1));
				edificio.setNombreEdificio(rs.getString(2));
				edificio.setDireccionEdificio(rs.getString(3));
				edificio.setCiudadEdificio(rs.getString(4));
				edificio.setLongitud(rs.getDouble(5));
				edificio.setLatitud(rs.getDouble(6));
				edificio.setObservaciones(rs.getString(7));
				edificio.setRutaFotografia(rs.getString(8));
				edificio.setDireccionCompletaEdificio(rs.getString(9));
				edificio.setFotografia(obtenerFotografia(edificio.getRutaFotografia()));
				//--Fecha
				Timestamp fechaCreacion = rs.getTimestamp(10);
				
				String fecha = UtilFechas.fechaComoString(fechaCreacion);
				edificio.setFechaCreacion(fecha.toString());
				edificio.setNumeroEdificio(rs.getInt(11));
				//--
				
				resp.add(edificio);
				
				//cargar Canridad de Alternativas por tipo (1 tipos : de envolvente y de artefactos);
				edificio = cantidadAlternativas(edificio);
				
			}
			
			rs.close();
			pstmt.close();
			
			
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
		}
		finally{
			if(pstmt != null){
				try{
					if(rs!=null)
						rs.close();
					pstmt.close();
				}catch(SQLException sqle){
					UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp;
	}
	private EdificioTO cantidadAlternativas(EdificioTO edificio){
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int tipoActual = -1;

			String query = "select  count(a.id_alternativa) ,  t.id_tipo_alternativa "+
			"from alternativa a "+
			"inner join tipo_alternativa t on a.id_tipo_alternativa = t.id_tipo_alternativa "+
			"where a.id_edificio = ? "+
			"group by a.id_tipo_alternativa , t.id_tipo_alternativa ";

		try{
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1, edificio.getCodigoEdificio());
			
			rs = pstmt.executeQuery();
			
			while(rs.next()){
				tipoActual = rs.getInt(2);
				//AlternativaEnvolventez
				if(tipoActual == 1)
					edificio.setCantidadAlternativasEnvolvente(rs.getInt(1));
				//Alternativa Artefactos
				else if(tipoActual == 2)
					edificio.setCantidadAlternativasArtefactos(rs.getInt(1));
			}

			rs.close();
			pstmt.close();
		}catch(SQLException sqle){
			UtilLog.registrar(sqle);
		}catch(Exception e){
			UtilLog.registrar(e);
		}
		
		return edificio; 
	}
//	@Override
//	public EdificioTO obtenerEdificioPorID(int IdEdificio) {
//		EdificioTO resp = null;
//		return resp;
//	}
	@Override
	public int guardarEdificio(EdificioTO edificioNuevo){
		
		PreparedStatement  pstmt = null;
		
		int resp = -1;
		String query = "INSERT INTO edificio(id_edificio, " +
											"id_cliente, " +	
											"nombre_edificio, " +
											"observaciones_edificio, " +
											"ruta_fotografia, " +
											"direccion_edificio, " +
											"pais_edificio, " +
											"region_edificio, " +
											"ciudad_edificio, " +
											"longitud_edificio, " +
											"latitud_edificio, " +
											"altura_piso, " +
											"largo, " +
											"ancho, " +
											"superficie_util," +
											"superficie_bruta," +
											"volumen_aire," +
											"perimetro," +
											"direccion_completa, " +
											"id_factor , " +
											"aire_acondicionado , " +
											"fecha_creacion ," +
											"valor_factor_renovacion , " +
											"numero_actual_edificio )" +
											"values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
		try{
			int id = edificioNuevo.getCodigoEdificio();// obtenerSiguienteId();
			edificioNuevo.setIdEdificio(id);
			 
			if(edificioNuevo.getFotografia()!=null)
				edificioNuevo.setRutaFotografia(id+".jpg");
			
			
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1, edificioNuevo.getCodigoEdificio());
			pstmt.setInt(2, edificioNuevo.getIdCliente());
			pstmt.setString(3, edificioNuevo.getNombreEdificio());
			pstmt.setString(4, edificioNuevo.getObservaciones());
			pstmt.setString(5,edificioNuevo.getRutaFotografia());
			
			pstmt.setString(6, edificioNuevo.getDireccionEdificio());
			pstmt.setString(7,edificioNuevo.getPaisEdificio());
			pstmt.setString(8,edificioNuevo.getRegionEdificio());
			pstmt.setString(9,edificioNuevo.getCiudadEdificio());
			pstmt.setDouble(10, edificioNuevo.getLongitud());
			pstmt.setDouble(11, edificioNuevo.getLatitud());
			
			pstmt.setFloat(12, edificioNuevo.getAltoPiso());
			pstmt.setFloat(13, edificioNuevo.getLargo());
			pstmt.setFloat(14, edificioNuevo.getAncho());

			pstmt.setFloat(15, edificioNuevo.getSuperficieUtil());
			pstmt.setFloat(16, edificioNuevo.getSuperficieBruta());
			pstmt.setFloat(17, edificioNuevo.getVolumenDeAire());
			pstmt.setFloat(18, edificioNuevo.getPerimetro());
			
			pstmt.setString(19, edificioNuevo.getDireccionCompletaEdificio());
			pstmt.setInt(20,edificioNuevo.getIdFactorRenovacion());

			pstmt.setBoolean(21,edificioNuevo.getAireAcondicionado());
			pstmt.setTimestamp(22,UtilFechas.hoy()); 
			pstmt.setFloat(23, edificioNuevo.getValorFactorRenovacion());
			pstmt.setInt(24, edificioNuevo.getNumeroEdificio());
			resp = pstmt.executeUpdate();
			
			
			//--
			 if( resp > 0  ){
				 
				 if(edificioNuevo.getFotografia()!=null )
					 resp = this.guardarFotografia(edificioNuevo.getFotografia() ,edificioNuevo.getRutaFotografia());
				 
				 if(edificioNuevo.getArrCollPisosEdificio()!=null) 
					 resp = this.guardarPisos(edificioNuevo.getArrCollPisosEdificio() , edificioNuevo.getCodigoEdificio());

				 if(edificioNuevo.getArrCollSensoresEdificio()!=null)  
					 resp = this.guardarSensores(edificioNuevo.getArrCollSensoresEdificio(), edificioNuevo.getCodigoEdificio(), 0, 0);
				 
				 if(edificioNuevo.getArrCollParametrosEdificio()!=null)
					 resp = this.guardarParametros(edificioNuevo.getArrCollParametrosEdificio(), edificioNuevo.getCodigoEdificio());
			  }
			 //--
			 
			 resp = ( resp > 0 ) ? edificioNuevo.getCodigoEdificio() : resp;
			 pstmt.close();
			
		}
		catch(SQLException sqle){
			UtilLog.registrar(sqle);
		}
		finally{
				if( pstmt != null ){
					try{
						pstmt.close();
					}catch(SQLException sqle){
						UtilLog.registrar(sqle);
					}
				}
		}
		   
		
 		return resp;
	}
	@Override
	public int guardarFotografia (byte[] fotografia, String nombreFotografia) {
		int resp = -1;
		File directorio; 
		
		OutputStream out;
		try {
			//comprobar si esta el directorio, sino lo crea
			directorio = new File(DirectoriosImagen.DIRECTORIO_DEFINITIVO);
		  	directorio.mkdirs();
		  	
		  	
			out = new FileOutputStream(DirectoriosImagen.DIRECTORIO_DEFINITIVO+nombreFotografia);
			out.write(fotografia);
			out.close();
			resp = 0;
		} 
		catch (FileNotFoundException e) {
			UtilLog.registrar(e);
		}
		catch (IOException e) {
			UtilLog.registrar(e);
		}
		
		return resp;
	}
	private int guardarPisos(List<PisoTO> pisos, int idEdificio){
		int resp = -1;
		PreparedStatement pstmt = null;
	 
		String query = "Insert Into PISO(id_piso, id_edificio, nombre_piso) VALUES(?,?,?);";
		try{
			pstmt = this.conn.prepareStatement(query);
			

			for(int i=0; i<pisos.size() ; i++){
				PisoTO pisoActual = pisos.get(i);
				
				if(pisoActual.getGuardado()){
					resp = actualizarPisos(pisoActual, idEdificio);
				}
				else{
					//Podira haber existido antes, por lo tanto, verificar esto, si es si (eliminar sus posibles zonas/sensores) si no, continuar y guardar
					if(existePiso(pisoActual.getIdPiso())){
						eliminarPiso(pisoActual.getIdPiso());//Ellimina sensor_piso, zona, sensor_zona, sensor (eventualmente sus datos)
					}
					
					pstmt.setInt(1, pisoActual.getIdPiso()); 
					pstmt.setInt(2, idEdificio);
					pstmt.setString(3, pisoActual.getNombrePiso());


					resp = pstmt.executeUpdate();

				}

					if(resp>0){
						if(pisoActual.getArrCollZonasPiso()!=null && pisoActual.getArrCollZonasPiso().size()>1) // valida cantidad 
							resp = this.guardarZonas(pisoActual.getArrCollZonasPiso(),idEdificio, pisoActual.getIdPiso());
					}
					if(resp>0){
						if(pisoActual.getArrCollSensoresPiso()!=null && pisoActual.getArrCollSensoresPiso().size()>0)
							resp = this.guardarSensores(pisoActual.getArrCollSensoresPiso(), idEdificio, pisoActual.getIdPiso(), 0); // pisoActual.getIdPiso() = 0 ??
					}
			}
			 
			
			pstmt.close();	
			
		}catch (Exception e) {
			UtilLog.registrar(e);
		}
		finally{
			if( pstmt != null ){
				try{
					pstmt.close();
				}catch(SQLException sqle){
					UtilLog.registrar(sqle);
				}
			}
	}
		 
		return resp;
	}
	//funcciones de Piso
	private int actualizarPisos(PisoTO piso, int idEdificio){
		int resp = -1;
		PreparedStatement pstmt = null;
		
		String queryActualizar = "UPDATE PISO SET nombre_piso = ?  WHERE id_piso = ?;" ;
		try {
			pstmt = this.conn.prepareStatement(queryActualizar);
			
			pstmt.setString(1, piso.getNombrePiso());
			pstmt.setInt(2, piso.getIdPiso());	
			resp= pstmt.executeUpdate();
			
			
			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
						UtilLog.registrar(sqle);
				} 
			}
		}
		
		return resp;
	}
	
	private boolean eliminarPiso(int idPiso){
		boolean resp = false;
		PreparedStatement pstmt = null;
		
		String query = "DELETE FROM piso WHERE id_piso:?;";
		//Eliminar Piso, ZOna, Sensor_piso, sensor_zona , sensor(con trigger)
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1, idPiso);
			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			try {
				pstmt.close();
			} catch (SQLException e2) {
				UtilLog.registrar(e2);
			}
		}
		return resp;
	}
	
	//
	private int guardarZonas(List<ZonaTO> zonas, int idEdificio, int idPiso){
		int resp = -1;
		PreparedStatement pstmt = null;
		
		String query = "Insert Into ZONA(id_zona, id_piso, id_edificio, nombre_zona) VALUES(?,?,?,?);";
		
		try{
			
			pstmt = this.conn.prepareStatement(query);
			
			//Guardar Sensores de la Zona 0, es decir, del Piso Completo al que pertenece dicha zona.

			for(int i = 1 ; i< zonas.size() ; i++){
				
				ZonaTO zonaActual = new ZonaTO();
				zonaActual = zonas.get(i);
				
				if(zonaActual.getGuardado()){
					resp = actualizarZonas(zonaActual, idEdificio);
				}
				else{
					//zonaActual.setIdZona(this.obtenerSiguienteIdZonas());

					pstmt.setInt(1, zonaActual.getIdZona());
					pstmt.setInt(2, idPiso);
					pstmt.setInt(3, idEdificio );
					pstmt.setString(4, zonaActual.getNombreZona());

					resp = 	pstmt.executeUpdate();
				}
				
				if(resp>0){
					if(zonaActual.getArrCollSensoresZona()!=null && zonaActual.getArrCollSensoresZona().size()>0)
						resp = this.guardarSensores(zonaActual.getArrCollSensoresZona(), idEdificio, idPiso, zonaActual.getIdZona());
				}
			}
			pstmt.close();
			
		}catch (Exception e) {
			UtilLog.registrar(e);
		}
		finally{
			if( pstmt != null ){
				try{
					pstmt.close();
				}catch(SQLException sqle){
					UtilLog.registrar(sqle);
				}
			}
	}
		return resp;
	}
//	
	//funcciones de Piso
	private int actualizarZonas(ZonaTO zona, int idEdificio){
		int resp = -1;
		PreparedStatement pstmt = null;
		
		String queryActualizar = "UPDATE ZONA SET nombre_zona=? WHERE id_zona = ?;" ;
		
		try {
			pstmt = this.conn.prepareStatement(queryActualizar);
			
				
			pstmt.setString(1, zona.getNombreZona());
			pstmt.setInt(2, zona.getIdZona());	
			
			resp = pstmt.executeUpdate();
			
			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
						UtilLog.registrar(sqle);
				} 
			}
		}
		
		return resp;
	}
	private boolean EliminarZona(int idZona){
		boolean resp = false;
		PreparedStatement pstmt = null;
		
		String query = "DELETE FROM zona WHERE id_zona = ?;";
		//Eliminar  ZOna,   sensor_zona , sensor(con trigger)
		try {
			pstmt = this.conn.prepareStatement(query);
			
			pstmt.setInt(1, idZona);
			
			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			try {
				pstmt.close();
			} catch (SQLException e2) {
				UtilLog.registrar(e2);
			}
		}
		return resp;
	}
//	
	private int guardarSensores(List<SensorTO> sensores, int idEdificio, int idPiso, int idZona){
		int resp = -1;
		PreparedStatement pstmt = null;
		FacadeArtefactos fa = new FacadeArtefactos();;
		String query = "Insert Into SENSOR(id_sensor, id_consumo, id_edificio  , descripcion_artefactos ,  horas_por_dia , dias_por_mes , consumo_anual_estimado) VALUES( ? , ? , ? , ? , ? , ? , ? );"; //eliminar idEdificio en esta tabla
		//CONSULTAS
		
		try{
		
			pstmt = this.conn.prepareStatement(query);
			 
			
			
			for(int i=0 ; i<sensores.size() ; i++){
				SensorTO sensorActual = sensores.get(i);
			
				if(sensorActual.getGuardado()==false){
					pstmt.setString(1, sensorActual.getCodigoSensor());
					pstmt.setString(2, sensorActual.getCodigoTipoConsumo());
					pstmt.setInt(3,idEdificio);
					pstmt.setString(4,sensorActual.getDescripcionArtefactos());
					pstmt.setInt(5,sensorActual.getUsoHorasDiarias());
					pstmt.setInt(6,sensorActual.getUsoDiasMensuales());
					pstmt.setDouble(7,sensorActual.getConsumoTotalEstimado());
					
					
					resp = pstmt.executeUpdate();
					
					   //GUARDAR ARTEFACTOS
				    fa.guardarComposicionArtefactos(sensorActual.getArtefactosMedidios(), sensorActual.getCodigoSensor(), this.conn);
					
				}
			}
			
			if(resp>0)
				resp = guardarSensorPorUbicacion(sensores, idEdificio, idPiso, idZona);
			pstmt.close();
			
			
			
		}catch (Exception e) {
			UtilLog.registrar(e);
		}
		finally{
			if( pstmt != null ){
				try{
					pstmt.close();
				}catch(SQLException sqle){
					UtilLog.registrar(sqle);
				}
			}
	}
		return resp;
	}
	
	private int guardarSensorPorUbicacion(List<SensorTO> sensores, int idEdificio, int idPiso, int idZona){
		int resp = -1;
		PreparedStatement pstmt = null;
		
		String querySensorEdificio = "Insert Into SENSOR_EDIFICIO(id_consumo, id_edificio, id_sensor) VALUES(?,?,?)";
		String querySensorPiso 	   = "Insert Into SENSOR_PISO(id_consumo, id_piso, id_sensor) VALUES(?,?,?)";
		String querySensorZona     = "Insert Into SENSOR_ZONA(id_consumo, id_zona, id_sensor) VALUES(?,?,?)";
		

		try{
			
			if(idPiso==0)
			{
				//GUardar sensor en sensor_edificio
				pstmt = this.conn.prepareStatement(querySensorEdificio);
				for(int i=0 ; i<sensores.size() ; i++){
					SensorTO sensorActual = sensores.get(i);
					if(sensorActual.getGuardado()==false){
						pstmt.setString(1, sensorActual.getCodigoTipoConsumo());
						pstmt.setInt(2,idEdificio);
						pstmt.setString(3,sensorActual.getCodigoSensor());
						resp = pstmt.executeUpdate();
					}
				}
			}
			else if(idPiso>0 && idZona==0){
				//GUardar sensor en sensor_piso
				pstmt = this.conn.prepareStatement(querySensorPiso);
				for(int i=0 ; i<sensores.size() ; i++){
					SensorTO sensorActual = sensores.get(i);
					if(sensorActual.getGuardado()==false){
						pstmt.setString(1, sensorActual.getCodigoTipoConsumo());
						pstmt.setInt(2,idPiso);
						pstmt.setString(3,sensorActual.getCodigoSensor());
						resp = pstmt.executeUpdate();
					}
				}
			}
			else if(idPiso>0 && idZona>0){
				//GUardar sensor en sensor_zona
				pstmt = this.conn.prepareStatement(querySensorZona);
				for(int i=0 ; i<sensores.size() ; i++){
					SensorTO sensorActual = sensores.get(i);
					if(sensorActual.getGuardado()==false){
						pstmt.setString(1, sensorActual.getCodigoTipoConsumo());
						pstmt.setInt(2,idZona);
						pstmt.setString(3,sensorActual.getCodigoSensor());
						resp = pstmt.executeUpdate();
					}
				}
			}
		
			pstmt.close();
			 

			resp = (resp>0) ? 1 : resp;
			pstmt.close();

		}catch(Exception e){
			UtilLog.registrar(e);
		}
		finally{
			if( pstmt != null ){
				try{
					pstmt.close();
				}catch(SQLException sqle){
					UtilLog.registrar(sqle);
				}
			}
		}
		return resp;
	}
	private int guardarParametros(List<ParametrosEdificioTO> parametrosEdificios, int idEdificio){
		int resp = -1;
		PreparedStatement pstmt = null;

		String query = "INSERT INTO PARAMETROS_EDIFICIO(id_edificio, id_parametro, valor_parametro) VALUES(?,?,?)";
		try{
			pstmt = conn.prepareStatement(query);
			
			for(ParametrosEdificioTO param : parametrosEdificios){
				if(param!=null){
					pstmt.setInt(1, idEdificio);
					pstmt.setInt(2,param.getIdParametro());
					pstmt.setFloat(3, param.getValorParametro());

					resp = pstmt.executeUpdate();
				}
			}

		

			resp = (resp>0) ? 1 : resp;
			pstmt.close();

		}catch(Exception e){
			UtilLog.registrar(e);
		}
		finally{
			if( pstmt != null ){
				try{
					pstmt.close();
				}catch(SQLException sqle){
					UtilLog.registrar(sqle);
				}
			}
		}


		return resp;
	}
	private int actualizarParametros(List<ParametrosEdificioTO> parametrosEdificios, int idEdificio){
		int resp = -1;
		PreparedStatement pstmt = null;

		String query = "UPDATE PARAMETROS_EDIFICIO SET valor_parametro = ? WHERE id_parametro = ? and id_edificio = ?;";
		try{
			pstmt = conn.prepareStatement(query);

			for(ParametrosEdificioTO param : parametrosEdificios){
				if(param!=null){
					pstmt.setFloat(1, param.getValorParametro());
					pstmt.setInt(2,param.getIdParametro());
					pstmt.setInt(3,idEdificio);
					resp = pstmt.executeUpdate();
				}
			}
			resp = (resp>0) ? 1 : resp;
			pstmt.close();

		}catch(Exception e){
			UtilLog.registrar(e);
		}
		finally{
			if( pstmt != null ){
				try{
					pstmt.close();
				}catch(SQLException sqle){
					UtilLog.registrar(sqle);
				}
			}
		}


		return resp;
	}
	
	public List<ParametrosEdificioTO> obtenerParametrosPorIdEdificio(int idEdificio){
		List<ParametrosEdificioTO> resp = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String query = "SELECT p.id_parametro, p.valor_parametro, pi.nombre_parametro, pi.simbolo_parametro , pi.unidad_parametro  " +
						"FROM PARAMETROS_EDIFICIO p " +
						"INNER JOIN PARAMETROS_INICIAL_EDIFICIO pi on p.id_parametro = pi.id_parametro " +
						"WHERE id_edificio = ?;";
		try{
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1,idEdificio);
			rs	= pstmt.executeQuery();
			 
			resp = new ArrayList<ParametrosEdificioTO>();
			
			while(rs.next()){
				ParametrosEdificioTO parametro = new ParametrosEdificioTO();
				
				parametro.setIdParametro(rs.getInt(1));
				parametro.setValorParametro(rs.getFloat(2));
				parametro.setNombreParametro(rs.getString(3));
				parametro.setSimboloParametro(rs.getString(4));
				parametro.setUnidadParametro(rs.getString(5));
				resp.add(parametro);
			}
			
			rs.close();
			pstmt.close();

		}catch(Exception e){
			UtilLog.registrar(e);
		}
		finally{
			if( pstmt != null ){
				try{
					if(rs!=null)
						rs.close();
					pstmt.close();
				}catch(SQLException sqle){
					UtilLog.registrar(sqle);
				}
			}
		}


		return resp;
		
	}
	@Override
	public byte[] obtenerFotografia(String rutaFotografia) {
		
		File file = new File(DirectoriosImagen.DIRECTORIO_DEFINITIVO+rutaFotografia);
		byte [] buffer = null;// 
		byte [] resp = null;
        ByteArrayOutputStream ous = new ByteArrayOutputStream();
	    InputStream ios;
	   
		try {
			if(file.exists()){
				buffer = new byte[4096];
				ios = new FileInputStream(file);
				int read = 0;
				while ( (read = ios.read(buffer)) != -1 ) {
					ous.write(buffer, 0, read);
				}
				resp = ous.toByteArray();
			}
		}catch (FileNotFoundException e) {
			UtilLog.registrar(e);
			
		} catch (IOException e) {
			UtilLog.registrar(e);		
		}
        
			return resp;
	}


	/*NEXTVAL*/
	public int obtenerSiguienteId()
	{
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        int resp = -1;
        String query = "SELECT nextval('edificio_id_edificio_seq') as NEXTVAL";
        
        try
        {                        
            pstmt = conn.prepareStatement(query);            
            rs = pstmt.executeQuery();
            
            if(rs.next())
                resp = rs.getInt("NEXTVAL");
            
            rs.close();
            pstmt.close();
            
        }
        catch (SQLException sqle) {
        	UtilLog.registrar(sqle);
        }
        finally{
            if (pstmt != null) {
                  try {
                       pstmt.close();
                       rs.close();
                  } catch (SQLException e) {
                       e.printStackTrace();
                  }
            }
        }        
        return resp;
    }
	
	/*NEXTVAL PISO*/
//	public int obtenerSiguienteIdPiso()
//	{
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        
//        int resp = -1;
//        String query = "SELECT nextval('piso_id_piso_seq') as NEXTVAL";
//        
//        try
//        {                        
//            pstmt = conn.prepareStatement(query);            
//            rs = pstmt.executeQuery();
//            
//            if(rs.next())
//                resp = rs.getInt("NEXTVAL");
//            
//            rs.close();
//            pstmt.close();
//            
//        }
//        catch (SQLException sqle) {
//        	UtilLog.registrar(sqle);
//        }
//        finally{
//            if (pstmt != null) {
//                  try {
//                       pstmt.close();
//                       rs.close();
//                  } catch (SQLException e) {
//                       e.printStackTrace();
//                  }
//            }
//        }        
//        return resp;
//    }
	/*NEXTVAL ZONAS*/
//	public int obtenerSiguienteIdZonas()
//	{
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        
//        int resp = -1;
//        String query = "SELECT nextval('zona_id_zona_seq') as NEXTVAL";
//        
//        try
//        {                        
//            pstmt = conn.prepareStatement(query);            
//            rs = pstmt.executeQuery();
//            
//            if(rs.next())
//                resp = rs.getInt("NEXTVAL");
//            
//            rs.close();
//            pstmt.close();
//            
//        }
//        catch (SQLException sqle) {
//        	UtilLog.registrar(sqle);
//        }
//        finally{
//            if (pstmt != null) {
//                  try {
//                       pstmt.close();
//                       rs.close();
//                  } catch (SQLException e) {
//                       e.printStackTrace();
//                  }
//            }
//        }        
//        return resp;
//    }
	@Override
	public int eliminarEdificio(int IdEdificio) {
		int resp = -1;
		PreparedStatement pstmt = null;
		
		String query =  "DELETE FROM EDIFICIO WHERE id_edificio = ?; ";
		 try{
			 pstmt = this.conn.prepareStatement(query);
			 pstmt.setInt(1, IdEdificio);
			 resp = pstmt.executeUpdate();
			 
			 
			 pstmt.close();
		 }catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try{
					pstmt.close();
				}catch(SQLException sqle){
					UtilLog.registrar(sqle); 
				}
			}
		}
		
		return resp;
	}
	@Override
	public int modificarEdificio(EdificioTO edificio) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public EdificioTO obtenerEdificioOpciones(int idEdificio,boolean envolvente, boolean parametros, boolean sensores, boolean fotografia) {
		EdificioTO resp = null; 
		PreparedStatement pstmt  = null;
		ResultSet rs = null;
		String query = "SELECT   id_cliente, " +
						"nombre_edificio, " +
						"direccion_edificio, " +
						"ciudad_edificio, " + 
						"longitud_edificio, " +
						"latitud_edificio, " +
						"observaciones_edificio,  " +
						"ruta_fotografia, " +
						"superficie_util, " +
						"superficie_bruta, " +
						"volumen_aire, " +
						"perimetro," +
						"direccion_completa, " +
						"largo," +
						"ancho," +
						"altura_piso," +
						"id_factor ," +
						"aire_acondicionado ," +
						"valor_factor_renovacion , " +
						"numero_actual_edificio " +
						"FROM edificio " + 
						"WHERE id_edificio = ? ";
		
		List<ParametrosEdificioTO> parametrosEdificio = null;
		List<PisoTO> pisosEdificio  = null;
		List<SensorTO> sensoresEdificio = null;
		//List<SeccionTO> seccionesEdificio = null;
		
		
		try{
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1,idEdificio);
			
			rs = pstmt.executeQuery();
			if(rs.next()){
				resp = new EdificioTO();
				resp.setIdEdificio(idEdificio);
				resp.setCodigoEdificio(idEdificio);
				
				
				resp.setIdCliente(rs.getInt(1));
				resp.setNombreEdificio(rs.getString(2));
				resp.setDireccionEdificio(rs.getString(3));
				resp.setCiudadEdificio(rs.getString(4));
				resp.setLongitud(rs.getDouble(5));
				resp.setLatitud(rs.getDouble(6));
				resp.setObservaciones(rs.getString(7));
				resp.setRutaFotografia(rs.getString(8));

				resp.setSuperficieUtil (rs.getFloat(9));
				resp.setSuperficieBruta(rs.getFloat(10));
				resp.setVolumenDeAire(rs.getFloat(11));
				resp.setPerimetro(rs.getFloat(12));
				resp.setDireccionCompletaEdificio(rs.getString(13));	
				
				resp.setLargo(rs.getFloat(14));
				resp.setAncho(rs.getFloat(15));
				resp.setAltoPiso(rs.getFloat(16));
				
				resp.setIdFactorRenovacion(rs.getInt(17));
				resp.setAireAcondicionado(rs.getBoolean(18));
		
				resp.setValorFactorRenovacion(rs.getFloat(19));
				resp.setNumeroEdificio(rs.getInt(20));
				
				if(fotografia==true)
					resp.setFotografia(obtenerFotografia(resp.getRutaFotografia()));
				
				if(parametros==true){
					//Cargar Parametros
					parametrosEdificio = obtenerParametrosPorIdEdificio(idEdificio);
					if(parametrosEdificio!= null) 
						resp.setArrCollParametrosEdificio(parametrosEdificio);

				}
				if(sensores==true){
					// Cargar Pisos - Zonas ( Sensores-Piso, Sensores-Zonas )
					pisosEdificio 	 = obtenerPisosEdificio(idEdificio); //Incluyen Zonas y Sensores
					if(pisosEdificio!=null)
						resp.setArrCollPisosEdificio(pisosEdificio);

					sensoresEdificio = obtenerSensoresPorUbicacion(idEdificio,0,0); //Sensores del Edificio
					if(sensoresEdificio!=null)
						resp.setArrCollSensoresEdificio(sensoresEdificio); 

				}
				if(envolvente==true){
					ISeccion seccDB = new SeccionDB(this.conn);
					resp.setArrCollSecciones(seccDB.obtenerSeccionesEdificio(idEdificio));
				}
			}
			
			
			//Cargar Seccions <- Materiales.
			
			rs.close();
			pstmt.close();
			
			
		}catch(Exception e){
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					if(rs!=null)
						rs.close();
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);	
				}
			}
		}
		
		
		
		return resp;
	}
	@Override
	public int guardarInversionTotalEdificio(int idEdificio, float costoEdificio) {
		int resp = -1;
		PreparedStatement pstmt = null;
		
		String query  = "INSERT INTO INVERSION_EDIFICIO(id_edificio,costo_total_edificio) VALUES(?,?)";
		
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1,idEdificio);
			pstmt.setFloat(2,costoEdificio);
			
			resp = pstmt.executeUpdate();
			
			
			pstmt.close();
		} catch (Exception e) {
				UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}	
			}
		}
		
		
		return resp;
	}
	@Override
	public List<EdificioTO> obtenerTodosLosEdificiosCompletosPorCliente(int idCliente) {
		List<EdificioTO> resp = null;
		int idEdificio = -1;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		
		try{
 
//			String query = " SELECT id_edificio, nombre_edificio, direccion_completa_edificio, longitud_edificio, latitud_edificio, altura, largo, ancho, perimetro, area , observaciones_edificio, volumen_total , ruta_fotografia" +
//			   "FROM edificio " + 
//			   "WHERE id_cliente = ? ";
			
			String query = "SELECT   id_edificio " +
							"FROM edificio " + 
			   				"WHERE id_cliente = ? " +
			   				"ORDER BY id_edificio ASC;	";
			
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, idCliente);
			rs = pstmt.executeQuery();
			
			resp = new ArrayList<EdificioTO>();
			
			while(rs.next()){

				EdificioTO edificio = new EdificioTO();
				idEdificio = rs.getInt("id_edificio");
				edificio = obtenerEdificio(idEdificio);
				
				resp.add(edificio);
				
			}
			
			rs.close();
			pstmt.close();
			
			
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
		}
		finally{
			if(pstmt != null){
				try{
					if(rs!=null)
						rs.close();
					pstmt.close();
				}catch(SQLException sqle){
					UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp;
		
		
	}
	@Override
	public int actualizarEdificio(EdificioTO edificioActualizar) {
		int resp = -1;
		PreparedStatement pstmt = null;
		
		
		String query = "UPDATE  edificio   SET  nombre_edificio = ? , " +
												"observaciones_edificio = ? , " +
												"ruta_fotografia = ? , " +
												"direccion_edificio = ? , " +
												"pais_edificio = ? , " +
												"region_edificio = ? , " +
												"ciudad_edificio = ? , " +
												"longitud_edificio = ? , " +
												"latitud_edificio = ? , " +
												"altura_piso = ? , " +
												"largo = ? , " +
												"ancho = ? , " +
												"superficie_util = ? ," +
												"superficie_bruta = ? ,  " +
												"volumen_aire = ? , " +
												"perimetro = ? , " +
												"direccion_completa = ? , " +
												"id_factor  = ? , " +
												"aire_acondicionado  = ? , " +
												"fecha_creacion  = ? , " +
												"valor_factor_renovacion = ? " +
												"WHERE id_edificio = ?;";
		
		try {
			pstmt = this.conn.prepareStatement(query);
			
			if(edificioActualizar.getFotografia()!=null)
				edificioActualizar.setRutaFotografia(edificioActualizar.getCodigoEdificio()+".jpg");
			
			pstmt.setString(1, edificioActualizar.getNombreEdificio());
			pstmt.setString(2, edificioActualizar.getObservaciones());
			pstmt.setString(3,edificioActualizar.getRutaFotografia());
			
			pstmt.setString(4, edificioActualizar.getDireccionEdificio());
			pstmt.setString(5,edificioActualizar.getPaisEdificio());
			pstmt.setString(6,edificioActualizar.getRegionEdificio());
			pstmt.setString(7,edificioActualizar.getCiudadEdificio());
			pstmt.setDouble(8, edificioActualizar.getLongitud());
			pstmt.setDouble(9, edificioActualizar.getLatitud());
			
			pstmt.setFloat(10, edificioActualizar.getAltoPiso());
			pstmt.setFloat(11, edificioActualizar.getLargo());
			pstmt.setFloat(12, edificioActualizar.getAncho());

			pstmt.setFloat(13, edificioActualizar.getSuperficieUtil());
			pstmt.setFloat(14, edificioActualizar.getSuperficieBruta());
			pstmt.setFloat(15, edificioActualizar.getVolumenDeAire());
			pstmt.setFloat(16, edificioActualizar.getPerimetro());
			
			pstmt.setString(17, edificioActualizar.getDireccionCompletaEdificio());
			pstmt.setInt(18,edificioActualizar.getIdFactorRenovacion());

			pstmt.setBoolean(19,edificioActualizar.getAireAcondicionado());
			pstmt.setTimestamp(20,UtilFechas.hoy()); 
			pstmt.setFloat(21, edificioActualizar.getValorFactorRenovacion());
			
			//WHERE->
			pstmt.setInt(22,edificioActualizar.getIdEdificio());
			resp = pstmt.executeUpdate();
			//--CAMBIAR A ACTUALIZAR
			Boolean ok = false;
			if(resp>0){
				if(edificioActualizar.getFotografia()!=null )
					 resp = this.guardarFotografia(edificioActualizar.getFotografia() ,edificioActualizar.getRutaFotografia());

					//A C T U A L I Z A R
					if(edificioActualizar.getSensoresEliminados()!=null)
						ok = eliminarSensores(edificioActualizar.getSensoresEliminados());
				
					//A C T U A L I Z A R
					if(edificioActualizar.getArrCollPisosEdificio()!=null)
						 resp = this.guardarPisos(edificioActualizar.getArrCollPisosEdificio() , edificioActualizar.getCodigoEdificio());

					//--A C T U A L I Z A R
					if(edificioActualizar.getArrCollSensoresEdificio()!=null)
						resp = this.guardarSensores(edificioActualizar.getArrCollSensoresEdificio(), edificioActualizar.getCodigoEdificio(), 0, 0);
					 
					//--A C T U A L I Z A R
					if(edificioActualizar.getArrCollParametrosEdificio()!=null)
						resp = this.actualizarParametros(edificioActualizar.getArrCollParametrosEdificio(), edificioActualizar.getCodigoEdificio());
			}
			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt != null){
				try{
					pstmt.close();
				}catch(SQLException sqle){
					UtilLog.registrar(sqle);
				}
			}
		}
		
		
		return resp;
	}
	
	private boolean existePiso(int idPpiso){
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean existe = false;
//		String query = " SELECT EXISTS (SELECT 1 FROM piso WHERE id_piso = ?);";
		String query = "SELECT COUNT(id_piso) FROM piso WHERE id_piso = ?;";
		
		try{
			pstmt = conn.prepareStatement(query);
			
			pstmt.setInt(1, idPpiso);			
			rs = pstmt.executeQuery();

			if(rs.next()){
				existe = (rs.getInt(1) > 0) ? true : false;
			}
			
			pstmt.close();
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
			return true;
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
			return true;
		}
		
		return existe;
	}
	private boolean existeZona(int idZona){
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean existe = false;
//		String query = " SELECT EXISTS (SELECT 1 FROM piso WHERE id_piso = ?);";
		String query = "SELECT COUNT(idZona) FROM zona WHERE idZona = ?;";
		
		try{
			pstmt = conn.prepareStatement(query);
			
			pstmt.setInt(1, idZona);			
			rs = pstmt.executeQuery();

			if(rs.next()){
				existe = (rs.getInt(1) > 0) ? true : false;
			}
			
			pstmt.close();
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
			return true;
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
			return true;
		}
		
		return existe;
	}
	
	//Eliminar sensores
	private boolean eliminarSensores(List<SensorTO> sensores){
		boolean resp = false;
		PreparedStatement pstmt = null;
		String query = "DELETE FROM sensor WHERE id_sensor = ?;";
		try {
			pstmt = this.conn.prepareStatement(query);
			int ok = -1;
			for(SensorTO s : sensores ){
				pstmt.setInt(1,s.getIdSensor());		
				ok = pstmt.executeUpdate();
				resp = (ok > -1) ? true : false;
//				if(!resp)
//					return resp;
			}
			
			resp = (ok > -1) ? true : false;
			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			try {
				pstmt.close();
			} catch (SQLException sqle) {
				UtilLog.registrar(sqle);
			}
		}
		
		return resp;
	} 
	 
}

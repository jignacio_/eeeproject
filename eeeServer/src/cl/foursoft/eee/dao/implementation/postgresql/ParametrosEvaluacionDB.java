package cl.foursoft.eee.dao.implementation.postgresql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cl.foursoft.eee.dao.implementation.DBBase;
import cl.foursoft.eee.dao.interfaces.IParametrosEvaluacion;
import cl.foursoft.eee.dao.transferObject.ParametrosEvaluacionTO;
import cl.foursoft.eee.util.UtilLog;

public class ParametrosEvaluacionDB extends DBBase implements
		IParametrosEvaluacion {

	
	
	public ParametrosEvaluacionDB(Connection c) {
		this.conn = c;
	}

	@Override
	public List<ParametrosEvaluacionTO> obtenerTodosParametrosEvaluacion() {
		List<ParametrosEvaluacionTO> resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = "SELECT  id_parametro_evaluacion, nombre_parametro, anios_evaluacion, tasa_descuento, variacion_precio " +
					   "FROM PARAMETROS_EVALUACION ";
		
		
		try{
			pstmt = this.conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			
			resp = new ArrayList<ParametrosEvaluacionTO>();
			
			while(rs.next()){
				ParametrosEvaluacionTO _parametros = new ParametrosEvaluacionTO();
				//Buscar
				resp.add(_parametros);
			}
			
			pstmt.close();
			rs.close();
		}
		catch (Exception e) {
		  UtilLog.registrar(e);
		}
		finally{
			if(pstmt!=null){
				try{
					if(rs!=null)
						rs.close();
					pstmt.close();
				}catch(SQLException sqle){
						UtilLog.registrar(sqle);
				}
			}
		}
		
		
		return resp;
	}

	@Override
	public ParametrosEvaluacionTO obtenerParametroEvaluacionPorId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int guradrParametroEvaluacion(ParametrosEvaluacionTO parametros) {
		int resp = -1;
		PreparedStatement pstmt = null;
		
		String query = "INSERT INTO PARAMETROS_EVALUACION ( id_parametro_evaluacion, " +
				               								"nombre_parametro, " +
				               								"horizonte_evaluacion, " +
				               								"tasa_descuento, " +
				               								"variacion_precio ," +
				               								"tarifa_combustible ," +
				               								"eficiencia_global ," +
				               								"tarifa_final ) " +
				               								"VALUES(?,?,?,?,?,?,?,?)";
		
		
		try{
			int idActual = obtenerSiguienteId();
			pstmt = this.conn.prepareStatement(query);
		
			
			
			pstmt.setInt(1, idActual);
			pstmt.setString(2, parametros.getNombreEvaluacion());
			pstmt.setFloat(3, parametros.getHorizonteEvaluacion());
			pstmt.setFloat(4,parametros.getTasaDescuento());
			pstmt.setFloat(5, parametros.getVariacionPrecio());
			pstmt.setFloat(6,parametros.getTarifaCombustible());
			pstmt.setFloat(7, parametros.getEficienciaGlobal());
			pstmt.setFloat(8, parametros.getTarifaFinal());
			resp = pstmt.executeUpdate();
			
			resp = (resp>0) ? idActual : -1;
			
			
			pstmt.close();
		}
		catch (Exception e) {
		  UtilLog.registrar(e);
		}
		finally{
			if(pstmt!=null){
				try{
					pstmt.close();
				}catch(SQLException sqle){
						UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp;
	}
	

	@Override
	public List<ParametrosEvaluacionTO> obtenerParametrosCliente(int idCliente) {
		List<ParametrosEvaluacionTO> resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = "SELECT  id_parametro_evaluacion, "+
								"nombre_parametro, " +
								"horizonte_evaluacion, " +
								"tasa_descuento, " +
								"variacion_precio, " +
								"tarifa_combustible, " +
								"eficiencia_global, " +
								"tarifa_final " +
					   "FROM parametros_evaluacion ;";
		
		try{
			pstmt = this.conn.prepareStatement(query);
		//	pstmt.setInt(1, idCliente);
			rs = pstmt.executeQuery();
			
			resp = new ArrayList<ParametrosEvaluacionTO>();
			
			while(rs.next()){
				ParametrosEvaluacionTO _parametros = new ParametrosEvaluacionTO();
				_parametros.setIdEvaluacion(rs.getInt(1));
				_parametros.setNombreEvaluacion(rs.getString(2));
				_parametros.setHorizonteEvaluacion(rs.getInt(3));
				_parametros.setTasaDescuento(rs.getFloat(4));
				_parametros.setVariacionPrecio(rs.getFloat(5));
				_parametros.setTarifaCombustible(rs.getFloat(6));
				_parametros.setEficienciaGlobal(rs.getFloat(7));
				_parametros.setTarifaFinal(rs.getFloat(8));
						
				resp.add(_parametros);
			}
			
			pstmt.close();
			rs.close();
		}
		catch (Exception e) {
		  UtilLog.registrar(e);
		}
		finally{
			if(pstmt!=null){
				try{
					if(rs!=null)
						rs.close();
					pstmt.close();
				}catch(SQLException sqle){
						UtilLog.registrar(sqle);
				}
			}
		}
		
		
		return resp;
	}
	
	/*NEXTVAL*/
	public int obtenerSiguienteId()
	{
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        int resp = -1;
        String query = "SELECT nextval('parametros_evaluacion_id_parametro_evaluacion_seq') as NEXTVAL";
        
        try
        {                        
            pstmt = conn.prepareStatement(query);            
            rs = pstmt.executeQuery();
            
            if(rs.next())
                resp = rs.getInt("NEXTVAL");
            
            rs.close();
            pstmt.close();
            
        }
        catch (SQLException sqle) {
        	UtilLog.registrar(sqle);
        }
        finally{
            if (pstmt != null) {
                  try {
                       pstmt.close();
                       rs.close();
                  } catch (SQLException e) {
                       e.printStackTrace();
                  }
            }
        }        
        return resp;
    }

	@Override
	public int eliminarParametroCliente(int idParametro, int idCliente) {
		int resp = -1;
		PreparedStatement pstmt = null;
		
		String query = "DELETE FROM parametros_evaluacion WHERE id_parametro_evaluacion = ?;";
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1,idParametro);
			
			resp = pstmt.executeUpdate();
			
			
		} catch (Exception e) {
			UtilLog.registrar(e);
		}
		finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp;
	}
 
	

}

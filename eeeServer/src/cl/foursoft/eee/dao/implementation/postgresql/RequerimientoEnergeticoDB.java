package cl.foursoft.eee.dao.implementation.postgresql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import cl.foursoft.eee.dao.implementation.DBBase;
import cl.foursoft.eee.dao.interfaces.IAlternativaInversion;
import cl.foursoft.eee.dao.interfaces.IRequerimientoEnergetico;
import cl.foursoft.eee.dao.transferObject.RequerimientoEnergeticoTO;
import cl.foursoft.eee.util.UtilLog;
import cl.foursoft.eee.util.tipoAlternativaUtil;

public class RequerimientoEnergeticoDB extends DBBase implements IRequerimientoEnergetico {


	public RequerimientoEnergeticoDB(Connection c){
		this.conn = c;
	}
	@Override
	public int guardarRequerimientoEnergeticoEdificio(RequerimientoEnergeticoTO requerimiento) {
		int resp = -1;
		PreparedStatement pstmt = null;
		
		String query ="INSERT INTO REQUERIMIENTO_ENERGETICO_EDIFIC(id_edificio, " +
																  "perdida_por_transmision, " +
																  "perdida_por_ventilacion, " +
																  "ganancia_termica_verano, " +
																  "ganancia_termica_invierno," +
																  "ganancia_interna," +
																  "requerimiento_calor_total," +
																  "requerimiento_frio_total," +
																  "requerimiento_total_edificio  ) " +
																  "VALUES(?,?,?,?,?,?,?,?,?);";
		
		try {
			pstmt = conn.prepareStatement(query);
			
			pstmt.setFloat(1,requerimiento.getIdEdificio());
			pstmt.setFloat(2,requerimiento.getPerdidasPorTransmision());
			pstmt.setFloat(3,requerimiento.getPerdidasPorVentilacion());
			pstmt.setFloat(4,requerimiento.getGananciaTermicaSolVerano());
			pstmt.setFloat(5,requerimiento.getGananciaTermicaSolInvierno());
			pstmt.setFloat(6,requerimiento.getGananciaInterna());
			pstmt.setFloat(7,requerimiento.getRequerimientoCalorTotal());
			pstmt.setFloat(8,requerimiento.getRequerimientoFrioTotal());
			pstmt.setFloat(9,requerimiento.getRequerimientoEnergeticoTotal());
			
			resp = pstmt.executeUpdate();
			
			//boolean exito = (resp>0);
			
			pstmt.close();
			
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException  sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp;
	}

	@Override
	public int guardarRequerimientoEnergeticoAlternativa(RequerimientoEnergeticoTO requerimiento) {
		int resp = -1;
		PreparedStatement pstmt = null;
		IAlternativaInversion resumenAlternativa = new AlternativaInversionDB(conn);
		String query ="INSERT INTO REQUERIMIENTO_ENERGETICO_ALTERN(id_alternativa, " +
																  "perdida_por_transmision, " +
																  "perdida_por_ventilacion, " +
																  "ganancia_termica_verano, " +
																  "ganancia_termica_invierno, " +
																  "ganancia_interna, " +
																  "requerimiento_calor_total, " +
																  "requerimiento_frio_total, " +
																  "requerimiento_alternativa_tatal  ) " +
																  "VALUES(?,?,?,?,?,?,?,?,? );";
		
		try {
			pstmt = conn.prepareStatement(query);
			
			pstmt.setFloat(1,requerimiento.getIdAlternativa());
			pstmt.setFloat(2,requerimiento.getPerdidasPorTransmision());
			pstmt.setFloat(3,requerimiento.getPerdidasPorVentilacion());
			pstmt.setFloat(4,requerimiento.getGananciaTermicaSolVerano());
			pstmt.setFloat(5,requerimiento.getGananciaTermicaSolInvierno());
			pstmt.setFloat(6,requerimiento.getGananciaInterna());
			pstmt.setFloat(7,requerimiento.getRequerimientoCalorTotal());
			pstmt.setFloat(8,requerimiento.getRequerimientoFrioTotal());
			pstmt.setFloat(9,requerimiento.getRequerimientoEnergeticoTotal());
			
			resp = pstmt.executeUpdate();
			
			//boolean exito = (resp>0);
			if(resp>0)
				resp = resumenAlternativa.guardarResumenRequerimientoAlternativa(requerimiento.getIdAlternativa(), (requerimiento.getRequerimientoEnergeticoTotal()), "kWh" ,tipoAlternativaUtil.tipoAlternativaEnergetica);
				
			
			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException  sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp;
		
	}

	@Override
	public RequerimientoEnergeticoTO obtenerRequerimientoEnergeticoEdificio(int idEdifcio) {
		RequerimientoEnergeticoTO resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = "SELECT  perdida_por_transmision , perdida_por_ventilacion , ganancia_termica_verano , ganancia_termica_invierno , " +
								" ganancia_interna  , requerimiento_frio_total , requerimiento_calor_total,   requerimiento_total_edificio  " +
					   "FROM REQUERIMIENTO_ENERGETICO_EDIFIC " +
					   "WHERE id_edificio = ?";
		
		try{
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1,idEdifcio);
			
			rs = pstmt.executeQuery();
			resp = new RequerimientoEnergeticoTO();

			if(rs.next()){
				resp.setIdEdificio(idEdifcio);
				resp.setPerdidasPorTransmision(rs.getFloat(1));
				resp.setPerdidasPorVentilacion(rs.getFloat(2));
				resp.setGananciaTermicaSolVerano(rs.getFloat(3));
				resp.setGananciaTermicaSolInvierno(rs.getFloat(4));
				resp.setGananciaInterna(rs.getFloat(5));
				resp.setRequerimientoFrioTotal(rs.getFloat(6));
				resp.setRequerimientoCalorTotal(rs.getFloat(7));
				resp.setRequerimientoEnergeticoTotal(rs.getFloat(8));
			}
			
			pstmt.close();
		}catch (Exception e) {
			UtilLog.registrar(e);	
		}finally{
			if(pstmt!=null){
				try {
					if(rs!=null)
						rs.close();
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		return resp;
	}

	@Override
	public RequerimientoEnergeticoTO obtenerRequerimientoEnergeticoAlternativa(int idEdificio, int idAlternativa) {
		RequerimientoEnergeticoTO resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = "SELECT  perdida_por_transmision , perdida_por_ventilacion , ganancia_termica_verano , ganancia_termica_invierno , " +
								" ganancia_interna , requerimiento_calor_total , requerimiento_frio_total ,  requerimiento_total_edificio " +
					   "FROM REQUERIMIENTO_ENERGETICO_ALTERN " +
					   "WHERE id_edificio = ? and id_alternativa = ? ";
		
		try{
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1,idEdificio);
			pstmt.setInt(2,idAlternativa);
			
			rs = pstmt.executeQuery();
			resp = new RequerimientoEnergeticoTO();
			
			
			if(rs.next()){
				resp.setIdEdificio(idEdificio);
				resp.setIdAlternativa(idAlternativa);
				resp.setPerdidasPorTransmision(rs.getFloat(1));
				resp.setPerdidasPorVentilacion(rs.getFloat(2));
				resp.setGananciaTermicaSolVerano(rs.getFloat(3));
				resp.setGananciaTermicaSolInvierno(rs.getFloat(4));
				resp.setGananciaInterna(rs.getFloat(5));
				resp.setRequerimientoFrioTotal(rs.getFloat(6));
				resp.setRequerimientoCalorTotal(rs.getFloat(7));
				resp.setRequerimientoEnergeticoTotal(rs.getFloat(8));
			}
			
			if(rs.next()){
				
			}
			pstmt.close();
		}catch (Exception e) {
			UtilLog.registrar(e);	
		}finally{
			if(pstmt!=null){
				try {
					if(rs!=null)
						rs.close();
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		return resp;
	}
	@Override
	public float obtenerValorRequerimientoTotal(int idEdificio, int idAlternativa) {
		float resp = 0f;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String queryEdificio = "SELECT  requerimiento_total_edificio " +
					   			"FROM REQUERIMIENTO_ENERGETICO_EDIFIC " +
					   			"WHERE id_edificio = ? ;";
		
		String queryAlternativa = "SELECT  requerimiento_alternativa_tatal " +
									"FROM REQUERIMIENTO_ENERGETICO_ALTERN " +
									"WHERE id_alternativa = ? ";
		
		
		try{
			if(idAlternativa == -1){
				pstmt = this.conn.prepareStatement(queryEdificio);
				pstmt.setInt(1,idEdificio);
			}
			else{
				pstmt = this.conn.prepareStatement(queryAlternativa);
				//pstmt.setInt(1,idEdificio);
				pstmt.setInt(1,idAlternativa);
			}
			
			rs = pstmt.executeQuery();
			
			
			if(rs.next()){
				resp = rs.getFloat(1); 
			} 
			pstmt.close();
		}catch (Exception e) {
			UtilLog.registrar(e);	
		}finally{
			if(pstmt!=null){
				try {
					if(rs!=null)
						rs.close();
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		return resp;
	}

}

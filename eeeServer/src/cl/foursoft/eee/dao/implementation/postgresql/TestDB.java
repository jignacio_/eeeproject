package cl.foursoft.eee.dao.implementation.postgresql;

import java.sql.Connection;

import cl.foursoft.eee.dao.implementation.DBBase;
import cl.foursoft.eee.dao.interfaces.ITest;
import cl.foursoft.eee.dao.transferObject.TestTO;

public class TestDB extends DBBase implements ITest {

	
	public TestDB(Connection conn) {
		this.conn = conn;
	}

	@Override
	public TestTO getTest(int id) {
		TestTO resp = new TestTO();
		
		resp.setId(id);
		resp.setMensaje("Wena nano!!");
		
		return resp;
	}

}

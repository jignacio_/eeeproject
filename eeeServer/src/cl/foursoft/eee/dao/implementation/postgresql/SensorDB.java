package cl.foursoft.eee.dao.implementation.postgresql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sun.corba.se.impl.javax.rmi.CORBA.Util;

import cl.foursoft.eee.dao.implementation.DBBase;
import cl.foursoft.eee.dao.interfaces.ISensor;
import cl.foursoft.eee.dao.transferObject.TipoMedicionConsumoSensorTO;
import cl.foursoft.eee.util.UtilLog;

public class SensorDB extends DBBase implements ISensor{

	public SensorDB(Connection c) {
		this.conn = c;
	}

	@Override
	public List<TipoMedicionConsumoSensorTO> obtenerTodosLosTiposDeSensores() {
		List<TipoMedicionConsumoSensorTO> resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String query = "SELECT  id_tipo_consumo, " +
							    "nombre_tipo_consumo , " +
							    "unidad_medicion " +
					   "FROM tipo_consumo_sensor " +
					   "WHERE id_tipo_consumo = id_tipo_medicion;";
		
		try {
			pstmt = this.conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			resp = new ArrayList<TipoMedicionConsumoSensorTO>();

			
			while(rs.next()){
				TipoMedicionConsumoSensorTO tipoSensor = new TipoMedicionConsumoSensorTO();
				List<TipoMedicionConsumoSensorTO> sensoresHijos;
				
				tipoSensor.setCodigoTipoMedicion(rs.getString(1));
				tipoSensor.setNombreTipoMedicion(rs.getString(2));
				tipoSensor.setUnidadMedicion(rs.getString(3));
				
				sensoresHijos = obtenerTodosLosTiposDeSensoresHijo(tipoSensor.getCodigoTipoMedicion());
				if(sensoresHijos!=null)
					tipoSensor.setArrColTipoConsumo(sensoresHijos);
				
				resp.add(tipoSensor);
			}
			rs.close();
			pstmt.close();
			
			
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					if(rs!=null)
						rs.close();
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		return resp;
	}
	@Override
	public List<TipoMedicionConsumoSensorTO> obtenerTodosLosTiposDeSensoresAdmin() {
		List<TipoMedicionConsumoSensorTO> resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String query = "SELECT  id_tipo_medicion , " +
		"nombre_tipo_medicion , " +
		"unidad_medicion , " +
		"descripcion_tipo_medicion " +
		"FROM tipo_consumo_sensor ;" ;
		
		try {
			pstmt = this.conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			resp = new ArrayList<TipoMedicionConsumoSensorTO>();
			
			
			while(rs.next()){
				TipoMedicionConsumoSensorTO tipoSensor = new TipoMedicionConsumoSensorTO();
				
				tipoSensor.setCodigoTipoMedicion(rs.getString(1));
				tipoSensor.setNombreTipoMedicion(rs.getString(2));
				tipoSensor.setUnidadMedicion(rs.getString(3));
				tipoSensor.setDescripcionTipoConsumo(rs.getString(4));
				//sensoresHijos = obtenerTodosLosTiposDeSensoresHijo(tipoSensor.getCodigoTipoMedicion());
				//if(sensoresHijos!=null)
				//	tipoSensor.setArrColTipoConsumo(sensoresHijos);
				
				resp.add(tipoSensor);
			}
			rs.close();
			pstmt.close();
			
			
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					if(rs!=null)
						rs.close();
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		return resp;
	}
	
	public TipoMedicionConsumoSensorTO obtenerUnTipoDeMedicion(String idMedicion) {
		TipoMedicionConsumoSensorTO resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String query = "SELECT  id_tipo_consumo, " +
							    "nombre_tipo_consumo , " +
							    "unidad_medicion " +
					   "FROM tipo_consumo_sensor " +
					   "WHERE id_tipo_consumo = id_tipo_medicion and id_tipo_medicion = ?;";
		
		try {
			resp = new TipoMedicionConsumoSensorTO();
			pstmt = this.conn.prepareStatement(query);
			pstmt.setString(1,idMedicion);
			rs = pstmt.executeQuery();

			
			//while(rs.next()){
				//TipoMedicionConsumoSensorTO tipoSensor = new TipoMedicionConsumoSensorTO();
			
			if(rs.next()){
				
				List<TipoMedicionConsumoSensorTO> sensoresHijos = new ArrayList<TipoMedicionConsumoSensorTO>();
				
				resp.setCodigoTipoMedicion(rs.getString(1));
				resp.setNombreTipoMedicion(rs.getString(2));
				resp.setUnidadMedicion(rs.getString(3));
				
				sensoresHijos = obtenerTodosLosTiposDeSensoresHijo(resp.getCodigoTipoMedicion());
				if(sensoresHijos!=null)
					resp.setArrColTipoConsumo(sensoresHijos);
				
				//resp.add(tipoSensor);
			}
			rs.close();
			pstmt.close();
			
			
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					if(rs!=null)
						rs.close();
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		return resp;
	}
	
	public  List<TipoMedicionConsumoSensorTO> obtenerTodosLosTiposDeSensoresHijo(String idTipoMedicion) {
		List<TipoMedicionConsumoSensorTO> resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String query = "SELECT id_tipo_consumo, " +
							  "nombre_tipo_consumo, " +
							  "descripcion_tipo_consumo , " +
							  "id_sensor , " +
							  "unidad_medicion  " +
					   "FROM tipo_consumo_sensor " +
					   "WHERE  id_tipo_medicion = ? and id_tipo_consumo != id_tipo_medicion and eliminado = ? ";
		
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setString(1,idTipoMedicion);
			pstmt.setBoolean(2,false);
			
			rs = pstmt.executeQuery();
			resp = new ArrayList<TipoMedicionConsumoSensorTO>();

			
			while(rs.next()){
				TipoMedicionConsumoSensorTO tipoSensor = new TipoMedicionConsumoSensorTO();
				
				tipoSensor.setCodigoTipoMedicion(rs.getString(1));
				tipoSensor.setNombreTipoMedicion(rs.getString(2));
				tipoSensor.setDescripcionTipoConsumo(rs.getString(3));
				tipoSensor.setIdTipoSensor(rs.getInt(4));
				tipoSensor.setUnidadMedicion(rs.getString(5));
				
				
				resp.add(tipoSensor);
			}
			rs.close();
			pstmt.close();
			
			
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					if(rs!=null)
						rs.close();
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		return resp;
	
	}

	private boolean existeTipoConsumo(String tipoConsumo){
		boolean resp = false;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int cantidad = -1; 
		String query = "Select COUNT(ID_SENSOR) FROM TIPO_CONSUMO_SENSOR WHERE ID_TIPO_CONSUMO = '?';";
		
		try{
			pstmt = this.conn.prepareStatement(query);
			pstmt.setString(1, tipoConsumo);
			
			rs = pstmt.executeQuery();
			if(rs.next()){
				cantidad = rs.getInt(1);
			}
			
			resp = (cantidad>0) ? true : false;
			
			pstmt.close();
			rs.close();
		}catch (SQLException sqle) {
				UtilLog.registrar(sqle);
		}catch (Exception e) {
			UtilLog.registrar(e);
		}
		
		return resp;
	}
	@Override
	public boolean guardarTipoSensor(TipoMedicionConsumoSensorTO _sensor, String idTipoMedicion) {
		int resp = -1;

		PreparedStatement pstmt = null;
		String query = "INSERT INTO TIPO_CONSUMO_SENSOR(id_sensor, id_tipo_consumo , id_tipo_medicion , nombre_tipo_consumo , descripcion_tipo_consumo , unidad_medicion , eliminado) " +
				"		VALUES(?,?,?,?,?,?,?)";
		try {
			int idTipoSensor = obtenerSiguienteID();
			pstmt = this.conn.prepareStatement(query);
			
			pstmt.setInt(1, idTipoSensor);
			pstmt.setString(2, _sensor.getCodigoTipoMedicion());
			pstmt.setString(3,idTipoMedicion);
			pstmt.setString(4,_sensor.getNombreTipoMedicion());
			pstmt.setString(5,_sensor.getDescripcionTipoConsumo());
			pstmt.setString(6, _sensor.getUnidadMedicion());
			pstmt.setBoolean(7,false);
			
			resp = pstmt.executeUpdate();
			
			if(resp<0) return false;
		
 
		} catch (Exception e) {
			UtilLog.registrar(e);
			return false;
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		return true;
	}
	@Override
	public boolean eliminarTipoConsumo(TipoMedicionConsumoSensorTO _sensor) {
		// TODO Auto-generated method stub
		int resp = -1;

		PreparedStatement pstmt = null;
		 
		String eliminar = "UPDATE TIPO_CONSUMO_SENSOR SET  eliminado = ?  WHERE id_tipo_medicion = ? ";
		
		try {
			
 
				pstmt = this.conn.prepareStatement(eliminar);
				pstmt.setBoolean(1, true);
				pstmt.setString(2,_sensor.getCodigoTipoMedicion());
				resp = pstmt.executeUpdate();
	 
			if(resp<0) return false;
		
 
		} catch (Exception e) {
			UtilLog.registrar(e);
			return false;
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		return true;
	}
	@Override
	public boolean actulizarTipoSensor(TipoMedicionConsumoSensorTO _sensor) {
		// TODO Auto-generated method stub
		int resp = -1;

		PreparedStatement pstmt = null;
		
		String query = "UPDATE TIPO_CONSUMO_SENSOR SET   nombre_tipo_consumo = ? , descripcion_tipo_consumo = ?   WHERE id_tipo_consumo = ? ";
		try {
			
				pstmt = this.conn.prepareStatement(query);
				pstmt.setString(1,_sensor.getNombreTipoMedicion()); 	
				pstmt.setString(2,_sensor.getDescripcionTipoConsumo());
				pstmt.setString(3, _sensor.getCodigoTipoMedicion());
				resp = pstmt.executeUpdate();
		
			if(resp<0) return false;
		
 
		} catch (Exception e) {
			UtilLog.registrar(e);
			return false;
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		return true;
	}
	public List<TipoMedicionConsumoSensorTO> recuperarTipoDeSensores(){
		List<TipoMedicionConsumoSensorTO> resp = null;
		
		return resp;
	}
	public boolean agregarSensorNuevamente(TipoMedicionConsumoSensorTO _sensor){
		boolean resp = false;
		
		return true;
	}
public boolean eliminarSensorFinal(TipoMedicionConsumoSensorTO _sensor){
	int resp = -1;

	PreparedStatement pstmt = null;
	 
//	String eliminar = "UPDATE TIPO_CONSUMO_SENSOR SET  eliminado = ?  WHERE id_tipo_consumo = ? ";
	String eliminar = "DELETE FROM TIPO_CONSUMO_SENSOR WHERE id_tipo_consumo = ? ";
	
	try {
		

			pstmt = this.conn.prepareStatement(eliminar);
			pstmt.setString(1,_sensor.getCodigoTipoMedicion());
			resp = pstmt.executeUpdate();
 
		if(resp<0) return false;
	

	} catch (Exception e) {
		UtilLog.registrar(e);
		return false;
	}finally{
		if(pstmt!=null){
			try {
				pstmt.close();
			} catch (SQLException sqle) {
				UtilLog.registrar(sqle);
			}
		}
	}
	
	return true;
}
private int obtenerSiguienteID()
{
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    
    int resp = -1;
    String query = "SELECT nextval('tipo_consumo_sensor_id_sensor_seq') as NEXTVAL"; 
    
    try
    {                        
        pstmt = conn.prepareStatement(query);            
        rs = pstmt.executeQuery();
        
        if(rs.next())
            resp = rs.getInt("NEXTVAL");
        
        rs.close();
        pstmt.close();
        
    }
    catch (SQLException sqle) {
    	UtilLog.registrar(sqle);
    }
    finally{
        if (pstmt != null) {
              try {
                   pstmt.close();
                   rs.close();
              } catch (SQLException e) {
                   e.printStackTrace();
              }
        }
    }        
    return resp;
}


}


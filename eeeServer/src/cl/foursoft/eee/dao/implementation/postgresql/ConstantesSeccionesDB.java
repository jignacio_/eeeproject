package cl.foursoft.eee.dao.implementation.postgresql;

 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.List;
import cl.foursoft.eee.dao.implementation.DBBase;
import cl.foursoft.eee.dao.interfaces.IConstantesSeccion;
import cl.foursoft.eee.dao.transferObject.ParametrosEdificioTO; 
import cl.foursoft.eee.util.UtilLog;

public class ConstantesSeccionesDB extends DBBase implements IConstantesSeccion {

	
	public ConstantesSeccionesDB(Connection c){
		this.conn = c;
	}
	@Override
	 public List<ParametrosEdificioTO> obtenerConstantes(){
		List<ParametrosEdificioTO> resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = "SELECT id_constante_seccion,nombre_constante,simbolo_constante,unidad_constante,valor_inicial_constante " +
						"FROM constantes_seccion_inicial";
		
		try {
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			
			resp = new ArrayList<ParametrosEdificioTO>();
			
			while(rs.next()){
				ParametrosEdificioTO constante = new ParametrosEdificioTO();
//				
//				constante.setid_constante(rs.getInt(1));
//				constante.setNombre_constante(rs.getString(2));
//				constante.setSimbolo_constante(rs.getString(3));
//				constante.setUnidad_constante(rs.getString(4));
//				constante.setValor_inicial_constante(rs.getFloat(5)); 
				
				resp.add(constante);	
			}
			
			rs.close();
			pstmt.close();

		} catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}catch(Exception e){
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try{
					if(rs!=null)
						rs.close();
					pstmt.close();
				}catch(SQLException sqle){
					UtilLog.registrar(sqle);
				}
			}
		}
		return resp;
	}

}

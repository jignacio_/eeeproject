package cl.foursoft.eee.dao.implementation.postgresql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

 

import cl.foursoft.eee.dao.implementation.DBBase;
import cl.foursoft.eee.dao.interfaces.IConstantesEdificio;
import cl.foursoft.eee.dao.transferObject.ParametrosEdificioTO;
import cl.foursoft.eee.dao.transferObject.FactorRenovacionTO;
import cl.foursoft.eee.util.UtilLog;

public class ConstantesEdificioDB extends DBBase implements IConstantesEdificio {

	
	public ConstantesEdificioDB(Connection c){
		this.conn = c;
	}
	@Override
	public List<ParametrosEdificioTO> obtenerConstantes()  { //Cambiar a obtener parametros
		List<ParametrosEdificioTO> resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = "SELECT id_parametro,nombre_parametro,simbolo_parametro,unidad_parametro,valor_parametro " +
						"FROM PARAMETROS_INICIAL_EDIFICIO;";
		
		try {
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			
			resp = new ArrayList<ParametrosEdificioTO>();
			
			while(rs.next()){
				ParametrosEdificioTO constante = new ParametrosEdificioTO();
				
				constante.setIdParametro(rs.getInt(1));
				constante.setNombreParametro(rs.getString(2));
				constante.setSimboloParametro(rs.getString(3));
				constante.setUnidadParametro(rs.getString(4));
				constante.setValorParametro(rs.getFloat(5)); 
				
				resp.add(constante);	
			}
			
			rs.close();
			pstmt.close();

		} catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}catch(Exception e){
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try{
					if(rs!=null)
						rs.close();
					pstmt.close();
				}catch(SQLException sqle){
					UtilLog.registrar(sqle);
				}
			}
		}
		return resp;
	}
	@Override
	public List<FactorRenovacionTO> obtenerFactorDeRenovacion() {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<FactorRenovacionTO> resp = null;
		
		
		String query = "SELECT id_factor, nombre_factor, cambio_aire_minimo, cambio_aire_maximo,discriminador " +
					   "FROM factor_de_renovacion " +
					   "ORDER BY nombre_factor ASC;";
		try {
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			
			resp = new ArrayList<FactorRenovacionTO>();
			
			while(rs.next()){
				FactorRenovacionTO f = new FactorRenovacionTO();
				
				f.setIdFactor(rs.getInt(1));
				f.setNombreFactor(rs.getString(2));
				f.setCambioAireMinimo(rs.getFloat(3));
				f.setCambioAireMaximo(rs.getFloat(4));
				f.setDiscriminador(rs.getString(5));
				
				resp.add(f);
				
			}
			
			
			rs.close();
			pstmt.close();
			
		} catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}catch (Exception e) {
			UtilLog.registrar(e);
			
		}finally{
			if(pstmt!=null){
				try{
					if(rs!=null)
						rs.close();
					pstmt.close();
				}catch (SQLException ex) {
					UtilLog.registrar(ex);
				}
			}
		}
		return resp;
	}
	
	@Override
	public boolean editarFactor(FactorRenovacionTO factor) {
		PreparedStatement pstmt = null;
		String query = " UPDATE factor_de_renovacion SET cambio_aire_minimo = ?, cambio_aire_maximo = ?, discriminador = ?, nombre_factor = ? " +
					   " WHERE id_factor = ?";
		
		try{
			pstmt = conn.prepareStatement(query);
			
			pstmt.setFloat(1, factor.getCambioAireMinimo());
			pstmt.setFloat(2, factor.getCambioAireMaximo());
			pstmt.setString(3, factor.getDiscriminador());
			pstmt.setString(4, factor.getNombreFactor());
			pstmt.setInt(5, factor.getIdFactor());
			
			
			int resp = pstmt.executeUpdate();
			
			pstmt.close();
			if(resp < 0) return false;
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
			return false;
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
			return false;
		}
		
		return true;
	}
	
	@Override
	public boolean guardarFactor(FactorRenovacionTO factor) {
		PreparedStatement pstmt = null;
		String query = " INSERT INTO factor_de_renovacion(nombre_factor, cambio_aire_minimo, cambio_aire_maximo, discriminador) " +
					   " VALUES(?,?,?,?) ";
		
		try{
			pstmt = conn.prepareStatement(query);
			
			pstmt.setString(1, factor.getNombreFactor());
			pstmt.setFloat(2, factor.getCambioAireMinimo());
			pstmt.setFloat(3, factor.getCambioAireMaximo());
			pstmt.setString(4, factor.getDiscriminador());			
			
			int resp = pstmt.executeUpdate();
			
			pstmt.close();
			if(resp < 0) return false;
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
			return false;
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
			return false;
		}
		
		return true;
	}
	
	@Override
	public boolean guardarFactorSync(FactorRenovacionTO factor) {
		PreparedStatement pstmt = null;
		String query = " INSERT INTO factor_de_renovacion(id_factor, nombre_factor, cambio_aire_minimo, cambio_aire_maximo, discriminador) " +
					   " VALUES(?,?,?,?,?) ";
		
		try{
			pstmt = conn.prepareStatement(query);
			
			pstmt.setInt(1, factor.getIdFactor());
			pstmt.setString(2, factor.getNombreFactor());
			pstmt.setFloat(3, factor.getCambioAireMinimo());
			pstmt.setFloat(4, factor.getCambioAireMaximo());
			pstmt.setString(5, factor.getDiscriminador());			
			
			int resp = pstmt.executeUpdate();
			
			pstmt.close();
			if(resp < 0) return false;
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
			return false;
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
			return false;
		}
		
		return true;
	}
	@Override
	public boolean existeFactor(int idFactor) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean existe = false;
		String query = " SELECT COUNT(id_factor) FROM factor_de_renovacion WHERE id_factor = ?;";
		
		try{
			pstmt = conn.prepareStatement(query);
			
			pstmt.setInt(1, idFactor);	
			rs = pstmt.executeQuery();
			//rs = pstmt.getResultSet();
			
			if(rs.next()){
				existe = (rs.getInt(1) > 0) ? true : false;
			}
			
			pstmt.close();
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
			return true;
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
			return true;
		}
		
		return existe;
	}
}

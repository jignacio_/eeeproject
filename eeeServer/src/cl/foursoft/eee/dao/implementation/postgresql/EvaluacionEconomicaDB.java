package cl.foursoft.eee.dao.implementation.postgresql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cl.foursoft.eee.dao.implementation.DBBase;
import cl.foursoft.eee.dao.interfaces.IEvaluacionEconomica;
import cl.foursoft.eee.dao.transferObject.EvaluacionEconomicaTO;
import cl.foursoft.eee.util.UtilFechas;
import cl.foursoft.eee.util.UtilLog;

public class EvaluacionEconomicaDB extends DBBase implements IEvaluacionEconomica {
	
	public EvaluacionEconomicaDB(Connection c){
		this.conn = c;
	}

	@Override
	public int guardarEvaluacion(EvaluacionEconomicaTO evaluacion) {
		int resp = -1;
		PreparedStatement pstmt = null;
		
		Timestamp HOY = UtilFechas.hoy();
		
		String query= "INSERT INTO EVALUACION_ECONOMICA_DETALLE(id_alternativa, " +
																"id_parametro_evaluacion, " +
																"PAYBACK_plazo_de_recuperacion," +
																"van_valor_actual_neto, " +
																"tir_tasa_interna_retorno , " +
																"fecha_creacion , " +
																"fecha_ultima_evaluacion  , " +
																"fecha_siguiente_evaluacion  )  VALUES(?,?,?,?,?,?,?,?);";
 
		try {
			pstmt = this.conn.prepareStatement(query);
					
			pstmt.setInt(1, evaluacion.getIdAlternativa());
			pstmt.setInt(2, evaluacion.getIdParametro());
			
			pstmt.setInt(3, evaluacion.getValorPayBack());
			pstmt.setDouble(4, evaluacion.getValorVan());
			pstmt.setDouble(5, evaluacion.getValorTir());
			
			pstmt.setTimestamp(6, HOY);
			pstmt.setTimestamp(7, HOY);
			pstmt.setTimestamp(8, UtilFechas.sumarMesesFecha(HOY, 6)); 
			//
			
			resp = pstmt.executeUpdate();
			pstmt.close();
		} catch (Exception e) {
				UtilLog.registrar(e);
		} finally{
			if(pstmt!=null){
				try {
					pstmt.close(); 
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		
		return resp;
	}
	
 

	@Override
	public int actualizarEvaluacion(EvaluacionEconomicaTO evaluacion) {
		int resp = -1;
		PreparedStatement pstmt = null;
		
	
		String query = "UPDATE EVALUACION_ECONOMICA_DETALLE " +
					   "SET PAYBACK_plazo_de_recuperacion = ? , van_valor_actual_neto = ? , tir_tasa_interna_retorno = ? " +
					   "WHERE id_alternativa = ? and id_parametro_evaluacion = ?;";
		
		try {
			pstmt = this.conn.prepareStatement(query);  
			
			pstmt.setInt(1,evaluacion.getValorPayBack());
			pstmt.setDouble(2,evaluacion.getValorVan());
			pstmt.setDouble(3,evaluacion.getValorTir());
			
			pstmt.setInt(4,evaluacion.getIdAlternativa());
			pstmt.setInt(5,evaluacion.getIdParametro());
			
			resp = pstmt.executeUpdate();

			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		
		return resp;
	}

	@Override
	public boolean existeEvaluacion(int idAlternativa, int idParametro) {
		boolean resp = false;
		int cantidad  = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs  = null;
		String query = "SELECT COUNT(*) FROM EVALUACION_ECONOMICA_DETALLE WHERE id_alternativa=? and id_parametro_evaluacion = ?;";
		
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1, idAlternativa);
			pstmt.setInt(2, idParametro);
			
			rs = pstmt.executeQuery();
			rs.next();
			cantidad  = rs.getInt(1);
			
			if(cantidad>0) resp = true;
			
			
			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp;
	}

	@Override
	public int eliminarEvaluacion(int idEvaluacion) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int eliminarEvaluacionPorIdAlternativaIdParametro(int idAlternativa, int idParametro) {
		int resp = -1;
		PreparedStatement pstmt = null;
		
		String query = "DELETE FROM EVALUACION_ECONOMICA_DETALLE WHERE id_alternativa=? and id_parametro_evaluacion = ?;";
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1,idAlternativa);
			pstmt.setInt(2,idParametro);
			
			resp = pstmt.executeUpdate();
			
			pstmt.close();
		} catch (Exception e) {
				UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
						UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp;
	}

	@Override
	public EvaluacionEconomicaTO obtenerEvaluacion(int idAlternativa, int idParametro) {
		EvaluacionEconomicaTO resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		
		String query = " SELECT e.van_valor_actual_neto , e.PAYBACK_plazo_de_recuperacion , e.tir_tasa_interna_retorno , e.fecha_creacion , e.fecha_ultima_evaluacion , e.fecha_siguiente_evaluacion , a.descripcion_alternativa " +
					   " FROM evaluacion_economica_detalle e" +
					   " INNER JOIN alternativa a ON  e.id_alternativa = a.id_alternativa " +
					   " WHERE e.id_alternativa = ? and e.id_parametro_evaluacion = ? ;";
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1,idAlternativa);
			pstmt.setInt(2,idParametro);
			
			rs = pstmt.executeQuery();
			if(rs.next()){
				resp = new EvaluacionEconomicaTO();
				resp.setIdAlternativa(idAlternativa);
				resp.setIdParametro(idParametro);
				
				
				resp.setValorVan(rs.getDouble(1));				
				resp.setValorPayBack(rs.getInt(2));				
				resp.setValorTir(rs.getDouble(3));		
				
				
				//Fechas
				Timestamp fechaCreacion = rs.getTimestamp(4);
				Timestamp fechaUltimaEvaluacion = rs.getTimestamp(5);
				Timestamp fechaSiguienteEvaluacion = rs.getTimestamp(6);
				
				resp.setFecha_creacion(UtilFechas.fechaComoString(fechaCreacion));          
				resp.setFecha_ultima_evaluacion(UtilFechas.fechaComoString(fechaUltimaEvaluacion));          
				resp.setFecha_siguiente_evaluacion(UtilFechas.fechaComoString(fechaSiguienteEvaluacion));
				
				resp.setDescripcionAlternativa(rs.getString(7));
			}
			
			
			rs.close();
			pstmt.close();
		} catch (Exception e) {
				UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					if(rs!=null)
						rs.close();
					pstmt.close();
				} catch (SQLException e2) {
						UtilLog.registrar(e2);
				}
			}
		}
		
		
		return resp;
	}

	@Override
	public List<EvaluacionEconomicaTO> obtenerTodasLasEvaluaciones(int idParametro, int idEdificio) {
		List<EvaluacionEconomicaTO> resp = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		
		String query = " SELECT e.van_valor_actual_neto , e.PAYBACK_plazo_de_recuperacion , e.tir_tasa_interna_retorno , e.fecha_creacion , e.fecha_ultima_evaluacion , e.fecha_siguiente_evaluacion , a.id_alternativa , a.descripcion_alternativa " +
					   " FROM evaluacion_economica_detalle e " +
					   " INNER JOIN alternativa a ON  e.id_alternativa = a.id_alternativa " +
					   " WHERE a.id_edificio = ? and e.id_parametro_evaluacion = ? ;";
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1,idEdificio);
			pstmt.setInt(2,idParametro);
			
			rs = pstmt.executeQuery();
			resp = new ArrayList<EvaluacionEconomicaTO>();
			while(rs.next()){
				EvaluacionEconomicaTO evaluacion = new EvaluacionEconomicaTO();
				evaluacion = new EvaluacionEconomicaTO();
				
				evaluacion.setValorVan(rs.getDouble(1));				
				evaluacion.setValorPayBack(rs.getInt(2));				
				evaluacion.setValorTir(rs.getDouble(3));		
				
				//Fechas
				Timestamp fechaCreacion = rs.getTimestamp(4);
				Timestamp fechaUltimaEvaluacion = rs.getTimestamp(5);
				Timestamp fechaSiguienteEvaluacion = rs.getTimestamp(6);
				
				evaluacion.setFecha_creacion(UtilFechas.fechaComoString(fechaCreacion));          
				evaluacion.setFecha_ultima_evaluacion(UtilFechas.fechaComoString(fechaUltimaEvaluacion));          
				evaluacion.setFecha_siguiente_evaluacion(UtilFechas.fechaComoString(fechaSiguienteEvaluacion));
				
				evaluacion.setIdAlternativa(rs.getInt(7));
				evaluacion.setIdParametro(idParametro);
				evaluacion.setDescripcionAlternativa(rs.getString(8));
				
				resp.add(evaluacion);
			}
			
			
			rs.close();
			pstmt.close();
		} catch (Exception e) {
				UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					if(rs!=null)
						rs.close();
					pstmt.close();
				} catch (SQLException e2) {
						UtilLog.registrar(e2);
				}
			}
		}
		
		
		return resp;
	}


	@Override
	public List<EvaluacionEconomicaTO> obtenerTodasLasEvaluacionesEdificio(int idEdificio) {
List<EvaluacionEconomicaTO> resp = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		
		String query = " SELECT e.van_valor_actual_neto , " +
							   "e.PAYBACK_plazo_de_recuperacion , " +
							   "e.tir_tasa_interna_retorno , " +
							   "e.fecha_creacion , " +
							   "e.fecha_ultima_evaluacion , " +
							   "e.fecha_siguiente_evaluacion , " +
							   "a.id_alternativa , " +
							   "a.descripcion_alternativa ," +
							   "e.id_parametro_evaluacion " +
					   " FROM evaluacion_economica_detalle e " +
					   " INNER JOIN alternativa a ON  e.id_alternativa = a.id_alternativa " +
					   " WHERE a.id_edificio = ?;";
		
		//INNER JOIN PARAMETROS
		
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1,idEdificio);
			
			rs = pstmt.executeQuery();
			resp = new ArrayList<EvaluacionEconomicaTO>();
			while(rs.next()){
				EvaluacionEconomicaTO evaluacion = new EvaluacionEconomicaTO();
				evaluacion = new EvaluacionEconomicaTO();
				
				evaluacion.setValorVan(rs.getDouble(1));				
				evaluacion.setValorPayBack(rs.getInt(2));				
				evaluacion.setValorTir(rs.getDouble(3));		
				
				//Fechas
				Timestamp fechaCreacion = rs.getTimestamp(4);
				Timestamp fechaUltimaEvaluacion = rs.getTimestamp(5);
				Timestamp fechaSiguienteEvaluacion = rs.getTimestamp(6);
				
				evaluacion.setFecha_creacion(UtilFechas.fechaComoString(fechaCreacion));          
				evaluacion.setFecha_ultima_evaluacion(UtilFechas.fechaComoString(fechaUltimaEvaluacion));          
				evaluacion.setFecha_siguiente_evaluacion(UtilFechas.fechaComoString(fechaSiguienteEvaluacion));
				
				evaluacion.setIdAlternativa(rs.getInt(7));
				evaluacion.setDescripcionAlternativa(rs.getString(8));
				evaluacion.setIdParametro(rs.getInt(9));
				
				resp.add(evaluacion);
			}
			
			
			rs.close();
			pstmt.close();
		} catch (Exception e) {
				UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					if(rs!=null)
						rs.close();
					pstmt.close();
				} catch (SQLException e2) {
						UtilLog.registrar(e2);
				}
			}
		}
		
		
		return resp;
	}
	@Override
	public Timestamp obtenerFechaUltimaEvaluacion(int idAlternativa, int idParametro) {
		Timestamp resp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = "SELECT fecha_ultima_evaluacion " +
					   "FROM evaluacion_economica_detalle " +
					   "WHERE  id_alternativa = ? AND id_parametro_evaluacion = ?;";
		try{
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1, idAlternativa);
			pstmt.setInt(2, idParametro);
			
			
			rs = pstmt.executeQuery();
			if(rs.next())
				resp = rs.getTimestamp(1);
			
			pstmt.close();
			rs.close();
		}catch(Exception e){
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					
					if(rs!=null)
						rs.close();
					pstmt.close();
					
				} catch (SQLException  sqle) {
						UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp;
	}

	@Override
	public int actualizarRequerimiento( EvaluacionEconomicaTO evaluacion) {
		int resp = -1;
		PreparedStatement pstmt = null;
		
		Timestamp fechaActual = UtilFechas.hoy();
		//Timestamp ultimaEvaluacion = obtenerFechaUltimaEvaluacion(evaluacion.getIdAlternativa(), evaluacion.getIdParametro());
		Timestamp siguienteEvalucion = UtilFechas.sumarMesesFecha(new Date(), 6) ; 
		
		String query = " UPDATE evaluacion_economica_detalle" +
					   " SET fecha_siguiente_evaluacion = ? , fecha_ultima_evaluacion = ? , PAYBACK_plazo_de_recuperacion = ? , van_valor_actual_neto = ? , tir_tasa_interna_retorno = ?  " +
					   " WHERE id_alternativa = ? and id_parametro_evaluacion = ?  ";
		
		try {
			pstmt = this.conn.prepareStatement(query);
			
			pstmt.setTimestamp(1,siguienteEvalucion );
			pstmt.setTimestamp(2,fechaActual );
			
			pstmt.setInt(3, evaluacion.getValorPayBack());
			pstmt.setDouble(4, evaluacion.getValorVan());
			pstmt.setDouble(5, evaluacion.getValorTir());
			
			
			pstmt.setInt(6,evaluacion.getIdAlternativa());
			pstmt.setInt(7,evaluacion.getIdParametro());
			 
			resp  = pstmt.executeUpdate();

			pstmt.close();
		} catch (Exception e) {
				UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException e2) {
						UtilLog.registrar(e2);
				}
			}
		}
		 
		
		return resp;
	}


}

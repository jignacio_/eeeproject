package cl.foursoft.eee.dao.implementation.postgresql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import cl.foursoft.eee.dao.implementation.DBBase;
import cl.foursoft.eee.dao.interfaces.ISistema;
import cl.foursoft.eee.dao.transferObject.EmailTO;
import cl.foursoft.eee.util.UtilLog;

public class SistemaDB extends DBBase implements ISistema {

	
	public SistemaDB(Connection c){
		this.conn = c;
	}
	
	@Override
	public boolean gurdarIpSincronizacion(Connection c, String ip) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean actualizarIPSincronizacion(String ip) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String obtenerIpSincronizacion() {
		String resp = "";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		
		String query = "SELECT ip_sincronizacion FROM ip_sincronizacion;";
		try {
			pstmt = this.conn.prepareStatement(query);
			
			rs = pstmt.executeQuery();
			
			if(rs.next())
				resp = rs.getString(1);
			
			pstmt.close();
		}catch (SQLException sqle) {
			UtilLog.registrar(sqle);

		} catch (Exception e) {
			UtilLog.registrar(e);
		}
		
		return resp;
	} 

	@Override
	public boolean existeIp() {
		boolean resp = false;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		
		String query = "SELECT COUNT(ip_sincronizacion) FROM ip_sincronizacion;";
		try {
			pstmt = this.conn.prepareStatement(query);
			
			rs = pstmt.executeQuery();
			
			if(rs.next())
				resp =  ( rs.getInt(1) > 0 ) ? true : false;
			
			
			
			pstmt.close();
		}catch (SQLException sqle) {
			UtilLog.registrar(sqle);

		} catch (Exception e) {
			UtilLog.registrar(e);
		}
		
		return resp;
	}
	@Override
	public boolean guardarEmail(Connection c, EmailTO email) {
		// TODO Auto-generated method stub
		return false;
	}



	
	@Override
	public boolean guardarIPSincronizacion(String ip) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public EmailTO obtenerDatosEmail() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean exiteEmail() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void actualizarDatosEmail(EmailTO email) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void guardarDatosEmail(EmailTO email) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean guardarFechaUltimaSincronizacion(Timestamp fechaActualizacion) {
		boolean resp = false;
		int value = -1;
		PreparedStatement pstmt = null;
		
		String queryGuardar = "INSERT INTO fecha_sincronizacion( sincronizacion_datos, id_fecha_sincro)  VALUES (?,?); ";
		String queryActualizar = "UPDATE fecha_sincronizacion  SET sincronizacion_datos=? WHERE id_fecha_sincro = ?;";
		try {
			
			if(existeFechaGuardada()==false)
				pstmt = this.conn.prepareStatement(queryGuardar);
			else
				pstmt = this.conn.prepareStatement(queryActualizar);
			
			
			pstmt.setTimestamp(1, fechaActualizacion);
			pstmt.setInt(2, 1); //Siempre id 1
			value = pstmt.executeUpdate();
			resp = (value>0) ? true : false;	
			 
			
			pstmt.close();
		}catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}catch (Exception e) {
			UtilLog.registrar(e);
		}
		
		return resp;
	}

	@Override
	public Timestamp obtenerFechaUltimaSincronizacion(Connection c) {
		Timestamp resp = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String query = "SELECT sincronizacion_datos FROM fecha_sincronizacion;";

			try {
				pstmt = this.conn.prepareStatement(query);

				rs = pstmt.executeQuery();
				if(rs.next())
					resp = rs.getTimestamp(1) ;
	 
					pstmt.close();
			}catch (SQLException sqle) {
				UtilLog.registrar(sqle);
			}catch (Exception e) {
				UtilLog.registrar(e);
			}	
			return resp;
	}
	private boolean existeFechaGuardada(){
		boolean resp = false;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String query = "SELECT COUNT(sincronizacion_datos) FROM fecha_sincronizacion";

			try {
				pstmt = this.conn.prepareStatement(query);

				rs = pstmt.executeQuery();
				if(rs.next())
					resp = (rs.getInt(1) > 0) ? true : false;
	 
					pstmt.close();
			}catch (SQLException sqle) {
				UtilLog.registrar(sqle);
			}catch (Exception e) {
				UtilLog.registrar(e);
			}	
			return resp;
	}




}

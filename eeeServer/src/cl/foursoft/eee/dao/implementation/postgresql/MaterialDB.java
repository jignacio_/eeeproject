package cl.foursoft.eee.dao.implementation.postgresql;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cl.foursoft.eee.dao.implementation.DBBase;
import cl.foursoft.eee.dao.interfaces.IMaterial;
import cl.foursoft.eee.dao.transferObject.MaterialTO;
import cl.foursoft.eee.dao.transferObject.SeccionTO;
import cl.foursoft.eee.util.DirectoriosImagen;
import cl.foursoft.eee.util.UtilLog;

public class MaterialDB extends DBBase implements IMaterial {

	
	public MaterialDB(Connection c){
		this.conn = c;
	}
	
	@Override
	public List<MaterialTO> obtenerTodosLosMateriales() {
		List<MaterialTO> resp = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String queryCompleta = "SELECT m.id_material, " +
									" m.id_tipo_seccion_envolvente , "+
									" m.nombre_material, " +
									"m.espesor_material, " +
									"m.imagen_material, " +
									"m.conductividad_termica, " +
									"m.transmitancia_termica, " +
									"m.resistencia_termica, " +
									"mp.precio_material , " +
									"mp.unidad_material , " +
									"ts.nombre_seccion_envolvente  " +
						" FROM materiales m " +
						" INNER JOIN material_precio mp ON m.id_material = mp.id_material " +
						" INNER JOIN tipo_seccion_envolvente ts ON ts.id_tipo_seccion_envolvente_padr = m.id_tipo_seccion_envolvente and ts.id_tipo_seccion_envolvente_padr = ts.id_tipo_seccion_envolvente " +
						" WHERE eliminado = ? " +
						" ORDER BY m.nombre_material ASC;";
		
 
		

		try {
			pstmt = conn.prepareStatement(queryCompleta);
			pstmt.setBoolean(1,false);
			rs = pstmt.executeQuery();
			resp = new ArrayList<MaterialTO>();
			
			while(rs.next()){
				MaterialTO _nuevoMaterial = new MaterialTO();
				_nuevoMaterial.setIdMaterial(rs.getInt(1));
				_nuevoMaterial.setIdSeccionEdificio(rs.getInt(2));
				_nuevoMaterial.setNombreMaterial(rs.getString(3));
				_nuevoMaterial.setEspesor(rs.getFloat(4));
				
				_nuevoMaterial.setRutaFotografia(rs.getString(5));
				_nuevoMaterial.setConductividad(rs.getFloat(6));
				
				_nuevoMaterial.setTransmitancia(rs.getFloat(7));
				_nuevoMaterial.setResistencia(rs.getFloat(8));
//				
				_nuevoMaterial.setPrecioMaterial(rs.getFloat(9));
				_nuevoMaterial.setUnidad_material(rs.getString(10));
				_nuevoMaterial.setNombreSeccionEdificio(rs.getString(11));
				_nuevoMaterial.setEliminado(false);
				if(_nuevoMaterial.getRutaFotografia().isEmpty()!=false){
					
					_nuevoMaterial.setFotografia(obtenerFotografia(_nuevoMaterial.getRutaFotografia()));
					
				}
				
				resp.add(_nuevoMaterial);
			}
			
			pstmt.close();
			rs.close();
			
		} catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt != null){
				try {
					if(rs != null)
						rs.close();
					pstmt.close();
				} catch (SQLException e2) {
					UtilLog.registrar(e2);
				}
			}
		}
		return resp;
	}
		@Override
		public List<MaterialTO> obtenerTodosLosMaterialesSync() {
			List<MaterialTO> resp = null;
			
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			
			String queryCompleta = "SELECT m.id_material, " +
			" m.id_tipo_seccion_envolvente , "+
			" m.nombre_material, " +
			"m.espesor_material, " +
			"m.imagen_material, " +
			"m.conductividad_termica, " +
			"m.transmitancia_termica, " +
			"m.resistencia_termica, " +
			"mp.precio_material , " +
			"mp.unidad_material , " +
			"ts.nombre_seccion_envolvente , " +
			"m.eliminado " +
			" FROM materiales m " +
			" INNER JOIN material_precio mp ON m.id_material = mp.id_material " +
			" INNER JOIN tipo_seccion_envolvente ts ON ts.id_tipo_seccion_envolvente_padr = m.id_tipo_seccion_envolvente and ts.id_tipo_seccion_envolvente_padr = ts.id_tipo_seccion_envolvente " +
			" ORDER BY m.nombre_material ASC;";
			
			
			
			
			try {
				pstmt = conn.prepareStatement(queryCompleta);
				rs = pstmt.executeQuery();
				resp = new ArrayList<MaterialTO>();
				
				while(rs.next()){
					MaterialTO _nuevoMaterial = new MaterialTO();
					_nuevoMaterial.setIdMaterial(rs.getInt(1));
					_nuevoMaterial.setIdSeccionEdificio(rs.getInt(2));
					_nuevoMaterial.setNombreMaterial(rs.getString(3));
					_nuevoMaterial.setEspesor(rs.getFloat(4));
					
					_nuevoMaterial.setRutaFotografia(rs.getString(5));
					_nuevoMaterial.setConductividad(rs.getFloat(6));
					
					_nuevoMaterial.setTransmitancia(rs.getFloat(7));
					_nuevoMaterial.setResistencia(rs.getFloat(8));
//				
					_nuevoMaterial.setPrecioMaterial(rs.getFloat(9));
					_nuevoMaterial.setUnidad_material(rs.getString(10));
					_nuevoMaterial.setNombreSeccionEdificio(rs.getString(11));
					_nuevoMaterial.setEliminado(rs.getBoolean(12));
					if(_nuevoMaterial.getRutaFotografia().isEmpty()!=false){
						
						_nuevoMaterial.setFotografia(obtenerFotografia(_nuevoMaterial.getRutaFotografia()));
						
					}
					
					resp.add(_nuevoMaterial);
				}
				
				pstmt.close();
				rs.close();
				
			} catch (SQLException sqle) {
				UtilLog.registrar(sqle);
			}catch (Exception e) {
				UtilLog.registrar(e);
			}finally{
				if(pstmt != null){
					try {
						if(rs != null)
							rs.close();
						pstmt.close();
					} catch (SQLException e2) {
						UtilLog.registrar(e2);
					}
				}
			}
		
		
		
		return resp;
	}
	public List<MaterialTO> obtenerTodosLosMaterialesCompleto() {
		List<MaterialTO> resp = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String queryCompleta = "SELECT m.id_material, " +
									" m.id_tipo_seccion_envolvente , "+
									" m.nombre_material, " +
									"m.espesor_material, " +
									"m.imagen_material, " +
									"m.conductividad_termica, " +
									"m.transmitancia_termica, " +
									"m.resistencia_termica, " +
									"mp.precio_material , " +
									"mp.unidad_material , " +
									"ts.nombre_seccion_envolvente  " +
						" FROM materiales m " +
						" INNER JOIN material_precio mp ON m.id_material = mp.id_material " +
						" INNER JOIN tipo_seccion_envolvente ts ON ts.id_tipo_seccion_envolvente_padr = m.id_tipo_seccion_envolvente and ts.id_tipo_seccion_envolvente_padr = ts.id_tipo_seccion_envolvente " +
						" WHERE eliminado = ? " +
						" ORDER BY m.nombre_material ASC;";
		
 
		

		try {
			pstmt = conn.prepareStatement(queryCompleta);
			pstmt.setBoolean(1,false);
			rs = pstmt.executeQuery();
			resp = new ArrayList<MaterialTO>();
			
			while(rs.next()){
				MaterialTO _nuevoMaterial = new MaterialTO();
				_nuevoMaterial.setIdMaterial(rs.getInt(1));
				_nuevoMaterial.setIdSeccionEdificio(rs.getInt(2));
				_nuevoMaterial.setNombreMaterial(rs.getString(3));
				_nuevoMaterial.setEspesor(rs.getFloat(4));
				
				_nuevoMaterial.setRutaFotografia(rs.getString(5));
				_nuevoMaterial.setConductividad(rs.getFloat(6));
				
				_nuevoMaterial.setTransmitancia(rs.getFloat(7));
				_nuevoMaterial.setResistencia(rs.getFloat(8));
//				
				_nuevoMaterial.setPrecioMaterial(rs.getFloat(9));
				_nuevoMaterial.setUnidad_material(rs.getString(10));
				_nuevoMaterial.setNombreSeccionEdificio(rs.getString(11));
				
				_nuevoMaterial.setEliminado(true);
				
				if(_nuevoMaterial.getRutaFotografia().isEmpty()!=false){
					
					_nuevoMaterial.setFotografia(obtenerFotografia(_nuevoMaterial.getRutaFotografia()));
					
				}
				
				resp.add(_nuevoMaterial);
			}
			
			pstmt.close();
			rs.close();
			
		} catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt != null){
				try {
					if(rs != null)
						rs.close();
					pstmt.close();
				} catch (SQLException e2) {
					UtilLog.registrar(e2);
				}
			}
		}
		
		
		
		return resp;
	}
	@Override
	public List<MaterialTO> obtenerMaterialesPorTipoSeccion(int idSeccion) {//Duplicar funcion con y sin precio
		List<MaterialTO> resp = null; 
		PreparedStatement pstmt = null;
		ResultSet rs = null;
 
		
		String query = 	" SELECT m.id_material, " +
								"m.nombre_material, " +
								"m.espesor_material, " +
								"m.conductividad_termica, " +
								"m.resistencia_termica, " +
								"m.transmitancia_termica, " +
								"m.id_tipo_seccion_envolvente, " +
								"mp.precio_material ," +
								"ts.nombre_seccion_envolvente ," +
								"mp.unidad_material , " +
								"m.imagen_material " +
						" FROM materiales m " +
						" INNER JOIN material_precio mp ON mp.id_material = m.id_material " +
						" INNER JOIN tipo_seccion_envolvente ts ON ts.id_tipo_seccion_envolvente_padr = m.id_tipo_seccion_envolvente and ts.id_tipo_seccion_envolvente_padr = ts.id_tipo_seccion_envolvente " +
						" WHERE m.id_tipo_seccion_envolvente = ? and eliminado = ? "+
						" ORDER BY nombre_material ASC; ";
	
 

		try {
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, idSeccion);
			pstmt.setBoolean(2,false);
			rs = pstmt.executeQuery();
			
			resp = new ArrayList<MaterialTO>();
			
			while(rs.next()){
				MaterialTO _nuevoMaterial = new MaterialTO();
				
				_nuevoMaterial.setIdMaterial(rs.getInt(1));
				_nuevoMaterial.setNombreMaterial(rs.getString(2));
				_nuevoMaterial.setEspesor(rs.getFloat(3));
				
				//_nuevoMaterial.setImagenMaterial(rs.getString(4));
				_nuevoMaterial.setConductividad(rs.getFloat(4));
				_nuevoMaterial.setResistencia(rs.getFloat(5));
				_nuevoMaterial.setTransmitancia(rs.getFloat(6));
				
				
				_nuevoMaterial.setIdSeccionEdificio(rs.getInt(7));
				_nuevoMaterial.setPrecioMaterial(rs.getFloat(8));
	
				_nuevoMaterial.setNombreSeccionEdificio(rs.getString(9));
				_nuevoMaterial.setUnidad_material(rs.getString(10));
				
				_nuevoMaterial.setEliminado(false);
				
 //FORMA CORRECTA DE CARGAR LAS FOTOS
//				rutaFotografia = rs.getString(11);
//				vacio = rutaFotografia.isEmpty();
//				vacio = rutaFotografia.equals("");
//				if(rutaFotografia!=null){
//					if(vacio!=true){
//						_nuevoMaterial.setRutaFotografia(rutaFotografia);
//						_nuevoMaterial.setFotografia(obtenerFotografia(rutaFotografia));
//					}
//				}
 
				_nuevoMaterial.setRutaFotografia(rs.getString(11));
				
				if(_nuevoMaterial.getRutaFotografia().isEmpty()!=true)
						_nuevoMaterial.setFotografia(obtenerFotografia(_nuevoMaterial.getRutaFotografia()));

				resp.add(_nuevoMaterial);
			}
			
			pstmt.close();
			rs.close();
			
		} catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt != null){
				try {
					if(rs != null)
						rs.close();
					pstmt.close();
				} catch (SQLException e2) {
					UtilLog.registrar(e2);
				}
			}
		}
		
		
		
		return resp;
	}

	@Override
	public int guardarComposicionEdificio(List<MaterialTO> materiales, int idSeccion, int idEdificio) {
		int resp = -1;
		PreparedStatement pstmt = null;
		MaterialTO materialActual = null;
		String query ="INSERT INTO COMPOSICION_ENVOLVENTE( id_material, " +
														  "id_tipo_seccion_envolvente, " +
														  "id_edificio ," +
														  "cantidad_material  )" +
														  " VALUES(?,?,?,?)";
		try {
			
			pstmt = this.conn.prepareStatement(query);
			
			for(int i = 0; i<materiales.size(); i++){
				materialActual = materiales.get(i);
				
				pstmt.setInt(1,materialActual.getIdMaterial());
				pstmt.setInt(2, idSeccion );
				pstmt.setInt(3, idEdificio);
				pstmt.setFloat(4, materialActual.getCantidad_material());

				resp = pstmt.executeUpdate();
				
			}
			pstmt.close();
			
		} catch (Exception e) {
			UtilLog.registrar(e);
		}
		finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sql) {
				 UtilLog.registrar(sql);
				}
			}
		}
		
		return resp;
	}
	public int guardarParametroComposicion(SeccionTO seccion, int idEdificio){
		int resp = 0;
		
		PreparedStatement pstmt = null;
		String query ="INSERT INTO PARAMETROS_CALCULADOS_COMPOSICI( ID_TIPO_SECCION_ENVOLVENTE, " +
																   "TRANSMITANCIA, " +
																   "RESISTENCIA_TOTAL ," +
																   "ID_EDIFICIO , RSE, RSI ) " +
							  "VALUES(?,?,?,?,?,?)";
		
		try {
			
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1, seccion.getIdSeccionPadre());
			pstmt.setFloat(2, seccion.getTransmitanciaTermica());
			pstmt.setFloat(3, seccion.getResistenciaSeccion());
			pstmt.setInt(4,idEdificio); 
			
			pstmt.setFloat(5, seccion.getRSE()); 
			pstmt.setFloat(6, seccion.getRSI()); 
				
			resp = pstmt.executeUpdate();
			
			pstmt.close();
			
			
		} catch (Exception e) {
			UtilLog.registrar(e);
		}
		finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sql) {
				 UtilLog.registrar(sql);
				}
			}
		}
		
		
		return resp;
	}
	@Override
	public List<MaterialTO> obtenerComposicionSeccionEdificio(int idTipoSeccion, int idEdifico) {
		List<MaterialTO> resp = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String query 			= "SELECT c.id_material , " +
										 "m.nombre_material, " +
										 "m.imagen_material , " +
										 "m.espesor_material, " +
										 "m.conductividad_termica , " +
										 "m.transmitancia_termica , " +
										 "m.resistencia_termica , " +
										 "mp.unidad_material , " +
										 "mp.precio_material , " +
										 "c.cantidad_material " +
								  " FROM composicion_envolvente c " +
								  " INNER JOIN materiales m on c.id_material = m.id_material " +
								  " INNER JOIN material_precio mp on c.id_material = mp.id_material "+ 
								  " WHERE  c.id_tipo_seccion_envolvente = ? and c.id_edificio = ? ";
		
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1,idTipoSeccion);
			pstmt.setInt(2,idEdifico);
			rs = pstmt.executeQuery();
			resp = new ArrayList<MaterialTO>();
			
			while(rs.next()){
				MaterialTO m = new MaterialTO();
				m.setIdSeccionEdificio(idTipoSeccion);
				m.setIdMaterial(rs.getInt(1));
				m.setNombreMaterial(rs.getString(2));
				m.setRutaFotografia(rs.getString(3));
				m.setEspesor(rs.getFloat(4));
				m.setConductividad(rs.getFloat(5));
				m.setTransmitancia(rs.getFloat(6));
				m.setResistencia(rs.getFloat(7));
				
				
				m.setUnidad_material(rs.getString(8));
				m.setPrecioMaterial(rs.getFloat(9));
				m.setCantidad_material(rs.getInt(10));
				//OBtener Fotograf�a
				resp.add(m);
			}
			
			pstmt.close();
			rs.close();
			
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
					if(rs!=null)
						rs.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		
		return resp;		
	}
	public SeccionTO obtenerParametroComposicion(SeccionTO secc, int idEdificio){
	
		SeccionTO resp = null;
		
		PreparedStatement pstmt = null;
		
		ResultSet rs = null;
		String query 			= " SELECT TRANSMITANCIA, RESISTENCIA_TOTAL , RSE , RSI " +
								  " FROM PARAMETROS_CALCULADOS_COMPOSICI " + 
								  " WHERE id_tipo_seccion_envolvente = ? and id_edificio=?;";
		
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1,secc.getIdSeccionPadre());
			pstmt.setInt(2,idEdificio);
			rs = pstmt.executeQuery();
			
			 if(rs.next()){
				 secc.setTransmitanciaTermica(rs.getFloat(1));
				 secc.setResistenciaSeccion(rs.getFloat(2));
				 
				 secc.setRSE(rs.getFloat(3));
				 secc.setRSI(rs.getFloat(4));
				 
				 secc.setResistenciaTotalMasRSERSI( secc.getResistenciaSeccion() + secc.getRSE() + secc.getRSI() );
			 }
			
			 resp = secc;
			 
			pstmt.close();
			rs.close();
			
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
					if(rs!=null)
						rs.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp;
	
	}
	
	//Clases Admin
	@Override
	public int guardarMaterial(MaterialTO material) {
		int resp = -1;
		int idMaterial = -1;
		PreparedStatement pstmt = null;
		 
		
		String query = " INSERT INTO  MATERIALES(id_material, " +
											"   id_tipo_seccion_envolvente, " +
											"   nombre_corto, " +
											"	nombre_material, " +
											"   espesor_material, " +
											"	imagen_material, " +
											"   conductividad_termica, " +
											"   transmitancia_termica, " +
											"   resistencia_termica , " +
											"	eliminado )" +
					   " VALUES (?,?,?,?,?,?,?,?,?,?);";
		try {
			pstmt = this.conn.prepareStatement(query);
			
		//	for(MaterialTO _m : materiales){
				idMaterial = obtenerSiguienteID();
				
				if(material.getFotografia()!=null)
					material.setRutaFotografia(idMaterial+".jpg");
				
				pstmt.setInt(1, idMaterial);
				pstmt.setInt(2, material.getIdSeccionEdificio());
				pstmt.setString(3, "");
				
				pstmt.setString(4, material.getNombreMaterial());
				pstmt.setFloat(5,material.getEspesor());
				pstmt.setString(6,material.getRutaFotografia());

				pstmt.setFloat(7, material.getConductividad());
				pstmt.setFloat(8, material.getTransmitancia());
				pstmt.setFloat(9, material.getResistencia());
				
				pstmt.setBoolean(10, false);
				
				resp = pstmt.executeUpdate();
				
				if(material.getFotografia()!=null)
					 guardarFotografia(material.getFotografia(),material.getRutaFotografia());
				
			
				guardarPrecioMaterial(idMaterial, material.getPrecioMaterial(),material.getUnidad_material());
			//}
				
			
			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp;
	}
	
	private int guardarPrecioMaterial(int idMaterial, float precioMaterial , String unidad) {
		int resp = -1;
		PreparedStatement pstmt = null;		 
		
		String query = "INSERT INTO  material_precio(id_material, precio_material, unidad_material) " +
					   "VALUES (?,?,?) ";
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1,idMaterial);
			pstmt.setFloat(2, precioMaterial);
			pstmt.setString(3, unidad);

			resp = pstmt.executeUpdate();
		 
			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp;
	}
	private int actualizarPrecioMaterial(int idMaterial, float precioMaterial , String unidad) {
		int resp = -1;
		PreparedStatement pstmt = null;
		 
		
		String query = "UPDATE  material_precio SET precio_material = ? , unidad_material = ? WHERE id_material = ?;";
			
		try {
			pstmt = this.conn.prepareStatement(query);

			 pstmt.setFloat(1, precioMaterial);
			 pstmt.setString(2, unidad);

			 pstmt.setInt(3,idMaterial);
			
			 resp = pstmt.executeUpdate();
		 
			 pstmt.close();
			 
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp;
	}

	@Override
	public int actualizarMaterial(MaterialTO material) {
		int resp = -1;
		PreparedStatement pstmt = null;
		
		String query = "UPDATE MATERIALES SET id_tipo_seccion_envolvente = ? ," +
											" nombre_corto = ? , " +
											"nombre_material = ? , " +
											"espesor_material = ? , " +
											"imagen_material = ? ," +
											"conductividad_termica = ? , " +
											"transmitancia_termica = ? , " +
											"resistencia_termica = ? , " +
											"eliminado = ?  " +
											" WHERE id_material = ? ;";
		try {
			pstmt = this.conn.prepareStatement(query);
			String _rutaFotografia = (material.getFotografia()!=null) ? material.getIdMaterial()+".jpg" : "";
			
			
			pstmt.setInt(1,material.getIdSeccionEdificio());
			pstmt.setString(2,"");
			pstmt.setString(3,material.getNombreMaterial());
			
			pstmt.setFloat(4,material.getEspesor());
			pstmt.setString(5, _rutaFotografia);
			pstmt.setFloat(6,material.getConductividad());
			pstmt.setFloat(7,material.getTransmitancia());
			pstmt.setFloat(8,material.getResistencia());
			
			pstmt.setBoolean(9,material.getEliminado());
			
			pstmt.setInt(10,material.getIdMaterial());
			resp = pstmt.executeUpdate();
			if(resp>-1){
				resp = actualizarPrecioMaterial(material.getIdMaterial(), material.getPrecioMaterial(), material.getUnidad_material());
				if(resp>-1 && material.getFotografia()!=null)
					guardarFotografia(material.getFotografia(), material.getRutaFotografia());
			}
	
			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp;
	}

	@Override
	public int eliminarMaterial(int  idMaterial) {
		int resp = -1;
		PreparedStatement pstmt = null;
		
//		String query = "DELETE FROM materiales WHERE id_material = ?;";
		String query = "UPDATE materiales SET eliminado=true WHERE id_material = ?;";
		try {
			pstmt = this.conn.prepareStatement(query);
			pstmt.setInt(1, idMaterial);
			
			resp = pstmt.executeUpdate();
			
			pstmt.close();
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp;
	}
	
	  private int guardarFotografia (byte[] fotografia, String nombreFotografia) {
		int resp = -1;
		File directorio; 
		
		OutputStream out;
		try {
			//comprobar si esta el directorio, sino lo crea
			directorio = new File(DirectoriosImagen.DIRECTORIO_DEFINITIVO_MATERIALES);
		  	directorio.mkdirs();
		  	
		  	
			out = new FileOutputStream(DirectoriosImagen.DIRECTORIO_DEFINITIVO_MATERIALES+nombreFotografia);
			out.write(fotografia);
			out.close();
			resp = 0;
		} 
		catch (FileNotFoundException e) {
			UtilLog.registrar(e);
		}
		catch (IOException e) {
			UtilLog.registrar(e);
		}
		
		return resp;
	}
	 
	private byte[] obtenerFotografia(String rutaFotografia) {
		
		File file = new File(DirectoriosImagen.DIRECTORIO_DEFINITIVO_MATERIALES+rutaFotografia);
		byte [] buffer = null;// 
		byte [] resp = null;
        ByteArrayOutputStream ous = new ByteArrayOutputStream();
	    InputStream ios;
	   
		try {
			if(file.exists()){
				buffer = new byte[4096];
				ios = new FileInputStream(file);
				int read = 0;
				while ( (read = ios.read(buffer)) != -1 ) {
					ous.write(buffer, 0, read);
				}
				resp = ous.toByteArray();
			}
		}catch (FileNotFoundException e) {
			UtilLog.registrar(e);
			
		} catch (IOException e) {
			UtilLog.registrar(e);		
		}
        
			return resp;
	}
	  
	public int obtenerSiguienteID()
	{
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        int resp = -1;
        String query = "SELECT nextval('materiales_id_material_seq') as NEXTVAL";
        
        try
        {                        
            pstmt = conn.prepareStatement(query);            
            rs = pstmt.executeQuery();
            
            if(rs.next())
                resp = rs.getInt("NEXTVAL");
            
            rs.close();
            pstmt.close();
            
        }
        catch (SQLException sqle) {
        	UtilLog.registrar(sqle);
        }
        finally{
            if (pstmt != null) {
                  try {
                       pstmt.close();
                       rs.close();
                  } catch (SQLException e) {
                       e.printStackTrace();
                  }
            }
        }        
        return resp;
    }

	@Override
	public MaterialTO obtenerMaterialPorID(int idMaterial) {
		MaterialTO resp = null;

		PreparedStatement pstmt = null;
		ResultSet rs = null;


		String query = 	" SELECT m.nombre_material, " +
								"m.espesor_material, " +
								"m.conductividad_termica, " +
								"m.resistencia_termica, " +
								"m.transmitancia_termica, " +
								"m.id_tipo_seccion_envolvente, " +
								"mp.precio_material ," +
								"ts.nombre_seccion_envolvente ," +
								"mp.unidad_material , " +
								"m.imagen_material " +
						" FROM materiales m " +
						" INNER JOIN material_precio mp ON mp.id_material = m.id_material " +
						" INNER JOIN tipo_seccion_envolvente ts ON ts.id_tipo_seccion_envolvente_padr = m.id_tipo_seccion_envolvente and ts.id_tipo_seccion_envolvente_padr = ts.id_tipo_seccion_envolvente " +
						" WHERE m.id_material = ? ";



		try {
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, idMaterial);
			rs = pstmt.executeQuery();

			resp = new MaterialTO();

			if(rs.next()){

				resp.setIdMaterial(idMaterial);
				resp.setNombreMaterial(rs.getString(1));
				resp.setEspesor(rs.getFloat(2));

				//_nuevoMaterial.setImagenMaterial(rs.getString(4));
				resp.setConductividad(rs.getFloat(3));
				resp.setResistencia(rs.getFloat(4));
				resp.setTransmitancia(rs.getFloat(5));


				resp.setIdSeccionEdificio(rs.getInt(6));
				resp.setPrecioMaterial(rs.getFloat(7));

				resp.setNombreSeccionEdificio(rs.getString(8));
				resp.setUnidad_material(rs.getString(9));
				resp.setRutaFotografia(rs.getString(10));
				
				resp.setFotografia(obtenerFotografia(resp.getRutaFotografia()));
				
			}
				
			pstmt.close();
			rs.close();

		} catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt != null){
				try {
					if(rs != null)
						rs.close();
					pstmt.close();
				} catch (SQLException e2) {
					UtilLog.registrar(e2);
				}
			}
		}
		
		return resp;
	}
	
	@Override
	public boolean guardarMaterialSync(MaterialTO material) {
		int resp = -1;
		int idMaterial = -1;
		PreparedStatement pstmt = null;		 
		
		String query = " INSERT INTO  MATERIALES(id_material, " +
											"   id_tipo_seccion_envolvente, " +
											"   nombre_corto, " +
											"	nombre_material, " +
											"   espesor_material, " +
											"	imagen_material, " +
											"   conductividad_termica, " +
											"   transmitancia_termica, " +
											"   resistencia_termica , " +
											"	eliminado )" +
					   " VALUES (?,?,?,?,?,?,?,?,?,?);";
		try {
			pstmt = this.conn.prepareStatement(query);
				
			pstmt.setInt(1, material.getIdMaterial());
			pstmt.setInt(2, material.getIdSeccionEdificio());
			pstmt.setString(3, material.getNombreCorto());
			
			pstmt.setString(4, material.getNombreMaterial());
			pstmt.setFloat(5,material.getEspesor());
			pstmt.setString(6,material.getRutaFotografia());

			pstmt.setFloat(7, material.getConductividad());
			pstmt.setFloat(8, material.getTransmitancia());
			pstmt.setFloat(9, material.getResistencia());
			
			pstmt.setBoolean(10, material.getEliminado());
			
			
			
			resp = pstmt.executeUpdate();
			
			if(resp > 0){
				if(material.getFotografia()!=null)
					 guardarFotografia(material.getFotografia(),material.getRutaFotografia());
						
				idMaterial = material.getIdMaterial(); 
				resp = guardarPrecioMaterial(idMaterial, material.getPrecioMaterial(),material.getUnidad_material());
			}
			
			
			pstmt.close();
			
		} catch (Exception e) {
			UtilLog.registrar(e);
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException sqle) {
					UtilLog.registrar(sqle);
				}
			}
		}
		
		return resp > 0;
	}

	@Override
	public boolean existeMaterial(int idMaterial) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean existe = false;
		String query = " SELECT COUNT(id_material) FROM materiales WHERE id_material = ?;";
		
		try{
			pstmt = conn.prepareStatement(query);
			
			pstmt.setInt(1, idMaterial);			
			rs = pstmt.executeQuery();
		
			//rs = pstmt.getResultSet();
			if(rs.next()){
				existe = (rs.getInt(1) > 0) ? true : false;
			}
			
			pstmt.close();
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
			return true;
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
			return true;
		}
		
		return existe;
	}
}

package cl.foursoft.eee.dao.implementation.postgresql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cl.foursoft.eee.dao.implementation.DBBase;
import cl.foursoft.eee.dao.interfaces.IParametrosEdificio;
import cl.foursoft.eee.dao.transferObject.ParametrosEdificioTO;
import cl.foursoft.eee.util.UtilLog;

public class ParametrosEdificioDB extends DBBase implements IParametrosEdificio{

	
	public ParametrosEdificioDB(Connection c) {
		 this.conn = c;
	}

	@Override
	public List<ParametrosEdificioTO> obtenerParametros() {
		List<ParametrosEdificioTO> resp = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String query = " SELECT id_parametro, nombre_parametro, simbolo_parametro, unidad_parametro, valor_parametro " +
					   " FROM parametros_inicial_edificio ";
		try{
			pstmt = conn.prepareStatement(query);
			rs	= pstmt.executeQuery();
			 
			resp = new ArrayList<ParametrosEdificioTO>();
			
			while(rs.next()){
				ParametrosEdificioTO parametro = new ParametrosEdificioTO();
				
				parametro.setIdParametro(rs.getInt("id_parametro"));
				parametro.setValorParametro(rs.getFloat("valor_parametro"));
				parametro.setNombreParametro(rs.getString("nombre_parametro"));
				parametro.setSimboloParametro(rs.getString("simbolo_parametro"));
				parametro.setUnidadParametro(rs.getString("unidad_parametro"));
				resp.add(parametro);
			}
			
			rs.close();
			pstmt.close();

		}catch(Exception e){
			UtilLog.registrar(e);
		}
		finally{
			if( pstmt != null ){
				try{
					if(rs!=null)
						rs.close();
					pstmt.close();
				}catch(SQLException sqle){
					UtilLog.registrar(sqle);
				}
			}
		}

		return resp;
	}

	@Override
	public boolean editarParametro(ParametrosEdificioTO parametro) {
		PreparedStatement pstmt = null;
		String query = " UPDATE parametros_inicial_edificio SET valor_parametro = ? " +
					   " WHERE id_parametro = ?";
		
		try{
			pstmt = conn.prepareStatement(query);
			
			pstmt.setFloat(1, parametro.getValorParametro());
			pstmt.setInt(2, parametro.getIdParametro());
			
			
			int resp = pstmt.executeUpdate();
			
			pstmt.close();
			if(resp < 0) return false;
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
			return false;
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
			return false;
		}
		
		return true;
	}
	
	@Override
	public boolean editarParametroSync(ParametrosEdificioTO parametro) {
		PreparedStatement pstmt = null;
		String query = " UPDATE parametros_inicial_edificio " +
					   " SET nombre_parametro = ?, " +
					   "	 valor_parametro = ?, " +
					   "	 simbolo_parametro = ?, " +
					   "     unidad_parametro = ? " +
					   " WHERE id_parametro = ?";
		
		try{
			pstmt = conn.prepareStatement(query);
			
			pstmt.setString(1, parametro.getNombreParametro());
			pstmt.setFloat(2, parametro.getValorParametro());
			pstmt.setString(3, parametro.getSimboloParametro());
			pstmt.setString(4, parametro.getUnidadParametro());
			pstmt.setInt(5, parametro.getIdParametro());
			
			
			int resp = pstmt.executeUpdate();
			
			pstmt.close();
			if(resp < 0) return false;
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
			return false;
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
			return false;
		}
		
		return true;
	}
}

package cl.foursoft.eee.dao.implementation.postgresql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cl.foursoft.eee.dao.implementation.DBBase;
import cl.foursoft.eee.dao.interfaces.IUsuario;
import cl.foursoft.eee.dao.transferObject.ClienteTO;
import cl.foursoft.eee.dao.transferObject.UsuarioTO;
import cl.foursoft.eee.util.UtilLog;

public class UsuarioDB extends DBBase implements IUsuario{

	
	public UsuarioDB(Connection c){
		this.conn = c;
	}

	@Override
	public UsuarioTO obtenerUsuario(String usuario, String contrasenia) {
		UsuarioTO resp = null;
		PreparedStatement pstmt = null;
		String query = " SELECT u.id_usuario, tp.id_tipo_usuario, tp.tipo_usuario, u.nombre_completo, u.email "+
					   " FROM usuario u " +
					   " INNER JOIN tipo_usuario tp ON (u.id_tipo_usuario = tp.id_tipo_usuario) " +
					   " WHERE u.nombre_usuario = ? and u.contrasena = ?; ";
		
	 
		ResultSet rs = null; 
		
		try{
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1,usuario );
			pstmt.setString(2, contrasenia );
			
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				resp = new UsuarioTO();
				resp.setIdUsuario(rs.getInt(1));
				resp.setIdTipoUsuario(rs.getInt(2));				
				resp.setNombreTipoUsuario(rs.getString(3));
				resp.setNombreCompleto(rs.getString(4));
				resp.setEmail(rs.getString(5));
			}
			
 
			pstmt.close();
			rs.close();
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
		}
		
		return resp;
	}
	
	@Override
	public UsuarioTO obtenerCliente(UsuarioTO usuario) {
		PreparedStatement pstmt = null;
		String query = " SELECT id_cliente, nombre_cliente "+
					   " FROM cliente ";
		ResultSet rs = null; 
		
		try{
			pstmt = conn.prepareStatement(query);
			rs  = pstmt.executeQuery();
			
			if(rs.next()){
				usuario.setIdCliente(rs.getInt("id_cliente"));
				usuario.setNombreCliente(rs.getString("nombre_cliente"));
			}
			pstmt.close();
			rs.close();
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
		}
		
		return usuario;
	}

	@Override
	public UsuarioTO obtenerUsuarioKipus(String usuario, String contrasenia) {
		UsuarioTO resp = null;
		PreparedStatement pstmt = null;
		String query = " SELECT u.id_usuario, tp.id_tipo_usuario , tp.tipo_usuario, u.nombre_completo, u.email "+
					   " FROM usuario u " +
					   " INNER JOIN tipo_usuario tp ON (u.id_tipo_usuario = tp.id_tipo_usuario) "+
					   " WHERE u.nombre_usuario = ? AND u.contrasena = ? AND u.id_tipo_usuario = 3 ";
		ResultSet rs = null; 
		
		try{
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1,usuario );
			pstmt.setString(2, contrasenia );
			rs  = pstmt.executeQuery();
			
			if(rs.next()){
				resp = new UsuarioTO();
				resp.setIdUsuario(rs.getInt(1));
				resp.setIdTipoUsuario(rs.getInt(2));				
				resp.setNombreTipoUsuario(rs.getString(3));
				resp.setNombreCompleto(rs.getString(4));
				resp.setEmail(rs.getString(5));
			}
			pstmt.close();
			rs.close();
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
		}
		
		return resp;
	}

	@Override
	public List<UsuarioTO> obtenerUsuarios() {
		List<UsuarioTO> listaUsuarios = new ArrayList<UsuarioTO>();
		UsuarioTO usuario = null;
		PreparedStatement pstmt = null;
		String query = " SELECT u.id_usuario, tp.id_tipo_usuario, tp.tipo_usuario, u.telefono, u.nombre_completo, u.email, u.nombre_usuario, u.contrasena, u.telefono "+
					   " FROM usuario u " +
					   " INNER JOIN tipo_usuario tp ON (u.id_tipo_usuario = tp.id_tipo_usuario) ";
		ResultSet rs = null; 
		
		try{
			pstmt = conn.prepareStatement(query);
			rs  = pstmt.executeQuery();
			
			while(rs.next()){
				usuario = new UsuarioTO();
				usuario.setIdUsuario(rs.getInt("id_usuario"));
				usuario.setIdTipoUsuario(rs.getInt("id_tipo_usuario"));
				usuario.setNombreUsuario(rs.getString("nombre_usuario"));
				usuario.setContrasena(rs.getString("contrasena"));
				usuario.setNombreTipoUsuario(rs.getString("tipo_usuario"));
				usuario.setNombreCompleto(rs.getString("nombre_completo"));
				usuario.setEmail(rs.getString("email"));
				usuario.setTelefono(rs.getInt("telefono"));
				
				listaUsuarios.add(usuario);
			}
			pstmt.close();
			rs.close();
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
		}
		
		return listaUsuarios;
	}

	@Override
	public boolean guardarUsuario(UsuarioTO usuario) {
		PreparedStatement pstmt = null;
		String query = " INSERT INTO usuario(id_tipo_usuario, nombre_completo, nombre_usuario, contrasena, email, telefono) "+
					   " VALUES (?, ?, ?, ?, ?, ?) ";
		
		try{
			pstmt = conn.prepareStatement(query);
			
			pstmt.setInt(1, usuario.getIdTipoUsuario());
			pstmt.setString(2, usuario.getNombreCompleto());
			pstmt.setString(3, usuario.getNombreUsuario());
			pstmt.setString(4, usuario.getContrasena());
			pstmt.setString(5, usuario.getEmail());
			pstmt.setInt(6, usuario.getTelefono());
			
			int resp = pstmt.executeUpdate();
			
			pstmt.close();
			if(resp <= 0) return false;
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
			return false;
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
			return false;
		}
		
		return true;
	}

	@Override
	public boolean editarUsuario(UsuarioTO usuario) {
		PreparedStatement pstmt = null;
		String query = " UPDATE usuario SET nombre_completo = ?, nombre_usuario = ?, contrasena = ?, email = ?, telefono = ? " +
					   " WHERE id_usuario = ?";
		
		try{
			pstmt = conn.prepareStatement(query);
			
			pstmt.setString(1, usuario.getNombreCompleto());
			pstmt.setString(2, usuario.getNombreUsuario());
			pstmt.setString(3, usuario.getContrasena());
			pstmt.setString(4, usuario.getEmail());
			pstmt.setInt(5, usuario.getTelefono());
			pstmt.setInt(6, usuario.getIdUsuario());
			
			
			int resp = pstmt.executeUpdate();
			
			pstmt.close();
			if(resp < 0) return false;
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
			return false;
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
			return false;
		}
		
		return true;
	}
	public UsuarioTO obtenerUsuarioPorId(UsuarioTO user) {
		UsuarioTO resp = null;
		PreparedStatement pstmt = null;
		String query = " SELECT tp.id_tipo_usuario, tp.tipo_usuario, u.nombre_completo, u.email , u.nombre_usuario , u.contrasena "+
					   " FROM usuario u " +
					   " INNER JOIN tipo_usuario tp ON (u.id_tipo_usuario = tp.id_tipo_usuario) " +
					   " WHERE u.id_usuario = ?; ";
		ResultSet rs = null; 
		
		try{
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1,user.getIdUsuario());
			rs  = pstmt.executeQuery();
			
			if(rs.next()){
				resp = new UsuarioTO();
				resp.setIdUsuario(user.getIdUsuario());
				resp.setIdTipoUsuario(rs.getInt(1));				
				resp.setNombreTipoUsuario(rs.getString(2));
				resp.setNombreCompleto(rs.getString(3));
				resp.setEmail(rs.getString(4));
				resp.setNombreUsuario(rs.getString(5));
				resp.setContrasena(rs.getString(6));
				
			}
			pstmt.close();
			rs.close();
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
		}
		
		return resp;
	}
	@Override
	public boolean eliminarUsuario(UsuarioTO usuario) {
		PreparedStatement pstmt = null;
		String query = " DELETE FROM usuario WHERE id_usuario = ? ";
		
		try{
			pstmt = conn.prepareStatement(query);
			
			pstmt.setInt(1, usuario.getIdUsuario());
			
			int resp = pstmt.executeUpdate();
			
			pstmt.close();
			if(resp < 0) return false;
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
			return false;
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
			return false;
		}
		
		return true;
	}

	@Override
	public ClienteTO obtenerCliente() {
		ClienteTO resp = null;
		PreparedStatement pstmt = null;
		String query = " SELECT id_cliente, nombre_cliente "+
					   " FROM cliente ";
		ResultSet rs = null; 
		
		try{
			pstmt = conn.prepareStatement(query);
			rs  = pstmt.executeQuery();
			
			if(rs.next()){
				resp = new ClienteTO();
				resp.setIdCliente(rs.getInt("id_cliente"));
				resp.setNombreCliente(rs.getString("nombre_cliente"));
			}
			pstmt.close();
			rs.close();
		}
		catch (SQLException sqle) {
			UtilLog.registrar(sqle);
		}
		catch (Exception e) { 
			UtilLog.registrar(e);
		}
 
		return resp;
	}

	
}

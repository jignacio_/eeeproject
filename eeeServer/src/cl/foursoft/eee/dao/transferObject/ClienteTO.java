package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;
import java.util.List;

public class ClienteTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int idCliente;
	private String nombreCliente;
	private String direccionPrincipalCliente;
	private int numeroTelefono;
	
	private String rutaFotografia;
	private byte fotografiaCliente[];
	private int cantidadUsuatios;
	private List<UsuarioTO> usuariosClientes;
	/**
	 * @return the idCliente
	 */
	public int getIdCliente() {
		return idCliente;
	}
	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	/**
	 * @return the nombreCliente
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}
	/**
	 * @param nombreCliente the nombreCliente to set
	 */
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	/**
	 * @return the direccionPrincipalCliente
	 */
	public String getDireccionPrincipalCliente() {
		return direccionPrincipalCliente;
	}
	/**
	 * @param direccionPrincipalCliente the direccionPrincipalCliente to set
	 */
	public void setDireccionPrincipalCliente(String direccionPrincipalCliente) {
		this.direccionPrincipalCliente = direccionPrincipalCliente;
	}
	/**
	 * @return the numeroTelefono
	 */
	public int getNumeroTelefono() {
		return numeroTelefono;
	}
	/**
	 * @param numeroTelefono the numeroTelefono to set
	 */
	public void setNumeroTelefono(int numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}
	/**
	 * @return the rutaFotografia
	 */
	public String getRutaFotografia() {
		return rutaFotografia;
	}
	/**
	 * @param rutaFotografia the rutaFotografia to set
	 */
	public void setRutaFotografia(String rutaFotografia) {
		this.rutaFotografia = rutaFotografia;
	}
	/**
	 * @return the fotografiaCliente
	 */
	public byte[] getFotografiaCliente() {
		return fotografiaCliente;
	}
	/**
	 * @param fotografiaCliente the fotografiaCliente to set
	 */
	public void setFotografiaCliente(byte[] fotografiaCliente) {
		this.fotografiaCliente = fotografiaCliente;
	}
	/**
	 * @return the cantidadUsuatios
	 */
	public int getCantidadUsuatios() {
		return cantidadUsuatios;
	}
	/**
	 * @param cantidadUsuatios the cantidadUsuatios to set
	 */
	public void setCantidadUsuatios(int cantidadUsuatios) {
		this.cantidadUsuatios = cantidadUsuatios;
	}
	/**
	 * @return the usuariosClientes
	 */
	public List<UsuarioTO> getUsuariosClientes() {
		return usuariosClientes;
	}
	/**
	 * @param usuariosClientes the usuariosClientes to set
	 */
	public void setUsuariosClientes(List<UsuarioTO> usuariosClientes) {
		this.usuariosClientes = usuariosClientes;
	}
	
	
	
	
	

}

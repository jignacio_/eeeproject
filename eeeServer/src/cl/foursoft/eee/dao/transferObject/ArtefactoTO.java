package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;

public class ArtefactoTO implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int idArtefacto;
	private String nombreArtefacto ;
	private String tipoMedificionArtefacto;
	private double consumoArtefacto ;
	private String unidadMedidaArtefacto;
	private int cantidadArtefacto;
	private double costo_unidad_artefacto; 
	private float eficiencia_artefacto;
	private boolean eliminado;
	
	private byte fotografia[];
	private String rutaFotografia;
	
	
	/**
	 * @return the fotografia
	 */
	public byte[] getFotografia() {
		return fotografia;
	}

	/**
	 * @param fotografia the fotografia to set
	 */
	public void setFotografia(byte[] fotografia) {
		this.fotografia = fotografia;
	}

	/**
	 * @return the rutaFotografia
	 */
	public String getRutaFotografia() {
		return this.idArtefacto+".jpg";// rutaFotografia;
	}

	/**
	 * @param rutaFotografia the rutaFotografia to set
	 */
	public void setRutaFotografia(String rutaFotografia) {
		this.rutaFotografia = rutaFotografia;
	}

	public ArtefactoTO() {
		super();
		// TODO Auto-generated constructor stub
		this.cantidadArtefacto = 1;
		rutaFotografia="";
	}

	/**
	 * @return the nombreArtefacto
	 */
	public String getNombreArtefacto() {
		return nombreArtefacto;
	}

	/**
	 * @param nombreArtefacto the nombreArtefacto to set
	 */
	public void setNombreArtefacto(String nombreArtefacto) {
		this.nombreArtefacto = nombreArtefacto;
	}

	/**
	 * @return the tipoMedificionArtefacto
	 */
	public String getTipoMedificionArtefacto() {
		return tipoMedificionArtefacto;
	}

	/**
	 * @param tipoMedificionArtefacto the tipoMedificionArtefacto to set
	 */
	public void setTipoMedificionArtefacto(String tipoMedificionArtefacto) {
		this.tipoMedificionArtefacto = tipoMedificionArtefacto;
	}

	/**
	 * @return the consumoArtefacto
	 */
	public double getConsumoArtefacto() {
		return consumoArtefacto;
	}

	/**
	 * @param consumoArtefacto the consumoArtefacto to set
	 */
	public void setConsumoArtefacto(double consumoArtefacto) {
		this.consumoArtefacto = consumoArtefacto;
	}

	public void setUnidadMedidaArtefacto(String unidadMedidaArtefacto) {
		this.unidadMedidaArtefacto = unidadMedidaArtefacto;
	}

	public String getUnidadMedidaArtefacto() {
		return unidadMedidaArtefacto;
	}

	public void setCantidadArtefacto(int cantidadArtefacto) {
		this.cantidadArtefacto = cantidadArtefacto;
	}

	public int getCantidadArtefacto() {
		return cantidadArtefacto;
	}

	public void setCosto_unidad_artefacto(double costo_unidad_artefacto) {
		this.costo_unidad_artefacto = costo_unidad_artefacto;
	}

	public double getCosto_unidad_artefacto() {
		return costo_unidad_artefacto;
	}
	public double getCostoTotalArtefactos(){
		return this.costo_unidad_artefacto * this.cantidadArtefacto;
	}
	
	public void setEficiencia_artefacto(float eficiencia_artefacto) {
		this.eficiencia_artefacto = eficiencia_artefacto;
	}

	public float getEficiencia_artefacto() {
		return eficiencia_artefacto;
	}

	public void setIdArtefacto(int idArtefacto) {
		this.idArtefacto = idArtefacto;
	}

	public int getIdArtefacto() {
		return idArtefacto;
	}

	public void setEliminado(boolean eliminado) {
		this.eliminado = eliminado;
	}

	public boolean getEliminado() {
		return eliminado;
	}
	
	
	

}

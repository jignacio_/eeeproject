package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;

public class ParametrosEvaluacionTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// Datos Propios
 	private int idEvaluacion ;
 	private int idCliente;
 	private String nombreEvaluacion ;
 
 	//Variables
	private int horizonteEvaluacion ;
	private Float tarifaCombustible ;
 	private Float eficienciaGlobal ;
 	private Float tasaDescuento ;
 	private Float variacionPrecio ;
	private Float tarifaFinal ;
	
	
	public ParametrosEvaluacionTO() {
		super(); 
	}


	public int getIdEvaluacion() {
		return idEvaluacion;
	}


	public void setIdEvaluacion(int idEvaluacion) {
		this.idEvaluacion = idEvaluacion;
	}


	public String getNombreEvaluacion() {
		return nombreEvaluacion;
	}


	public void setNombreEvaluacion(String nombreEvaluacion) {
		this.nombreEvaluacion = nombreEvaluacion;
	}


	public int getHorizonteEvaluacion() {
		return horizonteEvaluacion;
	}


	public void setHorizonteEvaluacion(int horizonteEvaluacion) {
		this.horizonteEvaluacion = horizonteEvaluacion;
	}


	public Float getTarifaCombustible() {
		return tarifaCombustible;
	}


	public void setTarifaCombustible(Float tarifaCombustible) {
		this.tarifaCombustible = tarifaCombustible;
	}


	public Float getEficienciaGlobal() {
		return eficienciaGlobal;
	}


	public void setEficienciaGlobal(Float eficienciaGlobal) {
		this.eficienciaGlobal = eficienciaGlobal;
	}


	public Float getTasaDescuento() {
		return tasaDescuento;
	}


	public void setTasaDescuento(Float tasaDescuento) {
		this.tasaDescuento = tasaDescuento;
	}


	public Float getVariacionPrecio() {
		return variacionPrecio;
	}


	public void setVariacionPrecio(Float variacionPrecio) {
		this.variacionPrecio = variacionPrecio;
	}


	public Float getTarifaFinal() {
		return tarifaFinal;
	}


	public void setTarifaFinal(Float tarifaFinal) {
		this.tarifaFinal = tarifaFinal;
	}


	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}


	public int getIdCliente() {
		return idCliente;
	}
	 
	
	
	 
	 
}

package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;
import java.util.List;

public class PisoTO implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String prefijo;
	private int numeroPisoActual  ; 
	private int idPiso;
	private String nombrePiso;
	private int numeroSensores;
	private String descripcionPiso;
	private List<SensorTO> arrCollSensoresPiso;
	private List<ZonaTO> arrCollZonasPiso ;
	private boolean guardado;
	
	public PisoTO() {
		super();
		guardado = false;
		// TODO Auto-generated constructor stub
	}
	public String getPrefijo() {
		return prefijo;
	}
	public void setPrefijo(String prefijo) {
		this.prefijo = prefijo;
	}
	public int getNumeroPisoActual() {
		return numeroPisoActual;
	}
	public void setNumeroPisoActual(int numeroPisoActual) {
		this.numeroPisoActual = numeroPisoActual;
	}
	public int getIdPiso() {
		return idPiso;
	}
	public void setIdPiso(int idPiso) {
		this.idPiso = idPiso;
	}
	public String getNombrePiso() {
		return nombrePiso;
	}
	public void setNombrePiso(String nombrePiso) {
		this.nombrePiso = nombrePiso;
	}
	public int getNumeroSensores() {
		return numeroSensores;
	}
	public void setNumeroSensores(int numeroSensores) {
		this.numeroSensores = numeroSensores;
	}
	public String getDescripcionPiso() {
		return descripcionPiso;
	}
	public void setDescripcionPiso(String descripcionPiso) {
		this.descripcionPiso = descripcionPiso;
	}
	public List<SensorTO> getArrCollSensoresPiso() {
		return arrCollSensoresPiso;
	}
	public void setArrCollSensoresPiso(List<SensorTO> arrCollSensoresPiso) {
		this.arrCollSensoresPiso = arrCollSensoresPiso;
	}
	public List<ZonaTO> getArrCollZonasPiso() {
		return arrCollZonasPiso;
	}
	public void setArrCollZonasPiso(List<ZonaTO> arrCollZonasPiso) {
		this.arrCollZonasPiso = arrCollZonasPiso;
	}
	public void setGuardado(boolean guardado) {
		this.guardado = guardado;
	}
	public boolean getGuardado() {
		return guardado;
	}
	
	
	
	
	
}

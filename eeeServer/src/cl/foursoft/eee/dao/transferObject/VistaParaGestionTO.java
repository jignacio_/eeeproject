package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;
import java.sql.Timestamp;

public class VistaParaGestionTO implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Timestamp fechaInicio ;
	private Timestamp fechaFin ;
	private String codigoSensor;
	
	// Puede ser 1 o m�s.- podr�a ser un array
	
	
	private String idTipoMedicion ;
	private String idConsumo ; 
	
	private String edificio ;
	private String pisos ;
	private String zonas ;
	private String numeroSensor ;
	
	public VistaParaGestionTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the fechaInicio
	 */
	public Timestamp getFechaInicio() {
		return fechaInicio;
	}

	/**
	 * @param fechaInicio the fechaInicio to set
	 */
	public void setFechaInicio(Timestamp fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	/**
	 * @return the fechaFin
	 */
	public Timestamp getFechaFin() {
		return fechaFin;
	}

	/**
	 * @param fechaFin the fechaFin to set
	 */
	public void setFechaFin(Timestamp fechaFin) {
		this.fechaFin = fechaFin;
	}

	/**
	 * @return the idTipoMedicion
	 */
	public String getIdTipoMedicion() {
		return idTipoMedicion;
	}

	/**
	 * @param idTipoMedicion the idTipoMedicion to set
	 */
	public void setIdTipoMedicion(String idTipoMedicion) {
		this.idTipoMedicion = idTipoMedicion;
	}

	/**
	 * @return the idConsumo
	 */
	public String getIdConsumo() {
		return idConsumo;
	}

	/**
	 * @param idConsumo the idConsumo to set
	 */
	public void setIdConsumo(String idConsumo) {
		this.idConsumo = idConsumo;
	}

	/**
	 * @return the edificio
	 */
	public String getEdificio() {
		return edificio;
	}

	/**
	 * @param edificio the edificio to set
	 */
	public void setEdificio(String edificio) {
		this.edificio = edificio;
	}

	/**
	 * @return the pisos
	 */
	public String getPisos() {
		return pisos;
	}

	/**
	 * @param pisos the pisos to set
	 */
	public void setPisos(String pisos) {
		this.pisos = pisos;
	}

	/**
	 * @return the zonas
	 */
	public String getZonas() {
		return zonas;
	}

	/**
	 * @param zonas the zonas to set
	 */
	public void setZonas(String zonas) {
		this.zonas = zonas;
	}

	/**
	 * @return the sensores
	 */
	public String getNumeroSensor() {
		return numeroSensor;
	}

	/**
	 * @param sensores the sensores to set
	 */
	public void setNumeroSensor(String numeroSensor) {
		this.numeroSensor = numeroSensor;
	}

	public void setCodigoSensor(String codigoSensor) {
		this.codigoSensor = codigoSensor;
	}

	public String getCodigoSensor() {
		return codigoSensor;
	}
	
	

}

package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;

public class MaterialTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	
	private int idMaterial;
	private String nombreMaterial;
	private String nombreCorto;
	private float espesor;
	private float conductividad;
	private float resistencia;
	private float transmitancia;
	private float precioMaterial;
	private int idSeccionEdificio;
	private String nombreSeccionEdificio;
	private byte fotografia[];
	private String rutaFotografia;
	
	private float cantidad_material ;
	private String unidad_material ;
	
	private boolean eliminado; 
	
	
	
	/**
	 * @return the eliminado
	 */
	public boolean getEliminado() {
		return eliminado;
	}
	/**
	 * @param eliminado the eliminado to set
	 */
	public void setEliminado(boolean eliminado) {
		this.eliminado = eliminado;
	}
	public MaterialTO() {
		super();
		// TODO Auto-generated constructor stub
		cantidad_material = 1;
		rutaFotografia="";
	}
	public String getNombreMaterial() {
		return nombreMaterial;
	}
	public void setNombreMaterial(String nombreMaterial) {
		this.nombreMaterial = nombreMaterial;
	}
	public String getNombreCorto() {
		return nombreCorto;
	}
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}
	public Float getEspesor() {
		return espesor;
	}
	public void setEspesor(Float espesor) {
		this.espesor = espesor;
	}
	public Float getConductividad() {
		return conductividad;
	}
	public void setConductividad(Float conductividad) {
		this.conductividad = conductividad;
	}
	public Float getResistencia() {
		return resistencia;
	}
	public void setResistencia(Float resistencia) {
		this.resistencia = resistencia;
	}
	public Float getTransmitancia() {
		return transmitancia;
	}
	public void setTransmitancia(Float transmitancia) {
		this.transmitancia = transmitancia;
	}
	public void setIdMaterial(int idMaterial) {
		this.idMaterial = idMaterial;
	}
	public int getIdMaterial() {
		return idMaterial;
	}
	public float getPrecioMaterial() {
		return precioMaterial;
	}
	public void setPrecioMaterial(float precioMaterial) {
		this.precioMaterial = precioMaterial;
	}
	public void setIdSeccionEdificio(int idSeccionEdificio) {
		this.idSeccionEdificio = idSeccionEdificio;
	}
	public int getIdSeccionEdificio() {
		return idSeccionEdificio;
	}
	public void setFotografia(byte fotografia[]) {
		this.fotografia = fotografia;
	}
	public byte[] getFotografia() {
		return fotografia;
	}
	public void setRutaFotografia(String rutaFotografia) {
		this.rutaFotografia = rutaFotografia;
	}
	public String getRutaFotografia() {
		return rutaFotografia;
	}
	public void setNombreSeccionEdificio(String nombreSeccionEdificio) {
		this.nombreSeccionEdificio = nombreSeccionEdificio;
	}
	public String getNombreSeccionEdificio() {
		return nombreSeccionEdificio;
	}
	public void setCantidad_material(float cantidad_material) {
		this.cantidad_material = cantidad_material;
	}
	public float getCantidad_material() {
		return cantidad_material;
	}
	public void setUnidad_material(String unidad_material) {
		this.unidad_material = unidad_material;
	}
	public String getUnidad_material() {
		return unidad_material;
	}
	
	public float getCostoTotalMaterial(){
		return (this.precioMaterial * this.cantidad_material);
	}
	//Funciones UTIL
	public float costoTotalMaterial(){
		return (this.cantidad_material * this.precioMaterial);
	}
	
	
}

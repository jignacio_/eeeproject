package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;

public class RequerimientoEnergeticoTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int idEdificio;
	private int idAlternativa;
	
	private float perdidasPorTransmision;
	private float perdidasPorVentilacion;
	private float gananciaTermicaSolInvierno;
	private float gananciaTermicaSolVerano;
	private float gananciaTermicaSol;
	private float gananciaInterna;
	
	private float requerimientoCalorTotal;
	private float requerimientoFrioTotal;
	private float requerimientoEnergeticoTotal;
	
	
	public RequerimientoEnergeticoTO() {

			this.idAlternativa = 0; //valor 0 para la situaci�n actual
			this.idEdificio = -1; //valor 0 para la situaci�n actual
	
	}
	public int getIdEdificio() {
		return idEdificio;
	}
	public void setIdEdificio(int idEdificio) {
		this.idEdificio = idEdificio;
	}
	public int getIdAlternativa() {
		return idAlternativa;
	}
	public void setIdAlternativa(int idAlternativa) {
		this.idAlternativa = idAlternativa;
	}
	public float getPerdidasPorTransmision() {
		return perdidasPorTransmision;
	}
	public void setPerdidasPorTransmision(float perdidasPorTransmisionTotal) {
		this.perdidasPorTransmision = perdidasPorTransmisionTotal;
	}
	public float getPerdidasPorVentilacion() {
		return perdidasPorVentilacion;
	}
	public void setPerdidasPorVentilacion(float perdidasPorVentilacion) {
		this.perdidasPorVentilacion = perdidasPorVentilacion;
	}
 
	public float getGananciaTermicaSol() {
		return gananciaTermicaSol;
	}
	public void setGananciaTermicaSol(float gananciaTermicaSol) {
		this.gananciaTermicaSol = gananciaTermicaSol;
	}
	public float getGananciaTermicaSolInvierno() {
		return gananciaTermicaSolInvierno;
	}
	public void setGananciaTermicaSolInvierno(float gananciaTermicaSolInviernoTotal) {
		this.gananciaTermicaSolInvierno = gananciaTermicaSolInviernoTotal;
	}
	public float getGananciaTermicaSolVerano() {
		return gananciaTermicaSolVerano;
	}
	public void setGananciaTermicaSolVerano(float gananciaTermicaSolVeranoTotal) {
		this.gananciaTermicaSolVerano = gananciaTermicaSolVeranoTotal;
	}
	public float getGananciaInterna() {
		return gananciaInterna;
	}
	public void setGananciaInterna(float gananciaInterna) {
		this.gananciaInterna = gananciaInterna;
	}
	/**
	 * @return the requerimientoCalorTotal
	 */
	public float getRequerimientoCalorTotal() {
		return requerimientoCalorTotal;
	}
	/**
	 * @param requerimientoCalorTotal the requerimientoCalorTotal to set
	 */
	public void setRequerimientoCalorTotal(float requerimientoCalorTotal) {
		this.requerimientoCalorTotal = requerimientoCalorTotal;
	}
	/**
	 * @return the requerimientoFrioTotal
	 */
	public float getRequerimientoFrioTotal() {
		return requerimientoFrioTotal;
	}
	/**
	 * @param requerimientoFrioTotal the requerimientoFrioTotal to set
	 */
	public void setRequerimientoFrioTotal(float requerimientoFrioTotal) {
		this.requerimientoFrioTotal = requerimientoFrioTotal;
	}
	/**
	 * @return the requerimientoEnergeticoTotal
	 */
	public float getRequerimientoEnergeticoTotal() {
		return requerimientoEnergeticoTotal;
	}
	/**
	 * @param requerimientoEnergeticoTotal the requerimientoEnergeticoTotal to set
	 */
	public void setRequerimientoEnergeticoTotal(float requerimientoEnergeticoTotal) {
		this.requerimientoEnergeticoTotal = requerimientoEnergeticoTotal;
	}
	
	
	

}

package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;
import java.util.List;

public class ZonaTO implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idZona ;
	private String prefijo;
	private String nombreZona ;
	private int numeroZonaActual;
	private int numeroSensores ;
	private String descripcionZona ;
	private List<SensorTO> arrCollSensoresZona ;
	private int pisoPadreId  ;
	private int idPiso ;
	private boolean guardado;
	
	
	public ZonaTO() {
		super();
		this.guardado = false;
		// TODO Auto-generated constructor stub
	}
	public int getIdZona() {
		return idZona;
	}
	public void setIdZona(int idZona) {
		this.idZona = idZona;
	}
	public String getPrefijo() {
		return prefijo;
	}
	public void setPrefijo(String prefijo) {
		this.prefijo = prefijo;
	}
	public String getNombreZona() {
		return nombreZona;
	}
	public void setNombreZona(String nombreZona) {
		this.nombreZona = nombreZona;
	}
	public int getNumeroZonaActual() {
		return numeroZonaActual;
	}
	public void setNumeroZonaActual(int numeroZonaActual) {
		this.numeroZonaActual = numeroZonaActual;
	}
	public int getNumeroSensores() {
		return numeroSensores;
	}
	public void setNumeroSensores(int numeroSensores) {
		this.numeroSensores = numeroSensores;
	}
	public String getDescripcionZona() {
		return descripcionZona;
	}
	public void setDescripcionZona(String descripcionZona) {
		this.descripcionZona = descripcionZona;
	}
	public List<SensorTO> getArrCollSensoresZona() {
		return arrCollSensoresZona;
	}
	public void setArrCollSensoresZona(List<SensorTO> arrCollSensoresZona) {
		this.arrCollSensoresZona = arrCollSensoresZona;
	}
	public int getPisoPadreId() {
		return pisoPadreId;
	}
	public void setPisoPadreId(int pisoPadreId) {
		this.pisoPadreId = pisoPadreId;
	}
	public int getIdPiso() {
		return idPiso;
	}
	public void setIdPiso(int pisoPadreNumeroActual) {
		this.idPiso = pisoPadreNumeroActual;
	}
	public void setGuardado(boolean guardado) {
		this.guardado = guardado;
	}
	public boolean getGuardado() {
		return guardado;
	}
	
	
	


}

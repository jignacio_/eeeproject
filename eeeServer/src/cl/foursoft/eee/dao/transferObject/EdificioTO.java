package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;
import java.util.List;

public class EdificioTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int idEdificio;
	private int codigoEdificio;
	private int idCliente;
	private String nombreEdificio;
	private String direccionCompletaEdificio;
	private Double latitud;
	private Double longitud;
	private String observaciones;
	
	private String direccionEdificio;
	private String ciudadEdificio;
	private String regionEdificio;
	private String paisEdificio;
	
	private Float altoPiso;
	private int numeroPIsos;
	private Float largo;
	private Float ancho;
	private Float perimetro;
	

	private Float areaTotal;
	private Float superficieBruta;
	private Float superficieUtil;
	private Float volumenDeAire; 
	private Float localizacion;
	
	private int numeroEdificio;
	
	private byte fotografia[];
	private String rutaFotografia;
	private String nombreArchivo ;
	
 
	
	private int cantidadAlternativasEnvolvente;
	private int cantidadAlternativasArtefactos;
	private Float _areaTotalEnvolvente;
	
	
	private String fechaCreacion;
	
	private List<SensorTO> sensoresEliminados;
	
	////Seonsores
	private List<SensorTO> arrCollSensoresEdificio;
	
	
	////DATOS Y DIMENSIONES DE ENVOLVENTE _ ventanas, puertas, cupula, escalera ._ <- incluye sus composiciones   
	private List<SeccionTO>  arrCollSecciones;
	
	private List<PisoTO> arrCollPisosEdificio;
	
	private List<ParametrosEdificioTO> arrCollParametrosEdificio;
	
//	private FactorRenovacionTO _factorDeRenovacion;
	private int idFactorRenovacion;
	private float valorFactorRenovacion;
	private boolean aireAcondicionado; 
	
	private double medicionSensor;
	private double medicionCombustible;
	private double medicionElectricidad;
	private double medicionAgua; 
	
	private double medicionCombustibleHoy;
	private double medicionElectricidadHoy;
	private double medicionAguaHoy; 
	
	public EdificioTO() {
		super();
		
		medicionAgua = 0;
		medicionElectricidad = 0;
		medicionCombustible = 0;
		medicionSensor = 0;
		
		cantidadAlternativasEnvolvente = 0;
		cantidadAlternativasArtefactos = 0;
		  
	}
	

	public int getIdEdificio() {
		
	
		return (idEdificio);
	}

	public void setIdEdificio(int idEdificio) {
		this.idEdificio = idEdificio;
	}

	public String getNombreEdificio() {
		return nombreEdificio;
	}

	public void setNombreEdificio(String nombreEdificio) {
		this.nombreEdificio = nombreEdificio;
	}

	public String getDireccionCompletaEdificio() {
		return direccionCompletaEdificio;
	}

	public void setDireccionCompletaEdificio(String direccionCompletaEdificio) {
		this.direccionCompletaEdificio = direccionCompletaEdificio;
	}

	public Double getLatitud() {
		return latitud;
	}

	public void setLatitud(Double latitud) {
		this.latitud = latitud;
	}

	public Double getLongitud() {
		return longitud;
	}

	public void setLongitud(Double longitud) {
		this.longitud = longitud;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Float getAltoPiso() {
		return altoPiso;
	}

	public void setAltoPiso(Float alto) {
		this.altoPiso = alto;
	}

	public Float getLargo() {
		return largo;
	}

	public void setLargo(Float largo) {
		this.largo = largo;
	}

	public Float getAncho() {
		return ancho;
	}

	public void setAncho(Float ancho) {
		this.ancho = ancho;
	}

	public Float getPerimetro() {
		return perimetro;
	}

	public void setPerimetro(Float perimetro) {
		this.perimetro = perimetro;
	}

	public Float getSuperficieBruta() {
		return superficieBruta;
	}

	public void setSuperficieBruta(Float superficie) {
		this.superficieBruta  = superficie;
	}
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	public int getIdCliente() {
		return idCliente;
	}
	public String getDireccionEdificio() {
		return direccionEdificio;
	}
	public void setDireccionEdificio(String direccionEdificio) {
		this.direccionEdificio = direccionEdificio;
	}
	public String getCiudadEdificio() {
		return ciudadEdificio;
	}
	public void setCiudadEdificio(String ciudadEdificio) {
		this.ciudadEdificio = ciudadEdificio;
	}
	public String getRegionEdificio() {
		return regionEdificio;
	}
	public void setRegionEdificio(String regionEdificio) {
		this.regionEdificio = regionEdificio;
	}
	public String getPaisEdificio() {
		return paisEdificio;
	}
	public void setPaisEdificio(String paisEdificio) {
		this.paisEdificio = paisEdificio;
	}
	public Float getSuperficieUtil() {
		return superficieUtil;
	}
	public void setSuperficieUtil(Float superficieUtil) {
		this.superficieUtil = superficieUtil;
	}
	public Float getVolumenDeAire() {
		return volumenDeAire;
	}
	public void setVolumenDeAire(Float volumenDeAire) {
		this.volumenDeAire = volumenDeAire;
	}
	public List<SeccionTO> getArrCollSecciones() {
		return arrCollSecciones;
	}
	public void setArrCollSecciones(List<SeccionTO> arrCollSecciones) {
		this.arrCollSecciones = arrCollSecciones;
	}
	public void setArrCollSensoresEdificio(List<SensorTO> arrCollSensoresEdificio) {
		this.arrCollSensoresEdificio = arrCollSensoresEdificio;
	}
	public List<SensorTO> getArrCollSensoresEdificio() {
		return arrCollSensoresEdificio;
	}

	public void setLocalizacion(Float localizacion) {
		this.localizacion = localizacion;
	}
	public Float getLocalizacion() {
		return localizacion;
	}
	
	public void setRutaFotografia(String rutaFotografia) {
		this.rutaFotografia = rutaFotografia;
	}
	public String getRutaFotografia() {
		return rutaFotografia;
	}
	public void setNombreArchivo(String _nombreArchivo) {
		this.nombreArchivo = _nombreArchivo;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setFotografia(byte fotografia[]) {
		this.fotografia = fotografia;
	}
	public byte[] getFotografia() {
		return fotografia;
	}
	public void setNumeroPIsos(int numeroPIsos) {
		this.numeroPIsos = numeroPIsos;
	}
	public int getNumeroPIsos() {
		return numeroPIsos;
	}


	public void setAreaTotal(Float areaTotal) {
		this.areaTotal = areaTotal;
	}


	public Float getAreaTotal() {
		return areaTotal;
	}


 


	public Float get_areaTotalEnvolvente() {
		return _areaTotalEnvolvente;
	}


	public void set_areaTotalEnvolvente(Float _areaTotalEnvolvente) {
		this._areaTotalEnvolvente = _areaTotalEnvolvente;
	}


	public void setArrCollPisosEdificio(List<PisoTO> arrCollPisosEdificio) {
		this.arrCollPisosEdificio = arrCollPisosEdificio;
	}


	public List<PisoTO> getArrCollPisosEdificio() {
		return arrCollPisosEdificio;
	}


	public int getNumeroEdificio() {
		return numeroEdificio;
	}


	public void setNumeroEdificio(int numeroEdificio) {
		this.numeroEdificio = numeroEdificio;
	}


	public void setCodigoEdificio(int codigoEdificio) {
		this.codigoEdificio = codigoEdificio;
	}


	public int getCodigoEdificio() {
		return codigoEdificio;
	}


	public void setArrCollParametrosEdificio(
			List<ParametrosEdificioTO> arrCollParametrosEdificio) {
		this.arrCollParametrosEdificio = arrCollParametrosEdificio;
	}


	public List<ParametrosEdificioTO> getArrCollParametrosEdificio() {
		return arrCollParametrosEdificio;
	}


	public void setIdFactorRenovacion(int idFactorRenovacion) {
		this.idFactorRenovacion = idFactorRenovacion;
	}


	public int getIdFactorRenovacion() {
		return idFactorRenovacion;
	}


	public void setAireAcondicionado(boolean aireAcondicionado) {
		this.aireAcondicionado = aireAcondicionado;
	}


	public boolean getAireAcondicionado() {
		return aireAcondicionado;
	}

	//Funciones UTIL
	public float costoEdificio(){
		float resp = 0f;
		
		for (SeccionTO s : this.arrCollSecciones){
			resp += s.obtenerCostoTotalSeccion();
		}
		
		return resp;
	}


	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}


	public String getFechaCreacion() {
		return fechaCreacion;
	}


	public void setValorFactorRenovacion(float valorFactorRenovacion) {
		this.valorFactorRenovacion = valorFactorRenovacion;
	}


	public float getValorFactorRenovacion() {
		return valorFactorRenovacion;
	}


	public void setSensoresEliminados(List<SensorTO> sensoresEliminados) {
		this.sensoresEliminados = sensoresEliminados;
	}


	public List<SensorTO> getSensoresEliminados() {
		return sensoresEliminados;
	}


	/**
	 * @return the medicionCombustible
	 */
	public double getMedicionCombustible() {
		return medicionCombustible;
	}


	/**
	 * @param medicionCombustible the medicionCombustible to set
	 */
	public void setMedicionCombustible(double medicionCombustible) {
		this.medicionCombustible = medicionCombustible;
	}


	/**
	 * @return the medicionSensor
	 */
	public double getMedicionSensor() {
		return medicionSensor;
	}


	/**
	 * @param medicionSensor the medicionSensor to set
	 */
	public void setMedicionSensor(double medicionSensor) {
		this.medicionSensor = medicionSensor;
	}


	/**
	 * @return the medicionElectricidad
	 */
	public double getMedicionElectricidad() {
		return medicionElectricidad;
	}


	/**
	 * @param medicionElectricidad the medicionElectricidad to set
	 */
	public void setMedicionElectricidad(double medicionElectricidad) {
		this.medicionElectricidad = medicionElectricidad;
	}


	/**
	 * @return the medicionAgua
	 */
	public double getMedicionAgua() {
		return medicionAgua;
	}


	/**
	 * @param medicionAgua the medicionAgua to set
	 */
	public void setMedicionAgua(double medicionAgua) {
		this.medicionAgua = medicionAgua;
	}


	/**
	 * @return the medicionCombustibleHoy
	 */
	public double getMedicionCombustibleHoy() {
		return medicionCombustibleHoy;
	}


	/**
	 * @param medicionCombustibleHoy the medicionCombustibleHoy to set
	 */
	public void setMedicionCombustibleHoy(double medicionCombustibleHoy) {
		this.medicionCombustibleHoy = medicionCombustibleHoy;
	}


	/**
	 * @return the medicionElectricidadHoy
	 */
	public double getMedicionElectricidadHoy() {
		return medicionElectricidadHoy;
	}


	/**
	 * @param medicionElectricidadHoy the medicionElectricidadHoy to set
	 */
	public void setMedicionElectricidadHoy(double medicionElectricidadHoy) {
		this.medicionElectricidadHoy = medicionElectricidadHoy;
	}


	/**
	 * @return the medicionAguaHoy
	 */
	public double getMedicionAguaHoy() {
		return medicionAguaHoy;
	}


	/**
	 * @param medicionAguaHoy the medicionAguaHoy to set
	 */
	public void setMedicionAguaHoy(double medicionAguaHoy) {
		this.medicionAguaHoy = medicionAguaHoy;
	}


	public void setCantidadAlternativasEnvolvente(
			int cantidadAlternativasEnvolvente) {
		this.cantidadAlternativasEnvolvente = cantidadAlternativasEnvolvente;
	}


	public int getCantidadAlternativasEnvolvente() {
		return cantidadAlternativasEnvolvente;
	}


	public void setCantidadAlternativasArtefactos(
			int cantidadAlternativasArtefactos) {
		this.cantidadAlternativasArtefactos = cantidadAlternativasArtefactos;
	}


	public int getCantidadAlternativasArtefactos() {
		return cantidadAlternativasArtefactos;
	}
 
	
	
	
}

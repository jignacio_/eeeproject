package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;

public class FactorRenovacionTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idFactor ;
	private String nombreFactor;
	private float cambioAireMinimo ;
	private float cambioAireMaximo ;
	private String discriminador ; //Unidad{camnbiar} 
	
	
	
	public FactorRenovacionTO() {
		super();
		this.idFactor = -1;
		this.nombreFactor = "";
		this.cambioAireMinimo = -1;
		this.cambioAireMaximo = -1;
		this.discriminador = "";
	}
	
	public int getIdFactor() {
		return idFactor;
	}
	public void setIdFactor(int idFactor) {
		this.idFactor = idFactor;
	}
	public String getNombreFactor() {
		return nombreFactor;
	}
	public void setNombreFactor(String nombreFactor) {
		this.nombreFactor = nombreFactor;
	}
	public float getCambioAireMinimo() {
		return cambioAireMinimo;
	}
	public void setCambioAireMinimo(float cambioAireMinimo) {
		this.cambioAireMinimo = cambioAireMinimo;
	}
	public float getCambioAireMaximo() {
		return cambioAireMaximo;
	}
	public void setCambioAireMaximo(float cambioAireMaximo) {
		this.cambioAireMaximo = cambioAireMaximo;
	}
	public String getDiscriminador() {
		return discriminador;
	}
	public void setDiscriminador(String discriminador) {
		this.discriminador = discriminador;
	}

	
	
	
}

package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;

public class EvaluacionEconomicaTO implements Serializable{
 

	private static final long serialVersionUID = 1L;
	
	private double valorVan;
	private double valorTir;
	private int valorPayBack;
	
	private int idAlternativa;
	private int idParametro;
	
	private String fecha_creacion;
	private String fecha_ultima_evaluacion;
	private String fecha_siguiente_evaluacion;
	
	private String descripcionAlternativa;
	
	
	private double flujos[];
	private double flujosAcumulados[];
	private double valores[];
	/**
	 * @return the valorVan
	 */
	public double getValorVan() {
		return valorVan;
	}
	/**
	 * @param valorVan the valorVan to set
	 */
	public void setValorVan(double valorVan) {
		this.valorVan = valorVan;
	}
	/**
	 * @return the valorTir
	 */
	public double getValorTir() {
		return valorTir;
	}
	/**
	 * @param valorTir the valorTir to set
	 */
	public void setValorTir(double valorTir) {
		this.valorTir = valorTir;
	}
	/**
	 * @return the valorPayBack
	 */
	public int getValorPayBack() {
		return valorPayBack;
	}
	/**
	 * @param valorPayBack the valorPayBack to set
	 */
	public void setValorPayBack(int valorPayBack) {
		this.valorPayBack = valorPayBack;
	}
	/**
	 * @return the idAlternativa
	 */
	public int getIdAlternativa() {
		return idAlternativa;
	}
	/**
	 * @param idAlternativa the idAlternativa to set
	 */
	public void setIdAlternativa(int idAlternativa) {
		this.idAlternativa = idAlternativa;
	}
	/**
	 * @return the idParametro
	 */
	public int getIdParametro() {
		return idParametro;
	}
	/**
	 * @param idParametro the idParametro to set
	 */
	public void setIdParametro(int idParametro) {
		this.idParametro = idParametro;
	}
	/**
	 * @return the fecha_creacion
	 */
	public String getFecha_creacion() {
		return fecha_creacion;
	}
	/**
	 * @param fecha_creacion the fecha_creacion to set
	 */
	public void setFecha_creacion(String fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}
	/**
	 * @return the fecha_ultima_evaluacion
	 */
	public String getFecha_ultima_evaluacion() {
		return fecha_ultima_evaluacion;
	}
	/**
	 * @param fecha_ultima_evaluacion the fecha_ultima_evaluacion to set
	 */
	public void setFecha_ultima_evaluacion(String fecha_ultima_evaluacion) {
		this.fecha_ultima_evaluacion = fecha_ultima_evaluacion;
	}
	/**
	 * @return the fecha_siguiente_evaluacion
	 */
	public String getFecha_siguiente_evaluacion() {
		return fecha_siguiente_evaluacion;
	}
	/**
	 * @param fecha_siguiente_evaluacion the fecha_siguiente_evaluacion to set
	 */
	public void setFecha_siguiente_evaluacion(String fecha_siguiente_evaluacion) {
		this.fecha_siguiente_evaluacion = fecha_siguiente_evaluacion;
	}
	/**
	 * @return the flujos
	 */
	public double[] getFlujos() {
		return flujos;
	}
	/**
	 * @param flujos the flujos to set
	 */
	public void setFlujos(double[] flujos) {
		this.flujos = flujos;
	}
	/**
	 * @return the flujosAcumulados
	 */
	public double[] getFlujosAcumulados() {
		return flujosAcumulados;
	}
	/**
	 * @param flujosAcumulados the flujosAcumulados to set
	 */
	public void setFlujosAcumulados(double[] flujosAcumulados) {
		this.flujosAcumulados = flujosAcumulados;
	}
	/**
	 * @return the valores
	 */
	public double[] getValores() {
		return valores;
	}
	/**
	 * @param valores the valores to set
	 */
	public void setValores(double[] valores) {
		this.valores = valores;
	}
	public void setDescripcionAlternativa(String descripcionAlternativa) {
		this.descripcionAlternativa = descripcionAlternativa;
	}
	public String getDescripcionAlternativa() {
		return descripcionAlternativa;
	}
	
	
 
	


}




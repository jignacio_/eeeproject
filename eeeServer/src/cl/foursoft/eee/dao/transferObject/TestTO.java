package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;

public class TestTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String mensaje;
	
	public TestTO(int id, String mensaje) {
		super();
		this.id = id;
		this.mensaje = mensaje;
	}
	
	public TestTO() {
		super();
		this.id = -1;
		this.mensaje = "";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}

package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;

public class FlujosEvaluacionTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private int anio ;
	private double flujosNeto ;
	private double flujosAcumulado ;
	private double variacionPrecio ;
	/**
	 * @return the anio
	 */
	public int getAnio() {
		return anio;
	}
	/**
	 * @param anio the anio to set
	 */
	public void setAnio(int anio) {
		this.anio = anio;
	}
	/**
	 * @return the flujosNeto
	 */
	public double getFlujosNeto() {
		return flujosNeto;
	}
	/**
	 * @param flujosNeto the flujosNeto to set
	 */
	public void setFlujosNeto(double flujosNeto) {
		this.flujosNeto = flujosNeto;
	}
	/**
	 * @return the flujosAcumulado
	 */
	public double getFlujosAcumulado() {
		return flujosAcumulado;
	}
	/**
	 * @param flujosAcumulado the flujosAcumulado to set
	 */
	public void setFlujosAcumulado(double flujosAcumulado) {
		this.flujosAcumulado = flujosAcumulado;
	}
	/**
	 * @return the variacionPrecio
	 */
	public double getVariacionPrecio() {
		return variacionPrecio;
	}
	/**
	 * @param variacionPrecio the variacionPrecio to set
	 */
	public void setVariacionPrecio(double variacionPrecio) {
		this.variacionPrecio = variacionPrecio;
	}
	
	
 
	

}

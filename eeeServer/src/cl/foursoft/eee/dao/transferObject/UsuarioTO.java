package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

public class UsuarioTO implements Serializable{

	/**
	 * JGuajardo
	 */
	
	private static final long serialVersionUID = 1L;
	
	private int idCliente;
	private int idUsuario;
	private int idTipoUsuario;
	private String nombreTipoUsuario;
	private String nombreCompleto;
	private String nombreCliente; 
	private String email;
	
	private int telefono;

	private String nombreUsuario; 
	private String contrasena;
	private String fechaIngreso;
	private Date fechaHoraIngreso;
	
	private boolean usuarioEditado;
	
	private HashMap<String, String> objetoMedicion;
 
	
	
	
	
	public UsuarioTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UsuarioTO(int idCliente, int idUsuario, int idTipoUsuario,
			String nombreTipoUsuario, String nombreCompleto,
			String nombreCliente, int telefono, String nombreUsuario,
			String contrasena, Date fecha) {
		super();
		this.idCliente = idCliente;
		this.idUsuario = idUsuario;
		this.idTipoUsuario = idTipoUsuario;
		this.nombreTipoUsuario = nombreTipoUsuario;
		this.nombreCompleto = nombreCompleto;
		this.nombreCliente = nombreCliente;
		this.telefono = telefono;
		this.nombreUsuario = nombreUsuario;
		this.contrasena = contrasena;
		this.fechaHoraIngreso = fecha;
		this.email = "";
		this.usuarioEditado = false;
		
	}
	public int getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getIdTipoUsuario() {
		return idTipoUsuario;
	}
	public void setIdTipoUsuario(int idTipoUsuario) {
		this.idTipoUsuario = idTipoUsuario;
	}
	public String getNombreTipoUsuario() {
		return nombreTipoUsuario;
	}
	public void setNombreTipoUsuario(String nombreTipoUsuario) {
		this.nombreTipoUsuario = nombreTipoUsuario;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public int getTelefono() {
		return telefono;
	}
	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public String getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public Date getFechaHoraIngreso() {
		return fechaHoraIngreso;
	}
	public void setFechaHoraIngreso(Date fechaHoraIngreso) {
		this.fechaHoraIngreso = fechaHoraIngreso;
	}
	public HashMap<String, String> getObjetoMedicion() {
		return objetoMedicion;
	}
	public void setObjetoMedicion(HashMap<String, String> objetoMedicion) {
		this.objetoMedicion = objetoMedicion;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail() {
		return email;
	}
	public void setUsuarioEditado(boolean usuarioEditado) {
		this.usuarioEditado = usuarioEditado;
	}
	public boolean getUsuarioEditado() {
		return usuarioEditado;
	}
 
	
}

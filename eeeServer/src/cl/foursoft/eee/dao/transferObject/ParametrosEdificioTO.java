package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;

public class ParametrosEdificioTO implements Serializable {
		
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int idParametro; 
	private String nombreParametro;
	private String simboloParametro;
	private String unidadParametro;
	private Float valorParametro;
	
	
	
	public ParametrosEdificioTO() {
		super();
		 
		this.idParametro = -1;
		this.nombreParametro = "";
		this.simboloParametro = "";
		this.unidadParametro = "";
		this.valorParametro = (float) 0.0;
	}



	public int getIdParametro() {
		return idParametro;
	}



	public void setIdParametro(int idParametro) {
		this.idParametro = idParametro;
	}



	public String getNombreParametro() {
		return nombreParametro;
	}



	public void setNombreParametro(String nombreParametro) {
		this.nombreParametro = nombreParametro;
	}



	public String getSimboloParametro() {
		return simboloParametro;
	}



	public void setSimboloParametro(String simboloParametro) {
		this.simboloParametro = simboloParametro;
	}



	public String getUnidadParametro() {
		return unidadParametro;
	}



	public void setUnidadParametro(String unidadParametro) {
		this.unidadParametro = unidadParametro;
	}



	public Float getValorParametro() {
		return valorParametro;
	}



	public void setValorParametro(Float valorParametro) {
		this.valorParametro = valorParametro;
	}




	
}

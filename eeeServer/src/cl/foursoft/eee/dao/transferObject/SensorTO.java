/**
 * 
 */
package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;
import java.util.List;
 
public class SensorTO implements Serializable {
 
	private static final long serialVersionUID = 1L;
	
	private int idSensor  ; 
	private String codigoSensor  ;
	
	private String nombreSensor;
	private String nombreTipoMedicion ;
	private String codigoTipoMedicion ;
	
	private String nombreTipoConsumo ;
	private String codigoTipoConsumo ;
	
	private int numeroSensorActual;
	
	private int idCliente ; 
	private int idEdificio ;
	private int idPiso ;
	private int idZona ;
	
	private List<ArtefactoTO> artefactosMedidios;
	
	private int usoHorasDiarias;
	private int usoDiasMensuales;
	private double consumoTotalEstimado;
	private String descripcionArtefactos;
	private String unidadMedicionSensor;
	
	private Boolean guardado;
	public SensorTO() {
		super();
		// TODO Auto-generated constructor stub
		setGuardado(false);
	}



	public int getIdSensor() {
		return idSensor;
	}



	public void setIdSensor(int idSensor) {
		this.idSensor = idSensor;
	}



	public String getCodigoSensor() {
		return codigoSensor;
	}



	public void setCodigoSensor(String codigoSensor) {
		this.codigoSensor = codigoSensor;
	}



	public String getNombreSensor() {
		return nombreSensor;
	}



	public void setNombreSensor(String nombreSensor) {
		this.nombreSensor = nombreSensor;
	}



	public String getNombreTipoMedicion() {
		return nombreTipoMedicion;
	}



	public void setNombreTipoMedicion(String nombreTipoMedicion) {
		this.nombreTipoMedicion = nombreTipoMedicion;
	}



	public String getCodigoTipoMedicion() {
		return codigoTipoMedicion;
	}



	public void setCodigoTipoMedicion(String codigoTipoMedicion) {
		this.codigoTipoMedicion = codigoTipoMedicion;
	}



	public String getNombreTipoConsumo() {
		return nombreTipoConsumo;
	}



	public void setNombreTipoConsumo(String nombreTipoConsumo) {
		this.nombreTipoConsumo = nombreTipoConsumo;
	}



	public String getCodigoTipoConsumo() {
		return codigoTipoConsumo;
	}



	public void setCodigoTipoConsumo(String codigoTipoConsumo) {
		this.codigoTipoConsumo = codigoTipoConsumo;
	}



	public int getNumeroSensorActual() {
		return numeroSensorActual;
	}



	public void setNumeroSensorActual(int numeroSensorActual) {
		this.numeroSensorActual = numeroSensorActual;
	}



	public int getIdCliente() {
		return idCliente;
	}



	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}



	public int getIdEdificio() {
		return idEdificio;
	}



	public void setIdEdificio(int idEdificio) {
		this.idEdificio = idEdificio;
	}



	public int getIdPiso() {
		return idPiso;
	}



	public void setIdPiso(int idPiso) {
		this.idPiso = idPiso;
	}



	public int getIdZona() {
		return idZona;
	}



	public void setIdZona(int idZona) {
		this.idZona = idZona;
	}



	public void setGuardado(Boolean guardado) {
		this.guardado = guardado;
	}



	public Boolean getGuardado() {
		return guardado;
	}



	public void setArtefactosMedidios(List<ArtefactoTO> artefactosMedidios) {
		this.artefactosMedidios = artefactosMedidios;
	}



	public List<ArtefactoTO> getArtefactosMedidios() {
		return artefactosMedidios;
	}



	public void setUsoHorasDiarias(int usoHorasDiarias) {
		this.usoHorasDiarias = usoHorasDiarias;
	}



	public int getUsoHorasDiarias() {
		return usoHorasDiarias;
	}



	public void setUsoDiasMensuales(int usoDiasMensuales) {
		this.usoDiasMensuales = usoDiasMensuales;
	}



	public int getUsoDiasMensuales() {
		return usoDiasMensuales;
	}



	public void setConsumoTotalEstimado(double consumoTotalEstimado) {
		this.consumoTotalEstimado = consumoTotalEstimado;
	}



	public double getConsumoTotalEstimado() {
		return consumoTotalEstimado;
	}



	public void setUnidadMedicionSensor(String unidadMedicionSensor) {
		this.unidadMedicionSensor = unidadMedicionSensor;
	}



	public String getUnidadMedicionSensor() {
		return unidadMedicionSensor;
	}



	public void setDescripcionArtefactos(String descripcionArtefactos) {
		this.descripcionArtefactos = descripcionArtefactos;
	}



	public String getDescripcionArtefactos() {
		return descripcionArtefactos;
	}

	
}

package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EnvolventeTO implements Serializable {
		
	
		private static final long serialVersionUID = 1L;
		private int idEdificio ;
		private SeccionTO areaSuperior ;
		private SeccionTO areaPiso ;
		private SeccionTO puertas;

		private SeccionTO ventanas ;
		private SeccionTO muros ;
		private List<SeccionTO> otrasSecciones; 
		
		
		public EnvolventeTO() {
			otrasSecciones = new ArrayList<SeccionTO>();
		}
		public int getIdEdificio() {
			return idEdificio;
		}
		public void setIdEdificio(int idEdificio) {
			this.idEdificio = idEdificio;
		}
		public SeccionTO getAreaSuperior() {
			return areaSuperior;
		}
		public void setAreaSuperior(SeccionTO areaSuperior) {
			this.areaSuperior = areaSuperior;
		}
		public SeccionTO getAreaPiso() {
			return areaPiso;
		}
		public void setAreaPiso(SeccionTO areaPiso) {
			this.areaPiso = areaPiso;
		}
		public SeccionTO getVentanas() {
			return ventanas;
		}
		public void setVentanas(SeccionTO ventanas) {
			this.ventanas = ventanas;
		}
		public SeccionTO getMuros() {
			return muros;
		}
		public void setMuros(SeccionTO muros) {
			this.muros = muros;
		}
		public void setOtrasSecciones(List<SeccionTO> otrasSecciones) {
			this.otrasSecciones = otrasSecciones;
		}
		public List<SeccionTO> getOtrasSecciones() {
			return otrasSecciones;
		}
		
		
		/**
		 * @return the puertas
		 */
		public SeccionTO getPuertas() {
			return puertas;
		}
		/**
		 * @param puertas the puertas to set
		 */
		public void setPuertas(SeccionTO puertas) {
			this.puertas = puertas;
		}
		
		
		public List<SeccionTO> obtenerListaSecciones(){
			List<SeccionTO> resp = new ArrayList<SeccionTO>();
			
			resp.add(areaSuperior);
			resp.add(muros);
			resp.add(ventanas);
			resp.add(areaPiso);
			resp.add(puertas);
			
			return resp;
		}
		
		
}

package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;

public class TipoAlternativaTO implements Serializable {

	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nombreAlternativa ;
	private String descripcionAlternativa ;
	private int idAlternativa ;
	
	
		public TipoAlternativaTO() {
		super();
		// TODO Auto-generated constructor stub
	}


		/**
		 * @return the nombreAlternativa
		 */
		public String getNombreAlternativa() {
			return nombreAlternativa;
		}


		/**
		 * @param nombreAlternativa the nombreAlternativa to set
		 */
		public void setNombreAlternativa(String nombreAlternativa) {
			this.nombreAlternativa = nombreAlternativa;
		}


		/**
		 * @return the descripcionAlternativa
		 */
		public String getDescripcionAlternativa() {
			return descripcionAlternativa;
		}


		/**
		 * @param descripcionAlternativa the descripcionAlternativa to set
		 */
		public void setDescripcionAlternativa(String descripcionAlternativa) {
			this.descripcionAlternativa = descripcionAlternativa;
		}


		/**
		 * @return the idAlternativa
		 */
		public int getIdAlternativa() {
			return idAlternativa;
		}


		/**
		 * @param idAlternativa the idAlternativa to set
		 */
		public void setIdAlternativa(int idAlternativa) {
			this.idAlternativa = idAlternativa;
		}

 

}

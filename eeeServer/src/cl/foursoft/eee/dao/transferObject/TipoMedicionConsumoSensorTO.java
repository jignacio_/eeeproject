package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;
import java.util.List;

public class TipoMedicionConsumoSensorTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private boolean eliminado;
	private int idTipoSensor;
	private String nombreTipoMedicion;
	private String codigoTipoMedicion;
	private String descripcionTipoConsumo;
	private String unidadMedicion;
	private List<TipoMedicionConsumoSensorTO> arrColTipoConsumo;
	
	
	
	
	public TipoMedicionConsumoSensorTO() {
		super();
		this.idTipoSensor = -1;
		this.nombreTipoMedicion = "";
		this.codigoTipoMedicion = "";
		this.arrColTipoConsumo = null;
		this.eliminado = false;
		// TODO Auto-generated constructor stub
	}


	public TipoMedicionConsumoSensorTO(String nombreTipoMedicion,
			String codigoTipoMedicion,
			List<TipoMedicionConsumoSensorTO> arrColTipoConsumo) {
		super();
		this.nombreTipoMedicion = nombreTipoMedicion;
		this.codigoTipoMedicion = codigoTipoMedicion;
		this.arrColTipoConsumo = arrColTipoConsumo;
	}
	
	
	public String getNombreTipoMedicion() {
		return nombreTipoMedicion;
	}
	public void setNombreTipoMedicion(String nombreTipoMedicion) {
		this.nombreTipoMedicion = nombreTipoMedicion;
	}
	public String getCodigoTipoMedicion() {
		return codigoTipoMedicion;
	}
	public void setCodigoTipoMedicion(String codigoTipoMedicion) {
		this.codigoTipoMedicion = codigoTipoMedicion;
	}
	public List<TipoMedicionConsumoSensorTO> getArrColTipoConsumo() {
		return arrColTipoConsumo;
	}
	public void setArrColTipoConsumo(
			List<TipoMedicionConsumoSensorTO> arrColTipoConsumo) {
		this.arrColTipoConsumo = arrColTipoConsumo;
	}


	public void setDescripcionTipoConsumo(String descripcionTipoConsumo) {
		this.descripcionTipoConsumo = descripcionTipoConsumo;
	}


	public String getDescripcionTipoConsumo() {
		return descripcionTipoConsumo;
	}


	public void setIdTipoSensor(int idTipoSensor) {
		this.idTipoSensor = idTipoSensor;
	}


	public int getIdTipoSensor() {
		return idTipoSensor;
	}


	public void setEliminado(boolean eliminado) {
		this.eliminado = eliminado;
	}


	public boolean getEliminado() {
		return eliminado;
	}


	public void setUnidadMedicion(String unidadMedicion) {
		this.unidadMedicion = unidadMedicion;
	}


	public String getUnidadMedicion() {
		return unidadMedicion;
	}

	
}

package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;

public class ArtefactoCombustibleDetalleTO implements Serializable{
	
			/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
			private int id_artefacto ;
			private String tipo_combustible ; //20 caracteres
			private String idTipoMedicion;
			private double potencia ;//
			private double precio_inversion ;
			private double indice_inversion ;
			private double eficiencia ;
			private double energia ;
			private double eficiencia_distribucion_calo;
			private double valor_unitario_combustible ;
			private double poder_calorifico ;
			private double valor_consumo_final ;
			/**
			 * @return the id_artefacto
			 */
			public int getId_artefacto() {
				return id_artefacto;
			}
			/**
			 * @param id_artefacto the id_artefacto to set
			 */
			public void setId_artefacto(int id_artefacto) {
				this.id_artefacto = id_artefacto;
			}
			/**
			 * @return the tipo_combustible
			 */
			public String getTipo_combustible() {
				return tipo_combustible;
			}
			/**
			 * @param tipo_combustible the tipo_combustible to set
			 */
			public void setTipo_combustible(String tipo_combustible) {
				this.tipo_combustible = tipo_combustible;
			}
			/**
			 * @return the potencia
			 */
			public double getPotencia() {
				return potencia;
			}
			/**
			 * @param potencia the potencia to set
			 */
			public void setPotencia(double potencia) {
				this.potencia = potencia;
			}
			/**
			 * @return the precio_inversion
			 */
			public double getPrecio_inversion() {
				return precio_inversion;
			}
			/**
			 * @param precio_inversion the precio_inversion to set
			 */
			public void setPrecio_inversion(double precio_inversion) {
				this.precio_inversion = precio_inversion;
			}
			/**
			 * @return the indice_inversion
			 */
			public double getIndice_inversion() {
				return indice_inversion;
			}
			/**
			 * @param indice_inversion the indice_inversion to set
			 */
			public void setIndice_inversion(double indice_inversion) {
				this.indice_inversion = indice_inversion;
			}
			/**
			 * @return the eficiencia
			 */
			public double getEficiencia() {
				return eficiencia;
			}
			/**
			 * @param eficiencia the eficiencia to set
			 */
			public void setEficiencia(double eficiencia) {
				this.eficiencia = eficiencia;
			}
			/**
			 * @return the energia
			 */
			public double getEnergia() {
				return energia;
			}
			/**
			 * @param energia the energia to set
			 */
			public void setEnergia(double energia) {
				this.energia = energia;
			}
			/**
			 * @return the eficiencia_distribucion_calo
			 */
			public double getEficiencia_distribucion_calo() {
				return eficiencia_distribucion_calo;
			}
			/**
			 * @param eficiencia_distribucion_calo the eficiencia_distribucion_calo to set
			 */
			public void setEficiencia_distribucion_calo(double eficiencia_distribucion_calo) {
				this.eficiencia_distribucion_calo = eficiencia_distribucion_calo;
			}
			/**
			 * @return the valor_unitario_combustible
			 */
			public double getValor_unitario_combustible() {
				return valor_unitario_combustible;
			}
			/**
			 * @param valor_unitario_combustible the valor_unitario_combustible to set
			 */
			public void setValor_unitario_combustible(double valor_unitario_combustible) {
				this.valor_unitario_combustible = valor_unitario_combustible;
			}
			/**
			 * @return the poder_calorifico
			 */
			public double getPoder_calorifico() {
				return poder_calorifico;
			}
			/**
			 * @param poder_calorifico the poder_calorifico to set
			 */
			public void setPoder_calorifico(double poder_calorifico) {
				this.poder_calorifico = poder_calorifico;
			}
			public void setValor_consumo_final(double valor_consumo_final) {
				this.valor_consumo_final = valor_consumo_final;
			}
			public double getValor_consumo_final() {
				return valor_consumo_final;
			}
			public void setIdTipoMedicion(String idTipoMedicion) {
				this.idTipoMedicion = idTipoMedicion;
			}
			public String getIdTipoMedicion() {
				return idTipoMedicion;
			}
			
			
			
			

}

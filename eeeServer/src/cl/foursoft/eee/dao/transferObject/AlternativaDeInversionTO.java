package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

public class AlternativaDeInversionTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String descripcionAlternativa ;
	private int idEdificio ;
	private int idParametrosEvaluacion ;
	private EnvolventeTO envolventeNueva ;
	private List<SensorTO> sensoresAlternativaArtefacto; 
	private int idAlternativa;
	private int idTipoAlternativa;
	
	private Date fechaUltimaEvaluacion ;
	private Date fechaSiguienteEvaluacion ;
	
	private float requerimientoEnergeticoTotal;
	private boolean estadoAlternativa ;
	
	private List<MaterialTO> envolventeDiferencia;
	private List<ArtefactoTO> artefactosDiferencia;
	private float inversionAlternativa;
	private String unidadMedicionAlternativa;
	private float otroCosto;
	
	
	
	public String getDescripcionAlternativa() {
		return descripcionAlternativa;
	}
	public void setDescripcionAlternativa(String descripcionAlternativa) {
		this.descripcionAlternativa = descripcionAlternativa;
	}
	public int getIdEdificio() {
		return idEdificio;
	}
	public void setIdEdificio(int idEdificio) {
		this.idEdificio = idEdificio;
	}
	public int getIdParametrosEvaluacion() {
		return idParametrosEvaluacion;
	}
	public void setIdParametrosEvaluacion(int idParametrosEvaluacion) {
		this.idParametrosEvaluacion = idParametrosEvaluacion;
	}
	public EnvolventeTO getEnvolventeNueva() {
		return envolventeNueva;
	}
	public void setEnvolventeNueva(EnvolventeTO envolventeNueva) {
		this.envolventeNueva = envolventeNueva;
	}
	public Date getFechaUltimaEvaluacion() {
		return fechaUltimaEvaluacion;
	}
	public void setFechaUltimaEvaluacion(Date fechaUltimaEvaluacion) {
		this.fechaUltimaEvaluacion = fechaUltimaEvaluacion;
	}
	public Date getFechaSiguienteEvaluacion() {
		return fechaSiguienteEvaluacion;
	}
	public void setFechaSiguienteEvaluacion(Date fechaSiguienteEvaluacion) {
		this.fechaSiguienteEvaluacion = fechaSiguienteEvaluacion;
	}
	public int getIdTipoAlternativa() {
		return idTipoAlternativa;
	}
	public void setIdTipoAlternativa(int idTipoAlternativa) {
		this.idTipoAlternativa = idTipoAlternativa;
	}
	public void setRequerimientoEnergeticoTotal(float requerimientoEnergeticoTotal) {
		this.requerimientoEnergeticoTotal = requerimientoEnergeticoTotal;
	}
	public float getRequerimientoEnergeticoTotal() {
		return requerimientoEnergeticoTotal;
	}
	public void setEstadoAlternativa(boolean estadoAlternativa) {
		this.estadoAlternativa = estadoAlternativa;
	}
	public boolean isEstadoAlternativa() {
		return estadoAlternativa;
	}
	public void setIdAlternativa(int idAlternativa) {
		this.idAlternativa = idAlternativa;
	}
	public int getIdAlternativa() {
		return idAlternativa;
	}
	public void setEnvolventeDiferencia(List<MaterialTO> envolventeDiferencia) {
		this.envolventeDiferencia = envolventeDiferencia;
	}
	public List<MaterialTO> getEnvolventeDiferencia() {
		return envolventeDiferencia;
	}
	public void setInversionAlternativa(float inversionAlternativa) {
		this.inversionAlternativa = inversionAlternativa;
	}
	public float getInversionAlternativa() {
		return inversionAlternativa;
	}
	public void setOtroCosto(float otroCosto) {
		this.otroCosto = otroCosto;
	}
	public float getOtroCosto() {
		return otroCosto;
	}
	public void setSensoresAlternativaArtefacto(List<SensorTO> sensoresAlternativas) {
		this.sensoresAlternativaArtefacto = sensoresAlternativas;
	}
	public List<SensorTO> getSensoresAlternativaArtefacto() {
		return sensoresAlternativaArtefacto;
	}
	public void setArtefactosDiferencia(List<ArtefactoTO> artefactosDiferencia) {
		this.artefactosDiferencia = artefactosDiferencia;
	}
	public List<ArtefactoTO> getArtefactosDiferencia() {
		return artefactosDiferencia;
	}
	public void setUnidadMedicionAlternativa(String unidadMedicionAlternativa) {
		this.unidadMedicionAlternativa = unidadMedicionAlternativa;
	}
	public String getUnidadMedicionAlternativa() {
		return unidadMedicionAlternativa;
	}
	

	
	

}

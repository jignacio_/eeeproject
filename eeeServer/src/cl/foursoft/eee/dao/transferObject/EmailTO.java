package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;

public class EmailTO implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String usuario_email;
	private String contrasenia_email;
	private String remitente;
	private String pie;
	
	
	public EmailTO(String usuario_email, String contrasenia_email,
			String remitente, String pie) {
		super();
		this.usuario_email = usuario_email;
		this.contrasenia_email = contrasenia_email;
		this.remitente = remitente;
		this.pie = pie;
	}


	/**
	 * @return the usuario_email
	 */
	public String getUsuario_email() {
		return usuario_email;
	}


	/**
	 * @param usuario_email the usuario_email to set
	 */
	public void setUsuario_email(String usuario_email) {
		this.usuario_email = usuario_email;
	}


	/**
	 * @return the contrasenia_email
	 */
	public String getContrasenia_email() {
		return contrasenia_email;
	}


	/**
	 * @param contrasenia_email the contrasenia_email to set
	 */
	public void setContrasenia_email(String contrasenia_email) {
		this.contrasenia_email = contrasenia_email;
	}


	/**
	 * @return the remitente
	 */
	public String getRemitente() {
		return remitente;
	}


	/**
	 * @param remitente the remitente to set
	 */
	public void setRemitente(String remitente) {
		this.remitente = remitente;
	}


	/**
	 * @return the pie
	 */
	public String getPie() {
		return pie;
	}


	/**
	 * @param pie the pie to set
	 */
	public void setPie(String pie) {
		this.pie = pie;
	}
	
	
}



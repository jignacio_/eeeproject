package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class MedicionTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Timestamp fechaMedicion; // D�a y Hora
	private SensorTO sensor;
	private double medicion;
	
	private List<Double> mediciones;
	
	private String idTipoMedicion;
	private String idTipoConsumo;
	private String unidadMedicion;
	private String idSensor;
	
	private boolean alerta;
	private double promedioMedicion;
	
	private String descripcionMedicion;
	 
	
	public MedicionTO() {
		super();
		// TODO Auto-generated constructor stub
		alerta = false;
		promedioMedicion = 0;
		mediciones = new ArrayList<Double>();
	}
	
	public MedicionTO(Timestamp fechaMedicion, SensorTO sensor, Float medicion) {
		super();
		this.fechaMedicion = fechaMedicion;
		this.sensor = sensor;
		this.medicion = medicion;
	}

	/**
	 * @return the fechaMedicion
	 */
	public Timestamp getFechaMedicion() {
		return fechaMedicion;
	}

	/**
	 * @param fechaMedicion the fechaMedicion to set
	 */
	public void setFechaMedicion(Timestamp fechaMedicion) {
		this.fechaMedicion = fechaMedicion;
	}

	/**
	 * @return the sensor
	 */
	public SensorTO getSensor() {
		return sensor;
	}

	/**
	 * @param sensor the sensor to set
	 */
	public void setSensor(SensorTO sensor) {
		this.sensor = sensor;
	}

	/**
	 * @return the medicion
	 */
	public double getMedicion() {
		return medicion;
	}

	/**
	 * @param medicion the medicion to set
	 */
	public void setMedicion(double medicion) {
		this.medicion = medicion;
	}

	/**
	 * @return the idTipoMedicion
	 */
	public String getIdTipoMedicion() {
		return idTipoMedicion;
	}

	/**
	 * @param idTipoMedicion the idTipoMedicion to set
	 */
	public void setIdTipoMedicion(String idTipoMedicion) {
		this.idTipoMedicion = idTipoMedicion;
	}

	/**
	 * @return the idTipoConsumo
	 */
	public String getIdTipoConsumo() {
		return idTipoConsumo;
	}

	/**
	 * @param idTipoConsumo the idTipoConsumo to set
	 */
	public void setIdTipoConsumo(String idTipoConsumo) {
		this.idTipoConsumo = idTipoConsumo;
	}

	/**
	 * @return the idSensor
	 */
	public String getIdSensor() {
		return idSensor;
	}

	/**
	 * @param idSensor the idSensor to set
	 */
	public void setIdSensor(String idSensor) {
		this.idSensor = idSensor;
	}

	/**
	 * @return the alerta
	 */
	public boolean isAlerta() {
		return alerta;
	}

	/**
	 * @param alerta the alerta to set
	 */
	public void setAlerta(boolean alerta) {
		this.alerta = alerta;
	}

	/**
	 * @return the promedioMedicion
	 */
	public double getPromedioMedicion() {
		return promedioMedicion;
	}

	/**
	 * @param promedioMedicion the promedioMedicion to set
	 */
	public void setPromedioMedicion(double promedioMedicion) {
		this.promedioMedicion = promedioMedicion;
	}

	public void setMediciones(List<Double> mediciones) {
		this.mediciones = mediciones;
	}

	public List<Double> getMediciones() {
		return mediciones;
	}

	public void setDescripcionMedicion(String descripcionMedicion) {
		this.descripcionMedicion = descripcionMedicion;
	}

	public String getDescripcionMedicion() {
		return descripcionMedicion;
	}

	public void setUnidadMedicion(String unidadMedicion) {
		this.unidadMedicion = unidadMedicion;
	}

	public String getUnidadMedicion() {
		return unidadMedicion;
	}
	
	
}
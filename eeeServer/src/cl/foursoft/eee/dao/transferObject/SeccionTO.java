package cl.foursoft.eee.dao.transferObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SeccionTO implements Serializable {

	
	private static final long serialVersionUID = 1L;
	//datos de identificación
	private int idSeccion;
	private int idTipoSeccion;
	private int idSeccionPadre ;
	private String nombreTipo ;
	private String nombre_seccion_padre;

	private float transmitanciaTermica;
	private float resistenciaTotalMasRSERSI;
	private float resistenciaSeccion;
	
	

	private float perdidaPorTransmision;
	//private float FactorGananciaSolVerano;
	//private float FactorGananciaSolInvierno;

	private float area;
	private List<MaterialTO> composicion;//AC de MaterialTO
	private List<SeccionTO> seccionesHijo;

	

	private float RSI;
	private float RSE;
	
	public SeccionTO() {
		
		composicion = new ArrayList<MaterialTO>();	
		transmitanciaTermica = 0f;
		resistenciaTotalMasRSERSI = 0f;
		nombreTipo="";
		
		RSE = 0;
		RSI = 0;
		
	}

	public float getArea() {
		return area;
	}
	
	public void setArea(float area) {
		this.area = area;
	}
	public List<MaterialTO> getComposicion() {
		return composicion;
	}
	public void setComposicion(List<MaterialTO> composicion) {
		this.composicion = composicion;
	}
	public int getIdTipoSeccion() {
		return idTipoSeccion;
	}
	public void setIdTipoSeccion(int idTipoSeccion) {
		this.idTipoSeccion = idTipoSeccion;
	}
 
	public String getNombreTipo() {
		return nombreTipo;
	}
	public void setNombreTipo(String nombreTipo) {
		this.nombreTipo = nombreTipo;
	}
 
	public String getNombre_seccion_padre() {
		return nombre_seccion_padre;
	}
	public void setNombre_seccion_padre(String nombre_seccion_padre) {
		this.nombre_seccion_padre = nombre_seccion_padre;
	}

	public void setTransmitanciaTermica(float transmitanciaTermica) {
		this.transmitanciaTermica = transmitanciaTermica;
	}

	public float getTransmitanciaTermica() {
		return transmitanciaTermica;
	}

	public void setSeccionesHijo(List<SeccionTO> seccionesHijo) {
		this.seccionesHijo = seccionesHijo;
	}

	public List<SeccionTO> getSeccionesHijo() {
		return seccionesHijo;
	}

	public void setIdSeccionPadre(int idSeccionPadre) {
		this.idSeccionPadre = idSeccionPadre;
	}

	public int getIdSeccionPadre() {
		return idSeccionPadre;
	}

	public void setResistenciaTotalMasRSERSI(float _resistenciaTotalMasRSERSI) {
		this.resistenciaTotalMasRSERSI = _resistenciaTotalMasRSERSI;
	}

	public float getResistenciaTotalMasRSERSI() {
		return resistenciaTotalMasRSERSI;
	}

	public void setIdSeccion(int _idSeccion) {
		this.idSeccion = _idSeccion;
	}

	public int getIdSeccion() {
		return idSeccion;
	}

	public void setPerdidaPorTransmision(float perdidaPorTransmision) {
		this.perdidaPorTransmision = perdidaPorTransmision;
	}

	public float getPerdidaPorTransmision() {
		return perdidaPorTransmision;
	}
	//Funciones UTIL
	public float obtenerCostoTotalSeccion(){
		float resp = 0f ; 
		
		for (MaterialTO m : this.composicion){
			resp +=m.costoTotalMaterial();
		}
		
		return resp;
	}
	/**
	 * @return the rSI
	 */
	public float getRSI() {
		return RSI;
	}

	/**
	 * @param rSI the rSI to set
	 */
	public void setRSI(float rSI) {
		RSI = rSI;
	}

	/**
	 * @return the rSE
	 */
	public float getRSE() {
		return RSE;
	}

	/**
	 * @param rSE the rSE to set
	 */
	public void setRSE(float rSE) {
		RSE = rSE;
	}
	
	/**
	 * @return the resistenciaSeccion
	 */
	public float getResistenciaSeccion() {
		return resistenciaSeccion;
	}

	/**
	 * @param resistenciaSeccion the resistenciaSeccion to set
	 */
	public void setResistenciaSeccion(float resistenciaSeccion) {
		this.resistenciaSeccion = resistenciaSeccion;
	}
	
}

package cl.foursoft.eee.restful;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

import cl.foursoft.eee.util.ProcesoSincronizacion;
import cl.foursoft.eee.util.Sincronizacion;

@Path("/syncronize")
public class SincronizacionResource {

	@GET
	@Path("/parameter")
	@Produces(MediaType.APPLICATION_JSON+ ";charset=utf-8")
	public String obtenerParametros(){
		
		ProcesoSincronizacion procSincronizacion = new ProcesoSincronizacion();
		Sincronizacion sync = procSincronizacion.obtenerDatosSincronizacion();
		
		System.out.println("..Datos listos para la sincronización.");
		
		Gson gson = new Gson();
		String json = gson.toJson(sync);
		
		return json;
	}
}

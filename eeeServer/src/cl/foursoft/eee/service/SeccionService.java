package cl.foursoft.eee.service;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.facade.FacadeSeccion;
import cl.foursoft.eee.dao.implementation.DBMannager;
import cl.foursoft.eee.dao.transferObject.SeccionTO;
import cl.foursoft.eee.util.UtilLog;

public class SeccionService  {

	
	
	public List<SeccionTO> obtenerSecciones(){
		List<SeccionTO> resp = null;
		 
		
		Connection c = DBMannager.getConnection();
		FacadeSeccion f = new FacadeSeccion();
		
		try{
			
			resp = f.obtenerSecciones(c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
			
		}catch(Exception e){
			
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
			
		}
		
	 
		return resp;
 
	}  	
	public List<SeccionTO> obtenerSeccionesPadre(){
		List<SeccionTO> resp = null;
		
		
		Connection c = DBMannager.getConnection();
		FacadeSeccion f = new FacadeSeccion();
		
		try{
			
			resp = f.obtenerSeccionesPadre(c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
			
		}catch(Exception e){
			
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
			
		}
		
		
		return resp;
		
	}  	
	
}

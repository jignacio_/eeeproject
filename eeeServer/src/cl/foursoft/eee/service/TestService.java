package cl.foursoft.eee.service;

import java.sql.Connection;
import cl.foursoft.eee.dao.facade.FacadeTest;

import cl.foursoft.eee.dao.implementation.DBMannager;
import cl.foursoft.eee.dao.transferObject.TestTO;
import cl.foursoft.eee.util.UtilLog;

/**
*
* @author ccornejo
*/
public class TestService {

	public TestTO getTest(int id){
		Connection c = DBMannager.getConnection();
		FacadeTest f = new FacadeTest();
		TestTO resp = null;
		
		try{
			resp = f.getTest(id, c);
			
			boolean exito = (resp != null);
			DBMannager.close(c, exito);
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
	}
 
}

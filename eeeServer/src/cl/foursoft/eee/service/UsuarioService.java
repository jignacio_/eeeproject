package cl.foursoft.eee.service;



import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cl.foursoft.eee.dao.facade.FacadeUsuario;
import cl.foursoft.eee.dao.implementation.DBMannager;
import cl.foursoft.eee.dao.transferObject.ClienteTO;
import cl.foursoft.eee.dao.transferObject.UsuarioTO;
import cl.foursoft.eee.util.UtilLog;

public class UsuarioService {
	
	public UsuarioTO obtenerUsuario(String usuario, String contrasenia){
		
		Connection c =  DBMannager.getConnection();
		FacadeUsuario f = new FacadeUsuario();
		UsuarioTO resp = null;  
		Date FechHoraIngreso = new Date(); 
		try{
	
			resp = f.obtenerUsuario(usuario, contrasenia, c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
	
			
			if(resp!=null){
				resp.setFechaHoraIngreso(FechHoraIngreso);
				resp.setFechaIngreso(obtenerFechaIngreso());
			}
			
			System.out.println("Ingreso  -> " + FechHoraIngreso + "("+usuario+")");
			
			
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		} 
		
		
		return resp;
	}
	public ClienteTO  obtenerCliente(){
		ClienteTO resp = null;
		Connection c =  DBMannager.getConnection();
		FacadeUsuario f = new FacadeUsuario();
 
		Date FechHoraIngreso = new Date(); 
		try{
	
			resp = f.obtenerCliente(c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
	
			 
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		} 
		
		
		return resp;
	}
	public UsuarioTO obtenerUsuarioKipus(String usuario, String contrasenia){
		
		Connection c =  DBMannager.getConnection();
		FacadeUsuario f = new FacadeUsuario();
		UsuarioTO resp = null;  
		Date FechHoraIngreso = new Date(); 
		try{
	
			resp = f.obtenerUsuarioKipus(usuario, contrasenia, c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
			
			resp.setFechaHoraIngreso(FechHoraIngreso);
			resp.setFechaIngreso(obtenerFechaIngreso());
			
			System.out.println("Ingreso Kipus ->" + FechHoraIngreso + "("+usuario+")");
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		} 
		
		
		
		
		return resp;
	}
	
	private static String obtenerFechaIngreso(){
  
		 Date ahora = new Date();
	     
		 SimpleDateFormat formateador = new SimpleDateFormat("EEEE dd 'de' MMMM 'de' yyyy", new Locale("ES"));
		 	
		 return formateador.format(ahora);
		
	}
	
	public List<UsuarioTO> obtenerUsuarios(){		
		Connection c =  DBMannager.getConnection();
		FacadeUsuario f = new FacadeUsuario();
		List<UsuarioTO> resp = null;  

		try{	
			resp = f.obtenerUsuarios(c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		} 
		
		return resp;
	}
	
	public boolean guardarUsuarios(List<UsuarioTO> usuarios){
		Connection c =  DBMannager.getConnection();
		FacadeUsuario f = new FacadeUsuario();
		boolean resp = false;  

		try{	
			resp = f.guardarUsuarios(usuarios, c);
			
			if(resp) c.commit();
			else c.rollback();
			
			DBMannager.close(c, resp);
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		} 
		
		return resp;
	}
	 
	public boolean reenviarDatos(UsuarioTO user){
		Connection c =  DBMannager.getConnection();
		FacadeUsuario f = new FacadeUsuario();
		boolean resp = false;  

		try{	
			resp = f.obtenerUsuarioPorId(user, c);
			
			
			DBMannager.close(c, resp);
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		} 
		
		return resp;
	}
	public boolean eliminarUsuario(UsuarioTO usuario){
		Connection c =  DBMannager.getConnection();
		FacadeUsuario f = new FacadeUsuario();
		boolean resp = false;  

		try{	
			resp = f.eliminarUsuario(usuario, c);
			DBMannager.close(c, resp);
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		} 
		
		return resp;
	}
}

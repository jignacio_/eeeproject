package cl.foursoft.eee.service;

import cl.foursoft.eee.dao.facade.FacadeConstantes;
import cl.foursoft.eee.dao.implementation.DBMannager;
import cl.foursoft.eee.dao.transferObject.ParametrosEdificioTO; 
import cl.foursoft.eee.dao.transferObject.FactorRenovacionTO;
import cl.foursoft.eee.util.UtilLog;

import java.sql.Connection;
import java.util.List;

 

public class ConstantesService {
	
	
	public List<ParametrosEdificioTO> obtenerParametrosEdificio(){
		List<ParametrosEdificioTO> resp = null;
		
		Connection c = DBMannager.getConnection();
		FacadeConstantes f = new FacadeConstantes();
		
		try {
			resp = f.obtenerConstantesEdificio(c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
			
		} catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
		
	}
	public List<ParametrosEdificioTO> obtenerConstantesEdificio(){
		List<ParametrosEdificioTO> resp = null;
		
		Connection c = DBMannager.getConnection();
		FacadeConstantes f = new FacadeConstantes();
		
		try {
			resp = f.obtenerConstantesEdificio(c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
			
		} catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
		
	}
	
	public List<ParametrosEdificioTO> obtenerConstantesSeccion(){
		List<ParametrosEdificioTO> resp = null;
		
		Connection c = DBMannager.getConnection();
		FacadeConstantes f = new FacadeConstantes();
		
		
		try{
			resp = f.obtenerConstantesSeccion(c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
		}catch(Exception e){
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		
		return resp;
		
	}
	
	public List<FactorRenovacionTO> obtenerFactorDeRenovacion(){
		List<FactorRenovacionTO> resp = null;
		
		Connection c = DBMannager.getConnection();
		FacadeConstantes f = new FacadeConstantes();
		
		try{
			resp = f.obtenerFactorDeRenovacion(c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
	}
	
	public boolean guardarFactores(List<FactorRenovacionTO> factores){
		Connection c =  DBMannager.getConnection();
		FacadeConstantes f = new FacadeConstantes();
		boolean resp = false;  

		try{
			resp = f.guardarFactores(factores, c);
			DBMannager.close(c, resp);
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		} 
		
		return resp;
	}
}

package cl.foursoft.eee.service;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.facade.FacadeAlternativaInversion;
import cl.foursoft.eee.dao.facade.FacadeEdificio;
import cl.foursoft.eee.dao.facade.FacadeEvaluacionEconomica;
import cl.foursoft.eee.dao.facade.FacadeParametrosEvaluacion;
import cl.foursoft.eee.dao.facade.FacadeRequerimientoEnergetico;
import cl.foursoft.eee.dao.implementation.DBMannager;
import cl.foursoft.eee.dao.transferObject.AlternativaDeInversionTO;
import cl.foursoft.eee.dao.transferObject.EdificioTO;
import cl.foursoft.eee.dao.transferObject.EvaluacionEconomicaTO;
import cl.foursoft.eee.dao.transferObject.FlujosEvaluacionTO;
import cl.foursoft.eee.dao.transferObject.ParametrosEvaluacionTO;
import cl.foursoft.eee.dao.transferObject.RequerimientoEnergeticoTO;
import cl.foursoft.eee.util.UtilLog;
import cl.foursoft.eee.util.tipoAlternativaUtil;
import cl.foursoft.eee.util.CalculosMatematicos.EvaluacionEconomicaUtil;

public class EvaluacionEconomicaService {

	
	public int crearEvaluacionEconomica( int idEdificio, ParametrosEvaluacionTO parametro , AlternativaDeInversionTO alternativa ){
		int resp = -1;
		
		Connection c = DBMannager.getConnection();
		FacadeEvaluacionEconomica f = new FacadeEvaluacionEconomica();
		
		try{
			
			if(alternativa.getIdTipoAlternativa() == tipoAlternativaUtil.tipoAlternativaEnergetica)
				resp = f.crearEvaluacionEconomica(c , idEdificio, parametro , alternativa); 
			else if(alternativa.getIdTipoAlternativa() == tipoAlternativaUtil.tipoAlternativaArtefacto )
				resp = f.crearEvaluacionEconomica(c , idEdificio, parametro , alternativa); // Cambiar Funcion  
				
				
			boolean exito = (resp>0);
			if(exito) c.commit();
			else c.rollback();
			
			DBMannager.close(c, exito);
			
		}catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
	}
	public int actualizarEvaluacionesSyn(){//Sincronizaci�n
		int resp = -1;
		List<EdificioTO> edificios = null;
		List<AlternativaDeInversionTO> alternativas = null;
		List<ParametrosEvaluacionTO> parametros = null;
		
		
		
		Connection c = DBMannager.getConnection();
		FacadeEdificio f_edificio = new FacadeEdificio();
		FacadeAlternativaInversion f_alternativa = new FacadeAlternativaInversion();
		FacadeParametrosEvaluacion f_parametros = new FacadeParametrosEvaluacion();
		
		try {
			
			
			//Edificio por edificio
			//Alternativa por alternativa
			//Parametro por parametro
			
			
			boolean exito = (resp>0);
			DBMannager.close(c, exito);
		} catch (Exception e) {
				UtilLog.registrar(e);
				DBMannager.closeOnError(c);
		}
		
		
		
		
		
		return resp;
	}
	public List<EvaluacionEconomicaTO>  obtenerEvaluacionesEdificioPorParametro(int idEdificio ,  int idParametros){
		List<EvaluacionEconomicaTO> resp = null;
		Connection c = DBMannager.getConnection();
		FacadeEvaluacionEconomica f = new FacadeEvaluacionEconomica();
		
		try{
			
			resp = f.obtenerEvaluacionesEdificioPorParametro(c , idEdificio, idParametros); 
			
			boolean exito = (resp!=null);
			
			DBMannager.close(c, exito);
			
		}catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
	}
	public List<EvaluacionEconomicaTO> obtenerEvaluacionesEdificio(int idEdificio){
		List<EvaluacionEconomicaTO> resp = null;
		Connection c = DBMannager.getConnection();
		FacadeEvaluacionEconomica f = new FacadeEvaluacionEconomica();
		
		try{
			
			resp = f.obtenerEvaluacionesEdificio(c , idEdificio); 
			
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
			
		}catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
	}
	public EvaluacionEconomicaTO obtenerEvaluacionPorID(int idAlternativa , int idParametro){
		EvaluacionEconomicaTO resp = null;

		
		Connection c = DBMannager.getConnection();
		FacadeEvaluacionEconomica f = new FacadeEvaluacionEconomica();
		
		try{
			
			resp = f.obtenerEvaluacionEconomica(idAlternativa, idParametro, c);
			
			boolean exito = (resp!=null);
			
			DBMannager.close(c, exito);
			
		}catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
	}
	public List<FlujosEvaluacionTO> obtenerFlujos(ParametrosEvaluacionTO parametro, AlternativaDeInversionTO alternativa, int idEdificio){
		List<FlujosEvaluacionTO> resp = null;
		
		Connection c = DBMannager.getConnection();
		
		
	try{
			
		FacadeRequerimientoEnergetico facadeReq = new FacadeRequerimientoEnergetico(); 
		RequerimientoEnergeticoTO req = facadeReq.obtenerRequerimientoEnergeticoEdificio(idEdificio, c);

		EvaluacionEconomicaUtil e = new EvaluacionEconomicaUtil();
		resp = e.obtenerFlujos(alternativa, parametro,  req);

		boolean exito = (resp!=null);

		DBMannager.close(c, exito);
			
		}catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
	}
}

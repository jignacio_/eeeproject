package cl.foursoft.eee.service;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.facade.FacadeAlternativaInversion;
import cl.foursoft.eee.dao.implementation.DBMannager;
import cl.foursoft.eee.dao.transferObject.AlternativaDeInversionTO;
import cl.foursoft.eee.dao.transferObject.TipoAlternativaTO;
import cl.foursoft.eee.util.UtilLog;
import cl.foursoft.eee.util.tipoAlternativaUtil;

public class AlternativaInversionService {
	
	
	public int guardarAlternativaEconomicaPorTipo(AlternativaDeInversionTO alternativa, int idTipo){
		int resp = -1;
		
		Connection c = DBMannager.getConnection();
		FacadeAlternativaInversion f = new FacadeAlternativaInversion();

		try{
			if(idTipo == tipoAlternativaUtil.tipoAlternativaEnergetica)
				resp = f.guardarAlternativaEconomicaEnergetica(c, alternativa);//alternativa economica de envolvente
			else if(idTipo == tipoAlternativaUtil.tipoAlternativaArtefacto){
				resp = f.guardarAlternativaDeArtefactos(c, alternativa);
			}
			
			boolean exito = (resp>-1);
			if(exito) c.commit();
			else c.rollback();
			
			DBMannager.close(c, exito);
			
		}catch(Exception e){
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
	}
	public AlternativaDeInversionTO obtenerAlternativaPorId(int idAlternativa, int idEdifico){
		AlternativaDeInversionTO resp = null;
		
		Connection c = DBMannager.getConnection();
		FacadeAlternativaInversion f = new FacadeAlternativaInversion();
		try {
			
			resp = f.obtenerAlternativaPorId(idAlternativa, idEdifico,  c);
			boolean exito = (resp != null);
			
			DBMannager.close(c, exito);
			
		} catch (Exception e) {
			UtilLog.registrar(e);
		}
		
		return resp;
	}
	public List<AlternativaDeInversionTO> obtenerAlternativasPorEdificio(int idEdificio){

		List<AlternativaDeInversionTO> resp = null;
		
		
		Connection c = DBMannager.getConnection();
		FacadeAlternativaInversion f = new FacadeAlternativaInversion();
		try{
			resp = f.obtenerAlternativasEdificio(idEdificio, c);

			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
		}catch(Exception e){
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		return resp;
	}
	public int eliminarAlternativaPorId(int idAlternativa){
		int resp = -1;
			
			Connection c = DBMannager.getConnection();
			FacadeAlternativaInversion f = new FacadeAlternativaInversion();
			try {
				resp = f.eliminarAlternativa(idAlternativa, c);
				
				boolean exito = (resp>-1);
				if(exito) c.commit();
				else c.rollback();
				
				DBMannager.close(c, exito);
				
			} catch (Exception e) {
				UtilLog.registrar(e);
				DBMannager.closeOnError(c);
			}
		
		return resp;
	}
	
	public List<TipoAlternativaTO> obtenerTiposAlternativas(){
		List<TipoAlternativaTO> resp = null;


		Connection c = DBMannager.getConnection();
		FacadeAlternativaInversion f = new FacadeAlternativaInversion();
		try{
			
			resp = f.obtenerTiposAlternativas( c);

			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
		}catch(Exception e){
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		return resp;
	}

}

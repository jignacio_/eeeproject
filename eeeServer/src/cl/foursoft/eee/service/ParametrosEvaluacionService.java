package cl.foursoft.eee.service;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.facade.FacadeParametrosEvaluacion;
import cl.foursoft.eee.dao.implementation.DBMannager;
import cl.foursoft.eee.dao.transferObject.ParametrosEvaluacionTO;
import cl.foursoft.eee.util.UtilLog;

public class ParametrosEvaluacionService {

	public ParametrosEvaluacionService() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public List<ParametrosEvaluacionTO> obtenerTodosParametrosEvaluacion(){
		List<ParametrosEvaluacionTO> resp = null;
	
		Connection c = DBMannager.getConnection();
		FacadeParametrosEvaluacion f = new FacadeParametrosEvaluacion();
		
		try{
			resp = f.obtenerTodosParametrosEvaluacion(c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
		}
		catch (Exception e) {
			UtilLog.registrar(e);
		}
		
		return resp;
	}
	
	public ParametrosEvaluacionTO obtenerParametroEvaluacionPorId(){
		ParametrosEvaluacionTO resp = null;
		
		Connection c = DBMannager.getConnection();
		FacadeParametrosEvaluacion f = new FacadeParametrosEvaluacion();
		
		try{
			resp = f.obtenerParametroEvaluacionPorId(c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
		}
		catch (Exception e) {
			UtilLog.registrar(e);
		}
		return resp;
	}
	
	public int guradrParametroEvaluacion(ParametrosEvaluacionTO parametros){
		
		int resp = -1;
		
		Connection c = DBMannager.getConnection();
		FacadeParametrosEvaluacion f = new FacadeParametrosEvaluacion();
		resp = f.guradrParametroEvaluacion(c, parametros);
		return resp;
	}
	
	
	
	
}

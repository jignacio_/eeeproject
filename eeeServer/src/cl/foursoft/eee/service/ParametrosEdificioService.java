package cl.foursoft.eee.service;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.facade.FacadeParametrosEdificio;
import cl.foursoft.eee.dao.implementation.DBMannager;
import cl.foursoft.eee.dao.transferObject.ParametrosEdificioTO;
import cl.foursoft.eee.util.UtilLog;

public class ParametrosEdificioService {
	public List<ParametrosEdificioTO> obtenerParametros(){
		Connection c =  DBMannager.getConnection();
		FacadeParametrosEdificio f = new FacadeParametrosEdificio();
		List<ParametrosEdificioTO> resp = null;  

		try{	
			resp = f.obtenerParametros(c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		} 
		
		return resp;
	}
	
	public boolean editarParametros(List<ParametrosEdificioTO> parametros){
		Connection c =  DBMannager.getConnection();
		FacadeParametrosEdificio f = new FacadeParametrosEdificio();
		boolean resp = false;  

		try{
			resp = f.editarParametros(parametros, c);
			DBMannager.close(c, resp);
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		} 
		
		return resp;
	}
}

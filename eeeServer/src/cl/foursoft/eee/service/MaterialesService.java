package cl.foursoft.eee.service;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.facade.FacadeMateriales;
import cl.foursoft.eee.dao.implementation.DBMannager;
import cl.foursoft.eee.dao.transferObject.MaterialTO;
import cl.foursoft.eee.util.UtilLog;

public class MaterialesService {
	

		public List<MaterialTO> obtenerTodosLosMateriales(){
			List<MaterialTO> resp = null;
			
			
			Connection c = DBMannager.getConnection();
			FacadeMateriales f = new FacadeMateriales();
			try{
				resp = f.obtenerTodosLosMateriales(c);
				boolean exito = (resp!=null);
				DBMannager.close(c, exito);
			}
			catch (Exception e) {
				UtilLog.registrar(e);
				DBMannager.closeOnError(c);
			}
			
			return resp;
		}
		
		public List<MaterialTO> obtenerMaterialesPorTipoSeccion(int idSeccion){
			List<MaterialTO> resp = null;
			
			
			Connection c = DBMannager.getConnection();
			FacadeMateriales f = new FacadeMateriales();
			try{
				resp = f.obtenerMaterialesPorTipoSeccion(idSeccion,c);
				boolean exito = (resp!=null);
				DBMannager.close(c, exito);
			}
			catch (Exception e) {
				UtilLog.registrar(e);
				DBMannager.closeOnError(c);
			}
			
			return resp;
		}
		
		public int guardarMaterial(List<MaterialTO> materiales){
			int resp = -1;
			
			Connection c = DBMannager.getConnection();
			FacadeMateriales f = new FacadeMateriales();
			
			try{
			//	if(material.getIdMaterial()<0)
				resp = f.guardarMaterial(materiales, c);
				
				
				boolean exito= (resp>0);
				if(exito) c.commit();
				else c.rollback();
				
				DBMannager.close(c, exito);
				
			}catch (Exception e) {
				UtilLog.registrar(e);
				DBMannager.closeOnError(c);
			}
			
			return resp;
			
		}
		public int eliminarMaterial(int idMaterial){
			int resp = -1;
			
			
			Connection c = DBMannager.getConnection();
			FacadeMateriales f = new FacadeMateriales();
			
			try{
				resp = f.eliminarMaterial(idMaterial, c);
				
				
				boolean exito= (resp>0);
				DBMannager.close(c, exito);
				
			}catch (Exception e) {
				// TODO: handle exception
				UtilLog.registrar(e);
				DBMannager.closeOnError(c);
			}
			
			return resp;
		}

}

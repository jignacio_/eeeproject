package cl.foursoft.eee.service;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.facade.FacadeEdificio;
import cl.foursoft.eee.dao.implementation.DBMannager;
import cl.foursoft.eee.dao.transferObject.EdificioTO;
import cl.foursoft.eee.dao.transferObject.ParametrosEdificioTO;
import cl.foursoft.eee.dao.transferObject.ParametrosEvaluacionTO;
import cl.foursoft.eee.util.UtilLog;

public class EdificioService {
	
	public EdificioTO obtenerEdificioPorId(int idEdificio){
		
		Connection c =  DBMannager.getConnection();
		FacadeEdificio f = new FacadeEdificio();
		EdificioTO resp = null; 
		
		try{
			resp = f.obtenerEdificio(idEdificio, c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
 
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		} 
		
		return resp;
	}
	public EdificioTO obtenerEdificioVistaDetalle(int idEdificio){
		
		Connection c =  DBMannager.getConnection();
		FacadeEdificio f = new FacadeEdificio();
		EdificioTO resp = null; 
		
		try{
			resp = f.obtenerEdificioVistasDetalle(idEdificio, c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
 
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		} 
		
		return resp;
	}
	public List<EdificioTO> obtenerTodosLosEdificios(int idCliente){
		Connection c =  DBMannager.getConnection();
		FacadeEdificio f = new FacadeEdificio();
		List<EdificioTO> resp = null;
		
		try{
			resp = f.obtenerTodosLosEdificios(idCliente, c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
		}
		catch(Exception e){
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
	}
	
	public List<EdificioTO> obtenerTodosLosEdificiosCompletos(int idCliente){
		Connection c =  DBMannager.getConnection();
		FacadeEdificio f = new FacadeEdificio();
		List<EdificioTO> resp = null;
		
		try{
			resp = f.obtenerTodosLosEdificiosCompletos(idCliente, c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
		}
		catch(Exception e){
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
	}
	public int guardarEdificio(EdificioTO edificioNuevo){
		int resp=-1;
		
		Connection c = DBMannager.getConnection();
		FacadeEdificio f = new FacadeEdificio();
		
		try{
			if(edificioNuevo.getIdEdificio()<0)
				resp = f.guardarEdificio(edificioNuevo, c);
			else
				resp = f.modificarEdificio(edificioNuevo, c);
				
			boolean exito = (resp!=-1);
			
			if(exito) c.commit();
			else  c.rollback();
			
			DBMannager.close(c, exito);
		}
		catch(Exception e){
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		//calcular req energetico
		return resp;
	}
	public int guardarCalculoInversionEdificio(int idEdificio){
		int resp = -1;
		Connection c = DBMannager.getConnection();
		FacadeEdificio f = new FacadeEdificio();
		
		try {
			resp  = f.guardarCalculoInversionEdificio(idEdificio, c);
			
			boolean exito = (resp>0);
			
			if(exito) c.commit();
			else c.rollback();
			
			DBMannager.close(c, exito);
			
		} catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
	}
	public int eliminarEdificio(int idEdificio){
		
		Connection c =  DBMannager.getConnection();
		FacadeEdificio f = new FacadeEdificio();
		int resp = -1; 
		
		try{
			resp = f.eliminarEdificio(idEdificio, c);
			boolean exito = (resp>-1);
			if(exito) c.commit();
			else  c.rollback();
			
			DBMannager.close(c, exito);
 
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		} 
		
		return resp;
	}
	public List<ParametrosEdificioTO> obtenerParametrosEdificioPorId(int idEdificio){
		Connection c =  DBMannager.getConnection();
		FacadeEdificio f = new FacadeEdificio();
		List<ParametrosEdificioTO> resp = null; 
		
		try{
			resp = f.obtenerParametrosEdificioPorId(idEdificio, c);
			boolean exito = (resp!=null);
			if(exito) c.commit();
			else  c.rollback();
			
			DBMannager.close(c, exito);
 
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		} 
		
		return resp;
	}
	
	
	//Cambiar de Service a uno de parametros
	
	public int guardarParametros(ParametrosEvaluacionTO parametros){
		int resp = -1;
		
		
		
		Connection c = DBMannager.getConnection();
		FacadeEdificio f = new FacadeEdificio();

		try {
			
			resp = f.guardarParametros(parametros, c);
			
			boolean exito = (resp!=-1);
			
			if(exito) c.commit();
			else  c.rollback();
			
			DBMannager.close(c, exito);
					
		} catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		 
		
		return resp;
	}
    public List<ParametrosEvaluacionTO> obtenerParametrosCliente(int idCliente){
    	List<ParametrosEvaluacionTO> resp = null;
    	
    	
    	Connection c =  DBMannager.getConnection();
		FacadeEdificio f = new FacadeEdificio();
 
		try{
			resp = f.obtenerParametrosCliente(idCliente, c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
		}
		catch(Exception e){
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		} 
		return resp;
    
    }
    
    public int eliminarParametroCliente(int idParametro, int idCliente){
    	int resp = -1;
    	Connection c = DBMannager.getConnection();
    	FacadeEdificio f = new FacadeEdificio();
    	try {
			resp = f.eliminarParametroCliente(idParametro, idCliente, c);
			boolean exito = (resp!=-1);
			
			if(exito) c.commit();
			else  c.rollback();
			
			
			DBMannager.close(c, exito);
    		
		} catch (Exception e) {
				UtilLog.registrar(e);
				DBMannager.closeOnError(c);
		}
    	
    	
    	return resp;
    }
    //Actualizar
    public int actualizarEdificio(EdificioTO edificioActualizar){
    	int resp = -1;
    	FacadeEdificio f = new FacadeEdificio();
    	Connection c = DBMannager.getConnection();
    	try {
			resp = f.actualizarEdificio(edificioActualizar,c);
			
			resp = (resp > -1) ? edificioActualizar.getIdEdificio() : -1;
			boolean exito = (resp>-1);
			
			if(exito) c.commit();
			else  c.rollback();

			DBMannager.close(c, exito);
		} catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
    
		return resp;
    }
    
}
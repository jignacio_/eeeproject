package cl.foursoft.eee.service;

import java.sql.Connection;
import java.util.List;

import javax.print.DocFlavor.STRING;

import cl.foursoft.eee.dao.facade.FacadeSensor;
import cl.foursoft.eee.dao.implementation.DBMannager;
import cl.foursoft.eee.dao.transferObject.TipoMedicionConsumoSensorTO;
import cl.foursoft.eee.util.UtilLog;

public class SensorService {
	
	
	public List<TipoMedicionConsumoSensorTO> obtenerTodosLosTiposDeSensores(){
		List<TipoMedicionConsumoSensorTO> resp = null;
		Connection c = DBMannager.getConnection();
		FacadeSensor facadeSensor = new FacadeSensor();


		try{		
			resp = facadeSensor.obtenerTodosLosTiposDeSensores(c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
		}catch(Exception e){
			UtilLog.registrar(e);
		}
		
		return resp;
		
	}
	public List<TipoMedicionConsumoSensorTO> obtenerTodosLosTiposDeSensoresAdmin(){
		List<TipoMedicionConsumoSensorTO> resp = null;
		Connection c = DBMannager.getConnection();
		FacadeSensor facadeSensor = new FacadeSensor();
		
		
		try{		
			resp = facadeSensor.obtenerTodosLosTiposDeSensoresAdmin(c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
		}catch(Exception e){
			UtilLog.registrar(e);
		}
		
		return resp;
		
	}
	public List<TipoMedicionConsumoSensorTO> obtenerTipoDeConsumo(String idTipoMedicion ){
		List<TipoMedicionConsumoSensorTO> resp = null;
		Connection c = DBMannager.getConnection();
		FacadeSensor facadeSensor = new FacadeSensor();
		
		
		try{		
			resp = facadeSensor.obtenerTipoDeConsumo(idTipoMedicion, c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
		}catch(Exception e){
			UtilLog.registrar(e);
		}
		
		return resp;
		
	}
	public TipoMedicionConsumoSensorTO obtenerTipoMedcion(String idTipoMedicion ){
		TipoMedicionConsumoSensorTO resp = null;
		Connection c = DBMannager.getConnection();
		FacadeSensor facadeSensor = new FacadeSensor();
		
		
		try{		
			resp = facadeSensor.obtenerTipoMedicion(idTipoMedicion, c);
			boolean exito = (resp!=null);
			DBMannager.close(c, exito);
		}catch(Exception e){
			UtilLog.registrar(e);
		}
		
		return resp;
		
	}
	
	//Eliminador L�gico	
	public boolean eliminarTipoConsumo(TipoMedicionConsumoSensorTO _sensor){
		Connection c =  DBMannager.getConnection();
		FacadeSensor f = new FacadeSensor();
		boolean resp = false;  
		
		try{	
			//resp = f.guardarUsuarios(usuarios, c);
		//	resp = true;
			
			resp = f.eliminarSensor(_sensor , c);
			
			if(resp) c.commit();
			else c.rollback();
			
			DBMannager.close(c, resp);
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		} 
		return resp;
	}
	public boolean guardarSensor(TipoMedicionConsumoSensorTO sensor,  String idTipoMedicion){
		Connection c =  DBMannager.getConnection();
		FacadeSensor f = new FacadeSensor();
		boolean resp = false;  

		 
		try{
			resp = f.guardarSensor(sensor,  idTipoMedicion , c);
			
			if(resp) c.commit();
			else c.rollback();
			
			DBMannager.close(c, resp);
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		} 
		
		return resp;
	}
	public boolean actualizarSensores(List<TipoMedicionConsumoSensorTO> sensores, String idTipoMedicion){
		Connection c =  DBMannager.getConnection();
		FacadeSensor f = new FacadeSensor();
		boolean resp = false;  
		 
		try{	
			//resp = f.guardarUsuarios(usuarios, c);
			//	resp = true;
			
			resp = f.actualizarSensores(sensores, idTipoMedicion, c);
			
			if(resp) c.commit();
			else c.rollback();
			
			DBMannager.close(c, resp);
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		} 
		
		return resp;
	}
	//Guardar Sensor
	//Eliminar Sensror

}

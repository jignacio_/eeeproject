package cl.foursoft.eee.service;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.facade.FacadeCliente;
import cl.foursoft.eee.dao.implementation.DBMannager;
import cl.foursoft.eee.dao.transferObject.ClienteTO;
import cl.foursoft.eee.util.UtilLog;

public class ClientesService {
	
	public List<ClienteTO> obtenerTodosLosClientes(){
		List<ClienteTO> resp = null ;
		FacadeCliente f = null;
		
		Connection c = DBMannager.getConnection();
		
		try {
			f = new FacadeCliente();
			resp = f.obtenerTodosLosClientes(c);
			boolean exito = (resp != null);
			DBMannager.close(c, exito);
			
			
		} catch (Exception e) {
				UtilLog.registrar(e);
				DBMannager.closeOnError(c);
		}
		
		
		return resp;
	}

}

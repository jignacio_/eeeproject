package cl.foursoft.eee.service;

 

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.facade.FacadeMedicion;
import cl.foursoft.eee.dao.implementation.DBMannager;
import cl.foursoft.eee.dao.transferObject.MedicionTO;
import cl.foursoft.eee.dao.transferObject.VistaParaGestionTO;
import cl.foursoft.eee.util.UtilLog;

public class MedicionService {

		//int idEdificio, String idConsumo, String idSensor
	
	//// VISTAS PARA GESTI�N	
	
	public List<MedicionTO> obtenerMedicionEdificioTipoConsumo(int idEdificio, String idMedicion){
		List<MedicionTO> resp =  null;
		
		Connection c = DBMannager.getConnection();
		FacadeMedicion fm = new FacadeMedicion();
		
		try{
			resp = fm.obtenerMedicionEdificioTipoConsumo(idEdificio, idMedicion,  c);
			boolean exito = (resp!=null);
			
			DBMannager.close(c, exito);
			
		}catch(Exception e){
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
	}
	
	public List<MedicionTO> obtenerAlertas(int idEdificio){
		List<MedicionTO> resp = null;
		
		Connection c = DBMannager.getConnection();
		FacadeMedicion fm = new FacadeMedicion();
		
		try{
			resp = fm.obtenerAlertas(idEdificio,  c);
			boolean exito = (resp!=null);
			
			DBMannager.close(c, exito);
			
		}catch(Exception e){
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
	 
		return resp;
		
	}
	public List<MedicionTO> obtenerMedidasPiso(String piso, String idConsumo){
		List<MedicionTO> resp = null;
		
		return resp;
	}
	
	
	public List<MedicionTO> obtenerMedidasEdificio(String idSensor,String idConsumo){
		List<MedicionTO> resp = null;
		
		
		return resp;
	}
	
	public List<MedicionTO> obtenerMedidasZona(int zona , String idConsumo){
		List<MedicionTO> resp = null;
		
		
		return resp;
	}
	public List<MedicionTO> obtenerDatosRango(VistaParaGestionTO peticion){
		List<MedicionTO> resp = null;
		
		Connection c = DBMannager.getConnection();
		FacadeMedicion fm = new FacadeMedicion();
		
		try{
			resp = fm.obtenerDatosRango(peticion,  c);
			boolean exito = (resp!=null);
			
			DBMannager.close(c, exito);
			
		}catch(Exception e){
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
	}

	//// VISTA DETALLE
	public List<MedicionTO> obtenerDatosMedicionHoy(String idMedicion, String idConsumo, int idEdificio , int idPiso , int idZana, String idSensor , int minutos){
		List<MedicionTO> resp = null;
		Connection c = DBMannager.getConnection();
		FacadeMedicion fmedicion = new FacadeMedicion();
		try{
			resp = fmedicion.obtenerDatosMedicionHoy(idMedicion, idConsumo ,idEdificio, idPiso , idZana ,  idSensor, minutos,  c);
			boolean exito = (resp!=null);

			DBMannager.close(c, exito);
		}catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
	}
	

	public List<MedicionTO> totalConsumos(String idMedicion, int idEdificio , int idPiso , int idZona , String idSensor , String periodo){
		List<MedicionTO> resp = null;
		
		Connection c = DBMannager.getConnection();
		FacadeMedicion fmedicion = new FacadeMedicion();
		try{
			resp = fmedicion.totalConsumos(idMedicion, idEdificio, idPiso, idZona, idSensor, periodo, c);
			boolean exito = (resp!=null);

			DBMannager.close(c, exito);
		}catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
		
	}
}

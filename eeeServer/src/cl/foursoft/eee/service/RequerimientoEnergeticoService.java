package cl.foursoft.eee.service;

import java.sql.Connection;

import cl.foursoft.eee.dao.facade.FacadeAlternativaInversion;
import cl.foursoft.eee.dao.facade.FacadeEdificio;
import cl.foursoft.eee.dao.facade.FacadeRequerimientoEnergetico;
import cl.foursoft.eee.dao.implementation.DBMannager;
import cl.foursoft.eee.dao.transferObject.AlternativaDeInversionTO;
import cl.foursoft.eee.dao.transferObject.EdificioTO;
import cl.foursoft.eee.dao.transferObject.RequerimientoEnergeticoTO;
import cl.foursoft.eee.util.UtilLog;
import cl.foursoft.eee.util.CalculosMatematicos.RequerimientoEnergeticoUtil;

public class RequerimientoEnergeticoService {

	public int guardarRequerimientoEnergetico(int idEdificio){
		int resp = -1;
		RequerimientoEnergeticoTO requerimento = null;
		
		EdificioService edificioService = new EdificioService();
		EdificioTO edificio =  edificioService.obtenerEdificioPorId(idEdificio);
		
		
		
		/*CALCULO DEL REQUERIMIENTO ENERG�TICO*/
		RequerimientoEnergeticoUtil req = new RequerimientoEnergeticoUtil(edificio);
		requerimento = req.calcularRequerimientoEnergetico();
		requerimento.setIdEdificio(idEdificio);
		
		
		Connection c = DBMannager.getConnection();
		FacadeRequerimientoEnergetico f = new FacadeRequerimientoEnergetico();
		
		try{
			resp = f.guardarRequerimientoEnergeticoEdificio(requerimento, c);
			
			boolean exito = (resp>0) ;
			
			if(exito) c.commit();
			else  c.rollback();
			
			DBMannager.close(c, exito);
		}
		catch(Exception e){
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		
		return resp;
	}
	//Evaluacion de envolvente
	public int guardarRequerimientoEnergeticoAlternativa(int idEdificio, int idAlternativa){
		int resp = -1;
		RequerimientoEnergeticoTO requerimiento = null;
		
		Connection c = DBMannager.getConnection();
		
		
		//Obtener Edificio
		FacadeEdificio facadeEdificio = new FacadeEdificio();
		EdificioTO edificioActual = facadeEdificio.obtenerEdificioParaAlternativa(idEdificio, c); // Obtener las secciones para guardar RSE y RSi de cada seccion
		
		//Obtener alternativa para la cual se calcular� el requerimiento energetico.
		FacadeAlternativaInversion alternativaFacade = new FacadeAlternativaInversion(); //llamar al facade
		AlternativaDeInversionTO alternativa = alternativaFacade.obtenerAlternativaPorId(idAlternativa, idEdificio, c);
		
		
		//
		RequerimientoEnergeticoUtil req = new RequerimientoEnergeticoUtil(alternativa, edificioActual);
		requerimiento = req.calcularRequerimientoEnergetico();
		requerimiento.setIdEdificio(idEdificio);
		requerimiento.setIdAlternativa(idAlternativa);

		
		FacadeRequerimientoEnergetico f = new FacadeRequerimientoEnergetico();
		
		try{
				resp = f.guardarRequerimientoEnergeticoAlternativa(requerimiento,c); 
				
			boolean exito = (resp>0);
			
			if(exito) c.commit();
			else  c.rollback();
			
			DBMannager.close(c, exito);
		}
		catch(Exception e){
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		
		return resp;
	}
	
	public RequerimientoEnergeticoTO obtenerRequerimientoEnergeticoEdificio(int idEdificio){
		RequerimientoEnergeticoTO resp = null;
		
		Connection c  = DBMannager.getConnection();
		FacadeRequerimientoEnergetico f = new FacadeRequerimientoEnergetico();
		try{
			
			resp = f.obtenerRequerimientoEnergeticoEdificio(idEdificio, c);
			
			boolean exito = (resp!=null);
			DBMannager.close(c, exito );
			
		}catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
	}
	
	
	public RequerimientoEnergeticoTO obtenerRequerimientoEnergeticoAlternativa(int idEdificio, int idAlternativa){
	RequerimientoEnergeticoTO resp = null;
		
		Connection c  = DBMannager.getConnection();
		FacadeRequerimientoEnergetico f = new FacadeRequerimientoEnergetico();
		try{
			
			resp = f.obtenerRequerimientoEnergeticoAlternativa(idEdificio, idAlternativa, c);
			
			boolean exito = (resp!=null);
			DBMannager.close(c, exito );
			
		}catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
	}
	public float obtenerValorRequerimientoTotal(int idEdificio, int idAlternativa){ // Solo deber�a obtener con el id de la alternativa
		float resp = -1f;
		
		Connection c  = DBMannager.getConnection();
		FacadeRequerimientoEnergetico f = new FacadeRequerimientoEnergetico();
		try{
			
			resp = f.obtenerValorRequerimientoTotal(idEdificio, idAlternativa, c);
			
			boolean exito = (resp>0);
			DBMannager.close(c, exito );
			
		}catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
	}
}

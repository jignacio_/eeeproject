package cl.foursoft.eee.service;

import java.sql.Connection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Locale;

import cl.foursoft.eee.dao.facade.FacadeSistema;
import cl.foursoft.eee.dao.implementation.DBMannager;
import cl.foursoft.eee.dao.transferObject.EmailTO;
import cl.foursoft.eee.util.UtilLog;

public class SistemaService {

	
	
	public String obtenerIPSincronizacion(){
		String resp = "";
		Connection c =  DBMannager.getConnection();
		FacadeSistema facadeSistema;
		try {
			facadeSistema = new FacadeSistema();
			
			resp = facadeSistema.obtenerIPSincronizacion(c);
			
			
			DBMannager.close(c, true);
		} catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		return resp;
		
	}
	public boolean gurdarIpSincronizacion(String ip){
		boolean resp = false;
		Connection c = DBMannager.getConnection();
		FacadeSistema facade = new FacadeSistema();
		
		try {
			
			resp = facade.gurdarIpSincronizacion(c, ip);
			
			if(resp) c.commit();
			else c.rollback();
			
			DBMannager.close(c, true);
		} catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c); 
		}
		return resp;
	}
	public EmailTO obtenerDatosEmail(){
		EmailTO resp = null;
		Connection c = DBMannager.getConnection();
		FacadeSistema facade = new FacadeSistema();
		
		try {
			
			resp = facade.obtenerDatosEmail(c);
			
			
			DBMannager.close(c, true);
		} catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c); 
		}
		return resp;
	}
	public boolean guardarDatosEmail(EmailTO email){
		boolean resp = false;
		Connection c = DBMannager.getConnection();
		FacadeSistema facade = new FacadeSistema();
		
		try {
			
			resp = facade.guardarEmail(c, email);
			
			if(resp) c.commit();
			else c.rollback();
			
			
			DBMannager.close(c, true);
		} catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c); 
		}
		return resp;
	}
	public String obtenerFechaUltimaSincronizacion(){
		Timestamp fecha=null;
		String resp="";
		
		Connection c = DBMannager.getConnection();
		FacadeSistema facade;
		try {
			facade = new FacadeSistema();
			
			fecha = facade.obtenerFechaUltimaSincronizacion(c);
			 SimpleDateFormat formateador = new SimpleDateFormat("EEEE dd 'de' MMMM 'de' yyyy , HH:mm", new Locale("ES"));
			 resp =  formateador.format(fecha);
			
			DBMannager.close(c, true);
		} catch (Exception e) {
			UtilLog.registrar(e);
		}
		
		return resp;
	}
	public boolean guardarFechaUltimaSincro() {
		  boolean resp=false;

		Connection c = DBMannager.getConnection();
		FacadeSistema facade;
		try {
			facade = new FacadeSistema();

			resp = facade.guardarFechaUltimaSincronizacion(c);
			
			if(resp) c.commit();
			else c.rollback();
			
			DBMannager.close(c, true);
		} catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c); 
		}

		return resp;
	}
}

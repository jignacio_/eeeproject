package cl.foursoft.eee.service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import cl.foursoft.eee.dao.facade.FacadeArtefactos;
import cl.foursoft.eee.dao.implementation.DBMannager;
import cl.foursoft.eee.dao.transferObject.ArtefactoCombustibleDetalleTO;
import cl.foursoft.eee.dao.transferObject.ArtefactoTO;
import cl.foursoft.eee.util.UtilLog;

public class ArtefactoService {
	
	public List<ArtefactoTO> obtenerTodosLosArtefactos(){
		List<ArtefactoTO> resp = null;
		
		Connection c = DBMannager.getConnection();
		FacadeArtefactos facade = new FacadeArtefactos();
		try {
			
			resp = facade.obtenerTodosLosArtefactos(c);
			
			DBMannager.close(c, true);
		} catch (Exception e) {
				UtilLog.registrar(e);
				DBMannager.close(c, false);
		}
		return resp;
	}
	//-- metodos para Artefactos detalle combustible(GUARDAR Y ACTUAZLIZAR)
	public int guardarArtefactoCombustibleDetalle(ArtefactoCombustibleDetalleTO artefacto){
		int resp= -1;
		
		Connection c = DBMannager.getConnection();
		FacadeArtefactos facade = new FacadeArtefactos();
		try {
			
			resp = facade.guardarArtefactoCombustibleDetalle(c, artefacto);
			
			if(resp>0) c.commit();
			else c.rollback();
			
			DBMannager.close(c, true);
			
		} catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		
		return resp;
	}
	
	public ArtefactoCombustibleDetalleTO obtenerArtefactoCombustibleCompleto(int idArtefacto){
		ArtefactoCombustibleDetalleTO resp = null;
		
		Connection c = DBMannager.getConnection();
		FacadeArtefactos facade = new FacadeArtefactos();
		try {
			
			resp = facade.obtenerArtefactoCombustibleCompleto(c, idArtefacto);
			
			DBMannager.close(c, true);
		} catch (Exception e) {
				UtilLog.registrar(e);
				DBMannager.close(c, false);
		}
		return resp;
	}
	//
	public ArrayList<ArtefactoTO> obtenerArtefactosPorMedicion(String idMedicion){
		ArrayList<ArtefactoTO> resp = null;
		
		
		Connection c = DBMannager.getConnection();
		FacadeArtefactos fartefacto = null;
		try{
			fartefacto = new FacadeArtefactos();
			resp = fartefacto.obtenerArtefactosPorMedicion(idMedicion, c);
			
			boolean exito = (resp!=null) ? true : false;
			DBMannager.close(c, exito);
			
		}catch (Exception e) {
				UtilLog.registrar(e);
				DBMannager.closeOnError(c);
		}
		
		
		return resp;
	}

	public int guardarArtefacto(ArtefactoTO a){
		int resp = -1;
		
		Connection c = DBMannager.getConnection();
		FacadeArtefactos facade =  new FacadeArtefactos();
		
		try{
			
			resp = facade.guardarArtefacto(c, a);
			
			
			if(resp>-1) c.commit();
			else c.rollback();
			
			DBMannager.close(c, true);
		}catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		}
		
		
		
		return resp;
	}

	public boolean eliminarArtefacto(int idArtefacto){
		boolean resp = false;
		FacadeArtefactos facade;
		
		Connection c = DBMannager.getConnection();
		try{
			facade = new FacadeArtefactos();
			resp = facade.eliminarArtefacto(idArtefacto, c);
			
			if(resp) c.commit();
			else c.rollback();
		}catch(Exception e){
			UtilLog.registrar(e);
		}

		return resp;	

	}
	public boolean guardarComposicion(ArrayList<ArtefactoTO> artefactos, String idSensor){
		boolean resp = false;
		
		Connection c = DBMannager.getConnection();
		FacadeArtefactos fartefactos = null;
		try {
			fartefactos = new FacadeArtefactos();
			resp = fartefactos.guardarComposicionArtefactos(artefactos, idSensor, c);
			
			if(resp) c.commit();
			else c.rollback();
			
			
			DBMannager.close(c, resp);
			
		} catch (Exception e) {
				UtilLog.registrar(e);
				DBMannager.closeOnError(c);
		}
		
		return resp;	
	}
	public boolean actualizarComposicion(ArrayList<ArtefactoTO> artefactos, String idSensor){
		boolean resp = false;
		
		Connection c = DBMannager.getConnection();
		FacadeArtefactos fartefactos = null;
		try {
			fartefactos = new FacadeArtefactos();
			resp = fartefactos.actualizarComposicion(artefactos, idSensor, c);
			
			if(resp) c.commit();
			else c.rollback();
			
			
			DBMannager.close(c, resp);
			
		} catch (Exception e) {
				UtilLog.registrar(e);
				DBMannager.closeOnError(c);
		}
		
		return resp;	
	}
}

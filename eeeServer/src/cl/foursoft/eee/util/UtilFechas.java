package cl.foursoft.eee.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class UtilFechas {

	
	
	public static Timestamp hoy(){

		Timestamp resp;
		Calendar calendar = Calendar.getInstance();
		java.util.Date hoy = calendar.getTime();
		resp  =  new Timestamp(hoy.getTime()); 
		return resp;
	}
	
	
	public static String fechaHoyComoString(){
		  
		 Date ahora = new Date();
		 SimpleDateFormat formateador = new SimpleDateFormat("EEEE dd 'de' MMMM 'de' yyyy", new Locale("ES"));
		 return formateador.format(ahora);
	}
	
	public static String fechaComoString(Timestamp fecha){
		  
		 if(fecha!=null){
			 SimpleDateFormat formateador = new SimpleDateFormat("EEEE dd 'de' MMMM 'de' yyyy" , new Locale("ES"));
			 return formateador.format(fecha);
		 }
		 else return "";
	}
	public static String fechaHoraComoString(Timestamp fecha){
		  
		 if(fecha!=null){
			 SimpleDateFormat formateador = new SimpleDateFormat("EEEE dd 'de' MMMM 'de' yyyy '( Hora : ' HH:mm:ss ' )'" , new Locale("ES"));
			 return formateador.format(fecha);
		 }
		 else return "";
	}
	// Suma o resta las horas recibidos a la fecha  
	 public static Timestamp sumarMesesFecha(Date fecha, int meses){
		 
		 Timestamp nuevaFecha = null;
		 Calendar calendar = Calendar.getInstance();
		 calendar.setTime(fecha); // Configuramos la fecha que se recibe
		 calendar.add(Calendar.MONTH, meses);  // numero de horas a a�adir, o restar en caso de horas<0

		 Date f = calendar.getTime();
		 nuevaFecha = new Timestamp(f.getTime()); 
	      
	      return nuevaFecha; // Devuelve el objeto Date con las nuevas horas a�adidas
	
	 }

}

package cl.foursoft.eee.util;

public class UtilLog {
	
	public static String getNombreMetodo(){
        //Retorna el nombre del metodo desde el cual se hace el llamado
        return new Exception().getStackTrace()[1].getMethodName();
	}
	
	public static String getNombreClase(){
        //Retorna el nombre de la clase desde el cual se hace el llamado
        return new Exception().getStackTrace()[1].getClassName();
	}
	
	public static void registrar(Exception ex){
		System.out.println("[ERROR] [" + ex.getStackTrace()[1].getClassName() + "] ["+ ex.getStackTrace()[1].getMethodName() +"][" + ex.getMessage() + "]");
	}
}

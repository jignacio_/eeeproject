package cl.foursoft.eee.util;

import java.io.File;

public class DirectoriosImagen {
	
	final static public  String DIRECTORIO_DEFINITIVO_UNIX = System.getProperty("user.dir")+"/imagenes_edificios/";
	final static public  String DIRECTORIO_DEFINITIVO_WIN = System.getProperty("user.dir")+"\\imagenes_edificios\\";
	final static public  String DIRECTORIO_DEFINITIVO = System.getProperty("user.dir")+File.separator+"imagenes_edificios"+File.separator; 
	final static public  String DIRECTORIO_DEFINITIVO_MATERIALES = System.getProperty("user.dir")+File.separator+"imagenes_materiales"+File.separator; 
	final static public  String DIRECTORIO_DEFINITIVO_ARTEFACTOS = System.getProperty("user.dir")+File.separator+"imagenes_artefactos"+File.separator; 

	
	final static public  String DIRECTORIO_DEFINITIVO_MATERIALES_ADMIN = System.getProperty("user.dir")+File.separator+"imagenes_materiales"+File.separator; 
	final static public  String DIRECTORIO_DEFINITIVO_ARTEFACTOS_ADMIN = System.getProperty("user.dir")+File.separator+"imagenes_artefactos"+File.separator; 
}
	
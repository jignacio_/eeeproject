package cl.foursoft.eee.util;

import java.util.List;

import cl.foursoft.eee.dao.transferObject.SeccionTO;

public class CalculosUtil {
	
	
	public static List<SeccionTO> calcularSuperficieTotalSecciones(List<SeccionTO> secciones){
		
		for (SeccionTO seccionTO : secciones) {
			seccionTO.setArea( calcularSuperficieSeccion(seccionTO.getSeccionesHijo()) );
		}
		
		return secciones;
	}
	public static float calcularSuperficieSeccion(List<SeccionTO> secciones){
		float resp = 0;
		
		for (SeccionTO seccionTO : secciones) {
			resp +=seccionTO.getArea();
		}
		
		return resp;
	}
	public static String fomatId(int id){
		
		
		String id_string = Integer.toString(id);
		if(id_string.length()==1)
			return ( "0"+id_string);
		else if(id_string.length() == 2)
			return id_string;
		else 
			return "00";

	
				
	}

}

package cl.foursoft.eee.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cl.foursoft.eee.dao.transferObject.ArtefactoTO;
import cl.foursoft.eee.dao.transferObject.FactorRenovacionTO;
import cl.foursoft.eee.dao.transferObject.MaterialTO;
import cl.foursoft.eee.dao.transferObject.ParametrosEdificioTO;

public class Sincronizacion implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<MaterialTO> materiales;
	private List<ParametrosEdificioTO> parametros;
	private List<FactorRenovacionTO> factores;
	private List<ArtefactoTO> artefactos;
	
	public Sincronizacion(List<MaterialTO> materiales,
			List<ParametrosEdificioTO> parametros,
			List<FactorRenovacionTO> factores, List<ArtefactoTO> artefactos) {
		super();
		this.materiales = materiales;
		this.parametros = parametros;
		this.factores = factores;
		this.artefactos = artefactos;
	}
	
	public Sincronizacion() {
		super();
		this.materiales = new ArrayList<MaterialTO>();
		this.parametros = new ArrayList<ParametrosEdificioTO>();
		this.factores = new ArrayList<FactorRenovacionTO>();
		this.artefactos = new ArrayList<ArtefactoTO>();
	}

	public List<MaterialTO> getMateriales() {
		return materiales;
	}

	public void setMateriales(List<MaterialTO> materiales) {
		this.materiales = materiales;
	}

	public List<ParametrosEdificioTO> getParametros() {
		return parametros;
	}

	public void setParametros(List<ParametrosEdificioTO> parametros) {
		this.parametros = parametros;
	}

	public List<FactorRenovacionTO> getFactores() {
		return factores;
	}

	public void setFactores(List<FactorRenovacionTO> factores) {
		this.factores = factores;
	}

	public void setArtefactos(List<ArtefactoTO> artefactos) {
		this.artefactos = artefactos;
	}

	public List<ArtefactoTO> getArtefactos() {
		return artefactos;
	}

}

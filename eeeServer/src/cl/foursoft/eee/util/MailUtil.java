package cl.foursoft.eee.util;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import cl.foursoft.eee.dao.transferObject.UsuarioTO;
import cl.foursoft.eee.properties.Propiedades;

public class MailUtil {

    private String usuario;
    private String password;
    
    private String destinatario="";
    private String mensaje="";
    private String remitente="";
    private String pie="";
    private String asunto="";
    private  Session session ;
    
    public MailUtil(){
        
    	//Obtener datos desde la BD
    	remitente = "";
    	pie="";
        usuario = Propiedades.getProperty("SMTP_USER");
        password = Propiedades.getProperty("SMTP_PASS");
        
        //sendMail();
    }
    public boolean sendMail(){
        
        if(destinatario.isEmpty()!=false)
            destinatario = usuario;
        try
        {
            // Propiedades de la conexi�n
            Properties props = new Properties();
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.user", usuario);
            props.setProperty("mail.smtp.password", password);
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.auth", "true");

            // Preparamos la sesion
           //
            
            Session session = Session.getInstance(props,null);
//            Session session = Session.getInstance(props, 
//                  new javax.mail.Authenticator() {
//                  @Override
//                    protected PasswordAuthentication getPasswordAuthentication() {
//                            return new PasswordAuthentication(usuario, password);
//                    }
//              });     
            
            // Construimos el mensaje
            MimeMessage message = new MimeMessage(session);
            message.addRecipient(
                Message.RecipientType.TO,
                new InternetAddress(destinatario));
            
            message.setFrom(new InternetAddress("no-reply@Kipus.cl", remitente));
            message.setSubject(this.asunto);
            message.setText(this.mensaje , "UTF-8");
            	
            // Lo enviamos.
            Transport t = session.getTransport("smtp");
            t.connect(usuario, password);
            t.sendMessage(message, message.getAllRecipients());

           // Transport.send(message);
            // Cierre.
           t.close();
           
            
        }
        catch (Exception e){
            UtilLog.registrar(e);
            return false;
        }
        return true;
    }
    
    public boolean enviarAlerta(String mensaje){
        
        return false;
        
    }
    public void usuarioAgregado(String destinatario, String usuarioCambiado, String password){
        
        
        this.destinatario = destinatario;
        this.asunto = "Nueva Cuenta de Usuario";
        this.mensaje = "Usted ha sido agregaado como usuario del Sistema de Eficiencia Emerg�tica\nSu Usuario es : "+usuarioCambiado +"Su contrase�a es : " + password +"\n\n--\n"+pie;
        
        
        
        sendMail();
    }
    public void passwordCambiado(String destinatario, String usuarioCambiado, String password){
        
        
        this.destinatario = destinatario;
        this.asunto = "Su cuenta de usuario ha cambiado";
        this.mensaje = "Su Usuario es : " + usuarioCambiado +"\n"+"Su contrase�a es : " + password+"\n\n--\n"+pie;
        
        
        
        sendMail();
    }
    public boolean reenviarDatos(UsuarioTO user){
    	boolean resp = false;
    	
        this.destinatario = user.getEmail();
        this.asunto = "Datos de Ingreso";
        this.mensaje = "Hola " +user.getNombreUsuario() +" : \n Su Usuario es : " + user.getNombreUsuario() +"\n"+" Su contrase�a es : " + user.getContrasena()+"\n\n--\n"+pie;
        
        resp = sendMail();
        
    	return resp;
    	
    }
    
    //EMAIL POR ENVIAR
    /*
     * --Alerta de Gr�fico
     * --Alerta de Alternativa
     * */
 
    
    
}

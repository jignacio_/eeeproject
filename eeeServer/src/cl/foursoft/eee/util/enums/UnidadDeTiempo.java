package cl.foursoft.eee.util.enums;

public enum UnidadDeTiempo {
	milisegundo, segundo, minuto, hora, dia, semana, mes
}

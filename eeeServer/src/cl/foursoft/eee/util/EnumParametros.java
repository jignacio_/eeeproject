package cl.foursoft.eee.util;

public class EnumParametros {
	public static String FSU  = "FSU";
	public static String FVA  = "FVA";
//	public static String RSE  = "RSE";
//	public static String RSI  = "RSI";
	public static String PVP  = "PVP";
	public static String PVT  = "PVT";
	public static String FE  = "FE";
	public static String FG1  = "FG1";
	public static String FG2  = "FG2";
	public static String FPV  = "FPV";
	public static String FGSI1  = "FGSI1";
	public static String FGSI2  = "FGSI2";
	public static String FGSI3  = "FGSI3";
	public static String FGSI4  = "FGSI4";
	public static String FGSI5  = "FGSI5";
	public static String FGSI6  = "FGSI6";
	public static String FGSV1  = "FGSV1";
	public static String FGSV2  = "FGSV2";
	public static String FGSV3  = "FGSV3";
	public static String FGSV4  = "FGSV4";
	public static String FGSV5  = "FGSV5";
	public static String FGSV6  = "FGSV6";
	public static String FS  = "FS";
	public static String FM  = "FM";
	public static String FC  = "FC";
	public static String G  = "G"; 
	public static String AVI  = "AVI";
	public static String QI  = "QI"; 
	public static String FPC  = "FPC";
	public static String FGC  = "FGC";
	public static String FPF  = "FPF";
	public static String FGF  = "FGF";
	public static String NCA = "NCA";
	public static String N = "NCA";

}

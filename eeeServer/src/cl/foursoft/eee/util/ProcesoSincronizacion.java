package cl.foursoft.eee.util;

import java.sql.Connection;
import java.util.List;

import cl.foursoft.eee.dao.facade.FacadeArtefactos;
import cl.foursoft.eee.dao.facade.FacadeConstantes;
import cl.foursoft.eee.dao.facade.FacadeMateriales;
import cl.foursoft.eee.dao.facade.FacadeParametrosEdificio;
import cl.foursoft.eee.dao.implementation.DBMannager;
import cl.foursoft.eee.dao.transferObject.ArtefactoTO;
import cl.foursoft.eee.dao.transferObject.FactorRenovacionTO;
import cl.foursoft.eee.dao.transferObject.MaterialTO;
import cl.foursoft.eee.dao.transferObject.ParametrosEdificioTO;

public class ProcesoSincronizacion {
	
	/**
	 * Método que obtiene datos de BD Admin para ser enviados a clientes
	 * @return
	 */
	public Sincronizacion obtenerDatosSincronizacion(){
		Connection c =  DBMannager.getConnection();
		
		FacadeParametrosEdificio facadeParametros = new FacadeParametrosEdificio();
		FacadeConstantes facadeConstantes = new FacadeConstantes();
		FacadeMateriales facadeMateriales = new FacadeMateriales();
		FacadeArtefactos facadeArtefactos = new FacadeArtefactos();
		
		List<ParametrosEdificioTO> listaParametros = null;
		List<FactorRenovacionTO> listaFactores = null;
		List<MaterialTO> listaMateriales = null;
		List<ArtefactoTO> listaArtefactos = null;
		
		boolean exito = false;
		Sincronizacion objetoSincronizacion = null;

		try{	
			listaParametros = facadeParametros.obtenerParametros(c);
			listaFactores   = facadeConstantes.obtenerFactorDeRenovacion(c);
			listaMateriales = facadeMateriales.obtenerTodosLosMaterialesSync(c);
			listaArtefactos = facadeArtefactos.obtenerTodosLosArtefactosSync(c);
			
			if(listaParametros != null && listaFactores != null && listaMateriales != null && listaArtefactos!=null){
				objetoSincronizacion = new Sincronizacion(listaMateriales, listaParametros, listaFactores, listaArtefactos);
				exito = true;
			}
			
			DBMannager.close(c, exito);
		}
		catch (Exception e) {
			UtilLog.registrar(e);
			DBMannager.closeOnError(c);
		} 
		
		return objetoSincronizacion;
	}
	
	/**
	 * Metodo que recibe los datos de sincronizacion desde Admin para ser cargados en BD cliente
	 * @param sincronizacion
	 */
	public boolean cargarDatosSincronizacion(Sincronizacion sincronizacion){
		
		boolean exito = false;
		if(sincronizacion != null){
		
			Connection c =  DBMannager.getConnection();
			//MOstrar error cuando Sinconizaci�n es NULL
			FacadeParametrosEdificio facadeParametros = new FacadeParametrosEdificio();
			FacadeConstantes facadeConstantes = new FacadeConstantes();
			FacadeMateriales facadeMateriales = new FacadeMateriales();
			FacadeArtefactos facadeArtefactos = new FacadeArtefactos();
			
			
			try{	
				exito = facadeParametros.editarParametrosSync(sincronizacion.getParametros(), c);
				if(exito)
					exito = facadeConstantes.guardarFactoresSync(sincronizacion.getFactores(), c);
				if(exito) 
					exito = facadeMateriales.editarMaterialesSync(sincronizacion.getMateriales(), c);
				if(exito)
					exito = facadeArtefactos.editarArtefactosSync(sincronizacion.getArtefactos(), c);
				
				DBMannager.close(c, exito);
			}
			catch (Exception e) {
				UtilLog.registrar(e);
				DBMannager.closeOnError(c);
			}
		}
		else{
			System.out.println("- No se pudo realizar la sincronizaci�n...");
		}
			
		
		return exito;
	}
}

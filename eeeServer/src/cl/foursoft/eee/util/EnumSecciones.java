package cl.foursoft.eee.util;

public class EnumSecciones {

	

	public static  String SECCION_AREA_SUPERIOR  = "Area Superior";
		public static  String SECCION_AREA_SUPERIOR_TECHO  = "Techo";
		public static  String SECCION_AREA_SUPERIOR_CUPULA  = "Cupula";
	
	public static  String SECCION_VENTANAS  = "Ventanas";
		public static  String VENTANAS_NORTE  			 = "Ventanas Norte";
		public static  String VENTANAS_SUR  		     = "Ventanas Sur";
		public static  String VENTANAS_ORIENTE  		 = "Ventanas Oriente";
		public static  String VENTANAS_PONIENTES  		 = "Ventanas Poniente";
		public static  String VENTANAS_NONP  		 = "Ventanas NO-NP";
		public static  String VENTANAS_SOSP  = "Ventanas SP-SP";
	
	public static  String SECCION_MUROS  = "Muros";
		public static  String MUROS_NORTE  = "Muro Norte";
		public static  String MUROS_SUR= "Muro Sur";
		public static  String MUROS_ORIENTE= "Muro Oriente";
		public static  String MUROS_PONIENTE  = "Muro Poniente";
		
	public static String SECCION_PUERTAS = "Puertas";
		public static  String PUERTAS_NORTE_SUR  = "Puertas Norte-Sur";
		public static  String PUERTAS_ORIENTE_PONIENTE  = "Puertas Oriente-Poniente";
	
		
	
	public static  String SECCION_AREA_BASE  = "Area del Piso";
		public static  String SECCION_PISO  = "Piso";
		public static  String SECCION_ESCALERA  = "Escaleras";
	
	public static String  SECCION_OTRAS_INSTALACIONES  = "Otras Instalaciones";
	
}
